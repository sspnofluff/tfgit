#ifndef _MAIN_H
#define _MAIN_H
#define BACK_ID     0   
#define FRONT_ID    1  
#define BOTTOM_ID   2  
#define TOP_ID      3  
#define LEFT_ID     4   
#define RIGHT_ID    5  
#define SCREEN_DEPTH 32
#define MAX_SHADOW_CASTER_POINTS 32
#define NUM_BONES_PER_VEREX 4
#define SAFE_DELETE(p) if (p) { delete p; p = NULL; }
#define INVALID_OGL_VALUE 0xFFFFFFFF
#define ARRAY_SIZE_IN_ELEMENTS(a) (sizeof(a)/sizeof(a[0]))
#define SEGMENT_COUNT 200;
//typedef BOOL (WINAPI * PFNWGLSWAPINTERVALEXTPROC) (int interval);

#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include <gl/gl.h>
#include <gl/glu.h>
#include "math_3d.h"
#include "3dsloader.h"
#include <vector>
#include "map.h"
#include <map>
#include "rapidxml.hpp"
#include "Glext.h"
#include "wglext.h"
#include "textfile.h"
#include "font.h"
#include <wchar.h>
#include <math.h>
#include "glut.h"
#include <atlstr.h>
#include <sstream>
#include <iostream>
#include <time.h>

#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>
#include <assimp/types.h>


 
static const unsigned int MAX_BONES = 100;






struct move_mark {
	int LineCh;
	int Liner_x[20];
	int Liner_y[20];
	int Liner_z[20];
	int Liner_ox[20];
	int Liner_oy[20];
	int LineStr;
	int LineStr_E;
	int LineEnd;
	bool on_mark;
	int markch;
	obj_type marknum[77];
};



class move_anim2 {

public:
	move_anim2();
	float t;
	bool anim_move_flag;
	bool anim_move_rotate;
	bool anim_move_trailer;
	bool anim_move_state;
	bool rotate_cross;
	bool rotate_car;
	bool double_rotate_car;
	bool end_animation;
	bool end_rotate;
	int ALineCh;
	int _side;
	float rotate;
	int oldRot;
	int oldRotTwo;

	

	int timer;

	Vector3i start;
	Vector3i end;

	Vector2f b_start;
	Vector2f b_exStart;
	Vector2f b_exEnd;
	Vector2f b_exStart2;
	Vector2f b_exEnd2;
	Vector2f b_end;

	Vector2i vehicleCellClose[3];

	void rotate_around();
	void anim_func(bool vehicle);
	void anim_move_main();
	bool Forward();
	int Side(int rot);
	void VehicleCells(int rot, int x, int y);
	bool VehicleCells(int x, int y);


	
};

class an_timer {

public:
	void *current;
	//unsigned int counts;
	unsigned int animation_count;
	unsigned int animation_index;
	unsigned int part;
	bool crossFlag;
	float crossTime;
	bool anim_move_walk;
	bool freeze;
	unsigned int crossIndex;

	struct one
	{
		float time;
		float diff;
		bool on;
		aiAnimation* Animation;
		aiNode* Data;

		void Start();
		
		void AddAnimation(const std::string& Filename);
		
		void Add(const aiScene* m_pScene);
		aiAnimation* LoadAnimation(const aiScene* m_pScene);
		aiNode* LoadNodes(const aiScene* m_pScene);
		aiNode* LoadNode(aiNode* pNode, aiNode* aNode);
	};

	struct crossData
	{
		float timeStart;
		float timeEnd;
		int animStart;
		int animEnd;
		float timeDuration;
		int animExit;
		bool add;
		float timeCheck;

		void Add(float _timeStart, float _timeEnd, int _animStart, int _animEnd, int _animExit, bool _add, float _timeDuration);
	};

	one *One;	
	crossData *crossdata;
	void Create(unsigned int animation_count, const aiScene* pScene);
	void Look(int type);
};

class Mesh
{
public:

	Mesh();
	~Mesh();

	
	bool LoadMesh(const std::string& Filename, int skinning_on, unsigned int animationCount, unsigned int _program);
	bool LoadMesh(const std::string& Filename, int skinning_on, unsigned int animationCount, std::vector<Vector3f>& _Positions2, std::vector<Vector3f>& _Normals2, std::vector<unsigned int>& Indices2, unsigned int _program);
	bool LoadMorphData(const std::string& Filename, std::vector<Vector3f>& Positions2, std::vector<Vector3f>& Normals2, std::vector<unsigned int>& Indices2, int side);
	void Render();
	void RenderSkinning();
	unsigned int program;

	unsigned int NumBones() const
	{
		return m_NumBones;
	}
	
	const aiScene* m_pScene;
	void BoneTransform(float TimeInSeconds, std::vector<Matrix4f>& Transforms);
	aiQuaternion ManualTransform(aiQuaternion quat);
	aiQuaternion CopyTransform(aiQuaternion *quat);
	an_timer timer;
	void Clear();

private:
	

struct BoneInfo
    {
        Matrix4f BoneOffset;
        Matrix4f FinalTransformation;        

        BoneInfo()
        {
            BoneOffset.SetZero();
            FinalTransformation.SetZero();            
        }
    };
    
    struct VertexBoneData
    {        
        unsigned int IDs[4];
        float Weights[4];

        VertexBoneData()
        {
            Reset();
        };
        
        void Reset()
        {
            ZERO_MEM(IDs);
            ZERO_MEM(Weights);        
        }
        
        void AddBoneData(unsigned int BoneID, float Weight);
    };

	void CalcInterpolatedScaling(aiVector3D& Out, float AnimationTime, const aiNodeAnim* pNodeAnim);
    void CalcInterpolatedRotation(aiQuaternion& Out, float AnimationTime, const aiNodeAnim* pNodeAnim);
    void CalcInterpolatedPosition(aiVector3D& Out, float AnimationTime, const aiNodeAnim* pNodeAnim);    
    unsigned int FindScaling(float AnimationTime, const aiNodeAnim* pNodeAnim);
    unsigned int FindRotation(float AnimationTime, const aiNodeAnim* pNodeAnim);
    unsigned int FindPosition(float AnimationTime, const aiNodeAnim* pNodeAnim);
	const aiNodeAnim* FindNodeAnim(const aiAnimation* pAnimation, const std::string NodeName);
	void ReadNodeHeirarchy(float AnimationTime, const aiNode* pNode, const Matrix4f& ParentTransform);
	void ReadNodeHeirarchyCross(float AnimationTime, const aiNode* pNode, const Matrix4f& ParentTransform, float time);
	bool InitFromScene(const aiScene* pScene, const std::string& Filename);
	bool InitFromSceneSkinning(const aiScene* pScene, const std::string& Filename);
	//bool InitFromSceneSkinningMorphing(const aiScene* pScene, const aiScene* pScene2, const std::string& Filename, const std::string& Filename2);
	bool InitFromSceneSkinningMorphing(const aiScene* pScene,
										const std::string& Filename,
										std::vector<Vector3f>& Positions,
										std::vector<Vector3f>& Positions2,
										std::vector<Vector3f>& Normals,
										std::vector<Vector3f>& Normals2,
										std::vector<Vector2f>& TexCoords,
										std::vector<VertexBoneData>& Bones,
										std::vector<unsigned int>& Indices,
										std::vector<unsigned int>& Indices2);
	bool InitFromSceneMorphing(const aiScene* pScene,
										const std::string& Filename,
										std::vector<Vector3f>& Positions2,
										std::vector<Vector3f>& Normals2,
										std::vector<unsigned int>& Indices2,
										int side);
	bool InitFromSceneMorphing(const aiScene* pScene,
										const std::string& Filename,
										std::vector<Vector3f>& Positions,
										std::vector<Vector3f>& Positions2,
										std::vector<Vector3f>& Normals,
										std::vector<Vector3f>& Normals2,
										std::vector<Vector2f>& TexCoords,
										std::vector<unsigned int>& Indices,
										std::vector<unsigned int>& Indices2);
	bool MergeMorphing(std::vector<Vector3f>& Positions,
						std::vector<Vector3f>& Positions2,
						std::vector<Vector3f>& Normals,
						std::vector<Vector3f>& Normals2,
						std::vector<Vector2f>& TexCoords,
						std::vector<VertexBoneData>& Bones,
						std::vector<unsigned int>& Indices);
	bool MergeMorphing(std::vector<Vector3f>& Positions,
						std::vector<Vector3f>& Positions2,
						std::vector<Vector3f>& Normals,
						std::vector<Vector3f>& Normals2,
						std::vector<Vector2f>& TexCoords,
						std::vector<unsigned int>& Indices);
	void InitMesh(unsigned int MeshIndex,
                  const aiMesh* paiMesh,
				  std::vector<Vector3f>& Positions,
                  std::vector<Vector3f>& Normals,
                  std::vector<Vector2f>& TexCoords,
                  std::vector<VertexBoneData>& Bones,
                  std::vector<unsigned int>& Indices);
	void InitMesh(unsigned int MeshIndex,
                  const aiMesh* paiMesh,
				  std::vector<Vector3f>& Positions,
                  std::vector<Vector3f>& Normals,
                  std::vector<Vector2f>& TexCoords,
                  std::vector<unsigned int>& Indices);
	void InitMesh(unsigned int MeshIndex,
					const aiMesh* paiMesh,
					std::vector<Vector3f>& Positions,
					std::vector<Vector3f>& Normals,
					std::vector<unsigned int>& Indices,
					int side
					);
	void LoadBones(unsigned int Index, const aiMesh* paiMesh, std::vector<VertexBoneData>& Bones);
	
	

#define INVALID_MATERIAL 0xFFFFFFFF

enum VB_TYPES {
    INDEX_BUFFER,
    POS_VB,
    NORMAL_VB,
    TEXCOORD_VB,
    BONE_VB,
	NUM_VBs
  };

enum sVB_TYPES {
    sINDEX_BUFFER,
    sPOS_VB,
    sNORMAL_VB,
    sTEXCOORD_VB,
    sNUM_VBs
  };

enum mVB_TYPES {
    mINDEX_BUFFER,
    mPOS_VB,
	mPOS_VB2,
	mTEXCOORD_VB,
    mNORMAL_VB,
	mNORMAL_VB2,
    mBONE_VB,
	mNUM_VBs
  };

	
	GLuint m_VAO;
    GLuint m_Buffers[NUM_VBs];
	GLuint m_BuffersM[mNUM_VBs];
	GLuint m_BuffersS[sNUM_VBs];

	struct MeshEntry {
        MeshEntry()
        {
            NumIndices    = 0;
            BaseVertex    = 0;
            BaseIndex     = 0;
            MaterialIndex = INVALID_MATERIAL;
        }
        
        unsigned int NumIndices;
        unsigned int BaseVertex;
        unsigned int BaseIndex;
        unsigned int MaterialIndex;
    };

	std::vector<MeshEntry> m_Entries;
	std::vector<Texture*> m_Textures;

	std::map<std::string, unsigned int> m_BoneMapping;
	unsigned int m_NumBones;
	std::vector<BoneInfo> m_BoneInfo;
	Matrix4f m_GlobalInverseTransform;

	
	Assimp::Importer m_Importer;

};



class unit {
public:
	unit();
	
	unit *Trailer;

	Mesh *Transform;
	Mesh *Main;
	Mesh *Alternative;
	Mesh *Gun;
	Mesh *Head;
	Mesh *Glass;
	Mesh *Glass02;
	Mesh *Glass03;
	Mesh *Face;
	Mesh *Current;
	
	Matrix4f GunRotation;
	Matrix4f HeadRotation;
	Matrix4f GlassRotation;
	Matrix4f LeftFistRotation;
	Matrix4f RightFistRotation;
	Matrix4f HeadFrameRotation;
	Matrix4f GlassFrameRotation;
		
	Texture diffuse_map;
	Texture normal_map;
	Texture emission_map;

	Texture head_diffuse_map;
	Texture head_normal_map;
	Texture head_emission_map;

	Texture gun_diffuse_map;
	Texture gun_normal_map;
	Texture gun_emission_map;

	Texture trailer_diffuse_map;
	Texture trailer_emission_map;
	

	int type;
	int form;
	bool morphing;
	bool transformer;
	bool glass;

	int X;
	int Y;
	int rotdir;

	float forward_angles;
	float side_angles;
	float z_up;
	
	Vector2f traildot;
	float trailangle;
	int trailpos_x;
	int trailpos_y;

	Vector2f widthBox;
	Vector2f heightBox;

	void Transformation();
	void SeparateTrailer();
	void MoveToSeparate();
	void MoveFromSeparate();
	void CarMoveBack();
	void CarMoveBackTwo();
	void CarMoveForward();
};





struct Mainmass 
{
	bool forest,plain,grass,hill;
	int rock;
	bool marked;
	int passab;
	bool move_marked;
	int dirrot;
	class unit *unitptr;
	int trailermark;
};



class BezierPath
{
public:
unsigned int ch;
float t_step;
float t_step2;
	//std::vector<Vector2f> controlPoints;
	struct _splinePoint {

		Vector2f startPoint;
		Vector2f startEx;
		Vector2f endEx;
		int type;
		
		float length;
		float rotation;
	
		_splinePoint(int _type, Vector2f _startPoint, Vector2f _startEx, Vector2f _endEx, float _rotation)
		{
		startPoint=_startPoint;
		type=_type;
		startEx=_startEx;
		endEx=_endEx;
		rotation=_rotation;
		};

		
		_splinePoint(int _type, Vector2f _startPoint, float _rotation)
		{

		startPoint=_startPoint;
		type=_type;
		rotation=_rotation;
		};
};
	
	void GetDrawingPoints(int setSpeed, bool amplitudeOn);
	void AddPoint(int _type, Vector2f _startPoint, Vector2f _startEx, Vector2f _endEx, float _rotation);
	void AddPoint(int _type, Vector2f _startPoint, float _rotation);
	void ClearPath();
	void Loop();

	std::vector<_splinePoint> splinePoint;
	Vector2f endPoint;
	float endRotation;
	std::vector<Vector2f> drawingPoints;
	std::vector<float> rotation;

	void start(int x, int y, int rot);
	void end(int x, int y, int rot);
	void endAlt(int x, int y, Vector2f startPos, int rot);

	void l1(int x, int y);
	void l2(int x, int y);
	void l3(int x, int y);
	void l4(int x, int y);
	void l5(int x, int y);
	void l6(int x, int y);

	void r1(int x, int y);
	void r2(int x, int y);
	void r3(int x, int y);
	void r4(int x, int y);
	void r5(int x, int y);
	void r6(int x, int y);
	void r7(int x, int y);
	void r8(int x, int y);
	void r9(int x, int y);
	void r10(int x, int y);
	void r11(int x, int y);
	void r12(int x, int y);
							
private:
	Vector2f CalculateBezierPoint(float t, Vector2f p0, Vector2f p1, Vector2f p2, Vector2f p3);
	Vector2f CalculateBezierPoint(float t, Vector2f p0, Vector2f p1);
	float BezierPath::CalculateRotation(float t, float p0, float p1);
};

class WheelController
{
public:
bool on;
bool half;
int forward;

Vector3f WheelMove;
float WheelSpeed;
aiQuaternion WheelEngine;

long time;


void Start(int forward);
void Loop();
void Stop();
};
 

//window
extern HWND  g_hWnd;
extern RECT  g_rRect;
extern HDC   g_hDC;
extern HGLRC g_hRC;
extern HINSTANCE g_hInstance;
//map
extern int GridWei;
extern int GridHei;
extern int GridSetWei;
extern int GridSetHei;
extern std::vector <vertex_type> MapVertexArray;
extern std::vector <vertex_type> MapNormalArray;
extern std::vector <polygon_type> MapFaceArray;
extern std::vector <vertex_type> ForestVertexArray;
extern std::vector <polygon_type> ForestFaceArray;
extern std::vector <mapcoord_type> ForestTexcoordArray;
extern Texture blend, blend2, bignorm, grain, plain, rock, rock_norm, grain_norm, hextex;

static int wheelDelta = 0;

//main
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hprev, PSTR cmdline, int ishow);
LRESULT CALLBACK WinProc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam);
WPARAM MainLoop();
HWND CreateMyWindow(LPSTR strWindowName, int width, int height, DWORD dwStyle, bool bFullScreen, HINSTANCE hInstance);
bool bSetupPixelFormat(HDC hdc);
void SizeOpenGLScreen(int width, int height, float perspective);
void InitializeOpenGL(int width, int height);
void Init(HWND hWnd);
void DeInit();
void GetFrameTime();
void set_vsync(bool enabled);

//render
void RenderScene();
void RenderSceneMenu();
void RenderToShadowMap();
void RenderShadowedScene();


//draw
void DrawMap();
void draw_obj(obj_type *obj, float tX, float tY, float tZ, float dirrot, int iX, int iY, Texture *tex1, Texture *tex2, Texture *tex3, bool shadow_on, float angle); //����� ������� ��������� ��������
void draw_skinning_obj(unit* unit, float tX, float tY, float tZ, float dirrot, float shadow_on, float forward_angles, float side_angles);
void draw_skinning_obj_morphing(unit* unit, float tX, float tY, float tZ, float dirrot, float shadow_on, float forward_angles, float side_angles);
void DrawComplexUnit(unit* unit, Vector2f p, float rotate, bool shadow_on);
void draw_state_obj(unit *unit, float tX, float tY, float tZ, float dirrot, float shadow_on, float forward_angle, float side_angle, int part);
void draw_frame_obj(unit* unit, float tX, float tY, float tZ, float dirrot, float shadow_on, Vector3f eyepos);
void draw_frame_part(unit *unit, float tX, float tY, float tZ, float dirrot, float shadow_on, int part, Vector3f eyepos);
void draw_forest();
void draw_GUI();
void Draw_move_anim(bool shadow_on);
void DrawMove();
void drawAvatar();
void draw_mark(int tX, int tY, int tZ, int iX, int iY);
void drawrect(int width, wchar_t *buf, int size);
void drawload(int width, wchar_t *buf, int size);
void DrawMenu();
void DrawButton(int x, int y, int check);
void DrawUnits(bool shadow_on);
void DrawUnit(int number, bool shadow_on);
void DrawScreen();
void DrawModel(float z);
void DrawPlane();
void DrawPlaneOld();
void DrawHexGrid();
void DrawHex();
void Draw_Window_Part(bool size, int x, int y);
void Draw_Window(int x, int y, int x_size, int y_size);
void Draw_Window_Canvas(int x, int y, int x_size, int y_size);
void DrawLine(float tX, float tY);
void DrawLine(float tX, float tY, float tZ, float dirrot);

//create
void MainCreate();
void LoadTextures(char *FileName, Texture *texture, int flag);
void LoadTextures(char *FileName, int target, Texture *texture, int flag);
bool LoadTGA(Texture *, const char *);
void MainCreateMap();
void ShadowInitialization();
void CreateSkyBox(float x, float y, float z, float width, float height, float length);
void CleanAll();
void DeleteMenu();
void ColladaLoader(const char *, obj_type_ptr p_object);

//logic
Vector2f MainToOxy(int x, int y);
Vector2f MainToOxy(int x, int y, float x_1, float y_1);
Vector2f ScreenToOxy(int mouse_x, int mouse_y);
Vector2f OxyToMain(float x, float y);
void MoveCamera();
void CalcMark(obj_type *obj, int wey, int hey);
void CalcAllMarks();
void trailer_fixings(unit *unit, float px, float py, float r, float OneObjDist, float back, float TwoObjDist);
void BoxGetZ(unit* unit, Vector2f pos, float rotate);
Vector2f GetLineZ(float x, float y, Vector2f pos, float rotate);
float GetAngle (float x, float y, float x2, float y2, float &b, int must);

//gui
void Mouseclick();
void CursorPosition();
Vector2f CursorPositionEx();
void MenuMouseclick();
void ButtonMenuMouseCheck();

//astar
void uphod(bool res);
int maxA();
void func(int x1, int y1 ,int f);
void fmove(int x, int y);
int SetLineSE(int x,int y);
int SetLineSE_E(int x,int y);
void SetLineCh();
void aStar_func1(int z);
bool aStar(int x, int y);
void aStar_func(int x,int y,int par);
int aStar_min();

//anim




//skeletal
void SetBoneTransform(unsigned int Index, const Matrix4f& Transform);

//other
float StringToFloat(LPCTSTR str);
int StringToInt(LPCTSTR str);
void InverseMatrix(float dst[16], float src[16]);

//shaders
void setShaders();
void setShader1();
void setShader2();
void setShader3();
void setShader4();
void setShader5();
void setShader6();
void setShader7();
void setShader8();
void setShader9();
void setShader10();
void setShader11();
void setShader12();

void shaderLog(unsigned int shader);
GLint GetUniformLocation(const char* pUniformName);

//bezier
void DrawSpline();
void DrawBox(float tX, float tY, float tZ, float dirrot, Vector2f width, Vector2f Height);

void setupCubeMap(GLuint& texture);
void setupCubeMap(GLuint& texture, Texture *xpos, Texture *xneg, Texture *ypos, Texture *yneg, Texture *zpos, Texture *zneg);
void deleteCubeMap(GLuint& texture);

void CreateRenderTexture(Texture texture, int size, int channels, int type);


void CreatePyramid();
void CreatePlane();
void RenderFrameFace();
void FaceFrameInitialization();
#endif