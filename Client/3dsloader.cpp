/*
 * ---------------- www.spacesimulator.net --------------
 *   ---- Space simulators and 3d engine tutorials ----
 *
 * Author: Damiano Vitulli <info@spacesimulator.net>
 *
 * ALL RIGHTS RESERVED
 *
 *
 * Tutorial 4: 3d engine - 3ds models loader
 * 
 * Include File: 3dsloader.cpp
 *  
 */

#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include <io.h>
#include "3dsloader.h"

vertex_type tvertex[MAX_VERTICES];
polygon_type tpolygon[MAX_POLYGONS];
mapcoord_type tmapcoord[MAX_VERTICES];

char Load3DS (obj_type_ptr p_object, char *p_filename)
{
	int i; //Index variable
	
	FILE *l_file; //File pointer
	
	unsigned short l_chunk_id; //Chunk identifier
	unsigned int l_chunk_lenght; //Chunk lenght

	unsigned char l_char; //Char variable
	unsigned short l_qty; //Number of elements in each chunk

	unsigned short l_face_flags; //Flag that stores some face information

	if ((l_file=fopen (p_filename, "rb"))== NULL) return 0; //Open the file

	while (ftell (l_file) < filelength (fileno (l_file))) //Loop to scan the whole file 
	{
		//getche(); //Insert this command for debug (to wait for keypress for each chuck reading)

		fread (&l_chunk_id, 2, 1, l_file); //Read the chunk header
		fread (&l_chunk_lenght, 4, 1, l_file); //Read the lenght of the chunk
		
		switch (l_chunk_id)
        {
			//----------------- MAIN3DS -----------------
			// Description: Main chunk, contains all the other chunks
			// Chunk ID: 4d4d 
			// Chunk Lenght: 0 + sub chunks
			//-------------------------------------------
			case 0x4d4d: 
			break;

			//----------------- EDIT3DS -----------------
			// Description: 3D Editor chunk, objects layout info 
			// Chunk ID: 3d3d (hex)
			// Chunk Lenght: 0 + sub chunks
			//-------------------------------------------
			case 0x3d3d:
			break;
			
			//--------------- EDIT_OBJECT ---------------
			// Description: Object block, info for each object
			// Chunk ID: 4000 (hex)
			// Chunk Lenght: len(object name) + sub chunks
			//-------------------------------------------
			case 0x4000: 
				i=0;
				do
				{
					fread (&l_char, 1, 1, l_file);
                    p_object->pname[i]=l_char;
					i++;
                }while(l_char != '\0' && i<20);
			break;

			//--------------- OBJ_TRIMESH ---------------
			// Description: Triangular mesh, contains chunks for 3d mesh info
			// Chunk ID: 4100 (hex)
			// Chunk Lenght: 0 + sub chunks
			//-------------------------------------------
			case 0x4100:
			break;
			
			//--------------- TRI_VERTEXL ---------------
			// Description: Vertices list
			// Chunk ID: 4110 (hex)
			// Chunk Lenght: 1 x unsigned short (number of vertices) 
			//             + 3 x float (vertex coordinates) x (number of vertices)
			//             + sub chunks
			//-------------------------------------------
			case 0x4110: 
				fread (&l_qty, sizeof (unsigned short), 1, l_file);
                p_object->vertices_qty = l_qty;
                printf("Number of vertices: %d\n",l_qty);
                for (i=0; i<l_qty; i++)
                {
					fread (&tvertex[i].x, sizeof(float), 1, l_file);
					fread (&tvertex[i].y, sizeof(float), 1, l_file);
					fread (&tvertex[i].z, sizeof(float), 1, l_file);
				}
				break;

			//--------------- TRI_FACEL1 ----------------
			// Description: Polygons (faces) list
			// Chunk ID: 4120 (hex)
			// Chunk Lenght: 1 x unsigned short (number of polygons) 
			//             + 3 x unsigned short (polygon points) x (number of polygons)
			//             + sub chunks
			//-------------------------------------------
			case 0x4120:
				fread (&l_qty, sizeof (unsigned short), 1, l_file);
                p_object->polygons_qty = l_qty;
                printf("Number of polygons: %d\n",l_qty); 
                for (i=0; i<l_qty; i++)
                {
					fread (&tpolygon[i].a, sizeof (unsigned short), 1, l_file);
					fread (&tpolygon[i].b, sizeof (unsigned short), 1, l_file);
					fread (&tpolygon[i].c, sizeof (unsigned short), 1, l_file);
					fread (&l_face_flags, sizeof (unsigned short), 1, l_file);
				}
                break;

			//------------- TRI_MAPPINGCOORS ------------
			// Description: Vertices list
			// Chunk ID: 4140 (hex)
			// Chunk Lenght: 1 x unsigned short (number of mapping points) 
			//             + 2 x float (mapping coordinates) x (number of mapping points)
			//             + sub chunks
			//-------------------------------------------
			case 0x4140:
				fread (&l_qty, sizeof (unsigned short), 1, l_file);
				for (i=0; i<l_qty; i++)
				{
					fread (&tmapcoord[i].u, sizeof (float), 1, l_file);
					fread (&tmapcoord[i].v, sizeof (float), 1, l_file);
					
				}
				break;

			//----------- Skip unknow chunks ------------
			//We need to skip all the chunks that currently we don't use
			//We use the chunk lenght information to set the file pointer
			//to the same level next chunk
			//-------------------------------------------
			default:
				 fseek(l_file, l_chunk_lenght-6, SEEK_CUR);
        } 
	}
	fclose (l_file); // Closes the file stream
	
	p_object->vertex = new vertex_type[p_object->vertices_qty];
	p_object->normals = new vertex_type[p_object->vertices_qty];
	p_object->mapcoord = new mapcoord_type[p_object->vertices_qty];
	p_object->polygon = new polygon_type[p_object->polygons_qty];

	for (unsigned int i = 0; i < p_object->vertices_qty; i++) {
	p_object->vertex[i]=tvertex[i];
	p_object->mapcoord[i]=tmapcoord[i];
	}
	for (unsigned int i = 0; i < p_object->polygons_qty; i++) {
	p_object->polygon[i]=tpolygon[i];
	}


	return (1); // Returns ok
}
vertex_type Cross(vertex_type vVector1, vertex_type vVector2)
{
	vertex_type vCross;

	vCross.x = ((vVector1.y * vVector2.z) - (vVector1.z * vVector2.y));
	vCross.y = ((vVector1.z * vVector2.x) - (vVector1.x * vVector2.z));
	vCross.z = ((vVector1.x * vVector2.y) - (vVector1.y * vVector2.x));

	return vCross;
}

vertex_type Normalize(vertex_type vNormal)
{
	double Magnitude;

	Magnitude = Mag(vNormal);

	vNormal.x /= (float)Magnitude;
    vNormal.y /= (float)Magnitude;
	vNormal.z /= (float)Magnitude;

	return vNormal;
}

float Dot(vertex_type Vector1, vertex_type Vector2)
{
	float dot=(Vector1.x * Vector2.x) + (Vector1.y * Vector2.y) + (Vector1.z * Vector2.z);
	return dot; 
}

vertex_type Vector(vertex_type vPoint1, vertex_type vPoint2)
{
	vertex_type vVector;           // ������ �������������� ������

	vVector.x = vPoint1.x - vPoint2.x;
	vVector.y = vPoint1.y - vPoint2.y;
	vVector.z = vPoint1.z - vPoint2.z;

	return vVector;             // ������ �������������� ������
}

vertex_type AddVector(vertex_type vVector1, vertex_type vVector2)
{
	vertex_type vResult;               // ������ �������������� ������

    vResult.x = vVector2.x + vVector1.x;
    vResult.y = vVector2.y + vVector1.y;
    vResult.z = vVector2.z + vVector1.z;

    return vResult;                 // ������ ���������
}

// ����� ������ �� ���������� ����� � ���������� ���������
vertex_type DivideVectorByScaler(vertex_type vVector1, float Scaler)
{
	vertex_type vResult;

	vResult.x = vVector1.x / Scaler;
    vResult.y = vVector1.y / Scaler;
    vResult.z = vVector1.z / Scaler;

    return vResult;
}

void ComputeNormals(obj_type *p_object)
{
	vertex_type vVector1, vVector2, vNormal, vPoly[3];

	vertex_type *pNormals      = new vertex_type [p_object->polygons_qty];
	vertex_type *pTempNormals  = new vertex_type [p_object->polygons_qty];
		
		for (unsigned int i=0;i<p_object->polygons_qty;i++)
		{

			// ��������� 3 ����� ����� ��������, ����� �������� �������� ����
			vPoly[0] = p_object->vertex[p_object->polygon[i].a];
			vPoly[1] = p_object->vertex[p_object->polygon[i].b];
			vPoly[2] = p_object->vertex[p_object->polygon[i].c];
			// ������ �������� ������� ���������

			vVector1 = Vector(vPoly[0], vPoly[2]);  // ������ �������� (�� 2� ��� ������)
			vVector2 = Vector(vPoly[2], vPoly[1]);  // ������ ������ ��������

			vNormal  = Cross(vVector1, vVector2);   // �������� cross product ��������
			pTempNormals[i] = vNormal;      // �������� ��������� ��-����������������� �������

			
		}



		vertex_type vSum = {0.0, 0.0, 0.0};
		vertex_type vZero = vSum;
        int shared=0;

		for (unsigned int i = 0; i < p_object->vertices_qty; i++)   // �������� ����� ��� �������
		{
			for (unsigned int j = 0; j < p_object->polygons_qty; j++)   // �������� ����� ��� ������������
			{               // ���������, ������������ �� ������� ������ ���������
				if (p_object->polygon[j].a == i ||
					p_object->polygon[j].b == i ||
					p_object->polygon[j].c == i)
				{
					vSum = AddVector(vSum, pTempNormals[j]);    // ���������� ��-
								// ����������������� ������� ������� ��������
					shared++;       // ����������� ����� ��������� � ������ ���������

				}
			}
            p_object->normals[i] = DivideVectorByScaler(vSum, float(-shared));
			vSum = vZero;           // ���������� �����
			shared = 0;         // � ����� ��������
		}
						
		for (unsigned int i = 0; i < p_object->vertices_qty; i++)   // �������� ����� ��� �������
		p_object->normals[i] = Normalize(p_object->normals[i]);
		

		// ����������� ������ ��������� ����������
		delete [] pTempNormals;
		delete [] pNormals;

}

void PrepareObject(obj_type *obj, float size, int normals_on, int vert_off)
{
if (normals_on==1)
ComputeNormals(obj);
//CalculateTangentsAndBinormals(obj);


for (unsigned int i=0; i<obj->vertices_qty; i++) {
obj->vertex[i].x=obj->vertex[i].x*size;
obj->vertex[i].y=obj->vertex[i].y*size;
if (vert_off==0)
obj->vertex[i].z=-obj->vertex[i].z*size;
if (vert_off==1)
obj->vertex[i].z=-obj->vertex[i].z;
}

if (normals_on==1)
for (unsigned int i=0;i<obj->vertices_qty;i++)
obj->normals[i].z=-obj->normals[i].z;


}

/*
void CalculateTangentsAndBinormals(obj_type *obj)
{
	
register int i, j;
	std::vector<vertex_type> tangents, binormals;	// ����������� ������� � ������������
 
	// ���������� �� ���� �������������, � ��� ������� ������� �����
	for (i = 0; i < obj->polygons_qty; i++ )
	{
		int ind0 = obj->polygon[i].a;
		int ind1 = obj->polygon[i].b;
		int ind2 = obj->polygon[i].c;
 
		vertex_type v1 = obj->vertex[ ind0 ];
		vertex_type v2 = obj->vertex[ ind1 ];
		vertex_type v3 = obj->vertex[ ind2 ];
		float s1      = obj->mapcoord[ ind0 ].u;
		float t1      = obj->mapcoord[ ind0 ].v;
		float s2      = obj->mapcoord[ ind1 ].u;
		float t2      = obj->mapcoord[ ind1 ].v;
		float s3      = obj->mapcoord[ ind2 ].u;
		float t3      = obj->mapcoord[ ind2 ].v;
 
		vertex_type  t, b;
		CalcTriangleBasis( v1, v2, v3, s1, t1, s2, t2, s3, t3, t, b );
		tangents.push_back(t); 
		binormals.push_back(b);
	}
 
	// ������ ��������� �� ���� ��������, ��� ������ �� ��� ������
	// ����� �� ����������, � ������� ��� ��� ��������� �� ������� =)
	for ( i = 0; i < obj->vertices_qty; i++ )
	{
		std::vector<vertex_type> rt, rb;
		for ( j = 0; j < obj->polygons_qty; j++ )
		{
			if ( obj->polygon[ j ].a == i || obj->polygon[ j ].b == i || obj->polygon[ j ].c == i )
			{
				// ����� ����� ������� ��� ������� �����������.
				// ������� ������� ���� ����� � ��� ������
				rt.push_back( tangents[ j ] );
				rb.push_back( binormals[ j ] );
			}
		}
 
		// ��� ���������� ������� �����, ������ ������������ ��
		// � �������� �� �� ����������, �.�. ��������.
		vertex_type tangentRes;
		tangentRes.x=0; tangentRes.y=0; tangentRes.z=0;
		vertex_type binormalRes;
		binormalRes.x=0; binormalRes.y=0; binormalRes.z=0;
		for ( j = 0; j < rt.size(); j++ )
		{
			tangentRes.x+=rt[j].x;
			tangentRes.y+=rt[j].y;
			tangentRes.z+=rt[j].z;
			binormalRes.x+=rb[j].x;
			binormalRes.y+=rb[j].y;
			binormalRes.z+=rb[j].z;
		}
		tangentRes.x /= float( rt.size() );
		tangentRes.y /= float( rt.size() );
		tangentRes.z /= float( rt.size() );
		binormalRes.x /= float( rb.size() );
		binormalRes.y /= float( rb.size() );
		binormalRes.z /= float( rb.size() );
 
		// � ������ ��, � ��� ������ ��������. ��� �� ������,
		// TBN ����� ������������ ����� �����-������� ������� ���������.
		// ������� ��� ��� ������������ ������� ����� ������
		// ������� ���� ������� ���������������. ��� �� ���� ��
		// ������ � �������������, �������� ���������������
		// �������� ������� �����-������
 
		tangentRes = Ortogonalize( obj->normals[ i ], tangentRes );
		binormalRes = Ortogonalize( obj->normals[ i ], binormalRes );
 
		// ��� � ���, ������ ������ ������� ���������
 
		obj->tangents[ i ] =  tangentRes;
		obj->binormals[ i ] =  binormalRes;
	}

}


void CalcTriangleBasis( const vertex_type& E, const vertex_type& F, const vertex_type& G, float sE,
		float tE, float sF, float tF, float sG, float tG, vertex_type& tangentX,
		vertex_type& tangentY )
{
	vertex_type P;
	vertex_type Q;
	P.x = F.x - E.x;
	P.y = F.y - E.y;
	P.z = F.z - E.z;
	Q.x = G.x - E.x;
	Q.y = G.y - E.y;
	Q.z = G.z - E.z;
	float s1 = sF - sE;
	float t1 = tF - tE;
	float s2 = sG - sE;
	float t2 = tG - tE;
	float pqMatrix[2][3];
	pqMatrix[0][0] = P.x;
	pqMatrix[0][1] = P.y;
	pqMatrix[0][2] = P.z;
	pqMatrix[1][0] = Q.x;
	pqMatrix[1][1] = Q.y;
	pqMatrix[1][2] = Q.z;
	float temp = 1.0f / ( s1 * t2 - s2 * t1);
	float stMatrix[2][2];
	stMatrix[0][0] =  t2 * temp;
	stMatrix[0][1] = -t1 * temp;
	stMatrix[1][0] = -s2 * temp;
	stMatrix[1][1] =  s1 * temp;
	float tbMatrix[2][3];
	// stMatrix * pqMatrix
	tbMatrix[0][0] = stMatrix[0][0] * pqMatrix[0][0] + stMatrix[0][1] * pqMatrix[1][0];
	tbMatrix[0][1] = stMatrix[0][0] * pqMatrix[0][1] + stMatrix[0][1] * pqMatrix[1][1];
	tbMatrix[0][2] = stMatrix[0][0] * pqMatrix[0][2] + stMatrix[0][1] * pqMatrix[1][2];
	tbMatrix[1][0] = stMatrix[1][0] * pqMatrix[0][0] + stMatrix[1][1] * pqMatrix[1][0];
	tbMatrix[1][1] = stMatrix[1][0] * pqMatrix[0][1] + stMatrix[1][1] * pqMatrix[1][1];
	tbMatrix[1][2] = stMatrix[1][0] * pqMatrix[0][2] + stMatrix[1][1] * pqMatrix[1][2];
	tangentX.x=tbMatrix[0][0];
	tangentX.y=tbMatrix[0][1];
	tangentX.z=tbMatrix[0][2];
	tangentY.x=tbMatrix[1][0];
	tangentY.y=tbMatrix[1][1];
	tangentY.z=tbMatrix[1][2];
	tangentX=Normalize(tangentX);
	tangentY=Normalize(tangentY);
}


vertex_type ClosestPointOnLine( const vertex_type &a, const vertex_type &b, const vertex_type &p )
{
	vertex_type c;
	c.x= p.x - a.x;
	c.y= p.y - a.y;
	c.z= p.z - a.z;
	vertex_type V;
	V.x= b.x - a.x;
	V.y= b.y - a.y;
	V.z= b.z - a.z;
	//float d = V.Length();
	float d = sqrt(V.x*V.x + V.y*V.y + V.z*V.z);
	V=Normalize(V);
	float t = Dot( V, c );		// ��������� ������������ ��������
	
	// �������� �� ����� �� ������� �������
	if ( t < 0.0f )
		return a;
	if ( t > d )
		return b;
 
	// ������ ����� ����� a � b
	V.x *= t;
	V.y *= t;
	V.z *= t;
	vertex_type cd;
	cd.x=a.x+V.x;
	cd.y=a.y+V.y;
	cd.z=a.z+V.z;
	return (cd);
}


vertex_type Ortogonalize( const vertex_type& v1, const vertex_type& v2 )
{
	vertex_type cd;
	cd.x=-v1.x;
	cd.y=-v1.y;
	cd.z=-v1.z;
	vertex_type v2ProjV1 = ClosestPointOnLine( v1, cd, v2 );
	vertex_type res;
	res.x = v2.x - v2ProjV1.x;
	res.y = v2.y - v2ProjV1.y;
	res.z = v2.z - v2ProjV1.z;
	res=Normalize(res);
	return res;
}

*/