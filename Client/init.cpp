#include "main.h"


HWND CreateMyWindow(LPSTR strWindowName, int width, int height, DWORD dwStyle, bool bFullScreen, HINSTANCE hInstance)
{
    HWND hWnd;     
    WNDCLASS wndclass;  
    memset(&wndclass, 0, sizeof(WNDCLASS));     
    wndclass.style = CS_CLASSDC; 
    wndclass.lpfnWndProc = WinProc;         
    wndclass.hInstance = hInstance;
    wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);   
    wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);    
    wndclass.hbrBackground = (HBRUSH) (COLOR_WINDOW+1); 
    wndclass.lpszClassName = strWindowName;     
    RegisterClass(&wndclass);             

    dwStyle = WS_POPUP; 

    g_hInstance = hInstance;

    
    RECT rWindow;
    rWindow.left    = 0;            
    rWindow.right   = width;        
    rWindow.top     = 0;        
    rWindow.bottom  = height;      

    AdjustWindowRect( &rWindow, dwStyle, false);    

                           
    hWnd = CreateWindow("TFPROJECT", strWindowName, dwStyle, 0, 0,
                rWindow.right  - rWindow.left, rWindow.bottom - rWindow.top,
                NULL, NULL, hInstance, NULL);

    if(!hWnd) return NULL;          
    ShowWindow(hWnd, SW_SHOWNORMAL);    
    UpdateWindow(hWnd);         
    SetFocus(hWnd);             

    return hWnd;
}


bool bSetupPixelFormat(HDC hdc)
{
    PIXELFORMATDESCRIPTOR pfd; 
    int pixelformat;
    pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);  
    pfd.nVersion = 1;              
          
    pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
    pfd.dwLayerMask = PFD_MAIN_PLANE;      
    pfd.iPixelType = PFD_TYPE_RGBA;   
    pfd.cColorBits = SCREEN_DEPTH;     
    pfd.cDepthBits = SCREEN_DEPTH;  
    pfd.cAccumBits = 0;
    pfd.cStencilBits = 16;
	

    if ( (pixelformat = ChoosePixelFormat(hdc, &pfd)) == FALSE )
    {
        MessageBox(NULL, "ChoosePixelFormat failed", "Error", MB_OK);
        return FALSE;
    }


    if (SetPixelFormat(hdc, pixelformat, &pfd) == FALSE)
    {
        MessageBox(NULL, "SetPixelFormat failed", "Error", MB_OK);
        return FALSE;
    }

    return TRUE;
}

void SizeOpenGLScreen(int width, int height, float perspective)
{
    if (height==0)      
        height=1;

    glViewport(0,0,width,height);
	glMatrixMode(GL_PROJECTION);   
    glLoadIdentity();       
    gluPerspective(perspective,(GLfloat)width/(GLfloat)height, 1.0f ,8000.0f);
    glMatrixMode(GL_MODELVIEW);  
    glShadeModel(GL_SMOOTH);
	glClearColor(1.0f, 1.0f, 1.0f, 1.5f);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST); 
	//glEnable (GL_CULL_FACE); 
	//glCullFace (GL_FRONT);
	
}


void InitializeOpenGL(int width, int height)
{
    g_hDC = GetDC(g_hWnd);  

    if (!bSetupPixelFormat(g_hDC))     
        PostQuitMessage (0);         

    g_hRC = wglCreateContext(g_hDC);        
    wglMakeCurrent(g_hDC, g_hRC);      
       

    SizeOpenGLScreen(width, height, 55.0f);
}


void DeInit()
{
    if (g_hRC)
    {
        wglMakeCurrent(NULL, NULL); 
        wglDeleteContext(g_hRC);    
    }

    if (g_hDC)
        ReleaseDC(g_hWnd, g_hDC);   

    UnregisterClass("TFPROJECT", g_hInstance);    
    PostQuitMessage (0);                   
}


int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hprev, PSTR cmdline, int ishow)
{
    HWND hWnd;
	

    hWnd = CreateMyWindow("TFPROJECT", GetSystemMetrics(SM_CXSCREEN), GetSystemMetrics(SM_CYSCREEN), 0, true, hInstance);


    if(hWnd == NULL) return TRUE;


    Init(hWnd);

	MainCreate();
	glClearColor(1.0f, 1.0f, 1.0f, 1.5f);

	//return 0;
    return MainLoop();
}

