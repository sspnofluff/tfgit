#include "map.h"

extern Mainmass mainmass[51][51];

int ForestMaskArray[15][16][61];
int RockArray[51][51];
float ForestZArray[184];

polygon_type MapTemp[385];
polygon_type MapTemp2[385];

Texture grass_mask[48];

obj_type rock_1[4], rock_2[4], rock_3[4], rock_4[4], rock_5[4], rock_6[4], rock_7[4], forest, hill_1[4], hill_2[4], hill_3[4], hill_4[4], hill_5[4], hill_6[4], hill_7[4];


void CreateGrid()
{

GridSetWei=GridWei*6+2;
GridSetHei=GridHei*7+4;


vertex_type Temp;
polygon_type Temp2;
polygon_type Temp_up;
polygon_type Temp_right;

for (int i=0; i < 3; i++) {
for (int j=0; j < 3; j++) {
Temp.x=6.25*i;
Temp.y=6.25*j;
Temp.z=0;
MapVertexArray.push_back(Temp);
}
}
Temp2.a=4; Temp2.b=0; Temp2.c=1;
MapFaceArray.push_back(Temp2);
Temp2.a=3; Temp2.b=0; Temp2.c=4;
MapFaceArray.push_back(Temp2);
Temp2.a=4; Temp2.b=6; Temp2.c=3;
MapFaceArray.push_back(Temp2);
Temp2.a=7; Temp2.b=6; Temp2.c=4;
MapFaceArray.push_back(Temp2);
Temp2.a=4; Temp2.b=8; Temp2.c=7;
MapFaceArray.push_back(Temp2);
Temp2.a=5; Temp2.b=8; Temp2.c=4;
MapFaceArray.push_back(Temp2);
Temp2.a=4; Temp2.b=2; Temp2.c=5;
MapFaceArray.push_back(Temp2);
Temp2.a=1; Temp2.b=2; Temp2.c=4;
MapFaceArray.push_back(Temp2);
Temp_up.a=2; Temp_up.b=5; Temp_up.c=8;
for (int w = 1; w < GridSetWei+1; w++)
{


if (w==1)
{

for (int h = 1; h < GridSetHei; h++) {
for (int i=0; i < 3; i++) {
for (int j=1; j < 3; j++) {
Temp.x=6.25*i;
Temp.y=6.25*j+12.5*h;
Temp.z=0;
MapVertexArray.push_back(Temp);
}
}

if (h == 1) {

Temp2.a=11; Temp2.b=2; Temp2.c=9;
MapFaceArray.push_back(Temp2);
Temp2.a=5; Temp2.b=2; Temp2.c=11;
MapFaceArray.push_back(Temp2);
Temp2.a=11; Temp2.b=8; Temp2.c=5;
MapFaceArray.push_back(Temp2);
Temp2.a=13; Temp2.b=8; Temp2.c=11;
MapFaceArray.push_back(Temp2);
Temp2.a=11; Temp2.b=14; Temp2.c=13;
MapFaceArray.push_back(Temp2);
Temp2.a=12; Temp2.b=14; Temp2.c=11;
MapFaceArray.push_back(Temp2);
Temp2.a=11; Temp2.b=10; Temp2.c=12;
MapFaceArray.push_back(Temp2);
Temp2.a=9; Temp2.b=10; Temp2.c=11;
MapFaceArray.push_back(Temp2);
Temp_up.a=10; Temp_up.b=12; Temp_up.c=14;

}
else
{

Temp2.a=11+6*(h-1); Temp2.b=Temp_up.a; Temp2.c=9+6*(h-1);
MapFaceArray.push_back(Temp2);
Temp2.a=Temp_up.b; Temp2.b=Temp_up.a; Temp2.c=11+6*(h-1);
MapFaceArray.push_back(Temp2);
Temp2.a=11+6*(h-1); Temp2.b=Temp_up.c; Temp2.c=Temp_up.b;
MapFaceArray.push_back(Temp2);
Temp2.a=13+6*(h-1); Temp2.b=Temp_up.c; Temp2.c=11+6*(h-1);
MapFaceArray.push_back(Temp2);
Temp2.a=11+6*(h-1); Temp2.b=14+6*(h-1); Temp2.c=13+6*(h-1);
MapFaceArray.push_back(Temp2);
Temp2.a=12+6*(h-1); Temp2.b=14+6*(h-1); Temp2.c=11+6*(h-1);
MapFaceArray.push_back(Temp2);
Temp2.a=11+6*(h-1); Temp2.b=10+6*(h-1); Temp2.c=12+6*(h-1);
MapFaceArray.push_back(Temp2);
Temp2.a=9+6*(h-1); Temp2.b=10+6*(h-1); Temp2.c=11+6*(h-1);
MapFaceArray.push_back(Temp2);
Temp_up.a+=6; Temp_up.b+=6; Temp_up.c+=6;

}
}
}
if (w==2)
{
for (int h = 1; h < GridSetHei+1; h++)
{
if (h==1) {
Temp_up.a=6; Temp_up.b=7; Temp_up.c=8;
	for (int i=1; i < 3; i++) {
	for (int j=0; j < 3; j++) {
	Temp.x=6.25*i+12.5*(w-1);
	Temp.y=6.25*j+12.5*(h-1);
	Temp.z=0;
	MapVertexArray.push_back(Temp);
	}
	}
Temp2.a=16+6*(GridSetHei-2); Temp2.b=Temp_up.a; Temp2.c=Temp_up.b;
MapFaceArray.push_back(Temp2);
Temp2.a=15+6*(GridSetHei-2); Temp2.b=Temp_up.a; Temp2.c=16+6*(GridSetHei-2);
MapFaceArray.push_back(Temp2);
Temp2.a=16+6*(GridSetHei-2); Temp2.b=18+6*(GridSetHei-2); Temp2.c=15+6*(GridSetHei-2);
MapFaceArray.push_back(Temp2);
Temp2.a=19+6*(GridSetHei-2); Temp2.b=18+6*(GridSetHei-2); Temp2.c=16+6*(GridSetHei-2);
MapFaceArray.push_back(Temp2);
Temp2.a=16+6*(GridSetHei-2); Temp2.b=20+6*(GridSetHei-2); Temp2.c=19+6*(GridSetHei-2);
MapFaceArray.push_back(Temp2);
Temp2.a=17+6*(GridSetHei-2); Temp2.b=20+6*(GridSetHei-2); Temp2.c=16+6*(GridSetHei-2);
MapFaceArray.push_back(Temp2);
Temp2.a=16+6*(GridSetHei-2); Temp2.b=Temp_up.c; Temp2.c=17+6*(GridSetHei-2);
MapFaceArray.push_back(Temp2);
Temp2.a=Temp_up.b; Temp2.b=Temp_up.c; Temp2.c=16+6*(GridSetHei-2);
MapFaceArray.push_back(Temp2);
Temp_right.b=17+6*(GridSetHei-2); Temp_right.c=20+6*(GridSetHei-2);
}


if (h==2) {
Temp_up.a=8; Temp_up.b=13; Temp_up.c=14;
	for (int i=1; i < 3; i++) {
	for (int j=1; j < 3; j++) {
	Temp.x=6.25*i+12.5*(w-1);
	Temp.y=6.25*j+12.5*(h-1);
	Temp.z=0;
	MapVertexArray.push_back(Temp);
	}
	}
Temp2.a=Temp_right.c+1; Temp2.b=Temp_up.a; Temp2.c=Temp_up.b;
MapFaceArray.push_back(Temp2);
Temp2.a=Temp_right.b; Temp2.b=Temp_up.a; Temp2.c=Temp_right.c+1;
MapFaceArray.push_back(Temp2);
Temp2.a=Temp_right.c+1; Temp2.b=Temp_right.c; Temp2.c=Temp_right.b;
MapFaceArray.push_back(Temp2);
Temp2.a=Temp_right.c+3; Temp2.b=Temp_right.c; Temp2.c=Temp_right.c+1;
MapFaceArray.push_back(Temp2);
Temp2.a=Temp_right.c+1; Temp2.b=Temp_right.c+4; Temp2.c=Temp_right.c+3;
MapFaceArray.push_back(Temp2);
Temp2.a=Temp_right.c+2; Temp2.b=Temp_right.c+4; Temp2.c=Temp_right.c+1;
MapFaceArray.push_back(Temp2);
Temp2.a=Temp_right.c+1; Temp2.b=Temp_up.c; Temp2.c=Temp_right.c+4;
MapFaceArray.push_back(Temp2);
Temp2.a=Temp_up.b; Temp2.b=Temp_up.c; Temp2.c=Temp_right.c+1;
MapFaceArray.push_back(Temp2);
Temp_right.b=Temp_right.b+5; Temp_right.c=Temp_right.c+4;
}
if (h>2) {

Temp_up.a=14+6*(h-3); Temp_up.b=19+6*(h-3);  Temp_up.c=20+6*(h-3);

	for (int i=1; i < 3; i++) {
	for (int j=1; j < 3; j++) {
	Temp.x=6.25*i+12.5*(w-1);
	Temp.y=6.25*j+12.5*(h-1);
	Temp.z=0;
	MapVertexArray.push_back(Temp);
	}
	}
Temp2.a=Temp_right.c+1; Temp2.b=Temp_up.a; Temp2.c=Temp_up.b;
MapFaceArray.push_back(Temp2);
Temp2.a=Temp_right.b; Temp2.b=Temp_up.a; Temp2.c=Temp_right.c+1;
MapFaceArray.push_back(Temp2);
Temp2.a=Temp_right.c+1; Temp2.b=Temp_right.c; Temp2.c=Temp_right.b;
MapFaceArray.push_back(Temp2);
Temp2.a=Temp_right.c+3; Temp2.b=Temp_right.c; Temp2.c=Temp_right.c+1;
MapFaceArray.push_back(Temp2);
Temp2.a=Temp_right.c+1; Temp2.b=Temp_right.c+4; Temp2.c=Temp_right.c+3;
MapFaceArray.push_back(Temp2);
Temp2.a=Temp_right.c+2; Temp2.b=Temp_right.c+4; Temp2.c=Temp_right.c+1;
MapFaceArray.push_back(Temp2);
Temp2.a=Temp_right.c+1; Temp2.b=Temp_up.c; Temp2.c=Temp_right.c+2;
MapFaceArray.push_back(Temp2);
Temp2.a=Temp_up.b; Temp2.b=Temp_up.c; Temp2.c=Temp_right.c+1;
MapFaceArray.push_back(Temp2);
Temp_right.b+=4; Temp_right.c+=4;
}
} /////���	
}
if (w>2)
{
for (int h = 1; h < GridSetHei+1; h++)
{
if (h==1) {
Temp_up.a=9+6*(GridSetHei-1)+(6+4*(GridSetHei-1))*(w-3)+3; Temp_up.b=Temp_up.a+1; Temp_up.c=Temp_up.b+1;
//Temp_up.a=30; Temp_up.b=Temp_up.a+1; Temp_up.c=Temp_up.b+1;
	for (int i=1; i < 3; i++) {
	for (int j=0; j < 3; j++) {
	Temp.x=6.25*i+12.5*(w-1);
	Temp.y=6.25*j+12.5*(h-1);
	Temp.z=0;
	MapVertexArray.push_back(Temp);
	}
	}

Temp2.a=16+6*(GridSetHei-2)+(6+4*(GridSetHei-1))*(w-2); Temp2.b=Temp_up.a; Temp2.c=Temp_up.b;
MapFaceArray.push_back(Temp2);
Temp2.a=15+6*(GridSetHei-2)+(6+4*(GridSetHei-1))*(w-2); Temp2.b=Temp_up.a; Temp2.c=16+6*(GridSetHei-2)+(6+4*(GridSetHei-1))*(w-2);
MapFaceArray.push_back(Temp2);
Temp2.a=16+6*(GridSetHei-2)+(6+4*(GridSetHei-1))*(w-2); Temp2.b=18+6*(GridSetHei-2)+(6+4*(GridSetHei-1))*(w-2); Temp2.c=15+6*(GridSetHei-2)+(6+4*(GridSetHei-1))*(w-2);
MapFaceArray.push_back(Temp2);
Temp2.a=19+6*(GridSetHei-2)+(6+4*(GridSetHei-1))*(w-2); Temp2.b=18+6*(GridSetHei-2)+(6+4*(GridSetHei-1))*(w-2); Temp2.c=16+6*(GridSetHei-2)+(6+4*(GridSetHei-1))*(w-2);
MapFaceArray.push_back(Temp2);
Temp2.a=16+6*(GridSetHei-2)+(6+4*(GridSetHei-1))*(w-2); Temp2.b=20+6*(GridSetHei-2)+(6+4*(GridSetHei-1))*(w-2); Temp2.c=19+6*(GridSetHei-2)+(6+4*(GridSetHei-1))*(w-2);
MapFaceArray.push_back(Temp2);
Temp2.a=17+6*(GridSetHei-2)+(6+4*(GridSetHei-1))*(w-2); Temp2.b=20+6*(GridSetHei-2)+(6+4*(GridSetHei-1))*(w-2); Temp2.c=16+6*(GridSetHei-2)+(6+4*(GridSetHei-1))*(w-2);
MapFaceArray.push_back(Temp2);
Temp2.a=16+6*(GridSetHei-2)+(6+4*(GridSetHei-1))*(w-2); Temp2.b=Temp_up.c; Temp2.c=17+6*(GridSetHei-2)+(6+4*(GridSetHei-1))*(w-2);
MapFaceArray.push_back(Temp2);
Temp2.a=Temp_up.b; Temp2.b=Temp_up.c; Temp2.c=16+6*(GridSetHei-2)+(6+4*(GridSetHei-1))*(w-2);
MapFaceArray.push_back(Temp2);
Temp_right.b=17+6*(GridSetHei-2)+(6+4*(GridSetHei-1))*(w-2); Temp_right.c=20+6*(GridSetHei-2)+(6+4*(GridSetHei-1))*(w-2);
}
if (h==2) {
Temp_up.a=8+6*GridSetHei+(6+4*(GridSetHei-1))*(w-3); Temp_up.b=Temp_up.a+3; Temp_up.c=Temp_up.b+1;
//Temp_up.a=4+4*(GridSetHei-1)+((6+4*(GridSetHei-1))*(w-2))*(w-2)+4*(h-2); Temp_up.b=Temp_up.a+3; Temp_up.c=Temp_up.b+1;
for (int i=1; i < 3; i++) {
for (int j=1; j < 3; j++) {
Temp.x=6.25*i+12.5*(w-1);
Temp.y=6.25*j+12.5*(h-1);
Temp.z=0;
MapVertexArray.push_back(Temp);
}
}
Temp2.a=Temp_right.c+1; Temp2.b=Temp_up.a; Temp2.c=Temp_up.b;
MapFaceArray.push_back(Temp2);
Temp2.a=Temp_right.b; Temp2.b=Temp_up.a; Temp2.c=Temp_right.c+1;
MapFaceArray.push_back(Temp2);
Temp2.a=Temp_right.c+1; Temp2.b=Temp_right.c; Temp2.c=Temp_right.b;
MapFaceArray.push_back(Temp2);
Temp2.a=Temp_right.c+3; Temp2.b=Temp_right.c; Temp2.c=Temp_right.c+1;
MapFaceArray.push_back(Temp2);
Temp2.a=Temp_right.c+1; Temp2.b=Temp_right.c+4; Temp2.c=Temp_right.c+3;
MapFaceArray.push_back(Temp2);
Temp2.a=Temp_right.c+2; Temp2.b=Temp_right.c+4; Temp2.c=Temp_right.c+1;
MapFaceArray.push_back(Temp2);
Temp2.a=Temp_right.c+1; Temp2.b=Temp_up.c; Temp2.c=Temp_right.c+2;
MapFaceArray.push_back(Temp2);
Temp2.a=Temp_up.b; Temp2.b=Temp_up.c; Temp2.c=Temp_right.c+1;
MapFaceArray.push_back(Temp2);
Temp_right.b+=5; Temp_right.c+=4;
}
if (h>2) {
Temp_up.a=8+6*GridSetHei+4*(h-2)+(6+4*(GridSetHei-1))*(w-3); Temp_up.b=Temp_up.a+3; Temp_up.c=Temp_up.b+1;
//Temp_up.a=4+4*(GridSetHei-1)+((6+4*(GridSetHei-1))*(w-2))*(w-2)+4*(h-2); Temp_up.b=Temp_up.a+3; Temp_up.c=Temp_up.b+1;
for (int i=1; i < 3; i++) {
for (int j=1; j < 3; j++) {
Temp.x=6.25*i+12.5*(w-1);
Temp.y=6.25*j+12.5*(h-1);
Temp.z=0;
MapVertexArray.push_back(Temp);
}
}
Temp2.a=Temp_right.c+1; Temp2.b=Temp_up.a; Temp2.c=Temp_up.b;
MapFaceArray.push_back(Temp2);
Temp2.a=Temp_right.b; Temp2.b=Temp_up.a; Temp2.c=Temp_right.c+1;
MapFaceArray.push_back(Temp2);
Temp2.a=Temp_right.c+1; Temp2.b=Temp_right.c; Temp2.c=Temp_right.b;
MapFaceArray.push_back(Temp2);
Temp2.a=Temp_right.c+3; Temp2.b=Temp_right.c; Temp2.c=Temp_right.c+1;
MapFaceArray.push_back(Temp2);
Temp2.a=Temp_right.c+1; Temp2.b=Temp_right.c+4; Temp2.c=Temp_right.c+3;
MapFaceArray.push_back(Temp2);
Temp2.a=Temp_right.c+2; Temp2.b=Temp_right.c+4; Temp2.c=Temp_right.c+1;
MapFaceArray.push_back(Temp2);
Temp2.a=Temp_right.c+1; Temp2.b=Temp_up.c; Temp2.c=Temp_right.c+2;
MapFaceArray.push_back(Temp2);
Temp2.a=Temp_up.b; Temp2.b=Temp_up.c; Temp2.c=Temp_right.c+1;
MapFaceArray.push_back(Temp2);
Temp_right.b+=4; Temp_right.c+=4;
}
}
}
}
Temp.x=0;
Temp.y=0;
Temp.z=-1;
for (unsigned int i = 0; i < MapVertexArray.size(); i++)
MapNormalArray.push_back(Temp);


unsigned int polch, u, v;

polch=0;
bignorm.width=8192;
bignorm.height=8192;
bignorm.imageData	= (byte *)malloc(bignorm.width*bignorm.height*3);
if (bignorm.imageData == NULL) 
MessageBox(NULL, "Could not allocate memory for allNormal", "ERROR", MB_OK);	
for (u = 0; u < bignorm.width; u++) {
for (v = 0; v < bignorm.height; v++) {
bignorm.imageData[polch]=127;
bignorm.imageData[polch+1]=127;
bignorm.imageData[polch+2]=255;
polch+=3;
}
}

polch=0;
blend.width=4096;
blend.height=4096;
blend.imageData	= (GLubyte *)malloc(blend.width*blend.height*4);
if (blend.imageData == NULL) MessageBox(NULL, "Could not allocate memory for blend", "ERROR", MB_OK);	
for (u = 0; u < blend.width; u++) {
for (v = 0; v < blend.height; v++) {
blend.imageData[polch]=0;
blend.imageData[polch+1]=0;
blend.imageData[polch+2]=255;
blend.imageData[polch+3]=0;
polch+=4;
}
}


polch=0;
blend2.width=4096;
blend2.height=4096;
blend2.imageData	= (GLubyte *)malloc(blend2.width*blend2.height*4);
if (blend2.imageData == NULL) MessageBox(NULL, "Could not allocate memory for blend2", "ERROR", MB_OK);	
for (u = 0; u < blend2.width; u++) {
for (v = 0; v < blend2.height; v++) {
blend2.imageData[polch]=255;
blend2.imageData[polch+1]=0;
blend2.imageData[polch+2]=0;
blend2.imageData[polch+3]=0;
polch+=4;
}
}



}


void LoadMapConfig()
{
	/*
char	cBuffer[50];
FILE *f;
f = fopen("mapconfig.txt", "rt");
while (fgets(cBuffer,50, f))
{
MapObjList.push_back(cBuffer);
//MessageBox(NULL, cBuffer, "INFO", MB_OK);
} 
fclose(f);

//MessageBox(NULL, rock_1[0].name, "INFO", MB_OK);

*/
}

void LoadMapObjects()
{
Load3DS (&forest,"meshes/forest.3ds");

Load3DS (&rock_1[0],"meshes/rock_1.3ds");
Load3DS (&rock_1[1],"meshes/rock_1.3ds");
Load3DS (&rock_1[2],"meshes/rock_1.3ds");
Load3DS (&rock_1[3],"meshes/rock_1.3ds");
Load3DS (&rock_2[0],"meshes/rock_2.3ds");
Load3DS (&rock_2[1],"meshes/rock_1.3ds");
Load3DS (&rock_2[2],"meshes/rock_1.3ds");
Load3DS (&rock_2[3],"meshes/rock_1.3ds");
Load3DS (&rock_3[0],"meshes/rock_3.3ds");
Load3DS (&rock_3[1],"meshes/rock_1.3ds");
Load3DS (&rock_3[2],"meshes/rock_1.3ds");
Load3DS (&rock_3[3],"meshes/rock_1.3ds");
Load3DS (&rock_4[0],"meshes/rock_4.3ds");
Load3DS (&rock_4[1],"meshes/rock_1.3ds");
Load3DS (&rock_4[2],"meshes/rock_1.3ds");
Load3DS (&rock_4[3],"meshes/rock_1.3ds");
Load3DS (&rock_5[0],"meshes/rock_5.3ds");
Load3DS (&rock_5[1],"meshes/rock_1.3ds");
Load3DS (&rock_5[2],"meshes/rock_1.3ds");
Load3DS (&rock_5[3],"meshes/rock_1.3ds");
Load3DS (&rock_6[0],"meshes/rock_6.3ds");
Load3DS (&rock_6[1],"meshes/rock_1.3ds");
Load3DS (&rock_6[2],"meshes/rock_1.3ds");
Load3DS (&rock_6[3],"meshes/rock_1.3ds");
Load3DS (&rock_7[0],"meshes/rock_7.3ds");
Load3DS (&rock_7[1],"meshes/rock_1.3ds");
Load3DS (&rock_7[2],"meshes/rock_1.3ds");
Load3DS (&rock_7[3],"meshes/rock_1.3ds");

Load3DS (&hill_1[0],"meshes/hill_1.3ds");
Load3DS (&hill_1[1],"meshes/hill_1.3ds");
Load3DS (&hill_1[2],"meshes/hill_1.3ds");
Load3DS (&hill_1[3],"meshes/hill_1.3ds");
Load3DS (&hill_2[0],"meshes/hill_2.3ds");
Load3DS (&hill_2[1],"meshes/hill_1.3ds");
Load3DS (&hill_2[2],"meshes/hill_1.3ds");
Load3DS (&hill_2[3],"meshes/hill_1.3ds");
Load3DS (&hill_3[0],"meshes/hill_3.3ds");
Load3DS (&hill_3[1],"meshes/hill_1.3ds");
Load3DS (&hill_3[2],"meshes/hill_1.3ds");
Load3DS (&hill_3[3],"meshes/hill_1.3ds");
Load3DS (&hill_4[0],"meshes/hill_4.3ds");
Load3DS (&hill_4[1],"meshes/hill_1.3ds");
Load3DS (&hill_4[2],"meshes/hill_1.3ds");
Load3DS (&hill_4[3],"meshes/hill_1.3ds");
Load3DS (&hill_5[0],"meshes/hill_5.3ds");
Load3DS (&hill_5[1],"meshes/hill_1.3ds");
Load3DS (&hill_5[2],"meshes/hill_1.3ds");
Load3DS (&hill_5[3],"meshes/hill_1.3ds");
Load3DS (&hill_6[0],"meshes/hill_6.3ds");
Load3DS (&hill_6[1],"meshes/hill_1.3ds");
Load3DS (&hill_6[2],"meshes/hill_1.3ds");
Load3DS (&hill_6[3],"meshes/hill_1.3ds");
Load3DS (&hill_7[0],"meshes/hill_7.3ds");
Load3DS (&hill_7[1],"meshes/hill_1.3ds");
Load3DS (&hill_7[2],"meshes/hill_1.3ds");
Load3DS (&hill_7[3],"meshes/hill_1.3ds");

MirrorVert(&rock_2[2], &rock_2[0]);
MirrorHorz(&rock_2[1], &rock_2[0]);
MirrorHorz(&rock_2[3], &rock_2[2]);

MirrorVert(&rock_1[2], &rock_1[0]);
MirrorHorz(&rock_1[1], &rock_1[0]);
MirrorHorz(&rock_1[3], &rock_1[2]);
	
MirrorVert(&rock_3[2], &rock_3[0]);
MirrorHorz(&rock_3[1], &rock_3[0]);
MirrorHorz(&rock_3[3], &rock_3[2]);

MirrorVert(&rock_4[2], &rock_4[0]);
MirrorHorz(&rock_4[1], &rock_4[0]);
MirrorHorz(&rock_4[3], &rock_4[2]);

MirrorVert(&rock_5[2], &rock_5[0]);
MirrorHorz(&rock_5[1], &rock_5[0]);
MirrorHorz(&rock_5[3], &rock_5[2]);

MirrorVert(&rock_6[2], &rock_6[0]);
MirrorHorz(&rock_6[1], &rock_6[0]);
MirrorHorz(&rock_6[3], &rock_6[2]);

MirrorVert(&rock_7[2], &rock_7[0]);
MirrorHorz(&rock_7[1], &rock_7[0]);
MirrorHorz(&rock_7[3], &rock_7[2]);

MirrorVert(&hill_2[2], &hill_2[0]);
MirrorHorz(&hill_2[1], &hill_2[0]);
MirrorHorz(&hill_2[3], &hill_2[2]);

MirrorVert(&hill_1[2], &hill_1[0]);
MirrorHorz(&hill_1[1], &hill_1[0]);
MirrorHorz(&hill_1[3], &hill_1[2]);

MirrorVert(&hill_3[2], &hill_3[0]);
MirrorHorz(&hill_3[1], &hill_3[0]);
MirrorHorz(&hill_3[3], &hill_3[2]);

MirrorVert(&hill_4[2], &hill_4[0]);
MirrorHorz(&hill_4[1], &hill_4[0]);
MirrorHorz(&hill_4[3], &hill_4[2]);

MirrorVert(&hill_5[2], &hill_5[0]);
MirrorHorz(&hill_5[1], &hill_5[0]);
MirrorHorz(&hill_5[3], &hill_5[2]);

MirrorVert(&hill_6[2], &hill_6[0]);
MirrorHorz(&hill_6[1], &hill_6[0]);
MirrorHorz(&hill_6[3], &hill_6[2]);

MirrorVert(&hill_7[2], &hill_7[0]);
MirrorHorz(&hill_7[1], &hill_7[0]);
MirrorHorz(&hill_7[3], &hill_7[2]);

PrepareObject(&rock_1[0],10,1,0);
PrepareObject(&rock_1[1],10,1,0);
PrepareObject(&rock_1[2],10,1,0);
PrepareObject(&rock_1[3],10,1,0);
PrepareObject(&rock_2[0],10,1,0);
PrepareObject(&rock_2[1],10,1,0);
PrepareObject(&rock_2[2],10,1,0);
PrepareObject(&rock_2[3],10,1,0);
PrepareObject(&rock_3[0],10,1,0);
PrepareObject(&rock_3[1],10,1,0);
PrepareObject(&rock_3[2],10,1,0);
PrepareObject(&rock_3[3],10,1,0);
PrepareObject(&rock_4[0],10,1,0);
PrepareObject(&rock_4[1],10,1,0);
PrepareObject(&rock_4[2],10,1,0);
PrepareObject(&rock_4[3],10,1,0);
PrepareObject(&rock_5[0],10,1,0);
PrepareObject(&rock_5[1],10,1,0);
PrepareObject(&rock_5[2],10,1,0);
PrepareObject(&rock_5[3],10,1,0);
PrepareObject(&rock_6[0],10,1,0);
PrepareObject(&rock_6[1],10,1,0);
PrepareObject(&rock_6[2],10,1,0);
PrepareObject(&rock_6[3],10,1,0);
PrepareObject(&rock_7[0],10,1,0);
PrepareObject(&rock_7[1],10,1,0);
PrepareObject(&rock_7[2],10,1,0);
PrepareObject(&rock_7[3],10,1,0);

PrepareObject(&forest,3.3,0,0);

PrepareObject(&hill_1[0],8,1,0);
PrepareObject(&hill_1[1],8,1,0);
PrepareObject(&hill_1[2],8,1,0);
PrepareObject(&hill_1[3],8,1,0);
PrepareObject(&hill_2[0],8,1,0);
PrepareObject(&hill_2[1],8,1,0);
PrepareObject(&hill_2[2],8,1,0);
PrepareObject(&hill_2[3],8,1,0);
PrepareObject(&hill_3[0],8,1,0);
PrepareObject(&hill_3[1],8,1,0);
PrepareObject(&hill_3[2],8,1,0);
PrepareObject(&hill_3[3],8,1,0);
PrepareObject(&hill_4[0],8,1,0);
PrepareObject(&hill_4[1],8,1,0);
PrepareObject(&hill_4[2],8,1,0);
PrepareObject(&hill_4[3],8,1,0);
PrepareObject(&hill_5[0],8,1,0);
PrepareObject(&hill_5[1],8,1,0);
PrepareObject(&hill_5[2],8,1,0);
PrepareObject(&hill_5[3],8,1,0);
PrepareObject(&hill_6[0],8,1,0);
PrepareObject(&hill_6[1],8,1,0);
PrepareObject(&hill_6[2],8,1,0);
PrepareObject(&hill_6[3],8,1,0);
PrepareObject(&hill_7[0],8,1,0);
PrepareObject(&hill_7[1],8,1,0);
PrepareObject(&hill_7[2],8,1,0);
PrepareObject(&hill_7[3],8,1,0);


LoadTGA(&blend,"textures/rock_masks/rock_1a_mask.tga");
LoadTGA(&bignorm,"textures/rock_masks/rock_1a_norm.tga");
LoadTGA(&blend2,"textures/rock_masks/rock_1a_mask.tga");

LoadTGA(&rock_1[0].texture,"textures/rock_masks/rock_1a_mask.tga");
LoadTGA(&rock_1[1].texture,"textures/rock_masks/rock_1b_mask.tga");
LoadTGA(&rock_1[2].texture,"textures/rock_masks/rock_1c_mask.tga");
LoadTGA(&rock_1[3].texture,"textures/rock_masks/rock_1d_mask.tga");
LoadTGA(&rock_2[0].texture,"textures/rock_masks/rock_2a_mask.tga");
LoadTGA(&rock_2[1].texture,"textures/rock_masks/rock_2b_mask.tga");
LoadTGA(&rock_2[2].texture,"textures/rock_masks/rock_2c_mask.tga");
LoadTGA(&rock_2[3].texture,"textures/rock_masks/rock_2d_mask.tga");
LoadTGA(&rock_3[0].texture,"textures/rock_masks/rock_3a_mask.tga");
LoadTGA(&rock_3[1].texture,"textures/rock_masks/rock_3b_mask.tga");
LoadTGA(&rock_3[2].texture,"textures/rock_masks/rock_3c_mask.tga");
LoadTGA(&rock_3[3].texture,"textures/rock_masks/rock_3d_mask.tga");
LoadTGA(&rock_4[0].texture,"textures/rock_masks/rock_4a_mask.tga");
LoadTGA(&rock_4[1].texture,"textures/rock_masks/rock_4b_mask.tga");
LoadTGA(&rock_4[2].texture,"textures/rock_masks/rock_4c_mask.tga");
LoadTGA(&rock_4[3].texture,"textures/rock_masks/rock_4d_mask.tga");
LoadTGA(&rock_5[0].texture,"textures/rock_masks/rock_5a_mask.tga");
LoadTGA(&rock_5[1].texture,"textures/rock_masks/rock_5b_mask.tga");
LoadTGA(&rock_5[2].texture,"textures/rock_masks/rock_5c_mask.tga");
LoadTGA(&rock_5[3].texture,"textures/rock_masks/rock_5d_mask.tga");
LoadTGA(&rock_6[0].texture,"textures/rock_masks/rock_6a_mask.tga");
LoadTGA(&rock_6[1].texture,"textures/rock_masks/rock_6b_mask.tga");
LoadTGA(&rock_6[2].texture,"textures/rock_masks/rock_6c_mask.tga");
LoadTGA(&rock_6[3].texture,"textures/rock_masks/rock_6d_mask.tga");
LoadTGA(&rock_7[0].texture,"textures/rock_masks/rock_7a_mask.tga");
LoadTGA(&rock_7[1].texture,"textures/rock_masks/rock_7b_mask.tga");
LoadTGA(&rock_7[2].texture,"textures/rock_masks/rock_7c_mask.tga");
LoadTGA(&rock_7[3].texture,"textures/rock_masks/rock_7d_mask.tga");

LoadTGA(&rock_1[0].texture2,"textures/rock_masks/rock_1a_mask2.tga");
LoadTGA(&rock_1[1].texture2,"textures/rock_masks/rock_1b_mask2.tga");
LoadTGA(&rock_1[2].texture2,"textures/rock_masks/rock_1c_mask2.tga");
LoadTGA(&rock_1[3].texture2,"textures/rock_masks/rock_1d_mask2.tga");
LoadTGA(&rock_2[0].texture2,"textures/rock_masks/rock_2a_mask2.tga");
LoadTGA(&rock_2[1].texture2,"textures/rock_masks/rock_2b_mask2.tga");
LoadTGA(&rock_2[2].texture2,"textures/rock_masks/rock_2c_mask2.tga");
LoadTGA(&rock_2[3].texture2,"textures/rock_masks/rock_2d_mask2.tga");
LoadTGA(&rock_3[0].texture2,"textures/rock_masks/rock_3a_mask2.tga");
LoadTGA(&rock_3[1].texture2,"textures/rock_masks/rock_3b_mask2.tga");
LoadTGA(&rock_3[2].texture2,"textures/rock_masks/rock_3c_mask2.tga");
LoadTGA(&rock_3[3].texture2,"textures/rock_masks/rock_3d_mask2.tga");
LoadTGA(&rock_4[0].texture2,"textures/rock_masks/rock_4a_mask2.tga");
LoadTGA(&rock_4[1].texture2,"textures/rock_masks/rock_4b_mask2.tga");
LoadTGA(&rock_4[2].texture2,"textures/rock_masks/rock_4c_mask2.tga");
LoadTGA(&rock_4[3].texture2,"textures/rock_masks/rock_4d_mask2.tga");
LoadTGA(&rock_5[0].texture2,"textures/rock_masks/rock_5a_mask2.tga");
LoadTGA(&rock_5[1].texture2,"textures/rock_masks/rock_5b_mask2.tga");
LoadTGA(&rock_5[2].texture2,"textures/rock_masks/rock_5c_mask2.tga");
LoadTGA(&rock_5[3].texture2,"textures/rock_masks/rock_5d_mask2.tga");
LoadTGA(&rock_6[0].texture2,"textures/rock_masks/rock_6a_mask2.tga");
LoadTGA(&rock_6[1].texture2,"textures/rock_masks/rock_6b_mask2.tga");
LoadTGA(&rock_6[2].texture2,"textures/rock_masks/rock_6c_mask2.tga");
LoadTGA(&rock_6[3].texture2,"textures/rock_masks/rock_6d_mask2.tga");
LoadTGA(&rock_7[0].texture2,"textures/rock_masks/rock_7a_mask2.tga");
LoadTGA(&rock_7[1].texture2,"textures/rock_masks/rock_7b_mask2.tga");
LoadTGA(&rock_7[2].texture2,"textures/rock_masks/rock_7c_mask2.tga");
LoadTGA(&rock_7[3].texture2,"textures/rock_masks/rock_7d_mask2.tga");

LoadTGA(&rock_1[0].texture_norm,"textures/rock_masks/rock_1a_norm.tga");
LoadTGA(&rock_1[1].texture_norm,"textures/rock_masks/rock_1b_norm.tga");
LoadTGA(&rock_1[2].texture_norm,"textures/rock_masks/rock_1c_norm.tga");
LoadTGA(&rock_1[3].texture_norm,"textures/rock_masks/rock_1d_norm.tga");
LoadTGA(&rock_2[0].texture_norm,"textures/rock_masks/rock_2a_norm.tga");
LoadTGA(&rock_2[1].texture_norm,"textures/rock_masks/rock_2b_norm.tga");
LoadTGA(&rock_2[2].texture_norm,"textures/rock_masks/rock_2c_norm.tga");
LoadTGA(&rock_2[3].texture_norm,"textures/rock_masks/rock_2d_norm.tga");
LoadTGA(&rock_3[0].texture_norm,"textures/rock_masks/rock_3a_norm.tga");
LoadTGA(&rock_3[1].texture_norm,"textures/rock_masks/rock_3b_norm.tga");
LoadTGA(&rock_3[2].texture_norm,"textures/rock_masks/rock_3c_norm.tga");
LoadTGA(&rock_3[3].texture_norm,"textures/rock_masks/rock_3d_norm.tga");
LoadTGA(&rock_4[0].texture_norm,"textures/rock_masks/rock_4a_norm.tga");
LoadTGA(&rock_4[1].texture_norm,"textures/rock_masks/rock_4b_norm.tga");
LoadTGA(&rock_4[2].texture_norm,"textures/rock_masks/rock_4c_norm.tga");
LoadTGA(&rock_4[3].texture_norm,"textures/rock_masks/rock_4d_norm.tga");
LoadTGA(&rock_5[0].texture_norm,"textures/rock_masks/rock_5a_norm.tga");
LoadTGA(&rock_5[1].texture_norm,"textures/rock_masks/rock_5b_norm.tga");
LoadTGA(&rock_5[2].texture_norm,"textures/rock_masks/rock_5c_norm.tga");
LoadTGA(&rock_5[3].texture_norm,"textures/rock_masks/rock_5d_norm.tga");
LoadTGA(&rock_6[0].texture_norm,"textures/rock_masks/rock_6a_norm.tga");
LoadTGA(&rock_6[1].texture_norm,"textures/rock_masks/rock_6b_norm.tga");
LoadTGA(&rock_6[2].texture_norm,"textures/rock_masks/rock_6c_norm.tga");
LoadTGA(&rock_6[3].texture_norm,"textures/rock_masks/rock_6d_norm.tga");
LoadTGA(&rock_7[0].texture_norm,"textures/rock_masks/rock_7a_norm.tga");
LoadTGA(&rock_7[1].texture_norm,"textures/rock_masks/rock_7b_norm.tga");
LoadTGA(&rock_7[2].texture_norm,"textures/rock_masks/rock_7c_norm.tga");
LoadTGA(&rock_7[3].texture_norm,"textures/rock_masks/rock_7d_norm.tga");

string grassmaskadress; 
for (int i = 0; i < 43; i++)  {
grassmaskadress="textures/MASK/"+IntToStr(i)+".tga";
const char * cbuf = grassmaskadress.c_str();
LoadTGA(&grass_mask[i],cbuf);
}
}




void CreateMounts()
{



for (int u = 0; u < GridWei; u++) {
for (int v = 0; v < GridHei; v++) {
if (mainmass[u][v].rock==1) {
if (0)
if (RockArray[u][v]>28 && RockArray[u][v]<300)
{
AddNewMount(u,v,&rock_1[0],12,12,12,10,3.0f);
GenerateBlendMap(u,v,&rock_1[0]);
GenerateBlendMap2(u,v,&rock_1[0]);
GenerateNormalMap(u,v,&rock_1[0]);
}

switch(RockArray[u][v]) {

case 0:
{
break;
}

case 1:
{
AddNewMount(u,v,&rock_1[0],12,12,12,10,3.0f);
GenerateBlendMap(u,v,&rock_1[0]);
GenerateBlendMap2(u,v,&rock_1[0]);
GenerateNormalMap(u,v,&rock_1[0]);
break;
}

case 2:
{
AddNewMount(u,v,&rock_1[1],12,12,12,10,3.0f);
GenerateBlendMap(u,v,&rock_1[1]);
GenerateBlendMap2(u,v,&rock_1[1]);
GenerateNormalMap(u,v,&rock_1[1]);
break;
}
case 3:
{
AddNewMount(u,v,&rock_1[2],12,12,12,10,3.0f);
GenerateBlendMap(u,v,&rock_1[2]);
GenerateBlendMap2(u,v,&rock_1[2]);
GenerateNormalMap(u,v,&rock_1[2]);
break;
}
case 4:
{
AddNewMount(u,v,&rock_1[3],12,12,10,10,3.0f);
GenerateBlendMap(u,v,&rock_1[3]);
GenerateBlendMap2(u,v,&rock_1[3]);
GenerateNormalMap(u,v,&rock_1[3]);
break;
}

case 5:
{
AddNewMount(u,v,&rock_2[1],11,5,12,6,2.5f);
GenerateBlendMap(u,v,&rock_2[1]);
GenerateBlendMap2(u,v,&rock_2[1]);
GenerateNormalMap(u,v,&rock_2[1]);
break;
}
case 6:
{
AddNewMount(u,v,&rock_2[2],5,11,6,12,2.5f);
GenerateBlendMap(u,v,&rock_2[2]);
GenerateBlendMap2(u,v,&rock_2[2]);
GenerateNormalMap(u,v,&rock_2[2]);
break;
}
case 7:
{
if (u%2==0) {
AddNewMount(u+1,v-1,&rock_2[0],5,11,12,6,2.5f);
GenerateBlendMap(u+1,v-1,&rock_2[0]);
GenerateBlendMap2(u+1,v-1,&rock_2[0]);
GenerateNormalMap(u+1,v-1,&rock_2[0]);
}
else
{
AddNewMount(u+1,v,&rock_2[0],5,11,12,6,2.5f);
GenerateBlendMap(u+1,v,&rock_2[0]);
GenerateBlendMap2(u+1,v,&rock_2[0]);
GenerateNormalMap(u+1,v,&rock_2[0]);
}
break;
}
case 8:
{
if (u%2==0) {
AddNewMount(u-1,v,&rock_2[3],11,5,6,12,2.5f);
GenerateBlendMap(u-1,v,&rock_2[3]);
GenerateBlendMap2(u-1,v,&rock_2[3]);
GenerateNormalMap(u-1,v,&rock_2[3]);
}
else {
AddNewMount(u-1,v+1,&rock_2[3],11,5,6,12,2.5f);
GenerateBlendMap(u-1,v+1,&rock_2[3]);
GenerateBlendMap2(u-1,v+1,&rock_2[3]);
GenerateNormalMap(u-1,v+1,&rock_2[3]);
}

break;
}

case 9:
{
AddNewMount(u,v,&rock_3[0],10,10,10,2,2.5f);
GenerateBlendMap(u,v,&rock_3[0]);
GenerateBlendMap2(u,v,&rock_3[0]);
GenerateNormalMap(u,v,&rock_3[0]);
break;
}

case 10:
{
AddNewMount(u,v,&rock_3[1],10,10,10,2,2.5f);
GenerateBlendMap(u,v,&rock_3[2]);
GenerateBlendMap2(u,v,&rock_3[2]);
GenerateNormalMap(u,v,&rock_3[2]);
break;
}
case 11:
{
AddNewMount(u,v,&rock_3[2],10,10,2,10,2.5f);
GenerateBlendMap(u,v,&rock_3[1]);
GenerateBlendMap2(u,v,&rock_3[1]);
GenerateNormalMap(u,v,&rock_3[1]);
break;
}
case 12:
{
AddNewMount(u,v,&rock_3[3],10,10,2,10,2.5f);
GenerateBlendMap(u,v,&rock_3[3]);
GenerateBlendMap2(u,v,&rock_3[3]);
GenerateNormalMap(u,v,&rock_3[3]);
break;
}
///*
case 13:
{
AddNewMount(u,v,&rock_4[0],5,11,12,3,2.5f);
GenerateBlendMap(u,v,&rock_4[0]);
GenerateBlendMap2(u,v,&rock_4[0]);
GenerateNormalMap(u,v,&rock_4[0]);
break;
}
case 14:
{
AddNewMount(u,v,&rock_4[2],5,11,3,12,2.5f);
GenerateBlendMap(u,v,&rock_4[1]);
GenerateBlendMap2(u,v,&rock_4[1]);
GenerateNormalMap(u,v,&rock_4[1]);
break;
}
case 15:
{
AddNewMount(u,v,&rock_4[1],11,5,12,3,2.5f);
GenerateBlendMap(u,v,&rock_4[2]);
GenerateBlendMap2(u,v,&rock_4[2]);
GenerateNormalMap(u,v,&rock_4[2]);
break;
}
case 16:
{
AddNewMount(u,v,&rock_4[3],11,5,3,12,2.5f);
GenerateBlendMap(u,v,&rock_4[3]);
GenerateBlendMap2(u,v,&rock_4[3]);
GenerateNormalMap(u,v,&rock_4[3]);
break;
}
case 17:
{
AddNewMount(u,v,&rock_5[0],5,5,10,6,2.5f);
GenerateBlendMap(u,v,&rock_5[0]);
GenerateBlendMap2(u,v,&rock_5[0]);
GenerateNormalMap(u,v,&rock_5[0]);
break;
}

case 18:
{
AddNewMount(u,v,&rock_5[3],5,5,6,10,2.5f);
GenerateBlendMap(u,v,&rock_5[3]);
GenerateBlendMap2(u,v,&rock_5[3]);
GenerateNormalMap(u,v,&rock_5[3]);
break;
}
case 19:
{
AddNewMount(u,v,&rock_5[2],5,5,6,10,2.5f);
GenerateBlendMap(u,v,&rock_5[1]);
GenerateBlendMap2(u,v,&rock_5[1]);
GenerateNormalMap(u,v,&rock_5[1]);
break;
}
case 20:
{
AddNewMount(u,v,&rock_5[1],5,5,10,6,2.5f);
GenerateBlendMap(u,v,&rock_5[2]);
GenerateBlendMap2(u,v,&rock_5[2]);
GenerateNormalMap(u,v,&rock_5[2]);
break;
}
case 21:
{
AddNewMount(u,v,&rock_6[0],5,11,6,6,2.5f);
GenerateBlendMap(u,v,&rock_6[0]);
GenerateBlendMap2(u,v,&rock_6[0]);
GenerateNormalMap(u,v,&rock_6[0]);
break;
}
case 22:
{
AddNewMount(u,v,&rock_6[1],11,5,6,6,2.5f);
GenerateBlendMap(u,v,&rock_6[2]);
GenerateBlendMap2(u,v,&rock_6[2]);
GenerateNormalMap(u,v,&rock_6[2]);
break;
}
case 23:
{
AddNewMount(u,v,&rock_6[2],5,11,6,6,2.5f);
GenerateBlendMap(u,v,&rock_6[1]);
GenerateBlendMap2(u,v,&rock_6[1]);
GenerateNormalMap(u,v,&rock_6[1]);
break;
}
case 24:
{
AddNewMount(u,v,&rock_6[3],11,5,6,6,2.5f);
GenerateBlendMap(u,v,&rock_6[3]);
GenerateBlendMap2(u,v,&rock_6[3]);
GenerateNormalMap(u,v,&rock_6[3]);
break;
}
case 25:
{
AddNewMount(u,v,&rock_7[0],6,5,6,6,2.5f);
GenerateBlendMap(u,v,&rock_7[0]);
GenerateBlendMap2(u,v,&rock_7[0]);
GenerateNormalMap(u,v,&rock_7[0]);
break;
}
case 26:
{
AddNewMount(u,v,&rock_7[1],6,5,6,6,2.5f);
GenerateBlendMap(u,v,&rock_7[2]);
GenerateBlendMap2(u,v,&rock_7[2]);
GenerateNormalMap(u,v,&rock_7[2]);
break;
}
case 27:
{
AddNewMount(u,v,&rock_7[2],5,6,6,8,2.5f);
GenerateBlendMap(u,v,&rock_7[1]);
GenerateBlendMap2(u,v,&rock_7[1]);
GenerateNormalMap(u,v,&rock_7[1]);
break;
}
case 28:
{
AddNewMount(u,v,&rock_7[3],5,6,6,8,2.5f);
GenerateBlendMap(u,v,&rock_7[3]);
GenerateBlendMap2(u,v,&rock_7[3]);
GenerateNormalMap(u,v,&rock_7[3]);
break;
}
}

}

if (mainmass[u][v].hill==1) {
switch(RockArray[u][v]) {
case 0:
break;
case 1:
AddNewMount(u,v,&hill_1[0],0,0,0,0,2.5f);
break;
case 2:
AddNewMount(u,v,&hill_1[1],6,6,6,6,2.5f);
break;
case 3:
AddNewMount(u,v,&hill_1[2],6,6,6,6,2.5f);
break;
case 4:
AddNewMount(u,v,&hill_1[3],6,6,6,6,2.5f);
break;
case 5:
AddNewMount(u,v,&hill_2[1],11,5,12,6,2.5f);
break;
case 6:
AddNewMount(u,v,&hill_2[2],5,11,6,12,2.5f);
break;
case 7:
{
if (u%2==0) 
AddNewMount(u+1,v-1,&hill_2[0],5,11,12,6,2.5f);
else
AddNewMount(u+1,v,&hill_2[0],5,11,12,6,2.5f);
break;
}
case 8:
{
if (u%2==0) 
AddNewMount(u-1,v,&hill_2[3],11,5,6,12,2.5f);
else 
AddNewMount(u-1,v+1,&hill_2[3],11,5,6,12,2.5f);
break;
}
case 9:
AddNewMount(u,v,&hill_3[0],10,10,10,2,2.5f);
break;
case 10:
AddNewMount(u,v,&hill_3[1],10,10,10,2,2.5f);
break;
case 11:
AddNewMount(u,v,&hill_3[2],10,10,2,10,2.5f);
break;
case 12:
AddNewMount(u,v,&hill_3[3],10,10,2,10,2.5f);
break;
case 13:
AddNewMount(u,v,&hill_4[0],5,11,12,3,2.5f);
break;
case 14:
AddNewMount(u,v,&hill_4[2],5,11,3,12,2.5f);
break;
case 15:
AddNewMount(u,v,&hill_4[1],11,5,12,3,2.5f);
break;
case 16:
AddNewMount(u,v,&hill_4[3],11,5,3,12,2.5f);
break;
case 17:
AddNewMount(u,v,&hill_5[0],3,3,7,6,2.5f);
break;
case 18:
AddNewMount(u,v,&hill_5[3],3,3,6,7,2.5f);
break;
case 19:
AddNewMount(u,v,&hill_5[2],3,3,6,7,2.5f);
break;
case 20:
AddNewMount(u,v,&hill_5[1],3,3,7,6,2.5f);
break;
case 21:
AddNewMount(u,v,&hill_6[0],3,9,4,4,2.5f);
break;
case 22:
AddNewMount(u,v,&hill_6[1],9,3,4,4,2.5f);
break;
case 23:
AddNewMount(u,v,&hill_6[2],3,9,4,4,2.5f);
break;
case 24:
AddNewMount(u,v,&hill_6[3],9,3,4,4,2.5f);
break;
case 25:
AddNewMount(u,v,&hill_7[0],6,5,6,6,2.5f);
break;
case 26:
AddNewMount(u,v,&hill_7[1],6,5,6,6,2.5f);
break;
case 27:
AddNewMount(u,v,&hill_7[2],5,6,6,8,2.5f);
break;
case 28:
AddNewMount(u,v,&hill_7[3],5,6,6,8,2.5f);
break;
}
}

}
}



//
//AddNewMount(3,2,&abase_rock,0,0,0,0,1);
}



void AddNewMount(int wey, int hey, obj_type *obj, int aX, int bX, int yX, int zX, float size)
{
int mirror_jump;
if (obj==&rock_2[2] || obj==&rock_2[3] || obj==&rock_1[2] || obj==&rock_1[3] || obj==&rock_3[2] || obj==&rock_3[3] || obj==&rock_4[2] || obj==&rock_4[3] || obj==&rock_5[2] || obj==&rock_5[3] || obj==&rock_6[2]
|| obj==&rock_6[3] || obj==&rock_7[2] || obj==&rock_7[3] || obj==&hill_1[2] || obj==&hill_1[3] || obj==&hill_2[2] || obj==&hill_2[3] || obj==&hill_3[2] || obj==&hill_3[3] || obj==&hill_4[2] || obj==&hill_4[3]
 || obj==&hill_5[2] || obj==&hill_5[3] || obj==&hill_6[2] || obj==&hill_6[3] || obj==&hill_7[2] || obj==&hill_7[3]) {
mirror_jump=8*3;
}
else
mirror_jump=0;
float jump;
if (wey%2==0) jump=0; else jump=24;
wey=(wey-2)*6;
hey=(hey-2)*7;
int polch=32*8*aX;
for (int j = aX; j < 32-bX; j++) {
polch=polch+8*yX;
for (int i = yX*8; i < (32-zX)*8; i++) {


if ((i+j*(32-zX)*8+hey*8+jump+GridSetHei*8*wey+(GridSetHei-32+zX)*8*j+mirror_jump>0) && (i+j*(32-zX)*8+hey*8+jump+GridSetHei*8*wey+(GridSetHei-32+zX)*8*j<GridSetHei*GridSetWei*8+mirror_jump))
{
if (obj->vertex[GetTriangleDotO(obj, 0,polch)].z*size<MapVertexArray[GetTriangleDot(0,i+j*(32-zX)*8+hey*8+jump+GridSetHei*8*wey+(GridSetHei-32+zX)*8*j+mirror_jump)].z)
{
MapVertexArray[GetTriangleDot(0,i+j*(32-zX)*8+hey*8+jump+GridSetHei*8*wey+(GridSetHei-32+zX)*8*j+mirror_jump)].z=obj->vertex[GetTriangleDotO(obj, 0,polch)].z*size;
MapNormalArray[GetTriangleDot(0,i+j*(32-zX)*8+hey*8+jump+GridSetHei*8*wey+(GridSetHei-32+zX)*8*j+mirror_jump)]=obj->normals[GetTriangleDotO(obj,0,polch)];

}
if (obj->vertex[GetTriangleDotO(obj, 1,polch)].z*size<MapVertexArray[GetTriangleDot(1,i+j*(32-zX)*8+hey*8+jump+GridSetHei*8*wey+(GridSetHei-32+zX)*8*j+mirror_jump)].z)
{
MapVertexArray[GetTriangleDot(1,i+j*(32-zX)*8+hey*8+jump+GridSetHei*8*wey+(GridSetHei-32+zX)*8*j+mirror_jump)].z=obj->vertex[GetTriangleDotO(obj,1,polch)].z*size;
MapNormalArray[GetTriangleDot(1,i+j*(32-zX)*8+hey*8+jump+GridSetHei*8*wey+(GridSetHei-32+zX)*8*j+mirror_jump)]=obj->normals[GetTriangleDotO(obj,1,polch)];
}
if (obj->vertex[GetTriangleDotO(obj, 2,polch)].z*size<MapVertexArray[GetTriangleDot(2,i+j*(32-zX)*8+hey*8+jump+GridSetHei*8*wey+(GridSetHei-32+zX)*8*j+mirror_jump)].z)
{
MapVertexArray[GetTriangleDot(2,i+j*(32-zX)*8+hey*8+jump+GridSetHei*8*wey+(GridSetHei-32+zX)*8*j+mirror_jump)].z=obj->vertex[GetTriangleDotO(obj,2,polch)].z*size;
MapNormalArray[GetTriangleDot(2,i+j*(32-zX)*8+hey*8+jump+GridSetHei*8*wey+(GridSetHei-32+zX)*8*j+mirror_jump)]=obj->normals[GetTriangleDotO(obj,2,polch)];
}
}
polch++;
}
polch=polch+8*zX;
}
}


unsigned int GetTriangleDot(int side, unsigned int polygon)
{
int nullside=0;
float a,b,c,d,e,f;
a = MapVertexArray[MapFaceArray[polygon].a].x;
b = MapVertexArray[MapFaceArray[polygon].a].y;
c = MapVertexArray[MapFaceArray[polygon].b].x;
d = MapVertexArray[MapFaceArray[polygon].b].y;
e = MapVertexArray[MapFaceArray[polygon].c].x;
f = MapVertexArray[MapFaceArray[polygon].c].y;

if (b==d)
nullside=2;
if (b==f)
nullside=1;
if (d==f)
nullside=0;


if (side==0)
{
if (nullside==0)
return MapFaceArray[polygon].a;
if (nullside==1)
return MapFaceArray[polygon].b;
if (nullside==2)
return MapFaceArray[polygon].c;
}



if (side==1) {
if (nullside==2) {
if (a<c)
return MapFaceArray[polygon].a;
if (c<a)
return MapFaceArray[polygon].b;
}
if (nullside==0) {
if (e<c)
return MapFaceArray[polygon].c;
if (c<e)
return MapFaceArray[polygon].b;
}
if (nullside==1) {
if (a<e)
return MapFaceArray[polygon].a;
if (e<a)
return MapFaceArray[polygon].c;
}
}

if (side==2) {
if (nullside==2) {
if (a>c)
return MapFaceArray[polygon].a;
if (c>a)
return MapFaceArray[polygon].b;
}
if (nullside==0) {
if (e>c)
return MapFaceArray[polygon].c;
if (c>e)
return MapFaceArray[polygon].b;
}
if (nullside==1) {
if (a>e)
return MapFaceArray[polygon].a;
if (e>a)
return MapFaceArray[polygon].c;
}
}
return MapFaceArray[polygon].a; //�� ������������, �� ������� ���������� VC ��������
}
//---------------------------------------------
unsigned int GetTriangleDotO(obj_type *obj, int side, unsigned int polygon)

{
int nullside=0;
float a,b,c,d,e,f;
a = obj->vertex[obj->polygon[polygon].a].x;
b = obj->vertex[obj->polygon[polygon].a].y;
c = obj->vertex[obj->polygon[polygon].b].x;
d = obj->vertex[obj->polygon[polygon].b].y;
e = obj->vertex[obj->polygon[polygon].c].x;
f = obj->vertex[obj->polygon[polygon].c].y;

if (b==d)
nullside=2;
if (b==f)
nullside=1;
if (d==f)
nullside=0;

if (side==0)
{
if (nullside==0)
return obj->polygon[polygon].a;
if (nullside==1)
return obj->polygon[polygon].b;
if (nullside==2)
return obj->polygon[polygon].c;
}



if (side==1) {
if (nullside==2) {
if (a<c)
return obj->polygon[polygon].a;
if (c<a)
return obj->polygon[polygon].b;
}
if (nullside==0) {
if (e<c)
return obj->polygon[polygon].c;
if (c<e)
return obj->polygon[polygon].b;
}
if (nullside==1) {
if (a<e)
return obj->polygon[polygon].a;
if (e<a)
return obj->polygon[polygon].c;
}
}

if (side==2) {
if (nullside==2) {
if (a>c)
return obj->polygon[polygon].a;
if (c>a)
return obj->polygon[polygon].b;
}
if (nullside==0) {
if (e>c)
return obj->polygon[polygon].c;
if (c>e)
return obj->polygon[polygon].b;
}
if (nullside==1) {
if (a>e)
return obj->polygon[polygon].a;
if (e>a)
return obj->polygon[polygon].c;
}
}
return obj->polygon[polygon].a; //�� ������������, �� ������� ���������� VC ��������
}


void LoadMap()
	{
/*

int numH, numV, cbuffer;
char csymbol; //������ ��� ����������
char str[255];
numV=0;
ifstream fMp("maps/Two_sidesREADY4.txt"); //��������� ����

	fMp.get(csymbol); //������ ������
	int type=atoi(&csymbol); //��� ��� �����
fMp.get();// ������� ������
//������ �����
	fMp.get(csymbol); //������ �����
	cbuffer=atoi(&csymbol); //� ������ ee
	fMp.get(csymbol); //������ �����
GridWei=cbuffer*10+atoi(&csymbol); //����������
//������ �����
	fMp.get(csymbol);
	cbuffer=atoi(&csymbol);
	fMp.get(csymbol);
GridHei=cbuffer*10+atoi(&csymbol);
fMp.get();// ������� ������

for (int j = 0; j < GridHei; j++)
for (int i = 0; i < GridWei; i++)
//mainmass[i][j].lands=&lplain; //�������� �����
//��������� ������ ���������
while (numV<GridHei) {
	numH=0;
while (numH<GridWei) {
	fMp.get(csymbol);
	cbuffer=atoi(&csymbol);
if (cbuffer==0) {
	mainmass[numH][numV].plain=1;
	mainmass[numH][numV].passab=0;
}
if (cbuffer==1) {
	mainmass[numH][numV].grass=1;
	mainmass[numH][numV].passab=0;
	}
numH++;
}
if (numV<GridHei && numH==GridWei)
	fMp.get();
numV++;
}
numV=0;

while (numV<GridHei) {
	numH=0;
while (numH<GridWei) {
	fMp.get(csymbol);
	cbuffer=atoi(&csymbol);
if (cbuffer==1) {
	mainmass[numH][numV].hill=1;
	mainmass[numH][numV].passab=1;
}
if (cbuffer==2) {
	mainmass[numH][numV].rock=1;
	mainmass[numH][numV].passab=3;
	}
numH++;
}
if (numV<GridHei && numH==GridWei)
	fMp.get();
numV++;
}
numV=0;

while (numV<GridHei) {
	numH=0;
while (numH<GridWei) {
	fMp.get(csymbol);
	cbuffer=atoi(&csymbol);
if (cbuffer==1) {
	mainmass[numH][numV].forest=1;
	mainmass[numH][numV].passab++;
}
numH++;
}
if (numV<GridHei && numH==GridWei)
	fMp.get();
numV++;
}
numV=0;
*/
/*
while (numV<GridHei) {
	numH=0;
while (numH<GridWei) {
	fMp.get(csymbol);
	cbuffer=atoi(&csymbol);
	fMp.get(csymbol);
	RockArray[numH][numV]=cbuffer*10+atoi(&csymbol);
	numH++;
}
if (numV<GridHei && numH==GridWei)
	fMp.get();
numV++;
}
numV=0;

//��������� ������ ��������

//while (numV<GridHei) {
//for (int i=0;i<10;i++)
//{
fMp.getline(str, sizeof(str));
RockArray[0][0] = atoi(str);
fMp.getline(str, sizeof(str));
RockArray[0][1] = atoi(str);
fMp.getline(str, sizeof(str));
RockArray[0][2] = atoi(str);
//}
//numV++;
//}

*/


//fMp.close();
//GridWei=50;
//GridHei=50;
}


void MirrorVert(obj_type *out, obj_type *in)
{

int polch=0;
for (int i = 0; i < 32; i++) {
for (int j = 255; j > -1; j--) {
out->vertex[GetTriangleDotO(out,0,polch)].z=in->vertex[GetTriangleDotO(out,0,j+256*i)].z;
out->vertex[GetTriangleDotO(out,1,polch)].z=in->vertex[GetTriangleDotO(out,1,j+256*i)].z;
out->vertex[GetTriangleDotO(out,2,polch)].z=in->vertex[GetTriangleDotO(out,2,j+256*i)].z;
polch++;
}
}

ComputeNormals(out);
}

void MirrorHorz(obj_type *out, obj_type *in)
{
for (int i = 0; i < 32; i++)
{
for (int j = 0; j < 32; j++) {
out->vertex[GetTriangleDotO(out,0,j*8+0+256*i)].z=in->vertex[GetTriangleDotO(out,0,3+j*8+256*(31-i))].z;
out->vertex[GetTriangleDotO(out,1,j*8+0+256*i)].z=in->vertex[GetTriangleDotO(out,2,3+j*8+256*(31-i))].z;
}
out->vertex[GetTriangleDotO(out,0,31*8+7+256*i)].z=in->vertex[GetTriangleDotO(out,0,4+31*8+256*(31-i))].z;
for (int j = 0; j < 32; j++) {
out->vertex[GetTriangleDotO(out,2,j*8+1+256*i)].z=in->vertex[GetTriangleDotO(out,2,1+j*8+256*(31-i))].z;
out->vertex[GetTriangleDotO(out,2,j*8+0+256*i)].z=in->vertex[GetTriangleDotO(out,2,0+j*8+256*(31-i))].z;
}
out->vertex[GetTriangleDotO(out,1,31*8+5+256*i)].z=in->vertex[GetTriangleDotO(out,1,5+31*8+256*(31-i))].z;
}
//
ComputeNormals(out);
}


void CreateTextures()
{
	glGenTextures(1, &bignorm.texID);
	glBindTexture(GL_TEXTURE_2D, bignorm.texID);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_NEAREST);
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
	glTexImage2D(GL_TEXTURE_2D, 0, bignorm.type, bignorm.width,  bignorm.height, 0,
	bignorm.type, GL_UNSIGNED_BYTE,  bignorm.imageData);
	free(bignorm.imageData);

	glGenTextures(1, &blend.texID);
	glBindTexture(GL_TEXTURE_2D, blend.texID);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_DECAL);
	glTexImage2D(GL_TEXTURE_2D, 0, blend.type, blend.width,  blend.height, 0,
	blend.type, GL_UNSIGNED_BYTE,  blend.imageData);
	free(blend.imageData);

	glGenTextures(1, &blend2.texID);
	glBindTexture(GL_TEXTURE_2D, blend2.texID);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_DECAL);
	glTexImage2D(GL_TEXTURE_2D, 0, blend2.type, blend2.width,  blend2.height, 0,
	blend2.type, GL_UNSIGNED_BYTE,  blend2.imageData);
	free(blend2.imageData);

}

void GenerateBlendMap(int x, int y, obj_type *obj)
{
long int polch=0;
int channelch=0, jump, location=0, dopint, sizech;
float bonus=x/4;
if (x%2==0) {
jump=blend.width*5;
}
else
jump=blend.width*36;
if (obj->texture.width==256)
sizech=4;
else
sizech=1;
if (obj==&rock_3[0] || obj==&rock_3[2] || obj==&rock_4[0] || obj==&rock_4[2] || obj==&rock_5[0] || obj==&rock_5[2] || obj==&rock_6[0] || obj==&rock_6[2] || obj==&rock_6[3] || obj==&rock_6[1] || obj==&rock_7[0] || obj==&rock_7[2] || obj==&rock_7[3] || obj==&rock_7[1]) {
location+=36*blend.width*4;
}
location-=20*4*sizech+34*blend.width*4*sizech;
location+=(blend.width*72*y+62*x+jump-floor(bonus))*4;

for (unsigned int i = 0; i < obj->texture.width; i++) {
for (unsigned int j = 0; j < obj->texture.height*4; j++) {
dopint=i*blend.width*4+j+location;
if (dopint>=0) {
if (channelch==0){
if (obj->texture.imageData[polch]>blend.imageData[i*blend.width*4+j+location])
blend.imageData[i*blend.width*4+j+location]=obj->texture.imageData[polch];
}
if (channelch==1){
if (obj->texture.imageData[polch]<blend.imageData[i*blend.width*4+j+location])
blend.imageData[i*blend.width*4+j+location]=obj->texture.imageData[polch];
}
if (channelch==2){
if (obj->texture.imageData[polch]<blend.imageData[i*blend.width*4+j+location])
blend.imageData[i*blend.width*4+j+location]=obj->texture.imageData[polch];
}
if (channelch==3){
if (obj->texture.imageData[polch]>blend.imageData[i*blend.width*4+j+location])
blend.imageData[i*blend.width*4+j+location]=obj->texture.imageData[polch];
}
}
polch++;
channelch++;
if (channelch>3)
channelch=0;
}
}
}

void GenerateNormalMap(int x, int y, obj_type *obj)
{
long int polch=0;
int channelch=0, dopint, jump, location=0, sizech;
float bonus=x/4;
if (x%2==0) {
jump=bignorm.width*5*2;
}
else
jump=bignorm.width*36*2;
if (obj->texture.width==256)
sizech=4;
else
sizech=1;
if (obj==&rock_3[0] || obj==&rock_3[2] || obj==&rock_4[0] || obj==&rock_4[2] || obj==&rock_5[0] || obj==&rock_5[2] || obj==&rock_6[0] || obj==&rock_6[2] || obj==&rock_6[3] || obj==&rock_6[1] || obj==&rock_7[0] || obj==&rock_7[2] || obj==&rock_7[3] || obj==&rock_7[1]) {
location+=36*bignorm.width*3*2;
}
location-=20*3*2*sizech+34*bignorm.width*3*2*sizech;
location+=(bignorm.width*144*y+124*x+jump-floor(bonus))*3;
for (unsigned int i = 0; i < obj->texture_norm.width; i++) {
for (unsigned int j = 0; j < obj->texture_norm.height*3; j++) {
dopint=i*bignorm.width*3+j+location;
if (dopint>0)
{
if (channelch<2) {
if (bignorm.imageData[i*bignorm.width*3+j+location]==127)
bignorm.imageData[i*bignorm.width*3+j+location]=obj->texture_norm.imageData[polch];
else
{
if (abs(obj->texture_norm.imageData[polch]-127)>abs(bignorm.imageData[i*bignorm.width*3+j+location]-127))
bignorm.imageData[i*bignorm.width*3+j+location]=obj->texture_norm.imageData[polch];
}
}
else
{
if (bignorm.imageData[i*bignorm.width*3+j+location]==255)
bignorm.imageData[i*bignorm.width*3+j+location]=obj->texture_norm.imageData[polch];
else
{
if (obj->texture_norm.imageData[polch]	<bignorm.imageData[i*bignorm.width*3+j+location])
bignorm.imageData[i*bignorm.width*3+j+location]=obj->texture_norm.imageData[polch];
}
}

}
polch++;
channelch++;
if (channelch>2)
channelch=0;
}
}
}

void GenerateBlendMap2(int x, int y, obj_type *obj)
{
long int polch=0;
int channelch=0, jump, location=0, dopint, sizech;
float bonus=x/4;
if (x%2==0) {
jump=blend2.width*5;
}
else
jump=blend2.width*9*4;
if (obj->texture2.width==256)
sizech=4;
else
sizech=1;
if (obj==&rock_3[0] || obj==&rock_3[2] || obj==&rock_4[0] || obj==&rock_4[2] || obj==&rock_5[0] || obj==&rock_5[2] || obj==&rock_6[0] || obj==&rock_6[2] || obj==&rock_6[3] || obj==&rock_6[1] || obj==&rock_7[0] || obj==&rock_7[2] || obj==&rock_7[3] || obj==&rock_7[1]) {
location+=36*blend2.width*4;
}
location-=20*4*sizech+34*blend2.width*4*sizech;
location+=(blend2.width*72*y+62*x+jump-floor(bonus))*4;

for (unsigned int i = 0; i < obj->texture2.width; i++) {
for (unsigned int j = 0; j < obj->texture2.height*4; j++) {
dopint=i*blend2.width*4+j+location;
if (dopint>=0) {
if (channelch==0){
if (obj->texture2.imageData[polch]<blend2.imageData[i*blend2.width*4+j+location])
blend2.imageData[i*blend2.width*4+j+location]=obj->texture2.imageData[polch];
}
if (channelch==1){
if (obj->texture2.imageData[polch]>blend2.imageData[i*blend2.width*4+j+location])
blend2.imageData[i*blend2.width*4+j+location]=obj->texture2.imageData[polch];
}
if (channelch==2){
if (obj->texture2.imageData[polch]>blend2.imageData[i*blend2.width*4+j+location])
blend2.imageData[i*blend2.width*4+j+location]=obj->texture2.imageData[polch];
}
if (channelch==3){
if (obj->texture2.imageData[polch]>blend2.imageData[i*blend2.width*4+j+location])
blend2.imageData[i*blend2.width*4+j+location]=obj->texture2.imageData[polch];
}
}
polch++;
channelch++;
if (channelch>3)
channelch=0;
}
}
}

void GenerateGrassArray()
{

checkmas ch_n;
for (int u = 0; u < GridWei; u++) {
for (int v = 0; v < GridHei; v++) {
if (mainmass[u][v].grass==1) {
ch_n=CheckHexesGrass(u,v);

if (ch_n.v[0]==1 && ch_n.v[1]==1 && ch_n.v[2]==1
&& ch_n.v[3]==1 && ch_n.v[4]==1 && ch_n.v[5]==1)
GenerateBlendGrassMap(u,v,0);
if (ch_n.v[0]==0 && ch_n.v[1]==0 && ch_n.v[2]==0
 && ch_n.v[3]==0 && ch_n.v[4]==0 && ch_n.v[5]==0)
GenerateBlendGrassMap(u,v,7);
//2
if (ch_n.v[0]==0 && ch_n.v[1]==1 && ch_n.v[2]==1
 && ch_n.v[3]==0)
GenerateBlendGrassMap(u,v,6);
if (ch_n.v[2]==0 && ch_n.v[3]==1 && ch_n.v[4]==0
 && ch_n.v[5]==1)
GenerateBlendGrassMap(u,v,2);
if (ch_n.v[0]==1 && ch_n.v[1]==0 && ch_n.v[4]==1
 && ch_n.v[5]==0)
GenerateBlendGrassMap(u,v,3);
if (ch_n.v[0]==0 && ch_n.v[3]==0 && ch_n.v[4]==1
 && ch_n.v[5]==1)
GenerateBlendGrassMap(u,v,1);
if (ch_n.v[1]==0 && ch_n.v[2]==1 && ch_n.v[3]==1
 && ch_n.v[5]==0)
GenerateBlendGrassMap(u,v,5);
if (ch_n.v[0]==1 && ch_n.v[1]==1 && ch_n.v[2]==0
 && ch_n.v[4]==0)
GenerateBlendGrassMap(u,v,4);
//3
if (ch_n.v[1]==0 && ch_n.v[2]==1 && ch_n.v[3]==1
 && ch_n.v[4]==0 && ch_n.v[5]==1)
GenerateBlendGrassMap(u,v,13);
if (ch_n.v[0]==0 && ch_n.v[2]==0 && ch_n.v[3]==1
 && ch_n.v[4]==1 && ch_n.v[5]==1)
GenerateBlendGrassMap(u,v,12);
if (ch_n.v[0]==1 && ch_n.v[1]==0 && ch_n.v[3]==0
 && ch_n.v[4]==1 && ch_n.v[5]==1)
GenerateBlendGrassMap(u,v,11);
if (ch_n.v[0]==1 && ch_n.v[1]==1 && ch_n.v[2]==0
 && ch_n.v[4]==1 && ch_n.v[5]==0)
GenerateBlendGrassMap(u,v,10);
if (ch_n.v[0]==1 && ch_n.v[1]==1 && ch_n.v[2]==1
 && ch_n.v[3]==0 && ch_n.v[4]==0)
GenerateBlendGrassMap(u,v,9);
if (ch_n.v[0]==0 && ch_n.v[1]==1 && ch_n.v[2]==1
 && ch_n.v[3]==1 && ch_n.v[5]==0)
GenerateBlendGrassMap(u,v,8);
//5
if (ch_n.v[0]==1 && ch_n.v[1]==1 && ch_n.v[2]==1
 && ch_n.v[3]==0 && ch_n.v[4]==1 && ch_n.v[5]==1)
GenerateBlendGrassMap(u,v,21);
if (ch_n.v[0]==0 && ch_n.v[1]==1 && ch_n.v[2]==1
 && ch_n.v[3]==1 && ch_n.v[4]==1 && ch_n.v[5]==1)
GenerateBlendGrassMap(u,v,24);
if (ch_n.v[0]==1 && ch_n.v[1]==1 && ch_n.v[2]==1
 && ch_n.v[3]==1 && ch_n.v[4]==0 && ch_n.v[5]==1)
GenerateBlendGrassMap(u,v,25);
if (ch_n.v[0]==1 && ch_n.v[1]==0 && ch_n.v[2]==1
 && ch_n.v[3]==1 && ch_n.v[4]==1 && ch_n.v[5]==1)
GenerateBlendGrassMap(u,v,23);
if (ch_n.v[0]==1 && ch_n.v[1]==1 && ch_n.v[2]==0
 && ch_n.v[3]==1 && ch_n.v[4]==1 && ch_n.v[5]==1)
GenerateBlendGrassMap(u,v,22);
if (ch_n.v[0]==1 && ch_n.v[1]==1 && ch_n.v[2]==1
 && ch_n.v[3]==1 && ch_n.v[4]==1 && ch_n.v[5]==0)
GenerateBlendGrassMap(u,v,20);
//4
if (ch_n.v[0]==0 && ch_n.v[1]==1 && ch_n.v[2]==1
 && ch_n.v[3]==1 && ch_n.v[4]==0 && ch_n.v[5]==1)
GenerateBlendGrassMap(u,v,19);
if (ch_n.v[0]==0 && ch_n.v[1]==0 && ch_n.v[2]==1
 && ch_n.v[3]==1 && ch_n.v[4]==1 && ch_n.v[5]==1)
GenerateBlendGrassMap(u,v,18);
if (ch_n.v[0]==1 && ch_n.v[1]==1 && ch_n.v[2]==1
 && ch_n.v[3]==0 && ch_n.v[4]==1 && ch_n.v[5]==0)
GenerateBlendGrassMap(u,v,15);
if (ch_n.v[0]==1 && ch_n.v[1]==1 && ch_n.v[2]==0
 && ch_n.v[3]==0 && ch_n.v[4]==1 && ch_n.v[5]==1)
GenerateBlendGrassMap(u,v,16);
if (ch_n.v[0]==1 && ch_n.v[1]==1 && ch_n.v[2]==1
 && ch_n.v[3]==1 && ch_n.v[4]==0 && ch_n.v[5]==0)
GenerateBlendGrassMap(u,v,14);
if (ch_n.v[0]==1 && ch_n.v[1]==0 && ch_n.v[2]==0
 && ch_n.v[3]==1 && ch_n.v[4]==1 && ch_n.v[5]==1)
GenerateBlendGrassMap(u,v,17);
//1
if (ch_n.v[0]==1 && ch_n.v[1]==0 && ch_n.v[2]==0
 && ch_n.v[3]==0 && ch_n.v[4]==0 && ch_n.v[5]==0)
GenerateBlendGrassMap(u,v,26);
if (ch_n.v[0]==0 && ch_n.v[1]==0 && ch_n.v[2]==0
 && ch_n.v[3]==1 && ch_n.v[4]==0 && ch_n.v[5]==0)
GenerateBlendGrassMap(u,v,29);
if (ch_n.v[0]==0 && ch_n.v[1]==0 && ch_n.v[2]==0
 && ch_n.v[3]==0 && ch_n.v[4]==1 && ch_n.v[5]==0)
GenerateBlendGrassMap(u,v,27);
if (ch_n.v[0]==0 && ch_n.v[1]==0 && ch_n.v[2]==0
 && ch_n.v[3]==0 && ch_n.v[4]==0 && ch_n.v[5]==1)
GenerateBlendGrassMap(u,v,28);
if (ch_n.v[0]==0 && ch_n.v[1]==0 && ch_n.v[2]==1
 && ch_n.v[3]==0 && ch_n.v[4]==0 && ch_n.v[5]==0)
GenerateBlendGrassMap(u,v,30);
if (ch_n.v[0]==0 && ch_n.v[1]==1 && ch_n.v[2]==0
 && ch_n.v[3]==0 && ch_n.v[4]==0 && ch_n.v[5]==0)
GenerateBlendGrassMap(u,v,31);
//1-1
if (ch_n.v[0]==1 && ch_n.v[1]==0 && ch_n.v[2]==0
 && ch_n.v[3]==1 && ch_n.v[4]==0 && ch_n.v[5]==0)
GenerateBlendGrassMap(u,v,32);
if (ch_n.v[0]==0 && ch_n.v[1]==1 && ch_n.v[2]==0
 && ch_n.v[3]==0 && ch_n.v[4]==0 && ch_n.v[5]==1)
GenerateBlendGrassMap(u,v,34);
if (ch_n.v[0]==0 && ch_n.v[1]==0 && ch_n.v[2]==1
 && ch_n.v[3]==0 && ch_n.v[4]==1 && ch_n.v[5]==0)
GenerateBlendGrassMap(u,v,33);
//strings
if (ch_n.v[0]==0 && ch_n.v[1]==1 && ch_n.v[2]==0
 && ch_n.v[3]==1 && ch_n.v[4]==1 && ch_n.v[5]==0)
GenerateBlendGrassMap(u,v,36);
if (ch_n.v[0]==1 && ch_n.v[1]==0 && ch_n.v[2]==1
 && ch_n.v[3]==0 && ch_n.v[4]==0 && ch_n.v[5]==1)
GenerateBlendGrassMap(u,v,35);
//duga
if (ch_n.v[0]==0 && ch_n.v[1]==1 && ch_n.v[2]==0
 && ch_n.v[3]==1 && ch_n.v[4]==0 && ch_n.v[5]==0)
GenerateBlendGrassMap(u,v,38);
if (ch_n.v[0]==1 && ch_n.v[1]==0 && ch_n.v[2]==1
 && ch_n.v[3]==0 && ch_n.v[4]==0 && ch_n.v[5]==0)
GenerateBlendGrassMap(u,v,37);
if (ch_n.v[0]==0 && ch_n.v[1]==1 && ch_n.v[2]==0
 && ch_n.v[3]==0 && ch_n.v[4]==1 && ch_n.v[5]==0)
GenerateBlendGrassMap(u,v,42);
if (ch_n.v[0]==0 && ch_n.v[1]==0 && ch_n.v[2]==1
 && ch_n.v[3]==0 && ch_n.v[4]==0 && ch_n.v[5]==1)
GenerateBlendGrassMap(u,v,39);
if (ch_n.v[0]==0 && ch_n.v[1]==0 && ch_n.v[2]==0
 && ch_n.v[3]==1 && ch_n.v[4]==1 && ch_n.v[5]==0)
GenerateBlendGrassMap(u,v,40);
if (ch_n.v[0]==1 && ch_n.v[1]==0 && ch_n.v[2]==0
 && ch_n.v[3]==0 && ch_n.v[4]==0 && ch_n.v[5]==1)
GenerateBlendGrassMap(u,v,41);
//off
if (ch_n.v[0]==1 && ch_n.v[1]==0 && ch_n.v[2]==1
 && ch_n.v[3]==1 && ch_n.v[4]==0 && ch_n.v[5]==1)
GenerateBlendGrassMap(u,v,32);
if (ch_n.v[0]==0 && ch_n.v[1]==1 && ch_n.v[2]==0
 && ch_n.v[3]==1 && ch_n.v[4]==1 && ch_n.v[5]==1)
GenerateBlendGrassMap(u,v,34);
if (ch_n.v[0]==1 && ch_n.v[1]==0 && ch_n.v[2]==1
 && ch_n.v[3]==0 && ch_n.v[4]==1 && ch_n.v[5]==1)
GenerateBlendGrassMap(u,v,33);
if (ch_n.v[0]==1 && ch_n.v[1]==1 && ch_n.v[2]==0
 && ch_n.v[3]==1 && ch_n.v[4]==1 && ch_n.v[5]==0)
GenerateBlendGrassMap(u,v,32);
if (ch_n.v[0]==1 && ch_n.v[1]==1 && ch_n.v[2]==1
 && ch_n.v[3]==0 && ch_n.v[4]==0 && ch_n.v[5]==1)
GenerateBlendGrassMap(u,v,34);
if (ch_n.v[0]==0 && ch_n.v[1]==0 && ch_n.v[2]==1
 && ch_n.v[3]==1 && ch_n.v[4]==1 && ch_n.v[5]==0)
GenerateBlendGrassMap(u,v,33);
//off2
//1
if (ch_n.v[0]==0 && ch_n.v[1]==1 && ch_n.v[2]==0
 && ch_n.v[3]==1 && ch_n.v[4]==0 && ch_n.v[5]==1)
{
if (rand()%1==0) GenerateBlendGrassMap(u,v,34);
else GenerateBlendGrassMap(u,v,38);
}
if (ch_n.v[0]==1 && ch_n.v[1]==0 && ch_n.v[2]==0
 && ch_n.v[3]==1 && ch_n.v[4]==0 && ch_n.v[5]==1)
{
if (rand()%1==0) GenerateBlendGrassMap(u,v,32);
else GenerateBlendGrassMap(u,v,37);
}
//2
if (ch_n.v[0]==0 && ch_n.v[1]==1 && ch_n.v[2]==0
 && ch_n.v[3]==0 && ch_n.v[4]==1 && ch_n.v[5]==1)
{
if (rand()%1==0) GenerateBlendGrassMap(u,v,34);
else GenerateBlendGrassMap(u,v,42);
}
if (ch_n.v[0]==0 && ch_n.v[1]==0 && ch_n.v[2]==1
 && ch_n.v[3]==0 && ch_n.v[4]==1 && ch_n.v[5]==1)
{
if (rand()%1==0) GenerateBlendGrassMap(u,v,33);
else GenerateBlendGrassMap(u,v,39);
}
//3
if (ch_n.v[0]==1 && ch_n.v[1]==0 && ch_n.v[2]==1
 && ch_n.v[3]==0 && ch_n.v[4]==1 && ch_n.v[5]==0)
{
if (rand()%1==0) GenerateBlendGrassMap(u,v,33);
else GenerateBlendGrassMap(u,v,37);
}
if (ch_n.v[0]==1 && ch_n.v[1]==0 && ch_n.v[2]==0
 && ch_n.v[3]==1 && ch_n.v[4]==1 && ch_n.v[5]==0)
{
if (rand()%1==0) GenerateBlendGrassMap(u,v,32);
else GenerateBlendGrassMap(u,v,40);
}
//4
if (ch_n.v[0]==1 && ch_n.v[1]==1 && ch_n.v[2]==0
 && ch_n.v[3]==1 && ch_n.v[4]==0 && ch_n.v[5]==0)
{
if (rand()%1==0) GenerateBlendGrassMap(u,v,32);
else GenerateBlendGrassMap(u,v,38);
}
if (ch_n.v[0]==1 && ch_n.v[1]==1 && ch_n.v[2]==0
 && ch_n.v[3]==0 && ch_n.v[4]==0 && ch_n.v[5]==1)
{
if (rand()%1==0) GenerateBlendGrassMap(u,v,34);
else GenerateBlendGrassMap(u,v,41);
}
//5
if (ch_n.v[0]==0 && ch_n.v[1]==1 && ch_n.v[2]==1
 && ch_n.v[3]==0 && ch_n.v[4]==0 && ch_n.v[5]==1)
{
if (rand()%1==0) GenerateBlendGrassMap(u,v,34);
else GenerateBlendGrassMap(u,v,39);
}
if (ch_n.v[0]==0 && ch_n.v[1]==1 && ch_n.v[2]==1
 && ch_n.v[3]==0 && ch_n.v[4]==1 && ch_n.v[5]==0)
{
if (rand()%1==0) GenerateBlendGrassMap(u,v,33);
else GenerateBlendGrassMap(u,v,42);
}
//6
if (ch_n.v[0]==1 && ch_n.v[1]==0 && ch_n.v[2]==1
 && ch_n.v[3]==1 && ch_n.v[4]==0 && ch_n.v[5]==0)
{
if (rand()%1==0) GenerateBlendGrassMap(u,v,32);
else GenerateBlendGrassMap(u,v,37);
}
if (ch_n.v[0]==0 && ch_n.v[1]==0 && ch_n.v[2]==1
 && ch_n.v[3]==1 && ch_n.v[4]==1 && ch_n.v[5]==0)
{
if (rand()%1==0) GenerateBlendGrassMap(u,v,33);
else GenerateBlendGrassMap(u,v,40);
}

}

}
}
}


void GenerateBlendGrassMap(int x, int y, int number)
{
long int polch=0;
int channelch=0, jump, location=0, dopint;
float bonus=y/5;
if (x%2==0) {
jump=0;
}
else
jump=blend.width*9*4;
location-=20*4+34*blend.width*4;
location+=(blend.width*72*y+62*x+jump-floor(bonus))*4;

for (unsigned int i = 0; i < grass_mask[number].width; i++) {
for (unsigned int j = 0; j < grass_mask[number].height*4; j++) {
dopint=i*blend.width*4+j+location;
if (dopint>=0) {
if (channelch==1){
if (grass_mask[number].imageData[polch]>blend.imageData[i*blend.width*4+j+location])
blend.imageData[i*blend.width*4+j+location]=grass_mask[number].imageData[polch];
}
if (channelch==2){
if (grass_mask[number].imageData[polch]<blend.imageData[i*blend.width*4+j+location])
blend.imageData[i*blend.width*4+j+location]=grass_mask[number].imageData[polch];
}
}
polch++;
channelch++;
if (channelch>3)
channelch=0;

}
}
}

checkmas CheckHexesGrass(int u, int v)
{
checkmas ch_n;
for (int i=0; i < 6; i++)
ch_n.v[i]=0;
if (mainmass[u][v-1].grass==1)
ch_n.v[0]=1;
if (mainmass[u][v+1].grass==1)
ch_n.v[3]=1;

if (u%2==0)
{
if (mainmass[u+1][v].grass==1)
ch_n.v[2]=1;
if (mainmass[u+1][v-1].grass==1)
ch_n.v[1]=1;
if (mainmass[u-1][v].grass==1)
ch_n.v[5]=1;
if (mainmass[u-1][v-1].grass==1)
ch_n.v[4]=1;
}

if (u%2==1)
{
if (mainmass[u+1][v+1].grass==1)
ch_n.v[2]=1;
if (mainmass[u+1][v].grass==1)
ch_n.v[1]=1;
if (mainmass[u-1][v+1].grass==1)
ch_n.v[5]=1;
if (mainmass[u-1][v].grass==1)
ch_n.v[4]=1;
}

return ch_n;

}


void FreeObject(obj_type *obj)
{
free(obj->texture.imageData);
free(obj->texture2.imageData);
free(obj->texture_norm.imageData);
delete [obj->vertices_qty] obj->vertex;
delete [obj->vertices_qty] obj->normals;
delete [obj->vertices_qty] obj->mapcoord;
delete [obj->polygons_qty] obj->polygon;
}

void FreeLandscapeMemory()
{
for (int i = 0; i < 4; i++) {
FreeObject(&rock_1[i]);
FreeObject(&rock_2[i]);
FreeObject(&rock_3[i]);
FreeObject(&rock_4[i]);
FreeObject(&rock_5[i]);
FreeObject(&rock_6[i]);
FreeObject(&rock_7[i]);
FreeObject(&hill_1[i]);
FreeObject(&hill_2[i]);
FreeObject(&hill_3[i]);
FreeObject(&hill_4[i]);
FreeObject(&hill_5[i]);
FreeObject(&hill_6[i]);
FreeObject(&hill_7[i]);
}
for (int i = 0; i < 43; i++)  {
free(grass_mask[i].imageData);
}
}


void CreateForest()
{
for (int i = 0; i < 62; i++) {
string str = IntToStr(i);
LoadForestMask(str, i);
}

int x_location=50;
int y_location;
int x_offset=0;
int y_offset=0;
int countz=0;
polygon_type Temp3;
int mask;
Temp3.a=0; Temp3.b=1; Temp3.c=2;

for (int u = 0; u < GridWei; u++) {
for (int v = 0; v < GridHei; v++) {
if (mainmass[u][v].forest==1 && mainmass[u][v].rock==0) {
countz=0;
GetForestZ(u,v);
mask=GenerateForestPart(u,v);
x_location=50+75*u;
if (u%2==0)
y_location=87.5*v;
else
y_location=87.5*v+43.75;
for (int j = 0; j < 7; j++) {
x_offset=-24-j*3+x_location;
y_offset=0+j*6+y_location;
for (int i = 0; i < 9+j; i++) {
if ((ForestMaskArray[j][i][61]==1 && mainmass[u][v].hill==1) || mainmass[u][v].hill==0)
if (ForestMaskArray[j][i][mask]==1)
{

AddTree(Temp3,x_offset,y_offset,countz);
Temp3=NextTree(Temp3);
}
x_offset=x_offset+6;
countz++;
}
}
for (int j = 0; j < 8; j++) {
x_offset=-45+j*3+x_location;
y_offset=42+j*6+y_location;
for (int i = 0; i < 16-j; i++) {
if (ForestMaskArray[j+7][i][mask]==1)
{
AddTree(Temp3,x_offset,y_offset,countz);
Temp3=NextTree(Temp3);
}
x_offset=x_offset+6;
countz++;
}
}

}
}
}
}

void LoadForestMask(string MaskPath, int number)
{

string maskadress="masks/" + MaskPath; 

char csymbol; //������ ��� ����������
ifstream fMp(maskadress.c_str()); //��������� ����

for (int j = 0; j < 8; j++) {
for (int i = 0; i < 9+j; i++)
{
fMp.get(csymbol);
ForestMaskArray[j][i][number]=atoi(&csymbol);
}
fMp.get();
}

for (int j = 8; j < 15; j++) {
for (int i = 0; i < 23-j; i++)
{
fMp.get(csymbol);
ForestMaskArray[j][i][number]=atoi(&csymbol);
}
fMp.get();
}
fMp.close();

}

void GetForestZ(int u, int v)
{
if (u%2==0) {
//1
ForestZArray[0]=MapVertexArray[GetTriangleDot(0,u*6*GridSetHei*8+v*7*8+2*GridSetHei*8)].z;
ForestZArray[2]=MapVertexArray[GetTriangleDot(0,u*6*GridSetHei*8+v*7*8+3*GridSetHei*8)].z;
ForestZArray[4]=MapVertexArray[GetTriangleDot(0,u*6*GridSetHei*8+v*7*8+4*GridSetHei*8)].z;
ForestZArray[6]=MapVertexArray[GetTriangleDot(0,u*6*GridSetHei*8+v*7*8+5*GridSetHei*8)].z;
ForestZArray[8]=MapVertexArray[GetTriangleDot(0,u*6*GridSetHei*8+v*7*8+6*GridSetHei*8)].z;

ForestZArray[1]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+2*GridSetHei*8+1)].z;
ForestZArray[3]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+3*GridSetHei*8+1)].z;
ForestZArray[5]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+4*GridSetHei*8+1)].z;
ForestZArray[7]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+5*GridSetHei*8+1)].z;
//2
 ForestZArray[9]=MapVertexArray[GetTriangleDot(1,u*6*GridSetHei*8+v*7*8+1*GridSetHei*8+3)].z;
ForestZArray[11]=MapVertexArray[GetTriangleDot(1,u*6*GridSetHei*8+v*7*8+2*GridSetHei*8+3)].z;
ForestZArray[13]=MapVertexArray[GetTriangleDot(1,u*6*GridSetHei*8+v*7*8+3*GridSetHei*8+3)].z;
ForestZArray[15]=MapVertexArray[GetTriangleDot(1,u*6*GridSetHei*8+v*7*8+4*GridSetHei*8+3)].z;
ForestZArray[17]=MapVertexArray[GetTriangleDot(1,u*6*GridSetHei*8+v*7*8+5*GridSetHei*8+3)].z;

ForestZArray[10]=MapVertexArray[GetTriangleDot(1,u*6*GridSetHei*8+v*7*8+2*GridSetHei*8)].z;
ForestZArray[12]=MapVertexArray[GetTriangleDot(1,u*6*GridSetHei*8+v*7*8+3*GridSetHei*8)].z;
ForestZArray[14]=MapVertexArray[GetTriangleDot(1,u*6*GridSetHei*8+v*7*8+4*GridSetHei*8)].z;
ForestZArray[16]=MapVertexArray[GetTriangleDot(1,u*6*GridSetHei*8+v*7*8+5*GridSetHei*8)].z;
ForestZArray[18]=MapVertexArray[GetTriangleDot(1,u*6*GridSetHei*8+v*7*8+6*GridSetHei*8)].z;
//3
ForestZArray[19]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+1*GridSetHei*8+8*1+1)].z;
ForestZArray[21]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+2*GridSetHei*8+8*1+1)].z;
ForestZArray[23]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+3*GridSetHei*8+8*1+1)].z;
ForestZArray[25]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+4*GridSetHei*8+8*1+1)].z;
ForestZArray[27]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+5*GridSetHei*8+8*1+1)].z;
ForestZArray[29]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+6*GridSetHei*8+8*1+1)].z;

ForestZArray[20]=MapVertexArray[GetTriangleDot(0,u*6*GridSetHei*8+v*7*8+2*GridSetHei*8+8*1)].z;
ForestZArray[22]=MapVertexArray[GetTriangleDot(0,u*6*GridSetHei*8+v*7*8+3*GridSetHei*8+8*1)].z;
ForestZArray[24]=MapVertexArray[GetTriangleDot(0,u*6*GridSetHei*8+v*7*8+4*GridSetHei*8+8*1)].z;
ForestZArray[26]=MapVertexArray[GetTriangleDot(0,u*6*GridSetHei*8+v*7*8+5*GridSetHei*8+8*1)].z;
ForestZArray[28]=MapVertexArray[GetTriangleDot(0,u*6*GridSetHei*8+v*7*8+6*GridSetHei*8+8*1)].z;
//4
ForestZArray[30]=MapVertexArray[GetTriangleDot(1,u*6*GridSetHei*8+v*7*8+1*GridSetHei*8+8*1)].z;
ForestZArray[32]=MapVertexArray[GetTriangleDot(1,u*6*GridSetHei*8+v*7*8+2*GridSetHei*8+8*1)].z;
ForestZArray[34]=MapVertexArray[GetTriangleDot(1,u*6*GridSetHei*8+v*7*8+3*GridSetHei*8+8*1)].z;
ForestZArray[36]=MapVertexArray[GetTriangleDot(1,u*6*GridSetHei*8+v*7*8+4*GridSetHei*8+8*1)].z;
ForestZArray[38]=MapVertexArray[GetTriangleDot(1,u*6*GridSetHei*8+v*7*8+5*GridSetHei*8+8*1)].z;
ForestZArray[40]=MapVertexArray[GetTriangleDot(1,u*6*GridSetHei*8+v*7*8+6*GridSetHei*8+8*1)].z;

ForestZArray[31]=MapVertexArray[GetTriangleDot(1,u*6*GridSetHei*8+v*7*8+1*GridSetHei*8+8*1+3)].z;
ForestZArray[33]=MapVertexArray[GetTriangleDot(1,u*6*GridSetHei*8+v*7*8+2*GridSetHei*8+8*1+3)].z;
ForestZArray[35]=MapVertexArray[GetTriangleDot(1,u*6*GridSetHei*8+v*7*8+3*GridSetHei*8+8*1+3)].z;
ForestZArray[37]=MapVertexArray[GetTriangleDot(1,u*6*GridSetHei*8+v*7*8+4*GridSetHei*8+8*1+3)].z;
ForestZArray[39]=MapVertexArray[GetTriangleDot(1,u*6*GridSetHei*8+v*7*8+5*GridSetHei*8+8*1+3)].z;
ForestZArray[41]=MapVertexArray[GetTriangleDot(1,u*6*GridSetHei*8+v*7*8+6*GridSetHei*8+8*1+3)].z;
//5
ForestZArray[42]=MapVertexArray[GetTriangleDot(0,u*6*GridSetHei*8+v*7*8+1*GridSetHei*8+8*2)].z;
ForestZArray[44]=MapVertexArray[GetTriangleDot(0,u*6*GridSetHei*8+v*7*8+2*GridSetHei*8+8*2)].z;
ForestZArray[46]=MapVertexArray[GetTriangleDot(0,u*6*GridSetHei*8+v*7*8+3*GridSetHei*8+8*2)].z;
ForestZArray[48]=MapVertexArray[GetTriangleDot(0,u*6*GridSetHei*8+v*7*8+4*GridSetHei*8+8*2)].z;
ForestZArray[50]=MapVertexArray[GetTriangleDot(0,u*6*GridSetHei*8+v*7*8+5*GridSetHei*8+8*2)].z;
ForestZArray[52]=MapVertexArray[GetTriangleDot(0,u*6*GridSetHei*8+v*7*8+6*GridSetHei*8+8*2)].z;
ForestZArray[54]=MapVertexArray[GetTriangleDot(0,u*6*GridSetHei*8+v*7*8+7*GridSetHei*8+8*2)].z;

ForestZArray[43]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+1*GridSetHei*8+8*2+1)].z;
ForestZArray[45]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+2*GridSetHei*8+8*2+1)].z;
ForestZArray[47]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+3*GridSetHei*8+8*2+1)].z;
ForestZArray[49]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+4*GridSetHei*8+8*2+1)].z;
ForestZArray[51]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+5*GridSetHei*8+8*2+1)].z;
ForestZArray[53]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+6*GridSetHei*8+8*2+1)].z;
//6
ForestZArray[55]=MapVertexArray[GetTriangleDot(1,u*6*GridSetHei*8+v*7*8+0*GridSetHei*8+8*2+3)].z;
ForestZArray[57]=MapVertexArray[GetTriangleDot(1,u*6*GridSetHei*8+v*7*8+1*GridSetHei*8+8*2+3)].z;
ForestZArray[59]=MapVertexArray[GetTriangleDot(1,u*6*GridSetHei*8+v*7*8+2*GridSetHei*8+8*2+3)].z;
ForestZArray[61]=MapVertexArray[GetTriangleDot(1,u*6*GridSetHei*8+v*7*8+3*GridSetHei*8+8*2+3)].z;
ForestZArray[63]=MapVertexArray[GetTriangleDot(1,u*6*GridSetHei*8+v*7*8+4*GridSetHei*8+8*2+3)].z;
ForestZArray[65]=MapVertexArray[GetTriangleDot(1,u*6*GridSetHei*8+v*7*8+5*GridSetHei*8+8*2+3)].z;
ForestZArray[67]=MapVertexArray[GetTriangleDot(1,u*6*GridSetHei*8+v*7*8+6*GridSetHei*8+8*2+3)].z;

ForestZArray[56]=MapVertexArray[GetTriangleDot(1,u*6*GridSetHei*8+v*7*8+1*GridSetHei*8+8*2)].z;
ForestZArray[58]=MapVertexArray[GetTriangleDot(1,u*6*GridSetHei*8+v*7*8+2*GridSetHei*8+8*2)].z;
ForestZArray[60]=MapVertexArray[GetTriangleDot(1,u*6*GridSetHei*8+v*7*8+3*GridSetHei*8+8*2)].z;
ForestZArray[62]=MapVertexArray[GetTriangleDot(1,u*6*GridSetHei*8+v*7*8+4*GridSetHei*8+8*2)].z;
ForestZArray[64]=MapVertexArray[GetTriangleDot(1,u*6*GridSetHei*8+v*7*8+5*GridSetHei*8+8*2)].z;
ForestZArray[66]=MapVertexArray[GetTriangleDot(1,u*6*GridSetHei*8+v*7*8+6*GridSetHei*8+8*2)].z;
ForestZArray[68]=MapVertexArray[GetTriangleDot(1,u*6*GridSetHei*8+v*7*8+7*GridSetHei*8+8*2)].z;
//7
ForestZArray[69]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+0*GridSetHei*8+8*3+1)].z;
ForestZArray[71]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+1*GridSetHei*8+8*3+1)].z;
ForestZArray[73]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+2*GridSetHei*8+8*3+1)].z;
ForestZArray[75]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+3*GridSetHei*8+8*3+1)].z;
ForestZArray[77]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+4*GridSetHei*8+8*3+1)].z;
ForestZArray[79]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+5*GridSetHei*8+8*3+1)].z;
ForestZArray[81]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+6*GridSetHei*8+8*3+1)].z;
ForestZArray[83]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+7*GridSetHei*8+8*3+1)].z;

ForestZArray[70]=MapVertexArray[GetTriangleDot(0,u*6*GridSetHei*8+v*7*8+1*GridSetHei*8+8*3)].z;
ForestZArray[72]=MapVertexArray[GetTriangleDot(0,u*6*GridSetHei*8+v*7*8+2*GridSetHei*8+8*3)].z;
ForestZArray[74]=MapVertexArray[GetTriangleDot(0,u*6*GridSetHei*8+v*7*8+3*GridSetHei*8+8*3)].z;
ForestZArray[76]=MapVertexArray[GetTriangleDot(0,u*6*GridSetHei*8+v*7*8+4*GridSetHei*8+8*3)].z;
ForestZArray[78]=MapVertexArray[GetTriangleDot(0,u*6*GridSetHei*8+v*7*8+5*GridSetHei*8+8*3)].z;
ForestZArray[80]=MapVertexArray[GetTriangleDot(0,u*6*GridSetHei*8+v*7*8+6*GridSetHei*8+8*3)].z;
ForestZArray[82]=MapVertexArray[GetTriangleDot(0,u*6*GridSetHei*8+v*7*8+7*GridSetHei*8+8*3)].z;
//8
ForestZArray[84]=MapVertexArray[GetTriangleDot(1,u*6*GridSetHei*8+v*7*8+0*GridSetHei*8+8*3)].z;
ForestZArray[86]=MapVertexArray[GetTriangleDot(1,u*6*GridSetHei*8+v*7*8+1*GridSetHei*8+8*3)].z;
ForestZArray[88]=MapVertexArray[GetTriangleDot(1,u*6*GridSetHei*8+v*7*8+2*GridSetHei*8+8*3)].z;
ForestZArray[90]=MapVertexArray[GetTriangleDot(1,u*6*GridSetHei*8+v*7*8+3*GridSetHei*8+8*3)].z;
ForestZArray[92]=MapVertexArray[GetTriangleDot(1,u*6*GridSetHei*8+v*7*8+4*GridSetHei*8+8*3)].z;
ForestZArray[94]=MapVertexArray[GetTriangleDot(1,u*6*GridSetHei*8+v*7*8+5*GridSetHei*8+8*3)].z;
ForestZArray[96]=MapVertexArray[GetTriangleDot(1,u*6*GridSetHei*8+v*7*8+6*GridSetHei*8+8*3)].z;
ForestZArray[98]=MapVertexArray[GetTriangleDot(1,u*6*GridSetHei*8+v*7*8+7*GridSetHei*8+8*3)].z;

ForestZArray[85]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+1*GridSetHei*8+8*3)].z;
ForestZArray[87]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+2*GridSetHei*8+8*3)].z;
ForestZArray[89]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+3*GridSetHei*8+8*3)].z;
ForestZArray[91]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+4*GridSetHei*8+8*3)].z;
ForestZArray[93]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+5*GridSetHei*8+8*3)].z;
ForestZArray[95]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+6*GridSetHei*8+8*3)].z;
ForestZArray[97]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+7*GridSetHei*8+8*3)].z;
ForestZArray[99]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+7*GridSetHei*8+8*3)].z;
//9
ForestZArray[100]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+0*GridSetHei*8+8*3)].z;
ForestZArray[102]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+1*GridSetHei*8+8*3)].z;
ForestZArray[104]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+2*GridSetHei*8+8*3)].z;
ForestZArray[106]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+3*GridSetHei*8+8*3)].z;
ForestZArray[108]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+4*GridSetHei*8+8*3)].z;
ForestZArray[110]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+5*GridSetHei*8+8*3)].z;
ForestZArray[112]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+6*GridSetHei*8+8*3)].z;
ForestZArray[114]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+7*GridSetHei*8+8*3)].z;

ForestZArray[101]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+0*GridSetHei*8+8*3+3)].z;
ForestZArray[103]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+1*GridSetHei*8+8*3+3)].z;
ForestZArray[105]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+2*GridSetHei*8+8*3+3)].z;
ForestZArray[107]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+3*GridSetHei*8+8*3+3)].z;
ForestZArray[109]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+4*GridSetHei*8+8*3+3)].z;
ForestZArray[111]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+5*GridSetHei*8+8*3+3)].z;
ForestZArray[113]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+6*GridSetHei*8+8*3+3)].z;
//10
ForestZArray[115]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+0*GridSetHei*8+8*4+1)].z;
ForestZArray[117]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+1*GridSetHei*8+8*4+1)].z;
ForestZArray[119]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+2*GridSetHei*8+8*4+1)].z;
ForestZArray[121]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+3*GridSetHei*8+8*4+1)].z;
ForestZArray[123]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+4*GridSetHei*8+8*4+1)].z;
ForestZArray[125]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+5*GridSetHei*8+8*4+1)].z;
ForestZArray[127]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+6*GridSetHei*8+8*4+1)].z;

ForestZArray[116]=MapVertexArray[GetTriangleDot(1,u*6*GridSetHei*8+v*7*8+1*GridSetHei*8+8*4+1)].z;
ForestZArray[118]=MapVertexArray[GetTriangleDot(1,u*6*GridSetHei*8+v*7*8+2*GridSetHei*8+8*4+1)].z;
ForestZArray[120]=MapVertexArray[GetTriangleDot(1,u*6*GridSetHei*8+v*7*8+3*GridSetHei*8+8*4+1)].z;
ForestZArray[122]=MapVertexArray[GetTriangleDot(1,u*6*GridSetHei*8+v*7*8+4*GridSetHei*8+8*4+1)].z;
ForestZArray[124]=MapVertexArray[GetTriangleDot(1,u*6*GridSetHei*8+v*7*8+5*GridSetHei*8+8*4+1)].z;
ForestZArray[126]=MapVertexArray[GetTriangleDot(1,u*6*GridSetHei*8+v*7*8+6*GridSetHei*8+8*4+1)].z;
ForestZArray[128]=MapVertexArray[GetTriangleDot(1,u*6*GridSetHei*8+v*7*8+7*GridSetHei*8+8*4+1)].z;
//11
ForestZArray[129]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+0*GridSetHei*8+8*4+3)].z;
ForestZArray[131]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+1*GridSetHei*8+8*4+3)].z;
ForestZArray[133]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+2*GridSetHei*8+8*4+3)].z;
ForestZArray[135]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+3*GridSetHei*8+8*4+3)].z;
ForestZArray[137]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+4*GridSetHei*8+8*4+3)].z;
ForestZArray[139]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+5*GridSetHei*8+8*4+3)].z;
ForestZArray[141]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+6*GridSetHei*8+8*4+3)].z;

ForestZArray[130]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+1*GridSetHei*8+8*4)].z;
ForestZArray[132]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+2*GridSetHei*8+8*4)].z;
ForestZArray[134]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+3*GridSetHei*8+8*4)].z;
ForestZArray[136]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+4*GridSetHei*8+8*4)].z;
ForestZArray[138]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+5*GridSetHei*8+8*4)].z;
ForestZArray[140]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+6*GridSetHei*8+8*4)].z;
//12
ForestZArray[142]=MapVertexArray[GetTriangleDot(0,u*6*GridSetHei*8+v*7*8+1*GridSetHei*8+8*5)].z;
ForestZArray[144]=MapVertexArray[GetTriangleDot(0,u*6*GridSetHei*8+v*7*8+2*GridSetHei*8+8*5)].z;
ForestZArray[146]=MapVertexArray[GetTriangleDot(0,u*6*GridSetHei*8+v*7*8+3*GridSetHei*8+8*5)].z;
ForestZArray[148]=MapVertexArray[GetTriangleDot(0,u*6*GridSetHei*8+v*7*8+4*GridSetHei*8+8*5)].z;
ForestZArray[150]=MapVertexArray[GetTriangleDot(0,u*6*GridSetHei*8+v*7*8+5*GridSetHei*8+8*5)].z;
ForestZArray[152]=MapVertexArray[GetTriangleDot(0,u*6*GridSetHei*8+v*7*8+6*GridSetHei*8+8*5)].z;

ForestZArray[143]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+1*GridSetHei*8+8*5+1)].z;
ForestZArray[145]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+2*GridSetHei*8+8*5+1)].z;
ForestZArray[147]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+3*GridSetHei*8+8*5+1)].z;
ForestZArray[149]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+4*GridSetHei*8+8*5+1)].z;
ForestZArray[151]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+5*GridSetHei*8+8*5+1)].z;
ForestZArray[153]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+6*GridSetHei*8+8*5+1)].z;
//13
ForestZArray[154]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+1*GridSetHei*8+8*5)].z;
ForestZArray[156]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+2*GridSetHei*8+8*5)].z;
ForestZArray[158]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+3*GridSetHei*8+8*5)].z;
ForestZArray[160]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+4*GridSetHei*8+8*5)].z;
ForestZArray[162]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+5*GridSetHei*8+8*5)].z;
ForestZArray[164]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+6*GridSetHei*8+8*5)].z;

ForestZArray[155]=MapVertexArray[GetTriangleDot(1,u*6*GridSetHei*8+v*7*8+2*GridSetHei*8+8*5)].z;
ForestZArray[157]=MapVertexArray[GetTriangleDot(1,u*6*GridSetHei*8+v*7*8+3*GridSetHei*8+8*5)].z;
ForestZArray[159]=MapVertexArray[GetTriangleDot(1,u*6*GridSetHei*8+v*7*8+4*GridSetHei*8+8*5)].z;
ForestZArray[161]=MapVertexArray[GetTriangleDot(1,u*6*GridSetHei*8+v*7*8+5*GridSetHei*8+8*5)].z;
ForestZArray[163]=MapVertexArray[GetTriangleDot(1,u*6*GridSetHei*8+v*7*8+6*GridSetHei*8+8*5)].z;
//14
ForestZArray[165]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+1*GridSetHei*8+8*6+1)].z;
ForestZArray[167]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+2*GridSetHei*8+8*6+1)].z;
ForestZArray[169]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+3*GridSetHei*8+8*6+1)].z;
ForestZArray[171]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+4*GridSetHei*8+8*6+1)].z;
ForestZArray[173]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+5*GridSetHei*8+8*6+1)].z;

ForestZArray[166]=MapVertexArray[GetTriangleDot(0,u*6*GridSetHei*8+v*7*8+2*GridSetHei*8+8*6)].z;
ForestZArray[168]=MapVertexArray[GetTriangleDot(0,u*6*GridSetHei*8+v*7*8+3*GridSetHei*8+8*6)].z;
ForestZArray[170]=MapVertexArray[GetTriangleDot(0,u*6*GridSetHei*8+v*7*8+4*GridSetHei*8+8*6)].z;
ForestZArray[172]=MapVertexArray[GetTriangleDot(0,u*6*GridSetHei*8+v*7*8+5*GridSetHei*8+8*6)].z;
ForestZArray[174]=MapVertexArray[GetTriangleDot(0,u*6*GridSetHei*8+v*7*8+6*GridSetHei*8+8*6)].z;
//15
ForestZArray[175]=MapVertexArray[GetTriangleDot(1,u*6*GridSetHei*8+v*7*8+2*GridSetHei*8+8*6)].z;
ForestZArray[177]=MapVertexArray[GetTriangleDot(1,u*6*GridSetHei*8+v*7*8+3*GridSetHei*8+8*6)].z;
ForestZArray[179]=MapVertexArray[GetTriangleDot(1,u*6*GridSetHei*8+v*7*8+4*GridSetHei*8+8*6)].z;
ForestZArray[181]=MapVertexArray[GetTriangleDot(1,u*6*GridSetHei*8+v*7*8+5*GridSetHei*8+8*6)].z;
ForestZArray[183]=MapVertexArray[GetTriangleDot(1,u*6*GridSetHei*8+v*7*8+6*GridSetHei*8+8*6)].z;

ForestZArray[176]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+2*GridSetHei*8+8*6)].z;
ForestZArray[178]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+3*GridSetHei*8+8*6)].z;
ForestZArray[180]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+4*GridSetHei*8+8*6)].z;
ForestZArray[182]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+5*GridSetHei*8+8*6)].z;
}
else
{
//1
ForestZArray[0]=MapVertexArray[GetTriangleDot(1,u*6*GridSetHei*8+v*7*8+2*GridSetHei*8+8*3)].z;
ForestZArray[2]=MapVertexArray[GetTriangleDot(1,u*6*GridSetHei*8+v*7*8+3*GridSetHei*8+8*3)].z;
ForestZArray[4]=MapVertexArray[GetTriangleDot(1,u*6*GridSetHei*8+v*7*8+4*GridSetHei*8+8*3)].z;
ForestZArray[6]=MapVertexArray[GetTriangleDot(1,u*6*GridSetHei*8+v*7*8+5*GridSetHei*8+8*3)].z;
ForestZArray[8]=MapVertexArray[GetTriangleDot(1,u*6*GridSetHei*8+v*7*8+6*GridSetHei*8+8*3)].z;

ForestZArray[1]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+2*GridSetHei*8+8*3)].z;
ForestZArray[3]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+3*GridSetHei*8+8*3)].z;
ForestZArray[5]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+4*GridSetHei*8+8*3)].z;
ForestZArray[7]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+5*GridSetHei*8+8*3)].z;
//2
 ForestZArray[9]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+1*GridSetHei*8+8*4+1)].z;
ForestZArray[11]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+2*GridSetHei*8+8*4+1)].z;
ForestZArray[13]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+3*GridSetHei*8+8*4+1)].z;
ForestZArray[15]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+4*GridSetHei*8+8*4+1)].z;
ForestZArray[17]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+5*GridSetHei*8+8*4+1)].z;

ForestZArray[10]=MapVertexArray[GetTriangleDot(1,u*6*GridSetHei*8+v*7*8+2*GridSetHei*8+8*4+1)].z;
ForestZArray[12]=MapVertexArray[GetTriangleDot(1,u*6*GridSetHei*8+v*7*8+3*GridSetHei*8+8*4+1)].z;
ForestZArray[14]=MapVertexArray[GetTriangleDot(1,u*6*GridSetHei*8+v*7*8+4*GridSetHei*8+8*4+1)].z;
ForestZArray[16]=MapVertexArray[GetTriangleDot(1,u*6*GridSetHei*8+v*7*8+5*GridSetHei*8+8*4+1)].z;
ForestZArray[18]=MapVertexArray[GetTriangleDot(1,u*6*GridSetHei*8+v*7*8+6*GridSetHei*8+8*4+1)].z;
//3
ForestZArray[19]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+1*GridSetHei*8+8*4)].z;
ForestZArray[21]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+2*GridSetHei*8+8*4)].z;
ForestZArray[23]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+3*GridSetHei*8+8*4)].z;
ForestZArray[25]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+4*GridSetHei*8+8*4)].z;
ForestZArray[27]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+5*GridSetHei*8+8*4)].z;
ForestZArray[29]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+6*GridSetHei*8+8*4)].z;

ForestZArray[20]=MapVertexArray[GetTriangleDot(1,u*6*GridSetHei*8+v*7*8+2*GridSetHei*8+8*4)].z;
ForestZArray[22]=MapVertexArray[GetTriangleDot(1,u*6*GridSetHei*8+v*7*8+3*GridSetHei*8+8*4)].z;
ForestZArray[24]=MapVertexArray[GetTriangleDot(1,u*6*GridSetHei*8+v*7*8+4*GridSetHei*8+8*4)].z;
ForestZArray[26]=MapVertexArray[GetTriangleDot(1,u*6*GridSetHei*8+v*7*8+5*GridSetHei*8+8*4)].z;
ForestZArray[28]=MapVertexArray[GetTriangleDot(1,u*6*GridSetHei*8+v*7*8+6*GridSetHei*8+8*4)].z;
//4
ForestZArray[30]=MapVertexArray[GetTriangleDot(0,u*6*GridSetHei*8+v*7*8+1*GridSetHei*8+8*5)].z;
ForestZArray[32]=MapVertexArray[GetTriangleDot(0,u*6*GridSetHei*8+v*7*8+2*GridSetHei*8+8*5)].z;
ForestZArray[34]=MapVertexArray[GetTriangleDot(0,u*6*GridSetHei*8+v*7*8+3*GridSetHei*8+8*5)].z;
ForestZArray[36]=MapVertexArray[GetTriangleDot(0,u*6*GridSetHei*8+v*7*8+4*GridSetHei*8+8*5)].z;
ForestZArray[38]=MapVertexArray[GetTriangleDot(0,u*6*GridSetHei*8+v*7*8+5*GridSetHei*8+8*5)].z;
ForestZArray[40]=MapVertexArray[GetTriangleDot(0,u*6*GridSetHei*8+v*7*8+6*GridSetHei*8+8*5)].z;

ForestZArray[31]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+1*GridSetHei*8+8*5+1)].z;
ForestZArray[33]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+2*GridSetHei*8+8*5+1)].z;
ForestZArray[35]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+3*GridSetHei*8+8*5+1)].z;
ForestZArray[37]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+4*GridSetHei*8+8*5+1)].z;
ForestZArray[39]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+5*GridSetHei*8+8*5+1)].z;
ForestZArray[41]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+6*GridSetHei*8+8*5+1)].z;
//5
ForestZArray[42]=MapVertexArray[GetTriangleDot(1,u*6*GridSetHei*8+v*7*8+1*GridSetHei*8+8*5)].z;
ForestZArray[44]=MapVertexArray[GetTriangleDot(1,u*6*GridSetHei*8+v*7*8+2*GridSetHei*8+8*5)].z;
ForestZArray[46]=MapVertexArray[GetTriangleDot(1,u*6*GridSetHei*8+v*7*8+3*GridSetHei*8+8*5)].z;
ForestZArray[48]=MapVertexArray[GetTriangleDot(1,u*6*GridSetHei*8+v*7*8+4*GridSetHei*8+8*5)].z;
ForestZArray[50]=MapVertexArray[GetTriangleDot(1,u*6*GridSetHei*8+v*7*8+5*GridSetHei*8+8*5)].z;
ForestZArray[52]=MapVertexArray[GetTriangleDot(1,u*6*GridSetHei*8+v*7*8+6*GridSetHei*8+8*5)].z;
ForestZArray[54]=MapVertexArray[GetTriangleDot(1,u*6*GridSetHei*8+v*7*8+7*GridSetHei*8+8*5)].z;

ForestZArray[43]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+1*GridSetHei*8+8*5)].z;
ForestZArray[45]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+2*GridSetHei*8+8*5)].z;
ForestZArray[47]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+3*GridSetHei*8+8*5)].z;
ForestZArray[49]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+4*GridSetHei*8+8*5)].z;
ForestZArray[51]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+5*GridSetHei*8+8*5)].z;
ForestZArray[53]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+6*GridSetHei*8+8*5)].z;
//6
ForestZArray[55]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+0*GridSetHei*8+8*6+1)].z;
ForestZArray[57]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+1*GridSetHei*8+8*6+1)].z;
ForestZArray[59]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+2*GridSetHei*8+8*6+1)].z;
ForestZArray[61]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+3*GridSetHei*8+8*6+1)].z;
ForestZArray[63]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+4*GridSetHei*8+8*6+1)].z;
ForestZArray[65]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+5*GridSetHei*8+8*6+1)].z;
ForestZArray[67]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+6*GridSetHei*8+8*6+1)].z;

ForestZArray[56]=MapVertexArray[GetTriangleDot(1,u*6*GridSetHei*8+v*7*8+1*GridSetHei*8+8*6+1)].z;
ForestZArray[58]=MapVertexArray[GetTriangleDot(1,u*6*GridSetHei*8+v*7*8+2*GridSetHei*8+8*6+1)].z;
ForestZArray[60]=MapVertexArray[GetTriangleDot(1,u*6*GridSetHei*8+v*7*8+3*GridSetHei*8+8*6+1)].z;
ForestZArray[62]=MapVertexArray[GetTriangleDot(1,u*6*GridSetHei*8+v*7*8+4*GridSetHei*8+8*6+1)].z;
ForestZArray[64]=MapVertexArray[GetTriangleDot(1,u*6*GridSetHei*8+v*7*8+5*GridSetHei*8+8*6+1)].z;
ForestZArray[66]=MapVertexArray[GetTriangleDot(1,u*6*GridSetHei*8+v*7*8+6*GridSetHei*8+8*6+1)].z;
ForestZArray[68]=MapVertexArray[GetTriangleDot(1,u*6*GridSetHei*8+v*7*8+7*GridSetHei*8+8*6+1)].z;
//7
ForestZArray[69]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+0*GridSetHei*8+8*6)].z;
ForestZArray[71]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+1*GridSetHei*8+8*6)].z;
ForestZArray[73]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+2*GridSetHei*8+8*6)].z;
ForestZArray[75]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+3*GridSetHei*8+8*6)].z;
ForestZArray[77]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+4*GridSetHei*8+8*6)].z;
ForestZArray[79]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+5*GridSetHei*8+8*6)].z;
ForestZArray[81]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+6*GridSetHei*8+8*6)].z;
ForestZArray[83]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+7*GridSetHei*8+8*6)].z;

ForestZArray[70]=MapVertexArray[GetTriangleDot(1,u*6*GridSetHei*8+v*7*8+1*GridSetHei*8+8*6)].z;
ForestZArray[72]=MapVertexArray[GetTriangleDot(1,u*6*GridSetHei*8+v*7*8+2*GridSetHei*8+8*6)].z;
ForestZArray[74]=MapVertexArray[GetTriangleDot(1,u*6*GridSetHei*8+v*7*8+3*GridSetHei*8+8*6)].z;
ForestZArray[76]=MapVertexArray[GetTriangleDot(1,u*6*GridSetHei*8+v*7*8+4*GridSetHei*8+8*6)].z;
ForestZArray[78]=MapVertexArray[GetTriangleDot(1,u*6*GridSetHei*8+v*7*8+5*GridSetHei*8+8*6)].z;
ForestZArray[80]=MapVertexArray[GetTriangleDot(1,u*6*GridSetHei*8+v*7*8+6*GridSetHei*8+8*6)].z;
ForestZArray[82]=MapVertexArray[GetTriangleDot(1,u*6*GridSetHei*8+v*7*8+7*GridSetHei*8+8*6)].z;
//8
ForestZArray[84]=MapVertexArray[GetTriangleDot(0,u*6*GridSetHei*8+v*7*8+0*GridSetHei*8+8*7)].z;
ForestZArray[86]=MapVertexArray[GetTriangleDot(0,u*6*GridSetHei*8+v*7*8+1*GridSetHei*8+8*7)].z;
ForestZArray[88]=MapVertexArray[GetTriangleDot(0,u*6*GridSetHei*8+v*7*8+2*GridSetHei*8+8*7)].z;
ForestZArray[90]=MapVertexArray[GetTriangleDot(0,u*6*GridSetHei*8+v*7*8+3*GridSetHei*8+8*7)].z;
ForestZArray[92]=MapVertexArray[GetTriangleDot(0,u*6*GridSetHei*8+v*7*8+4*GridSetHei*8+8*7)].z;
ForestZArray[94]=MapVertexArray[GetTriangleDot(0,u*6*GridSetHei*8+v*7*8+5*GridSetHei*8+8*7)].z;
ForestZArray[96]=MapVertexArray[GetTriangleDot(0,u*6*GridSetHei*8+v*7*8+6*GridSetHei*8+8*7)].z;
ForestZArray[98]=MapVertexArray[GetTriangleDot(0,u*6*GridSetHei*8+v*7*8+7*GridSetHei*8+8*7)].z;

ForestZArray[85]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+1*GridSetHei*8+8*7+1)].z;
ForestZArray[87]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+2*GridSetHei*8+8*7+1)].z;
ForestZArray[89]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+3*GridSetHei*8+8*7+1)].z;
ForestZArray[91]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+4*GridSetHei*8+8*7+1)].z;
ForestZArray[93]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+5*GridSetHei*8+8*7+1)].z;
ForestZArray[95]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+6*GridSetHei*8+8*7+1)].z;
ForestZArray[97]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+7*GridSetHei*8+8*7+1)].z;
ForestZArray[99]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+7*GridSetHei*8+8*7+1)].z;
//9
ForestZArray[100]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+0*GridSetHei*8+8*7+1)].z;
ForestZArray[102]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+1*GridSetHei*8+8*7+1)].z;
ForestZArray[104]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+2*GridSetHei*8+8*7+1)].z;
ForestZArray[106]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+3*GridSetHei*8+8*7+1)].z;
ForestZArray[108]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+4*GridSetHei*8+8*7+1)].z;
ForestZArray[110]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+5*GridSetHei*8+8*7+1)].z;
ForestZArray[112]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+6*GridSetHei*8+8*7+1)].z;
ForestZArray[114]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+7*GridSetHei*8+8*7+1)].z;

ForestZArray[101]=MapVertexArray[GetTriangleDot(0,u*6*GridSetHei*8+v*7*8+1*GridSetHei*8+8*7)].z;
ForestZArray[103]=MapVertexArray[GetTriangleDot(0,u*6*GridSetHei*8+v*7*8+2*GridSetHei*8+8*7)].z;
ForestZArray[105]=MapVertexArray[GetTriangleDot(0,u*6*GridSetHei*8+v*7*8+3*GridSetHei*8+8*7)].z;
ForestZArray[107]=MapVertexArray[GetTriangleDot(0,u*6*GridSetHei*8+v*7*8+4*GridSetHei*8+8*7)].z;
ForestZArray[109]=MapVertexArray[GetTriangleDot(0,u*6*GridSetHei*8+v*7*8+5*GridSetHei*8+8*7)].z;
ForestZArray[111]=MapVertexArray[GetTriangleDot(0,u*6*GridSetHei*8+v*7*8+6*GridSetHei*8+8*7)].z;
ForestZArray[113]=MapVertexArray[GetTriangleDot(0,u*6*GridSetHei*8+v*7*8+7*GridSetHei*8+8*7)].z;
//10
ForestZArray[115]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+0*GridSetHei*8+8*7)].z;
ForestZArray[117]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+1*GridSetHei*8+8*7)].z;
ForestZArray[119]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+2*GridSetHei*8+8*7)].z;
ForestZArray[121]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+3*GridSetHei*8+8*7)].z;
ForestZArray[123]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+4*GridSetHei*8+8*7)].z;
ForestZArray[125]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+5*GridSetHei*8+8*7)].z;
ForestZArray[127]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+6*GridSetHei*8+8*7)].z;

ForestZArray[116]=MapVertexArray[GetTriangleDot(1,u*6*GridSetHei*8+v*7*8+1*GridSetHei*8+8*7)].z;
ForestZArray[118]=MapVertexArray[GetTriangleDot(1,u*6*GridSetHei*8+v*7*8+2*GridSetHei*8+8*7)].z;
ForestZArray[120]=MapVertexArray[GetTriangleDot(1,u*6*GridSetHei*8+v*7*8+3*GridSetHei*8+8*7)].z;
ForestZArray[122]=MapVertexArray[GetTriangleDot(1,u*6*GridSetHei*8+v*7*8+4*GridSetHei*8+8*7)].z;
ForestZArray[124]=MapVertexArray[GetTriangleDot(1,u*6*GridSetHei*8+v*7*8+5*GridSetHei*8+8*7)].z;
ForestZArray[126]=MapVertexArray[GetTriangleDot(1,u*6*GridSetHei*8+v*7*8+6*GridSetHei*8+8*7)].z;
ForestZArray[128]=MapVertexArray[GetTriangleDot(1,u*6*GridSetHei*8+v*7*8+7*GridSetHei*8+8*7)].z;
//11
ForestZArray[129]=MapVertexArray[GetTriangleDot(0,u*6*GridSetHei*8+v*7*8+1*GridSetHei*8+8*8)].z;
ForestZArray[131]=MapVertexArray[GetTriangleDot(0,u*6*GridSetHei*8+v*7*8+2*GridSetHei*8+8*8)].z;
ForestZArray[133]=MapVertexArray[GetTriangleDot(0,u*6*GridSetHei*8+v*7*8+3*GridSetHei*8+8*8)].z;
ForestZArray[135]=MapVertexArray[GetTriangleDot(0,u*6*GridSetHei*8+v*7*8+4*GridSetHei*8+8*8)].z;
ForestZArray[137]=MapVertexArray[GetTriangleDot(0,u*6*GridSetHei*8+v*7*8+5*GridSetHei*8+8*8)].z;
ForestZArray[139]=MapVertexArray[GetTriangleDot(0,u*6*GridSetHei*8+v*7*8+6*GridSetHei*8+8*8)].z;
ForestZArray[141]=MapVertexArray[GetTriangleDot(0,u*6*GridSetHei*8+v*7*8+7*GridSetHei*8+8*8)].z;

ForestZArray[130]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+1*GridSetHei*8+8*8+1)].z;
ForestZArray[132]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+2*GridSetHei*8+8*8+1)].z;
ForestZArray[134]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+3*GridSetHei*8+8*8+1)].z;
ForestZArray[136]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+4*GridSetHei*8+8*8+1)].z;
ForestZArray[138]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+5*GridSetHei*8+8*8+1)].z;
ForestZArray[140]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+6*GridSetHei*8+8*8+1)].z;
//12
ForestZArray[142]=MapVertexArray[GetTriangleDot(1,u*6*GridSetHei*8+v*7*8+1*GridSetHei*8+8*8)].z;
ForestZArray[144]=MapVertexArray[GetTriangleDot(1,u*6*GridSetHei*8+v*7*8+2*GridSetHei*8+8*8)].z;
ForestZArray[146]=MapVertexArray[GetTriangleDot(1,u*6*GridSetHei*8+v*7*8+3*GridSetHei*8+8*8)].z;
ForestZArray[148]=MapVertexArray[GetTriangleDot(1,u*6*GridSetHei*8+v*7*8+4*GridSetHei*8+8*8)].z;
ForestZArray[150]=MapVertexArray[GetTriangleDot(1,u*6*GridSetHei*8+v*7*8+5*GridSetHei*8+8*8)].z;
ForestZArray[152]=MapVertexArray[GetTriangleDot(1,u*6*GridSetHei*8+v*7*8+6*GridSetHei*8+8*8)].z;

ForestZArray[143]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+1*GridSetHei*8+8*8)].z;
ForestZArray[145]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+2*GridSetHei*8+8*8)].z;
ForestZArray[147]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+3*GridSetHei*8+8*8)].z;
ForestZArray[149]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+4*GridSetHei*8+8*8)].z;
ForestZArray[151]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+5*GridSetHei*8+8*8)].z;
ForestZArray[153]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+6*GridSetHei*8+8*8)].z;
//13
ForestZArray[154]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+1*GridSetHei*8+8*9+1)].z;
ForestZArray[156]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+2*GridSetHei*8+8*9+1)].z;
ForestZArray[158]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+3*GridSetHei*8+8*9+1)].z;
ForestZArray[160]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+4*GridSetHei*8+8*9+1)].z;
ForestZArray[162]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+5*GridSetHei*8+8*9+1)].z;
ForestZArray[164]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+6*GridSetHei*8+8*9+1)].z;

ForestZArray[155]=MapVertexArray[GetTriangleDot(0,u*6*GridSetHei*8+v*7*8+2*GridSetHei*8+8*9)].z;
ForestZArray[157]=MapVertexArray[GetTriangleDot(0,u*6*GridSetHei*8+v*7*8+3*GridSetHei*8+8*9)].z;
ForestZArray[159]=MapVertexArray[GetTriangleDot(0,u*6*GridSetHei*8+v*7*8+4*GridSetHei*8+8*9)].z;
ForestZArray[161]=MapVertexArray[GetTriangleDot(0,u*6*GridSetHei*8+v*7*8+5*GridSetHei*8+8*9)].z;
ForestZArray[163]=MapVertexArray[GetTriangleDot(0,u*6*GridSetHei*8+v*7*8+6*GridSetHei*8+8*9)].z;
//14
ForestZArray[165]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+1*GridSetHei*8+8*9)].z;
ForestZArray[167]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+2*GridSetHei*8+8*9)].z;
ForestZArray[169]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+3*GridSetHei*8+8*9)].z;
ForestZArray[171]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+4*GridSetHei*8+8*9)].z;
ForestZArray[173]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+5*GridSetHei*8+8*9)].z;

ForestZArray[166]=MapVertexArray[GetTriangleDot(1,u*6*GridSetHei*8+v*7*8+2*GridSetHei*8+8*9)].z;
ForestZArray[168]=MapVertexArray[GetTriangleDot(1,u*6*GridSetHei*8+v*7*8+3*GridSetHei*8+8*9)].z;
ForestZArray[170]=MapVertexArray[GetTriangleDot(1,u*6*GridSetHei*8+v*7*8+4*GridSetHei*8+8*9)].z;
ForestZArray[172]=MapVertexArray[GetTriangleDot(1,u*6*GridSetHei*8+v*7*8+5*GridSetHei*8+8*9)].z;
ForestZArray[174]=MapVertexArray[GetTriangleDot(1,u*6*GridSetHei*8+v*7*8+6*GridSetHei*8+8*9)].z;
//15
ForestZArray[175]=MapVertexArray[GetTriangleDot(0,u*6*GridSetHei*8+v*7*8+2*GridSetHei*8+8*10)].z;
ForestZArray[177]=MapVertexArray[GetTriangleDot(0,u*6*GridSetHei*8+v*7*8+3*GridSetHei*8+8*10)].z;
ForestZArray[179]=MapVertexArray[GetTriangleDot(0,u*6*GridSetHei*8+v*7*8+4*GridSetHei*8+8*10)].z;
ForestZArray[181]=MapVertexArray[GetTriangleDot(0,u*6*GridSetHei*8+v*7*8+5*GridSetHei*8+8*10)].z;
ForestZArray[183]=MapVertexArray[GetTriangleDot(0,u*6*GridSetHei*8+v*7*8+6*GridSetHei*8+8*10)].z;

ForestZArray[176]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+2*GridSetHei*8+8*10+1)].z;
ForestZArray[178]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+3*GridSetHei*8+8*10+1)].z;
ForestZArray[180]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+4*GridSetHei*8+8*10+1)].z;
ForestZArray[182]=MapVertexArray[GetTriangleDot(2,u*6*GridSetHei*8+v*7*8+5*GridSetHei*8+8*10+1)].z;
}
}

void AddTree(polygon_type Temp3, int x_offset, int y_offset, int countz)
{
int treechange;
vertex_type Temp;
mapcoord_type Temp2;

Temp.x=forest.vertex[forest.polygon[0].a].x+x_offset+rand()%40*0.1;
Temp.y=forest.vertex[forest.polygon[0].a].y+y_offset+rand()%40*0.1;
Temp.z=forest.vertex[forest.polygon[0].a].z+ForestZArray[countz];
ForestVertexArray.push_back(Temp);
Temp.x=forest.vertex[forest.polygon[0].b].x+x_offset+rand()%40*0.1;
Temp.y=forest.vertex[forest.polygon[0].b].y+y_offset+rand()%40*0.1;
Temp.z=forest.vertex[forest.polygon[0].b].z+ForestZArray[countz];
ForestVertexArray.push_back(Temp);
Temp.x=forest.vertex[forest.polygon[0].c].x+x_offset;
Temp.y=forest.vertex[forest.polygon[0].c].y+y_offset;
Temp.z=forest.vertex[forest.polygon[0].c].z+ForestZArray[countz];
ForestVertexArray.push_back(Temp);
treechange=rand()%10;
Temp2.u=forest.mapcoord[forest.polygon[0].a].u+0.084053998*treechange;
Temp2.v=forest.mapcoord[forest.polygon[0].a].v;
ForestTexcoordArray.push_back(Temp2);
Temp2.u=forest.mapcoord[forest.polygon[0].b].u+0.084053998*treechange;
Temp2.v=forest.mapcoord[forest.polygon[0].b].v;
ForestTexcoordArray.push_back(Temp2);
Temp2.u=forest.mapcoord[forest.polygon[0].c].u+0.084053998*treechange;
Temp2.v=forest.mapcoord[forest.polygon[0].c].v;
ForestTexcoordArray.push_back(Temp2);
ForestFaceArray.push_back(Temp3);

}

polygon_type NextTree(polygon_type Temp3)
{
Temp3.a=Temp3.a+3; Temp3.b=Temp3.b+3; Temp3.c=Temp3.c+3;
return Temp3;
}


int GenerateForestPart(int u, int v)
{
checkmas ch_n;
ch_n=CheckHexesForest(u,v);

if (ch_n.v[0]==1 && ch_n.v[1]==1 && ch_n.v[2]==1
 && ch_n.v[3]==1 && ch_n.v[4]==1 && ch_n.v[5]==1)
return 0;
if (ch_n.v[0]==0 && ch_n.v[1]==0 && ch_n.v[2]==0
 && ch_n.v[3]==0 && ch_n.v[4]==0 && ch_n.v[5]==0)
return 7;
//2

if (ch_n.v[0]==0 && ch_n.v[1]==1 && ch_n.v[2]==1
 && ch_n.v[3]==0 && ch_n.v[4]==0 && ch_n.v[5]==0)
return 6;
if (ch_n.v[0]==0 && ch_n.v[1]==0 && ch_n.v[2]==0
 && ch_n.v[3]==1 && ch_n.v[4]==0 && ch_n.v[5]==1)
return 3;
if (ch_n.v[0]==1 && ch_n.v[1]==0 && ch_n.v[2]==0
 && ch_n.v[3]==0 && ch_n.v[4]==1 && ch_n.v[5]==0)
return 2;
if (ch_n.v[0]==0 && ch_n.v[1]==0 && ch_n.v[2]==0
 && ch_n.v[3]==0 && ch_n.v[4]==1 && ch_n.v[5]==1)
return 1;
if (ch_n.v[1]==0 && ch_n.v[1]==0 && ch_n.v[2]==0
 && ch_n.v[2]==1 && ch_n.v[3]==1 && ch_n.v[5]==0)
return 4;
if (ch_n.v[0]==1 && ch_n.v[1]==1 && ch_n.v[2]==0
 && ch_n.v[3]==0 && ch_n.v[4]==0 && ch_n.v[5]==0)
return 5;
//3

if (ch_n.v[0]==0 && ch_n.v[1]==0 && ch_n.v[2]==1
 && ch_n.v[3]==1 && ch_n.v[4]==0 && ch_n.v[5]==1)
return 10;
if (ch_n.v[0]==0 && ch_n.v[1]==0 && ch_n.v[2]==0
 && ch_n.v[3]==1 && ch_n.v[4]==1 && ch_n.v[5]==1)
return 11;
if (ch_n.v[0]==1 && ch_n.v[1]==0 && ch_n.v[2]==0
 && ch_n.v[3]==0 && ch_n.v[4]==1 && ch_n.v[5]==1)
return 12;
if (ch_n.v[0]==1 && ch_n.v[1]==1 && ch_n.v[2]==0
 && ch_n.v[3]==0 && ch_n.v[4]==1 && ch_n.v[5]==0)
return 13;
if (ch_n.v[0]==1 && ch_n.v[1]==1 && ch_n.v[2]==1
 && ch_n.v[3]==0 && ch_n.v[4]==0 && ch_n.v[5]==0)
return 8;
if (ch_n.v[0]==0 && ch_n.v[1]==1 && ch_n.v[2]==1
 && ch_n.v[3]==1 && ch_n.v[4]==0 && ch_n.v[5]==0)
return 9;

//5
if (ch_n.v[0]==1 && ch_n.v[1]==1 && ch_n.v[2]==1
 && ch_n.v[3]==0 && ch_n.v[4]==1 && ch_n.v[5]==1)
return 24;
if (ch_n.v[0]==0 && ch_n.v[1]==1 && ch_n.v[2]==1
 && ch_n.v[3]==1 && ch_n.v[4]==1 && ch_n.v[5]==1)
return 21;
if (ch_n.v[0]==1 && ch_n.v[1]==1 && ch_n.v[2]==1
 && ch_n.v[3]==1 && ch_n.v[4]==0 && ch_n.v[5]==1)
return 20;
if (ch_n.v[0]==1 && ch_n.v[1]==0 && ch_n.v[2]==1
 && ch_n.v[3]==1 && ch_n.v[4]==1 && ch_n.v[5]==1)
return 22;
if (ch_n.v[0]==1 && ch_n.v[1]==1 && ch_n.v[2]==0
 && ch_n.v[3]==1 && ch_n.v[4]==1 && ch_n.v[5]==1)
return 23;
if (ch_n.v[0]==1 && ch_n.v[1]==1 && ch_n.v[2]==1
 && ch_n.v[3]==1 && ch_n.v[4]==1 && ch_n.v[5]==0)
return 25;
//4

if (ch_n.v[0]==0 && ch_n.v[1]==1 && ch_n.v[2]==1
 && ch_n.v[3]==1 && ch_n.v[4]==0 && ch_n.v[5]==1)
return 15;
if (ch_n.v[0]==0 && ch_n.v[1]==0 && ch_n.v[2]==1
 && ch_n.v[3]==1 && ch_n.v[4]==1 && ch_n.v[5]==1)
return 16;
if (ch_n.v[0]==1 && ch_n.v[1]==1 && ch_n.v[2]==1
 && ch_n.v[3]==0 && ch_n.v[4]==1 && ch_n.v[5]==0)
return 19;
if (ch_n.v[0]==1 && ch_n.v[1]==1 && ch_n.v[2]==0
 && ch_n.v[3]==0 && ch_n.v[4]==1 && ch_n.v[5]==1)
return 18;
if (ch_n.v[0]==1 && ch_n.v[1]==1 && ch_n.v[2]==1
 && ch_n.v[3]==1 && ch_n.v[4]==0 && ch_n.v[5]==0)
return 14;
if (ch_n.v[0]==1 && ch_n.v[1]==0 && ch_n.v[2]==0
 && ch_n.v[3]==1 && ch_n.v[4]==1 && ch_n.v[5]==1)
return 17;
//1

if (ch_n.v[0]==1 && ch_n.v[1]==0 && ch_n.v[2]==0
 && ch_n.v[3]==0 && ch_n.v[4]==0 && ch_n.v[5]==0)
return 29;
if (ch_n.v[0]==0 && ch_n.v[1]==0 && ch_n.v[2]==0
 && ch_n.v[3]==1 && ch_n.v[4]==0 && ch_n.v[5]==0)
return 26;
if (ch_n.v[0]==0 && ch_n.v[1]==0 && ch_n.v[2]==0
 && ch_n.v[3]==0 && ch_n.v[4]==1 && ch_n.v[5]==0)
return 28;
if (ch_n.v[0]==0 && ch_n.v[1]==0 && ch_n.v[2]==0
 && ch_n.v[3]==0 && ch_n.v[4]==0 && ch_n.v[5]==1)
return 27;
if (ch_n.v[0]==0 && ch_n.v[1]==0 && ch_n.v[2]==1
 && ch_n.v[3]==0 && ch_n.v[4]==0 && ch_n.v[5]==0)
return 31;
if (ch_n.v[0]==0 && ch_n.v[1]==1 && ch_n.v[2]==0
 && ch_n.v[3]==0 && ch_n.v[4]==0 && ch_n.v[5]==0)
return 30;
//1-1

if (ch_n.v[0]==1 && ch_n.v[1]==0 && ch_n.v[2]==0
 && ch_n.v[3]==1 && ch_n.v[4]==0 && ch_n.v[5]==0)
return 32;
if (ch_n.v[0]==0 && ch_n.v[1]==1 && ch_n.v[2]==0
 && ch_n.v[3]==0 && ch_n.v[4]==0 && ch_n.v[5]==1)
return 33;
if (ch_n.v[0]==0 && ch_n.v[1]==0 && ch_n.v[2]==1
 && ch_n.v[3]==0 && ch_n.v[4]==1 && ch_n.v[5]==0)
return 34;
//strings
if (ch_n.v[0]==0 && ch_n.v[1]==1 && ch_n.v[2]==0
 && ch_n.v[3]==1 && ch_n.v[4]==1 && ch_n.v[5]==0)
return 35;
if (ch_n.v[0]==1 && ch_n.v[1]==0 && ch_n.v[2]==1
 && ch_n.v[3]==0 && ch_n.v[4]==0 && ch_n.v[5]==1)
return 36;
//duga
if (ch_n.v[0]==0 && ch_n.v[1]==1 && ch_n.v[2]==0
 && ch_n.v[3]==1 && ch_n.v[4]==0 && ch_n.v[5]==0)
return 37;
if (ch_n.v[0]==1 && ch_n.v[1]==0 && ch_n.v[2]==1
 && ch_n.v[3]==0 && ch_n.v[4]==0 && ch_n.v[5]==0)
return 38;
if (ch_n.v[0]==0 && ch_n.v[1]==1 && ch_n.v[2]==0
 && ch_n.v[3]==0 && ch_n.v[4]==1 && ch_n.v[5]==0)
return 39;
if (ch_n.v[0]==0 && ch_n.v[1]==0 && ch_n.v[2]==1
 && ch_n.v[3]==0 && ch_n.v[4]==0 && ch_n.v[5]==1)
return 42;
if (ch_n.v[0]==0 && ch_n.v[1]==0 && ch_n.v[2]==0
 && ch_n.v[3]==1 && ch_n.v[4]==1 && ch_n.v[5]==0)
return 41;
if (ch_n.v[0]==1 && ch_n.v[1]==0 && ch_n.v[2]==0
 && ch_n.v[3]==0 && ch_n.v[4]==0 && ch_n.v[5]==1)
return 40;
//off

if (ch_n.v[0]==1 && ch_n.v[1]==0 && ch_n.v[2]==1
 && ch_n.v[3]==1 && ch_n.v[4]==0 && ch_n.v[5]==1)
return 55;
if (ch_n.v[0]==0 && ch_n.v[1]==1 && ch_n.v[2]==0
 && ch_n.v[3]==1 && ch_n.v[4]==1 && ch_n.v[5]==1)
return 60;
if (ch_n.v[0]==1 && ch_n.v[1]==0 && ch_n.v[2]==1
 && ch_n.v[3]==0 && ch_n.v[4]==1 && ch_n.v[5]==1)
return 59;
if (ch_n.v[0]==1 && ch_n.v[1]==1 && ch_n.v[2]==0
 && ch_n.v[3]==1 && ch_n.v[4]==1 && ch_n.v[5]==0)
return 58;
if (ch_n.v[0]==1 && ch_n.v[1]==1 && ch_n.v[2]==1
 && ch_n.v[3]==0 && ch_n.v[4]==0 && ch_n.v[5]==1)
return 57;
if (ch_n.v[0]==0 && ch_n.v[1]==0 && ch_n.v[2]==1
 && ch_n.v[3]==1 && ch_n.v[4]==1 && ch_n.v[5]==0)
return 56;
//off2
//1

if (ch_n.v[0]==0 && ch_n.v[1]==1 && ch_n.v[2]==0
 && ch_n.v[3]==1 && ch_n.v[4]==0 && ch_n.v[5]==1)
{
return 45;
}
if (ch_n.v[0]==1 && ch_n.v[1]==0 && ch_n.v[2]==0
 && ch_n.v[3]==1 && ch_n.v[4]==0 && ch_n.v[5]==1)
{
return 46;
}
//2
if (ch_n.v[0]==0 && ch_n.v[1]==1 && ch_n.v[2]==0
 && ch_n.v[3]==0 && ch_n.v[4]==1 && ch_n.v[5]==1)
{
return 47;
}
if (ch_n.v[0]==0 && ch_n.v[1]==0 && ch_n.v[2]==1
 && ch_n.v[3]==0 && ch_n.v[4]==1 && ch_n.v[5]==1)
{
return 48;
}
//3
if (ch_n.v[0]==1 && ch_n.v[1]==0 && ch_n.v[2]==1
 && ch_n.v[3]==0 && ch_n.v[4]==1 && ch_n.v[5]==0)
{
return 49;
}
if (ch_n.v[0]==1 && ch_n.v[1]==0 && ch_n.v[2]==0
 && ch_n.v[3]==1 && ch_n.v[4]==1 && ch_n.v[5]==0)
{
return 50;
}
//4
if (ch_n.v[0]==1 && ch_n.v[1]==1 && ch_n.v[2]==0
 && ch_n.v[3]==1 && ch_n.v[4]==0 && ch_n.v[5]==0)
{
return 52;
}
if (ch_n.v[0]==1 && ch_n.v[1]==1 && ch_n.v[2]==0
 && ch_n.v[3]==0 && ch_n.v[4]==0 && ch_n.v[5]==1)
{
return 51;
}
//5
if (ch_n.v[0]==0 && ch_n.v[1]==1 && ch_n.v[2]==1
 && ch_n.v[3]==0 && ch_n.v[4]==0 && ch_n.v[5]==1)
{
return 54;
}
if (ch_n.v[0]==0 && ch_n.v[1]==1 && ch_n.v[2]==1
 && ch_n.v[3]==0 && ch_n.v[4]==1 && ch_n.v[5]==0)
{
return 53;
}
//6
if (ch_n.v[0]==1 && ch_n.v[1]==0 && ch_n.v[2]==1
 && ch_n.v[3]==1 && ch_n.v[4]==0 && ch_n.v[5]==0)
{
return 43;
}
if (ch_n.v[0]==0 && ch_n.v[1]==0 && ch_n.v[2]==1
 && ch_n.v[3]==1 && ch_n.v[4]==1 && ch_n.v[5]==0)
{
return 44;
}
return 0;
}

checkmas CheckHexesForest(int u, int v)
{
checkmas ch_n;
for (int i=0; i < 6; i++)
ch_n.v[i]=0;
if (mainmass[u][v-1].forest==1)
ch_n.v[0]=1;
if (mainmass[u][v+1].forest==1)
ch_n.v[3]=1;

if (u%2==0)
{
if (mainmass[u+1][v].forest==1)
ch_n.v[2]=1;
if (mainmass[u+1][v-1].forest==1)
ch_n.v[1]=1;
if (mainmass[u-1][v].forest==1)
ch_n.v[5]=1;
if (mainmass[u-1][v-1].forest==1)
ch_n.v[4]=1;
}

if (u%2==1)
{
if (mainmass[u+1][v+1].forest==1)
ch_n.v[2]=1;
if (mainmass[u+1][v].forest==1)
ch_n.v[1]=1;
if (mainmass[u-1][v+1].forest==1)
ch_n.v[5]=1;
if (mainmass[u-1][v].forest==1)
ch_n.v[4]=1;
}

return ch_n;

}



void LoadMap2(char *p_filename)
{
int numH, numV;
char csymbol;
int cbuffer;
FILE *l_file;

if ((l_file=fopen (p_filename, "r"))== NULL)
MessageBox(NULL, "���������� ������� �����", "INFO", MB_OK);
//-----------------------------------------------------BEGIN
csymbol=fgetc(l_file);
RockArray[0][0] = atoi(&csymbol);

fgetc(l_file);

csymbol=fgetc(l_file);
cbuffer=atoi(&csymbol);
csymbol=fgetc(l_file);
GridWei=atoi(&csymbol)+cbuffer*10;

csymbol=fgetc(l_file);
cbuffer=atoi(&csymbol);
csymbol=fgetc(l_file);
GridHei=atoi(&csymbol)+cbuffer*10;

fgetc(l_file);
numV=0;
//-----------------------------------------------------LAND
while (numV<GridHei) {
	numH=0;
while (numH<GridWei) {
	csymbol=fgetc(l_file);
	cbuffer=atoi(&csymbol);
if (cbuffer==0) {
	mainmass[numH][numV].plain=1;
	mainmass[numH][numV].passab=0;
	//RockArray[numH][numV] = 0;
}
if (cbuffer==1) {
	mainmass[numH][numV].grass=1;
	mainmass[numH][numV].passab=0;
	//RockArray[numH][numV] = 1;
}
numH++;
}
if (numV<GridHei && numH==GridWei)
	fgetc(l_file);
numV++;
}
numV=0;
//-----------------------------------------------------MOUNTS
while (numV<GridHei) {
	numH=0;
while (numH<GridWei) {
	csymbol=fgetc(l_file);
	cbuffer=atoi(&csymbol);
if (cbuffer==1) {
	mainmass[numH][numV].hill=1;
	mainmass[numH][numV].passab=1;
	//RockArray[numH][numV] = 1;
}
if (cbuffer==2) {
	mainmass[numH][numV].rock=1;
	mainmass[numH][numV].passab=3;
	//RockArray[numH][numV] = 2;
	}
numH++;
}
if (numV<GridHei && numH==GridWei)
	fgetc(l_file);
numV++;
}
numV=0;
//-----------------------------------------------------FOREST
while (numV<GridHei) {
	numH=0;
while (numH<GridWei) {
	csymbol=fgetc(l_file);
	cbuffer=atoi(&csymbol);
if (cbuffer==1) {
	mainmass[numH][numV].forest=1;
	mainmass[numH][numV].passab++;
	RockArray[numH][numV] = 1;
}
numH++;
}
if (numV<GridHei && numH==GridWei)
	fgetc(l_file);
numV++;
}
numV=0;
//-----------------------------------------------------ROCKS
while (numV<GridHei) {
	numH=0;
while (numH<GridWei) {
	csymbol=fgetc(l_file);
	cbuffer=atoi(&csymbol);
	csymbol=fgetc(l_file);
	RockArray[numH][numV] = cbuffer*10+atoi(&csymbol);;

numH++;
}
if (numV<GridHei && numH==GridWei)
	fgetc(l_file);
numV++;
}
numV=0;



fclose (l_file);
}


float GetZ(Vector2f p)
{
Vector2f ret;
Vector2f ret2;
ret.x=p.x/12.5f;
ret.y=p.y/12.5f;
int quad = (int)ret.x*GridSetHei+(int)ret.y;
if ((int)ret.x!=0)
ret2.x=fmod((float)ret.x,(int)ret.x);
else
ret2.x=ret.x;
if ((int)ret.y!=0)
ret2.y=fmod((float)ret.y,(int)ret.y);
else
ret2.y=ret.y;

unsigned int pol=0;

if (ret2.y>=0.5f)
pol+=4;
else
if (ret2.x>=0.5f)
pol+=2;
if (ret2.x<0.5f && ret2.y>=0.5f)
pol+=2;

if (ret2.x>=ret2.y)
{
if (ret2.x<0.5) pol++;
else
if (ret2.y<0.5 && ret2.y+ret2.x>1.0f) pol++;
}
else
{
if (ret2.y>=0.5 && ret2.y+ret2.x<1.0f) pol++;
else
if (ret2.x>=0.5) pol++;
}

int fin = pol+quad*8; 
float Z;
if (fin>0)
{
Vector3f a=Vector3f(MapVertexArray[MapFaceArray[fin].a].x, MapVertexArray[MapFaceArray[fin].a].y, MapVertexArray[MapFaceArray[fin].a].z);
Vector3f b=Vector3f(MapVertexArray[MapFaceArray[fin].b].x, MapVertexArray[MapFaceArray[fin].b].y, MapVertexArray[MapFaceArray[fin].b].z);
Vector3f c=Vector3f(MapVertexArray[MapFaceArray[fin].c].x, MapVertexArray[MapFaceArray[fin].c].y, MapVertexArray[MapFaceArray[fin].c].z);

float A, B, C, D;
            
A=(b.y-a.y)*(c.z-a.z)-(b.z-a.z)*(c.y-a.y);
B=(b.z-a.z)*(c.x-a.x)-(b.x-a.x)*(c.z-a.z);
C=(b.x-a.x)*(c.y-a.y)-(b.y-a.y)*(c.x-a.x);
D=-(A*a.x+B*a.y+C*a.z);
Z = -(A*p.x+B*p.y+D)/C;
}

else
Z = 0.0f;


return Z;
}



