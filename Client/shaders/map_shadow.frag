varying vec4 pos;
varying vec4 lpos;
varying vec3 lightDir;


vec4 finalcolor;
float NdotL;


uniform sampler2D plainTexture;
uniform sampler2D rockTexture;
uniform sampler2D grassTexture;

uniform sampler2D hexSample;
uniform sampler2D grassNormal;
uniform sampler2D rockNormal;

uniform sampler2D allNormal;
uniform sampler2D allBlend;
uniform sampler2D allBlend2;

uniform sampler2DShadow shadowMap;

void main( void )
{
vec3 smcoord = lpos.xyz / lpos.w;
float shadow = shadow2D(shadowMap, smcoord).x;


vec4 Tex[6];
Tex[0]=texture2D(rockTexture,  gl_TexCoord[2].xy);
Tex[1]=texture2D(plainTexture, gl_TexCoord[0].xy);
Tex[2]=texture2D(allBlend,	   gl_TexCoord[1].xy);
Tex[3]=texture2D(hexSample,    gl_TexCoord[3].xy);
Tex[4]=texture2D(grassTexture, gl_TexCoord[0].xy);
Tex[5]=texture2D(allBlend2,    gl_TexCoord[1].xy);
//Tex[6]=texture2D(allNormal,    gl_TexCoord[1].xy);

float perspective_far = 30000.0;
float fog_cord = (gl_FragCoord.z / gl_FragCoord.w + gl_FragCoord.y * 5.0) / perspective_far;
float fog_density = 3.0;
float fog = fog_cord * fog_density;
vec4 fog_color = vec4(1.0, 1.0, 1.0, 1.0);

vec4 hex_color = vec4 (0.98, 1.0, 0.44, 1.0);
vec3  fvLightDirection = normalize(lightDir);
vec3 fvNormal= normalize(texture2D(grassNormal,gl_TexCoord[4].xy).xyz * 2.0*Tex[5].r  + texture2D(allNormal,gl_TexCoord[1].xy).xyz*Tex[5].b*2.0  + texture2D(rockNormal,gl_TexCoord[7].xy).xyz * 2.0*Tex[5].g - 1.0 );
NdotL = max(dot(fvNormal,fvLightDirection),0.0);

finalcolor = (Tex[4]*Tex[2].g+Tex[1]*Tex[2].b+Tex[0]*Tex[2].r);
finalcolor *= ((gl_LightSource[0].diffuse * NdotL)*shadow + gl_LightSource[0].ambient);
//finalcolor *= ((gl_LightSource[0].diffuse * NdotL) + gl_LightSource[0].ambient);
finalcolor+=hex_color*Tex[3].a;
gl_FragColor = mix(fog_color, finalcolor, clamp(1.4-fog,0.0,1.0));
//gl_FragColor=Tex[6];

  
  
  
  
  
}
