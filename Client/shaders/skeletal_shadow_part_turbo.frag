uniform float fSpecularPower;
uniform float shadow_on;

varying vec3 lightDir;
varying vec3 ViewDirection;
varying vec2 TexCoord1;
varying vec4 lpos;
varying vec3 normal;

uniform sampler2D diffuseMap;
uniform sampler2D normalMap;
uniform sampler2D emissionMap;
uniform sampler2DShadow shadowMap;

void main( void )
{
vec4 endcolor;
if (shadow_on==1.0)
{
   vec4 finalcolor;
   float perspective_far = 30000.0;
   float fog_cord = (gl_FragCoord.z / gl_FragCoord.w + gl_FragCoord.y * 5.0) / perspective_far;
   float fog_density = 3.0;
   float fog = fog_cord * fog_density;
   vec4 fog_color = vec4(1.0, 1.0, 1.0, 1.0);

   vec3 smcoord = lpos.xyz / lpos.w;
   
   float shadow = shadow2D(shadowMap, smcoord).x;
   float emission = texture2D(emissionMap, TexCoord1.xy).a;
 
   vec3  fvLightDirection = normalize(lightDir);
   vec4  fvNormalColor=texture2D(normalMap,TexCoord1.xy);
   vec3  fvNormal = normalize( vec3( fvNormalColor * 2.0 ) - 1.0 );
   float NdotL = max(dot(fvNormal,fvLightDirection),0.0);

   vec3  fvReflection     = normalize( ( ( 2.0 * fvNormal ) * NdotL ) - fvLightDirection ); 
   vec3  fvViewDirection  = normalize( ViewDirection );
   float fRDotV           = max( 0.0, dot( fvReflection, fvViewDirection ) );
  
   vec4  fvBaseColor = texture2D(diffuseMap,TexCoord1.xy);
   
   vec4  fvTotalAmbient   = gl_LightSource[1].ambient * fvBaseColor; 
   vec4  fvTotalDiffuse   = gl_LightSource[1].diffuse * NdotL * fvBaseColor; 
   vec4  fvTotalSpecular  = gl_LightSource[1].specular * ( pow( fRDotV, fSpecularPower ) );
   fvTotalSpecular*=fvBaseColor.a;
   finalcolor = ( fvTotalAmbient*emission + (fvTotalDiffuse + fvTotalSpecular)) + fvNormalColor.a;
   
   endcolor = mix(fog_color, finalcolor, clamp(1.4-fog,0.0,1.0));
  }
if (shadow_on==0.0)
{
	endcolor = vec4(0.0,0.0,0.0,1.0);
}
gl_FragColor=endcolor;
}
