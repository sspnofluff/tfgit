in vec3  Position;                                            
in vec2  TexCoord;                                           
in vec3  Normal; 
in ivec4 BoneIDs;
in vec4  Weights;

uniform mat4 lightMatrix;
uniform vec3 lightPos;
uniform vec3 fvEyePosition;
uniform mat4 gBones;
uniform float shadow_on;

varying vec3 lightDir;
varying vec3 ViewDirection;
varying vec4 lpos;
varying vec3 normal;
varying vec2 TexCoord1;


void main(void)
{     

	//mat4 BoneTransform	= gBones;
    
    //vec4 PosL      = BoneTransform * vec4(Position, 1.0);
	vec4 PosL      = vec4(Position, 1.0);
	gl_Position = gl_ModelViewProjectionMatrix * PosL;
	if (shadow_on==1.0)
	{
	//vec4 NormalL   = BoneTransform * vec4(Normal, 0.0);
	vec4 NormalL   = vec4(Normal, 0.0);
	vec3 normal = normalize(gl_NormalMatrix * NormalL.xyz);
	vec3 fvBinormal = cross(normal, vec3(0.0, 0.0, 1.0));
	vec3 fvTangent = cross(normal, vec3(0.0, 1.0, 0.0));
	
	lpos = lightMatrix * gl_ModelViewMatrix * PosL;
		
	vec3 fvLightDirection = normalize(vec3(gl_LightSource[1].position-PosL));
	vec3 fvViewDirection  = normalize(vec3(fvEyePosition - PosL.xyz));

	lightDir.x  = dot( fvTangent, fvLightDirection.xyz );
	lightDir.y  = dot( fvBinormal, fvLightDirection.xyz );
	lightDir.z  = dot( normal, fvLightDirection.xyz );
	
	

    ViewDirection.x  = dot( fvTangent, fvViewDirection );
    ViewDirection.y  = dot( fvBinormal, fvViewDirection );
    ViewDirection.z  = dot( normal, fvViewDirection );
	
	TexCoord1 = TexCoord;
	}
	
}



