in vec3  Position;     
in vec3  Position2;                                        
in vec2  TexCoord;                                           
in vec3  Normal; 
in vec3  Normal2;

uniform vec3 fvEyePosition;
uniform mat4 lightMatrix;
uniform vec3 lightPos;
uniform mat4 gBones;
uniform float shadow_on;
uniform float time;


varying vec3 reflective, normal;
varying vec3 lightDir;
varying vec3 ViewDirection;
varying vec4 lpos;
varying vec2 TexCoord1;

void main(void)
{     

	mat4 BoneTransform	= gBones;
    vec4 PosL      = BoneTransform * vec4(Position, 1.0);
	vec4 PosL2		= BoneTransform * vec4(Position2, 1.0);
	vec4 PosL3 = mix(PosL,PosL2,time);
	gl_Position = gl_ModelViewProjectionMatrix * PosL3;
	
	vec4 NormalL   = BoneTransform * vec4(Normal, 0.0);
	vec4 NormalL2   = BoneTransform * vec4(Normal2, 0.0);
	vec4 NormalL3   = mix(NormalL,NormalL2,time);
	
    
	normal = normalize(gl_NormalMatrix * NormalL3.xyz);
	vec3 fvBinormal = cross(normal, vec3(0.0, 0.0, 1.0));
	vec3 fvTangent = cross(normal, vec3(0.0, 1.0, 0.0));
	
	vec3 pos = vec3(gl_ModelViewMatrix * PosL3* 25.0); 
	reflective = (pos - fvEyePosition);
	
	vec3 fvLightDirection = normalize(vec3(gl_LightSource[1].position-PosL3));
	vec3 fvViewDirection  = normalize(vec3(fvEyePosition - PosL3.xyz));
	
	lightDir.x  = dot( fvTangent, fvLightDirection.xyz );
	lightDir.y  = dot( fvBinormal, fvLightDirection.xyz );
	lightDir.z  = dot( normal, fvLightDirection.xyz );

    ViewDirection.x  = dot( fvTangent, fvViewDirection );
    ViewDirection.y  = dot( fvBinormal, fvViewDirection );
    ViewDirection.z  = dot( normal, fvViewDirection );
	
	TexCoord1 = TexCoord;
	
}



