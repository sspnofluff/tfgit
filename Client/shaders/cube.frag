varying vec3 reflective;
uniform samplerCube cubeMap;

void main( void )
{
const float reflect_factor = 0.5;
vec3 cube_color = textureCube(cubeMap, gl_TexCoord[1].xyz).rgb; 
vec3 base_color = vec3(0.4, 0.4, 0.4);
gl_FragColor = vec4( mix(base_color, cube_color, reflect_factor), 1.0);
}
