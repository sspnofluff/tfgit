in vec3  Position;                                            
in vec2  TexCoord;                                           
in vec3  Normal; 
in ivec4 BoneIDs;
in vec4  Weights;

const int MAX_BONES = 100;

uniform mat4 gBones[MAX_BONES];

void main(void)
{       
    mat4 BoneTransform	= gBones[BoneIDs.x]*Weights.x;
    BoneTransform     += gBones[BoneIDs.y]*Weights.y;
    BoneTransform     += gBones[BoneIDs.z]*Weights.z;
	BoneTransform     += gBones[BoneIDs.w]*Weights.w;

    vec4 PosL      = BoneTransform * vec4(Position, 1.0);
	gl_Position = gl_ModelViewProjectionMatrix * PosL;
		
}



