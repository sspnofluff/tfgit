in vec3  Position;                                            
in vec2  TexCoord;                                           
in vec3  Normal; 

uniform vec3 fvEyePosition;

uniform mat4 lightMatrix;
uniform vec3 lightPos;

varying vec4 lpos;
varying vec3 normal;
varying vec3 lightDir;
varying vec3 ViewDirection;
varying vec2 Texcoord1;


void main( void )
{
  
  
  normal = normalize(gl_NormalMatrix * Normal);
  vec3 fvBinormal = cross(normal, vec3(0.0, 0.0, 1.0));
  vec3 fvTangent = cross(normal, vec3(0.0, 1.0, 0.0));
 
  vec4 pos = vec4(Position, 1.0);
  gl_Position = gl_ModelViewProjectionMatrix * pos;
  vec3 fvLightDirection = normalize(vec3(gl_LightSource[1].position-pos));
  vec3 fvViewDirection  = normalize(fvEyePosition - pos.xyz);
  
  lpos = lightMatrix * gl_ModelViewMatrix * pos;
    
  lightDir.x  = dot( fvTangent, fvLightDirection.xyz );
  lightDir.y  = dot( fvBinormal, fvLightDirection.xyz );
  lightDir.z  = dot( normal, fvLightDirection.xyz );

   ViewDirection.x  = dot( fvTangent, fvViewDirection );
   ViewDirection.y  = dot( fvBinormal, fvViewDirection );
   ViewDirection.z  = dot( normal, fvViewDirection );

  
  Texcoord1 = TexCoord;

}
