uniform mat4 lightMatrix;


varying vec4 pos;
varying vec4 lpos;
varying vec3 normal;
varying vec3 lightDir;


attribute vec3 rm_Binormal;
attribute vec3 rm_Tangent;

void main( void )
{
  gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;

  vec4 pos2=vec4(gl_Vertex);
  vec4 pos3=vec4(gl_Vertex);
  pos3.x+=349.0;
  pos3.y+=440.0;
  
  normal = gl_NormalMatrix * gl_Normal;
  vec3 fvBinormal = gl_NormalMatrix * rm_Binormal; 
  vec3 fvTangent = gl_NormalMatrix * rm_Tangent; 
  
  pos = gl_ModelViewMatrix * gl_Vertex;
  lpos = lightMatrix * pos;
  
  
  vec3 fvLightDirection = normalize(vec3(gl_LightSource[0].position-pos));
  
  lightDir.x  = dot( fvTangent, fvLightDirection.xyz );
  lightDir.y  = dot( fvBinormal, fvLightDirection.xyz );
  lightDir.z  = dot( normal, fvLightDirection.xyz );
  
  gl_TexCoord[0]=0.00805*pos2;
  gl_TexCoord[4]=0.00905*pos2;
  gl_TexCoord[1]=0.000201 * pos2;
  gl_TexCoord[2]=0.0075*pos2;
  gl_TexCoord[3]=0.0009521*pos3;
  gl_TexCoord[5]=0.009*pos2;
  gl_TexCoord[6]=0.0001*pos2;
  gl_TexCoord[7]=0.01605*pos2;
  
}

