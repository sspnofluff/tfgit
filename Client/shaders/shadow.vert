uniform vec4 fvEyePosition;
uniform mat4 lightMatrix;
uniform vec3 lightPos;
uniform vec3 lightDir;

varying vec4 lpos;
varying vec3 normal;
varying vec3 light_vec;
varying vec3 light_dir;
varying vec3 ViewDirection;

void main(void)
{
	
	gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;
	
	vec4 vpos = gl_ModelViewMatrix * gl_Vertex;
	lpos = lightMatrix * vpos;
	gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;

	light_vec = vpos.xyz - lightPos;
	light_dir = gl_NormalMatrix * lightDir;
	normal = gl_NormalMatrix * gl_Normal;
	vec3 fvBinormal = cross(normal, vec3(0.0, 0.0, 1.0));
    vec3 fvTangent = cross(normal, vec3(0.0, 1.0, 0.0));
	
	vec3 fvViewDirection  = normalize(vec3(fvEyePosition - vpos));
	
		
	ViewDirection.x  = dot( fvTangent, fvViewDirection );
    ViewDirection.y  = dot( fvBinormal, fvViewDirection );
    ViewDirection.z  = dot( normal, fvViewDirection );
	
		
	gl_FrontColor = gl_Color;
	
	gl_TexCoord[0] = gl_MultiTexCoord0;
}
