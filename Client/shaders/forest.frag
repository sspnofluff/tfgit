uniform sampler2D forestMap;
void main( void )
{
float perspective_far = 30000.0;
float fog_cord = (gl_FragCoord.z/gl_FragCoord.w+gl_FragCoord.y*5.0) / perspective_far;
float fog_density = 3.0;
float fog = fog_cord * fog_density;
vec4 fog_color = vec4(1.0,1.0,1.0,1.0);

vec4 finalcolor = texture2D(forestMap,gl_TexCoord[0].xy);  // Add texel color to vertex color

if (finalcolor.a<0.5)
discard;

gl_FragColor=mix(fog_color, finalcolor, clamp(1.4-fog,0.0,1.0));
}
