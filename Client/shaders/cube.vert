



uniform vec3 fvEyePosition;

varying vec3 reflective;


void main(void)
{     


	gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;
	gl_TexCoord[0] = gl_MultiTexCoord0 * 2.0;
	
	vec4 WorldPos = gl_ModelViewMatrix *  gl_Vertex;	
	
	// find world space normal.
	vec3 N = normalize( gl_NormalMatrix * gl_Normal ); 
	
	// find world space eye vector.
	vec3 E = normalize( (WorldPos.xyz - fvEyePosition) );	
	
	// calculate the reflection vector in world space.
	gl_TexCoord[1].xyz = reflect( -E, N ); 
	
	
	
	
}

/*
vec3 normal = normalize(gl_NormalMatrix * gl_Normal);
	vec3 pos = vec3(gl_ModelViewMatrix * gl_Vertex);     
	vec3 eyevec = normalize(fvEyePosition - pos);
	reflective=reflect(normalize(eyevec) , normalize(normal));
	*/

