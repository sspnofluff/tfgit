uniform sampler2DShadow shadowMap;
uniform sampler2D diffuseMap;

varying vec4 lpos;
varying vec3 normal;
varying vec3 light_vec;
varying vec3 light_dir;

const float inner_angle = 0.809017;
const float outer_angle = 0.707107;

void main( void )
{
  vec4 my_vec;
  my_vec = vec4(1.0f, 0.0f, 0.0f, 1.0f);
  //vec4 finalcolor = texture2D(diffuseMap,gl_TexCoord[0].xy);
  //vec4 finalcolor = my_vec;
  //gl_FragColor = finalcolor;

 
	vec3 smcoord = lpos.xyz / lpos.w;
	//smcoord = clamp(smcoord, 0.5, 1.0);
	float shadow = shadow2D(shadowMap, smcoord).x;
	
	vec3 lvec = normalize(light_vec);
	float diffuse = max(dot(-lvec, normalize(normal)), 0.0);
	float angle = dot(lvec, normalize(light_dir));
	float spot = clamp((angle - outer_angle) / (inner_angle - outer_angle), 0.0, 1.0);
	
	vec4  fvBaseColor = texture2D(diffuseMap,gl_TexCoord[0].xy);
	//if (shadow<1)
	//vec4 dark = shadow * gl_LightSource[1].ambient;
	//else
	//vec4 dark = shadow;
	//gl_FragColor = shadow2D(shadowMap, smcoord).x;
	gl_FragColor = fvBaseColor * shadow;// * diffuse + fvBaseColor * gl_LightSource[0].ambient;
	//gl_FragColor = vec4(gl_Color.xyz * shadow, 1);
    
	
 }
