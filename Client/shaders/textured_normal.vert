uniform vec3 fvEyePosition;

varying vec3 lightDir;
varying vec3 ViewDirection;


void main( void )
{
  vec4 pos;
  vec3 normal;
  normal = gl_NormalMatrix * gl_Normal;
  vec3 fvBinormal = cross(normal, vec3(0.0, 0.0, 1.0));
  vec3 fvTangent = cross(normal, vec3(0.0, 1.0, 0.0));
 
  pos = gl_ModelViewMatrix * gl_Vertex;
  gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;
  vec3 fvLightDirection = normalize(vec3(gl_LightSource[1].position-pos));
  vec3 fvViewDirection  = normalize(vec3(fvEyePosition - pos));

  lightDir.x  = dot( fvTangent, fvLightDirection.xyz );
  lightDir.y  = dot( fvBinormal, fvLightDirection.xyz );
  lightDir.z  = dot( normal, fvLightDirection.xyz );

   ViewDirection.x  = dot( fvTangent, fvViewDirection );
   ViewDirection.y  = dot( fvBinormal, fvViewDirection );
   ViewDirection.z  = dot( normal, fvViewDirection );

  
  gl_TexCoord[0] = gl_MultiTexCoord0;

}
