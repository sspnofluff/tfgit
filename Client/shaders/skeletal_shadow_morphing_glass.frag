
uniform float fSpecularPower;
uniform samplerCube cubeMap;
uniform sampler2DShadow shadowMap;
uniform sampler2D glass_ao;
uniform float shadow_on;

varying vec3 reflective, normal;
varying vec3 lightDir;
varying vec3 ViewDirection;
varying vec4 lpos;
varying vec2 TexCoord1;

void main( void )
{
vec4 endcolor;
if (shadow_on==1.0)
{
float perspective_far = 30000.0;
float fog_cord = (gl_FragCoord.z / gl_FragCoord.w + gl_FragCoord.y * 5.0) / perspective_far;
float fog_density = 3.0;
float fog = fog_cord * fog_density;
vec4 fog_color = vec4(1.0, 1.0, 1.0, 1.0);

vec3 smcoord = lpos.xyz / lpos.w;
float shadow = shadow2D(shadowMap, smcoord).x;

vec3 reflectiveN=normalize(reflective);
vec3 normalN=normalize(normal);
vec3 reflVec = reflect(reflectiveN , normalN);

float ambientocclusion = texture2D(glass_ao, TexCoord1.xy).a;

 vec3  fvLightDirection = normalize(lightDir);
 float NdotL = max(dot(normal,fvLightDirection),0.0);

   vec3  fvReflection     = normalize( ( ( 2.0 * normal ) * NdotL ) - fvLightDirection ); 
   vec3  fvViewDirection  = normalize( ViewDirection );
   float fRDotV           = max( 0.0, dot( fvReflection, fvViewDirection ) );
  
   //vec3 baseColor=vec3(0.8,0.8,0.8);
   vec3 envColor = textureCube(cubeMap, reflectiveN).rgb;
   //vec4 fullColor = vec4(mix(envColor, baseColor, 0.5), 1.0);
   vec4 fullColor = vec4(envColor, 1.0);
   
   vec4  fvTotalAmbient   = gl_LightSource[1].ambient * fullColor; 
   vec4  fvTotalDiffuse   = gl_LightSource[1].diffuse * fullColor * fullColor; 
   vec4  fvTotalSpecular  = gl_LightSource[1].specular * ( pow( fRDotV, fSpecularPower ) );
   
   vec4 finalcolor = ( fvTotalAmbient*ambientocclusion + (fvTotalDiffuse + fvTotalSpecular*0.3) * shadow);
   endcolor =  mix(fog_color, finalcolor, clamp(1.4-fog,0.0,1.0));
}
	

if (shadow_on==0.0)
{
	endcolor = vec4(0.0,0.0,0.0,1.0);
}
gl_FragColor=endcolor;

}
