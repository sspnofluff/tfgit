#version 120

varying vec4 pos;
varying vec3 lightDir;

//float distance;
vec4 finalcolor;
float NdotL;

float attenuation;

uniform sampler2D textureGrain;
uniform sampler2D textureRock;
uniform sampler2D textureBlend;
uniform sampler2D textureHex;
uniform sampler2D textureNorm;
uniform sampler2D textureNorm2;
uniform sampler2D textureGrass;
uniform sampler2D textureBignorm;
uniform sampler2D textureBlend2;

void main( void )
{
vec4 Tex[6];
Tex[0]=texture2D(textureRock,gl_TexCoord[2].xy);
Tex[1]=texture2D(textureGrain,gl_TexCoord[0].xy);
Tex[2]=texture2D(textureBlend,gl_TexCoord[1].xy);
Tex[3]=texture2D(textureHex,gl_TexCoord[3].xy);
Tex[4]=texture2D(textureGrass,gl_TexCoord[0].xy);
Tex[5]=texture2D(textureBlend2,gl_TexCoord[1].xy);


float perspective_far = 30000.0;
float fog_cord = (gl_FragCoord.z / gl_FragCoord.w  + gl_FragCoord.y*5) / perspective_far;
float fog_density = 3.0;
float fog = fog_cord * fog_density;
vec4 fog_color = vec4(1,1,1,0);


  vec4 my_vec;
  my_vec = vec4(1.0, 1.0, 1.0, 1.0);
  vec3  fvLightDirection = normalize(lightDir);
  vec3 fvNormal= normalize(  texture2D(textureNorm,gl_TexCoord[4].xy).xyz * 2.0*Tex[5].r  + texture2D(textureBignorm,gl_TexCoord[1].xy).xyz*Tex[5].b*2.0  + texture2D(textureNorm2,gl_TexCoord[7].xy).xyz * 2.0*Tex[5].g - 1.0 );
  //vec3 fvNormal= normalize( mix(texture2D(textureBignorm,gl_TexCoord[1].xy).xyz * 2.0, texture2D(textureNorm,gl_TexCoord[4].xy).xyz * 2.0*Tex[5].r+texture2D(textureNorm2,gl_TexCoord[7].xy).xyz * 2.0*Tex[5].g,step(Tex[5].b,0.5)) - 1.0 );
  //lightDir = normalize(vec3(gl_LightSource[0].position-pos));
  //distance = length(vec3(gl_LightSource[0].position-pos));
  NdotL = max(dot(fvNormal,fvLightDirection),0.0);

  //attenuation = 1.00 / (gl_LightSource[0].constantAttenuation +
   //gl_LightSource[0].linearAttenuation * distance +
   //gl_LightSource[0].quadraticAttenuation * distance * distance);

 finalcolor=Tex[4]*Tex[2].g+Tex[1]*Tex[2].b+Tex[0]*Tex[2].r+my_vec*Tex[2].a;
  //finalcolor=mix(Tex[0],Tex[1]*Tex[2].b+Tex[4]*Tex[2].g+Tex[1]*Tex[2].r,step(Tex[2].r,0.5));
  finalcolor *=(gl_LightSource[0].diffuse * NdotL + gl_LightSource[0].ambient)*shadow;

  finalcolor+=Tex[3]*Tex[3].a;
  //finalcolor=Tex[5];
//gl_FragColor = finalcolor;
 gl_FragColor = mix(fog_color, finalcolor, clamp(1.4-fog,0.0,1.0));
  
  
  
  
  
  
}
