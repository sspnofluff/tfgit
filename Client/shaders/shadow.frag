uniform float fSpecularPower;
uniform sampler2DShadow shadowMap;
uniform sampler2D diffuseMap;

varying vec4 lpos;
varying vec3 normal;
varying vec3 light_vec;
varying vec3 light_dir;
varying vec3 ViewDirection;

const float inner_angle = 0.809017;
const float outer_angle = 0.707107;

void main (void)
{
	
	vec3 smcoord = lpos.xyz / lpos.w;
	//smcoord = clamp(smcoord, 0.5, 1.0);
	float shadow = shadow2D(shadowMap, smcoord).x;
	
	float fNDotL           = dot( normal, light_dir ); 
   
    vec3  fvReflection     = normalize(( 2.0 * normal  * fNDotL ) - light_dir); 
    vec3  fvViewDirection  = normalize( ViewDirection );
    float fRDotV           = max( 0.0, dot( fvReflection, fvViewDirection ) ); 

	
	
	vec3 lvec = normalize(light_vec);
	float diffuse = max(dot(-lvec, normalize(normal)), 0.0);
	float angle = dot(lvec, normalize(light_dir));
	float spot = clamp((angle - outer_angle) / (inner_angle - outer_angle), 0.0, 1.0);
	
	vec4  fvBaseColor = texture2D(diffuseMap,gl_TexCoord[0].xy);
	
	vec4  fvTotalSpecular  = gl_LightSource[1].specular * ( pow( fRDotV, fSpecularPower ) );
    fvTotalSpecular*=fvBaseColor.a;
	
	gl_FragColor = fvBaseColor * diffuse * shadow;// + fvBaseColor * gl_LightSource[1].ambient;
	 //gl_FragColor = vec4(gl_Color.xyz * shadow, 1);
	//vec4 lvec2 = vec4(1.0, 0.0, 0.0, 1.0);
	//gl_FragColor = lvec2;
}
