uniform float fSpecularPower;

varying vec3 lightDir;
varying vec3 ViewDirection;

uniform sampler2D diffuseMap;
uniform sampler2D normalMap;

void main( void )
{

   vec3  fvLightDirection = normalize(lightDir);
   vec4  fvNormalColor=texture2D(normalMap,gl_TexCoord[0].xy);
   vec3  fvNormal = normalize( vec3( fvNormalColor * 2.0 ) - 1.0 );
   float NdotL = max(dot(fvNormal,fvLightDirection),0.0);

   vec3  fvReflection     = normalize( ( ( 2.0 * fvNormal ) * NdotL ) - fvLightDirection ); 
   vec3  fvViewDirection  = normalize( ViewDirection );
   float fRDotV           = max( 0.0, dot( fvReflection, fvViewDirection ) );
  
   vec4  fvBaseColor = texture2D(diffuseMap,gl_TexCoord[0].xy);
   
   vec4  fvTotalAmbient   = gl_LightSource[1].ambient * fvBaseColor; 
   vec4  fvTotalDiffuse   = gl_LightSource[1].diffuse * NdotL * fvBaseColor; 
   vec4  fvTotalSpecular  = gl_LightSource[1].specular * ( pow( fRDotV, fSpecularPower ) );
   fvTotalSpecular*=fvBaseColor.a;
   if (fvNormalColor.a>0.9)
   gl_FragColor = fvBaseColor;
   else
   gl_FragColor = ( fvTotalAmbient + fvTotalDiffuse + fvTotalSpecular);
}
