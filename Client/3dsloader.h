#ifndef __3DSLOADER_H__
#define __3DSLOADER_H__

#include "texture.h"
#include <atlstr.h>
#include <vector>
#include "math_3d.h"

#define MAX_VERTICES 100000 // Max number of vertices (for each object)
#define MAX_POLYGONS 8000 // Max number of polygons (for each object)
#define Mag(Normal) (sqrt(Normal.x*Normal.x + Normal.y*Normal.y + Normal.z*Normal.z))
#include <math.h>
//#include "Unit1.h"
// Our vertex type


// The mapcoord type, 2 texture coordinates for each vertex
typedef struct{
    float u,v;
}mapcoord_type;



typedef struct{
    float x,y,z;
} Vector3f;

typedef struct{
	unsigned int a,b,c;
} face;

typedef struct {

	unsigned int verticesCount;
	unsigned int facesCount;

	Vector3f *vertex;
	face *polygon;

} model;

void PrepareObject(obj_type *obj, float size, int normals_on, int vert_off);

extern char Load3DS (obj_type_ptr ogg, char *filename);

void ComputeNormals(obj_type *obj_type);
/*
void CalculateTangentsAndBinormals(obj_type *obj_type);

float Dot(vertex_type Vector1, vertex_type Vector2);

void CalcTriangleBasis( const vertex_type& E, const vertex_type& F, const vertex_type& G, float sE,
		float tE, float sF, float tF, float sG, float tG, vertex_type& tangentX,
		vertex_type& tangentY );

vertex_type ClosestPointOnLine( const vertex_type &a, const vertex_type &b, const vertex_type &p );

vertex_type Ortogonalize( const vertex_type& v1, const vertex_type& v2 );

*/
vertex_type Vector(vertex_type vPoint1, vertex_type vPoint2);

vertex_type Cross(vertex_type vVector1, vertex_type vVector2);

vertex_type AddVector(vertex_type vVector1, vertex_type vVector2);

vertex_type DivideVectorByScaler(vertex_type vVector1, float Scaler);

vertex_type Normalize(vertex_type vNormal);


#endif