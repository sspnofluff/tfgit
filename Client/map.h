#ifndef _MAP_H
#define _MAP_H

#include <cstdlib>
#include "math_3d.h"
#include <iostream>
#include <atlstr.h>
#include "3dsloader.h"
#include "main.h"
#include <fstream>
#include <string>
#include <sstream>
#include <stdlib.h>
#include <math.h>

#include "tgaloader.h"
#include <vector>

using namespace std;

typedef struct
{
  bool v[6];
} checkmas;



void CreateGrid();

void LoadMapConfig();

void LoadMapObjects();

void AddNewMount(int wey, int hey, obj_type *obj, int aX, int bX, int yX, int zX, float size);

void CreateMounts();

unsigned int GetTriangleDot(int side, unsigned int polygon);

unsigned int GetTriangleDotO(obj_type *obj, int side, unsigned int polygon);

void LoadMap();

void LoadMap2(char *p_filename);

void MirrorVert(obj_type *out, obj_type *in);

void MirrorHorz(obj_type *out, obj_type *in);

void CreateTextures();

void GenerateBlendMap2(int x, int y, obj_type *obj);

void GenerateNormalMap(int x, int y, obj_type *obj);

void GenerateBlendMap(int x, int y, obj_type *obj);

void GenerateGrassArray();

void GenerateBlendGrassMap(int x, int y, int number);

checkmas CheckHexesGrass(int u, int v);

string IntToStr(int x);

void FreeLandscapeMemory();

void FreeObject(obj_type *obj);

void CreateForest();

void LoadForestMask(string MaskPath, int number);

void GetForestZ(int u, int v);

void AddTree(polygon_type Temp3, int x_offset, int y_offset, int countz);

polygon_type NextTree(polygon_type Temp3);

int GenerateForestPart(int u, int v);

checkmas CheckHexesForest(int u, int v);

float GetZ(Vector2f p);

#endif