#pragma comment(lib, "opengl32.lib")
#pragma comment(lib, "glu32.lib")
#pragma comment(lib, "glut32.lib")
#pragma comment(lib, "ftgl_dynamic_MT.lib")
#pragma comment(lib, "assimp.lib")
#include <glew.h>
#include "main.h"
#include <assert.h>



#ifdef WIN32
static long long GetCurrentTimeMillis()
{
	return GetTickCount();
}
#else
static long long GetCurrentTimeMillis()
{
	timeval t;
	gettimeofday(&t, NULL);

	long long ret = t.tv_sec * 1000 + t.tv_usec / 1000;

	return ret;
}
#endif


GLuint FaceFrameBuffer = 0;
GLuint FaceColorBuffer;
GLuint renderedTexture;
GLuint depthrenderbuffer;


FILE * myfile;
FILE * myfile2;

int offset=2;
int offset2=300;

CFont *Font;

/*
float move_x=0.0f;
float move_y=9.0f;
float move_z=-40.0f;

float center_x=9.0f;
float center_y=9.0f;
float center_z=0.0f;

float move_x=6.0f;
float move_y=-8.0f;
float move_z=-28.0f;

float center_x=6.0f;
float center_y=-1.0f;
float center_z=16.0f;

float move_x=8.0f;
float move_y=-12.0f;
float move_z=-46.0f;

float center_x=8.0f;
float center_y=-10.0f;
float center_z=32.0f;

float move_x=9.0f;
float move_y=-34.0f;
float move_z=-191.0f;

float center_x=9.0f;
float center_y=-9.0f;
float center_z=32.0f;
*/
float move_x=-1300.0f;
float move_y=3000.0f;
float move_z=-650.0f;

float center_x=0.0f;
float center_y=0.0f;
float center_z=0.0f;

std::vector <vertex_type> MapVertexArray;
std::vector <vertex_type> MapNormalArray;
std::vector <polygon_type> MapFaceArray;
std::vector <vertex_type> ForestVertexArray;
std::vector <polygon_type> ForestFaceArray;
std::vector <mapcoord_type> ForestTexcoordArray;

Vector2f trl;
Vector2f TTP;

int buttoncheck[2];
int current_mapmode=0;

float mainSpeed=1.0f;

unsigned int BoneLocation;

Vector3f lpos ( 0.0f, 0.0f, 15.0f ); 
float SplineFactor=0.0f;
float RunningTime=0.0f;
float g_FrameInterval=0.0f;
float LastTime=0.0f;
float FPSTime=0.0f;

int framesPerSecond=0;
int FPS=0;

float crossTime;

float specularPower=8.4f;
int gamestate=0;

int GridSetWei=0;
int GridSetHei=0;
int GridWei=0;
int GridHei=0;

//float side1=0;
//float side2=0;

int temp_var=0;

float rotcub=25.0f;

Vector2f cakepos;
Vector2f ScrP;
Vector2f MainP;
Vector2f MPos;
Vector3f CamRotate = Vector3f(0.0f,0.0f,0.0f);


long long m_startTime=0;

int size_win=48;
int primerot=0;

bool tf;

const int texDepthSizeX = 8196;
const int texDepthSizeY = 8196;
float lightAngle = 0.0;
float cameraProjectionMatrix[16];
float cameraModelViewMatrix[16];
float cameraInverseModelViewMatrix[16];
float lightProjectionMatrix[16];
float lightModelViewMatrix[16];
float lightMatrix[16];
float mvLightPos[3];

float perspective=35.0f;
float perspective2=30.0f;
int display_check=0;
float cam_watch=0;

//float lightPos[3] = {-150.0f, 150.0f, -300.0f};
//float lightEye[3] = {0.0f, 0.0f, 0.0f};
//float lightUp[3] = {0.0f, 0.0f, -1.0f};
//float Orthobasic[4] = {-571, 2152, -910.0f, 2000}; 

float lightPos[3] = {-100.0f, 0.0f, -60.0f};
float lightEye[3] = {0.0f, 0.0f, 0.0f};
float lightUp[3] = {0.0f, 0.0f, -1.0f};
float Orthobasic[4] = {0.0f, 1362.0f, 0.0f, 1374.0f}; 

float Orthonew[4];

obj_type trailer, trailer_wheels, new_obj, palka;
Texture nobjtex;

vector<Matrix4f> Transforms;


Texture blend, blend2, bignorm, 
	grain, plain, rock, rock_norm, grain_norm,
	hextex, gextexture[22], clear_normal, wheels,
	button, button_on, TRI1, TRI2, LIN1, LIN2,
	texDepth, skytextures[6], forest,
	U_W, D_W, R_W, L_W, RD_W, LD_W, RU_W, LU_W, Canvas_W,
	fist_diffuse_map, fist_ao_map, fist_normal_map,
	faceframe, prime2D, renderTexture, glass_ao;

Mesh *RightFist;
Mesh *LeftFist;
Mesh *Temp;

GLuint fbDepth = 0;
GLuint vertfile = 0; 
GLuint fragfile = 0;
GLuint texCubeMap;
GLuint skeletal = 0;
GLuint skeletal_shadow = 0;
GLuint skeletal_shadow_morphing = 0;
GLuint skeletal_shadow_morphing_glass = 0;
GLuint skeletal_shadow_part = 0;
GLuint skeletal_shadow_glass = 0;
GLuint program_textured_normal_shadow = 0;
GLuint skeletal_shadow_part_turbo = 0;
GLuint program_map_shadow = 0;
GLuint program_forest = 0;
GLuint shadow_textured = 0;
GLuint cube = 0;

std::vector<Vector3f> NormalsTemp;

BezierPath bezierPath;

WheelController wheelController;



GLuint m_boneLocation[MAX_BONES];


//Mesh new_obj2, new_obj3, palka2;


move_mark move_mark;

move_anim2 move_anim2;

unit jazz, prime, prime_trailer, *unitptr, *current;


unit ** unitlist = new unit * [10];



int openMa[5][1000];
int closeMa[5][1000];
int open[3][1000];
int close[3][1000];

PFNGLMULTITEXCOORD2FARBPROC			glMultiTexCoord2fARB		= NULL;
PFNGLACTIVETEXTUREARBPROC			glActiveTextureARB			= NULL;
PFNGLCLIENTACTIVETEXTUREARBPROC		glClientActiveTextureARB	= NULL;
PFNGLCREATESHADERPROC				glCreateShader				= NULL;
PFNGLSHADERSOURCEPROC				glShaderSource				= NULL;
PFNGLCOMPILESHADERARBPROC			glCompileShader				= NULL;
PFNGLCREATEPROGRAMOBJECTARBPROC		glCreateProgram				= NULL;
PFNGLATTACHSHADERPROC				glAttachShader				= NULL;
PFNGLLINKPROGRAMPROC				glLinkProgram				= NULL;
PFNGLUSEPROGRAMPROC					glUseProgram				= NULL;
PFNGLGETSHADERINFOLOGPROC			glGetShaderInfoLog			= NULL;
PFNGLGETSHADERIVPROC				glGetShaderiv				= NULL;
PFNGLGETUNIFORMLOCATIONPROC			glGetUniformLocation		= NULL;
PFNGLUNIFORM4FVPROC					glUniform4fv				= NULL;
PFNGLUNIFORM4FPROC					glUniform4f					= NULL;
PFNGLUNIFORM3FPROC					glUniform3f					= NULL;
PFNGLUNIFORM1FPROC					glUniform1f					= NULL;
PFNGLUNIFORM1IPROC					glUniform1i					= NULL;
PFNGLGETATTRIBLOCATIONPROC			glGetAttribLocation			= NULL;
PFNGLGENBUFFERSPROC					glGenBuffers				= NULL;
PFNGLBINDBUFFERPROC					glBindBuffer				= NULL;
PFNGLBUFFERDATAPROC					glBufferData				= NULL;
PFNGLENABLEVERTEXATTRIBARRAYPROC	glEnableVertexAttribArray	= NULL;
PFNGLVERTEXATTRIBPOINTERPROC		glVertexAttribPointer		= NULL;
PFNGLDELETEBUFFERSPROC				glDeleteBuffers				= NULL;
PFNGLUNIFORMMATRIX4FVPROC			glUniformMatrix4fv			= NULL;
PFNGLGENFRAMEBUFFERSPROC			glGenFramebuffers			= NULL;
PFNGLBINDFRAMEBUFFERPROC			glBindFramebuffer			= NULL;
PFNGLFRAMEBUFFERTEXTURE2DPROC		glFramebufferTexture2D		= NULL;
PFNGLDRAWELEMENTSBASEVERTEXPROC		glDrawElementsBaseVertex	= NULL;
PFNGLGENVERTEXARRAYSPROC			glGenVertexArrays			= NULL;
PFNGLBINDVERTEXARRAYPROC			glBindVertexArray			= NULL;
PFNGLVERTEXATTRIBIPOINTERPROC		glVertexAttribIPointer		= NULL;
PFNGLDELETEVERTEXARRAYSPROC			glDeleteVertexArrays		= NULL;
PFNGLACTIVETEXTUREPROC				glActiveTexture				= NULL;
PFNGLBINDRENDERBUFFERPROC			glBindRenderbuffer			= NULL;
PFNGLFRAMEBUFFERRENDERBUFFERPROC	glFramebufferRenderbuffer	= NULL;
PFNGLFRAMEBUFFERTEXTUREPROC			glFramebufferTexture		= NULL;
PFNGLDRAWBUFFERSPROC				glDrawBuffers				= NULL;
PFNGLGENRENDERBUFFERSPROC			glGenRenderbuffers			= NULL;
PFNGLRENDERBUFFERSTORAGEPROC		glRenderbufferStorage		= NULL;
PFNGLRENDERBUFFERSTORAGEMULTISAMPLEPROC		glRenderbufferStorageMultisample		= NULL;
PFNWGLCHOOSEPIXELFORMATARBPROC wglChoosePixelFormatARB		= NULL;



Mainmass mainmass[51][51];

class CVector3
{
public:
	float x,y,z;
} CamPos, CamTgt, CamM;

int CamX, CamY, CamZ, CamXC, CamYC, CamZC;

// ����������� �����������:
HWND  g_hWnd;
RECT  g_rRect;
HDC   g_hDC;
HGLRC g_hRC;
HINSTANCE g_hInstance;
// ������
GLfloat LightAmbient[]		=	{ 0.45f		,	0.45f		, 0.45f		,	1.0f };
GLfloat LightAmbient2[]		=	{ 0.45f		,	0.45f		, 0.45f		,	1.0f };
GLfloat LightDiffuse[]		=	{ 0.85f		,	0.85f		, 0.85f		,	1.0f };
GLfloat LightDiffuse2[]		=	{ 0.65f		,	0.65f		, 0.65f		,	1.0f };
GLfloat LightSpecular[]		=	{ 0.8f		,	0.8f		, 0.8f		,	1.0f };
GLfloat LightPosition[]		=	{-1300.0f	,	3000.0f		,-650.0f	,	0.0f };
GLfloat LightPosition2[]	=	{-1300.0f	,	3000.0f		,-650.0f	,	0.0f };
GLfloat LightPosition3[]	=	{-61.0f		,	24.0f		,-39.0f		,	0.0f };
/*
GLfloat LightAmbient[]		=	{ 0.75f		,	0.75f		, 0.75f		,	1.0f };
GLfloat LightAmbient2[]		=	{ 0.45f		,	0.45f		, 0.45f		,	1.0f };
GLfloat LightDiffuse[]		=	{ 0.85f		,	0.85f		, 0.85f		,	1.0f };
GLfloat LightDiffuse2[]		=	{ 0.75f		,	0.75f		, 0.75f		,	1.0f };
GLfloat LightSpecular[]		=	{ 0.8f		,	0.8f		, 0.73f		,	1.0f };
GLfloat LightPosition[]		=	{-1300.0f	,	-3000.0f	,-650.0f	,	0.0f };
GLfloat LightPosition2[]	=	{-1300.0f	,	-3000.0f	,-650.0f	,	0.0f };
*/
///////////////////////////////////////////////////////////////
//
//          ������� ���������� ������ ���� � �������� �����
//
///////////////////////////////////////////////////////////////

void RenderScene()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glColor3f(0.0f,0.0f,0.0f);

	glLightModelfv(GL_LIGHT_MODEL_AMBIENT, LightAmbient);

	glLightfv(GL_LIGHT0, GL_AMBIENT, LightAmbient);
	glLightfv(GL_LIGHT1, GL_SPECULAR, LightSpecular);
	glLightfv(GL_LIGHT1, GL_AMBIENT, LightAmbient2);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, LightDiffuse);
	glLightfv(GL_LIGHT1, GL_DIFFUSE, LightDiffuse2);
	glLightfv(GL_LIGHT0, GL_POSITION, LightPosition);
	glLightfv(GL_LIGHT1, GL_POSITION, LightPosition2);
	DrawMap();
	//if (current_mapmode==0)
	draw_forest();
	//DrawMove();
	//DrawUnits();
	//draw_GUI();
	SwapBuffers(g_hDC);
	glLoadIdentity();
	MoveCamera();
	CamPos.x+=CamXC;
	CamPos.y+=CamYC;
	CamPos.z+=CamZC;
	CamTgt.x=CamPos.x+CamM.x;
	CamTgt.y=CamPos.y+CamM.y;
	CamTgt.z=CamPos.z+CamM.z;
	gluLookAt(CamPos.x,CamPos.y,CamPos.z,CamTgt.x,CamTgt.y,CamTgt.z,0,0,-1);
	CamZC=0;
	CamYC=0;
	CamXC=0;


}


void Init(HWND hWnd)
{
	g_hWnd = hWnd;

	GetClientRect(g_hWnd, &g_rRect);
	InitializeOpenGL(g_rRect.right, g_rRect.bottom);
	glClearColor(0.0f, 0.0f, 0.0f, 1.5f);
	Font = new CFont("arial.ttf",16,16);

}


WPARAM MainLoop()
{
	MSG msg;

	while(true)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if(msg.message == WM_QUIT)
				break;
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
		else
		{
			GetFrameTime();


			if (gamestate==0)

				RenderSceneMenu();

			if (gamestate==1)
			{
				
				CursorPosition();
				
				
				prime.Current->timer.Look(0);
				prime.Face->timer.Look(1);
				prime_trailer.Current->timer.Look(0);

				if (wheelController.on==true)
				wheelController.Loop();
				
								
				
				if (move_mark.on_mark==1 && mainmass[(int)cakepos.x][(int)cakepos.y].marked==1) {
				if ((int)cakepos.x!=unitptr->X || (int)cakepos.y!=unitptr->Y) 
						if (aStar(unitptr->X,unitptr->Y))
							SetLineCh();
				}
				
					//RunningTime = (float)((double)GetCurrentTimeMillis() - (double)m_startTime) / 1000.0f;
				
				//CamPos.x+=5;
				RenderFrameFace();
				if (current_mapmode==0)
				RenderToShadowMap();
				
				RenderShadowedScene();
				
				if (move_anim2.anim_move_flag==true && move_anim2.anim_move_rotate==false && move_anim2.anim_move_state==false && prime.Current->timer.freeze==false)
				bezierPath.Loop();
				

			}
		}
	}

	DeInit();

	return(msg.wParam);
}


LRESULT CALLBACK WinProc(HWND hWnd,UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	LONG    lRet = 0;
	PAINTSTRUCT    ps;

	switch (uMsg)
	{
	case WM_SIZE:       

		SizeOpenGLScreen(LOWORD(lParam),HIWORD(lParam), 60.0f);// LoWord=Width, HiWord=Height
		GetClientRect(hWnd, &g_rRect);      
		break;

	case WM_PAINT:         
		BeginPaint(hWnd, &ps);  
		EndPaint(hWnd, &ps);    
		break;



	case WM_KEYDOWN:    
		
		switch(wParam)
		{
		case VK_ESCAPE:
			gamestate=2;// ���� ����� ESCAPE
			CleanAll();
			PostQuitMessage(0); // �������
			break;
		case VK_F1:
			Orthobasic[0]+=5.0f;
			break;
		case VK_F2:
			Orthobasic[0]-=5.0f;
			break;
		case VK_F3:
			Orthobasic[1]+=5.0f;
			break;
		case VK_F4:
			Orthobasic[1]-=5.0f;
			break;
		case VK_F5:
			Orthobasic[2]+=5.0f;
			break;
		case VK_F6:
			Orthobasic[2]-=5.0f;
			break;
		case VK_F7:
			Orthobasic[3]+=5.0f;
			break;
		case VK_F8:
			Orthobasic[3]-=5.0f;
			break;
		case VK_F9:
			center_y+=1.0f;
			break;
		case VK_F10:
			center_y-=1.0f;
			break;
		case VK_F11:
			center_z+=1.0f;
			break;
		case VK_F12:
			center_z-=1.0f;
			break;

		case VK_NUMPAD7:
			if (display_check==0)
				lightPos[0]++;
			if (display_check==1)
				lightEye[0]++;
			if (display_check==2)
				lightUp[0]+=0.1;
			break;
		case VK_NUMPAD8:
			if (display_check==0)
				lightPos[0]--;
			if (display_check==1)
				lightEye[0]--;
			if (display_check==2)
				lightUp[0]-=0.1;
			break;
		case VK_NUMPAD4:
			if (display_check==0)
				lightPos[1]++;
			if (display_check==1)
				lightEye[1]++;
			if (display_check==2)
				lightUp[1]+=0.1;
			break;
		case VK_NUMPAD5:
			if (display_check==0)
				lightPos[1]--;
			if (display_check==1)
				lightEye[1]--;
			if (display_check==2)
				lightUp[1]-=0.1;
			break;
		case VK_NUMPAD1:
			if (display_check==0)
				lightPos[2]++;
			if (display_check==1)
				lightEye[2]++;
			if (display_check==2)
				lightUp[2]+=0.1;	
			break;
		case VK_NUMPAD2:
			if (display_check==0)
				lightPos[2]--;
			if (display_check==1)
				lightEye[2]--;
			if (display_check==2)
				lightUp[2]-=0.1;
			break;
		
			/*
		case VK_UP:
			perspective++;
			break;
		case VK_DOWN:
			perspective--;
			break;
		case VK_LEFT:
			primerot++;
			break;
		case VK_RIGHT:
			primerot--;
			break;
			*/
		case VK_UP:
			offset+=1.0f;
			break;
		case VK_DOWN:
			offset-=1.0f;
			break;
		case VK_LEFT:
			offset2-=1.0f;
			break;
		case VK_RIGHT:
			offset2+=1.0f;
			break;
			
		case VK_SPACE:
			if (prime.type==2)
			prime.MoveToSeparate();
			else
			prime.Transformation();
			//prime_trailer.Transformation();
			break;
		}
		break;

	case WM_MOUSEWHEEL:

		wheelDelta += GET_WHEEL_DELTA_WPARAM(wParam);
		for(; wheelDelta > WHEEL_DELTA; wheelDelta -= WHEEL_DELTA)
		{
			if (gamestate==1) {
				CamZC=-70;
				CamYC=70;
				//CamRotate.y+=0.1;
			}
			if (gamestate==0) {
				//CamZC=-5;
			}

		}
		for(; wheelDelta < 0; wheelDelta += WHEEL_DELTA)
		{
			if (gamestate==1) {
				CamZC=70;
				CamYC=-70;
				//CamRotate.y-=0.1;
			}
			if (gamestate==0)
			{
				//CamZC=+5;
			}

		}
		break;

	case WM_LBUTTONDOWN:
		if (gamestate==0)
			MenuMouseclick();
		if (gamestate==1)
			if (move_anim2.anim_move_flag==0)
				Mouseclick();
		break;
	case WM_MOUSEMOVE:
		if (gamestate==0)
			ButtonMenuMouseCheck();
		break;

	case WM_CLOSE:      
		fclose(myfile);
		//fclose(myfile2);
		PostQuitMessage(0);
		
		break;

	default:       
		lRet = DefWindowProc (hWnd, uMsg, wParam, lParam);
		break;
	}

	return lRet;
}



void MoveCamera()
{
	POINT mousePos; 	
	GetCursorPos(&mousePos);
	if (gamestate==1)	{
		if (mousePos.y<=10)
			CamYC=-10;
		if (mousePos.x<=10)
			CamXC=-10;
		if (mousePos.y>=GetSystemMetrics(SM_CYSCREEN)-10)
			CamYC=10;
		if (mousePos.x>=GetSystemMetrics(SM_CXSCREEN)-10)
			CamXC=10;
	}
	if (gamestate==0)	{
		//if (mousePos.y<=5)
		//CamYC=-5;
		//if (mousePos.x<=5)
		//CamXC=-5;
		//if (mousePos.y>=GetSystemMetrics(SM_CYSCREEN)-5)
		//CamYC=5;
		//if (mousePos.x>=GetSystemMetrics(SM_CXSCREEN)-5)
		//CamXC=5;
	}

}


void MainCreateMap()
{

	//DeleteMenu();
	/*
	CubeMapTarget[0] = GL_TEXTURE_CUBE_MAP_POSITIVE_X_ARB;
	CubeMapTarget[1] = GL_TEXTURE_CUBE_MAP_NEGATIVE_X_ARB;
	CubeMapTarget[2] = GL_TEXTURE_CUBE_MAP_POSITIVE_Y_ARB;
	CubeMapTarget[3] = GL_TEXTURE_CUBE_MAP_NEGATIVE_Y_ARB;
	CubeMapTarget[4] = GL_TEXTURE_CUBE_MAP_POSITIVE_Z_ARB;
	CubeMapTarget[5] = GL_TEXTURE_CUBE_MAP_NEGATIVE_Z_ARB;
	glEnable ( GL_TEXTURE_CUBE_MAP_ARB );
	glGenTextures ( 1, &texCubeMap );
	glPixelStorei ( GL_UNPACK_ALIGNMENT, 1 );
	glBindTexture ( GL_TEXTURE_CUBE_MAP_ARB, texCubeMap );
	glTexEnvi ( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE );
	glTexParameteri ( GL_TEXTURE_CUBE_MAP_ARB,  GL_TEXTURE_WRAP_S,GL_REPEAT);
	glTexParameteri ( GL_TEXTURE_CUBE_MAP_ARB,  GL_TEXTURE_WRAP_T,GL_REPEAT);
	glTexParameteri ( GL_TEXTURE_CUBE_MAP_ARB,  GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR );
	glTexParameteri ( GL_TEXTURE_CUBE_MAP_ARB,  GL_TEXTURE_MAG_FILTER, GL_LINEAR );
	glTexParameteri ( GL_TEXTURE_CUBE_MAP_ARB, GL_GENERATE_MIPMAP_SGIS, GL_TRUE );
	for ( int i = 0; i < 6; i++ )
	glTexImage2D  ( CubeMapTarget[i], 0, GL_RGB, 512, 512, 0, GL_RGB,
    GL_UNSIGNED_BYTE, NULL);
	glDisable(GL_TEXTURE_CUBE_MAP_ARB);
	CreateCubemap();
	*/

    /*
    glGenTextures   ( 1, &texCubeMap );
    glBindTexture   ( GL_TEXTURE_CUBE_MAP_ARB, texCubeMap);
    glPixelStorei   ( GL_UNPACK_ALIGNMENT, 1 );

    glTexParameteri ( GL_TEXTURE_CUBE_MAP_ARB, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE );
    glTexParameteri ( GL_TEXTURE_CUBE_MAP_ARB, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE );
	LoadTextures("textures/Menu/Back.tga", GL_TEXTURE_CUBE_MAP_POSITIVE_X_ARB, &skytextures[BACK_ID], 1);
	LoadTextures("textures/Menu/Front.tga", GL_TEXTURE_CUBE_MAP_NEGATIVE_X_ARB, &skytextures[FRONT_ID], 1);
	LoadTextures("textures/Menu/Bottom.tga", GL_TEXTURE_CUBE_MAP_POSITIVE_Y_ARB, &skytextures[BOTTOM_ID], 1);
	LoadTextures("textures/Menu/Top.tga", GL_TEXTURE_CUBE_MAP_NEGATIVE_Y_ARB, &skytextures[TOP_ID], 1);
	LoadTextures("textures/Menu/Left.tga", GL_TEXTURE_CUBE_MAP_POSITIVE_Z_ARB, &skytextures[RIGHT_ID], 1);
	LoadTextures("textures/Menu/Right.tga", GL_TEXTURE_CUBE_MAP_NEGATIVE_Z_ARB, &skytextures[LEFT_ID], 1);
    glEnable ( GL_TEXTURE_CUBE_MAP_ARB );

    glTexParameteri ( GL_TEXTURE_CUBE_MAP_ARB, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR );
    glTexParameteri ( GL_TEXTURE_CUBE_MAP_ARB, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
    */

	
	LoadTextures("textures/Menu/nBack.tga",&skytextures[BACK_ID],0);
	LoadTextures("textures/Menu/nFront.tga",&skytextures[FRONT_ID],0);
	LoadTextures("textures/Menu/nBottom.tga",&skytextures[BOTTOM_ID],0);
	LoadTextures("textures/Menu/nTop.tga",&skytextures[TOP_ID],0);
	LoadTextures("textures/Menu/nLeft.tga",&skytextures[RIGHT_ID],0);
	LoadTextures("textures/Menu/nRight.tga",&skytextures[LEFT_ID],0);
	
	//setupCubeMap(texCubeMap, &skytextures[TOP_ID], &skytextures[BOTTOM_ID], &skytextures[RIGHT_ID], &skytextures[LEFT_ID], &skytextures[FRONT_ID], &skytextures[BACK_ID]);
	setupCubeMap(texCubeMap, &skytextures[RIGHT_ID],&skytextures[LEFT_ID] , &skytextures[FRONT_ID], &skytextures[BACK_ID], &skytextures[BOTTOM_ID], &skytextures[TOP_ID]);
	//void setupCubeMap(GLuint& texture, Texture *xpos, Texture *xneg, Texture *ypos, Texture *yneg, Texture *zpos, Texture *zneg);
	/*
	glGenBuffers(1, &vbo_cube_vertices);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_cube_vertices);
	glBufferData(GL_ARRAY_BUFFER, sizeof(cube_vertices), cube_vertices, GL_STATIC_DRAW);

	glGenBuffers(1, &ibo_cube_indices);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo_cube_indices);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(cube_indices), cube_indices, GL_STATIC_DRAW);

	GLint PVM    = glGetUniformLocation(glProgram, "PVM");
	GLint vertex = glGetAttribLocation(glProgram, "vertex");

	glEnableVertexAttribArray(vertex);
	glVertexAttribPointer(vertex, 3, GL_FLOAT, GL_FALSE, 0, 0);

	glm::mat4 Projection = glm::perspective(45.0f, (float)WIDTH / (float)HEIGHT, 0.1f, 100.0f); 
	glm::mat4 View       = glm::mat4(1.0f);
	glm::mat4 Model      = glm::scale(glm::mat4(1.0f),glm::vec3(50,50,50));

	if (kb.getKeyState(KEY_UP))    alpha += 180.0f*elapsed0;
	if (kb.getKeyState(KEY_DOWN))  alpha -= 180.0f*elapsed0;
	if (kb.getKeyState(KEY_LEFT))  beta  -= 180.0f*elapsed0;
	if (kb.getKeyState(KEY_RIGHT)) beta  += 180.0f*elapsed0;
	jp[0] = js.joystickPosition(0);
	alpha += jp[0].y*elapsed0*180.0f;
	beta  += jp[0].x*elapsed0*180.0f;
 
	glm::mat4 RotateX = glm::rotate(glm::mat4(1.0f), alpha, glm::vec3(-1.0f, 0.0f, 0.0f));
	glm::mat4 RotateY = glm::rotate(RotateX, beta, glm::vec3(0.0f, 1.0f, 0.0f));
	glm::mat4 M = Projection * View * Model * RotateY;
	glUniformMatrix4fv(PVM, 1, GL_FALSE, glm::value_ptr(M));
	*/

	//CreateRenderTexture(renderTexture, 512, 3, GL_RGB);

	/*****/

	


	/*****/

	wchar_t buf[256];
	swprintf(buf, sizeof(buf), L"����� �����");
	drawload(10, buf, 45);
	if (current_mapmode==0) {
		LoadTextures("textures/grain_diffuse.tga",&grain,1);
		LoadTextures("textures/grass_diffuse.tga",&plain,1);
		LoadTextures("textures/rock_diffuse.tga",&rock,1);
		LoadTextures("textures/hex.tga",&hextex,1);
		LoadTextures("textures/rock_normal.tga",&rock_norm,1);
		LoadTextures("textures/grain_normal.tga",&grain_norm,1);
		LoadTextures("textures/forest.tga",&forest,1);
	}
	LoadTextures("textures/GEX/GEX.tga",&gextexture[0],1);
	LoadTextures("textures/GEX/GEX0.tga",&gextexture[1],1);
	LoadTextures("textures/GEX/GEX1.tga",&gextexture[2],1);
	LoadTextures("textures/GEX/GEX2.tga",&gextexture[3],1);
	LoadTextures("textures/GEX/GEX3.tga",&gextexture[4],1);
	LoadTextures("textures/GEX/GEX4.tga",&gextexture[5],1);
	LoadTextures("textures/GEX/GEX5.tga",&gextexture[6],1);
	LoadTextures("textures/GEX/GEX6.tga",&gextexture[7],1);
	LoadTextures("textures/GEX/GEX7.tga",&gextexture[8],1);
	LoadTextures("textures/GEX/GEX8.tga",&gextexture[9],1);
	swprintf(buf, sizeof(buf), L"��������������� �����");
	drawload(70, buf, 90);
	LoadTextures("textures/GEX/GEX_STR_0.tga",&gextexture[10],1);
	LoadTextures("textures/GEX/GEX_STR_1.tga",&gextexture[11],1);
	LoadTextures("textures/GEX/GEX_STR_2.tga",&gextexture[12],1);
	LoadTextures("textures/GEX/GEX_STR_3.tga",&gextexture[13],1);
	LoadTextures("textures/GEX/GEX_STR_4.tga",&gextexture[14],1);
	LoadTextures("textures/GEX/GEX_STR_5.tga",&gextexture[15],1);
	drawload(100, buf, 90);
	LoadTextures("textures/GEX/GEX_END_0.tga",&gextexture[16],1);
	LoadTextures("textures/GEX/GEX_END_1.tga",&gextexture[17],1);
	LoadTextures("textures/GEX/GEX_END_2.tga",&gextexture[18],1);
	LoadTextures("textures/GEX/GEX_END_3.tga",&gextexture[19],1);
	LoadTextures("textures/GEX/GEX_END_4.tga",&gextexture[20],1);
	LoadTextures("textures/GEX/GEX_END_5.tga",&gextexture[21],1);
	swprintf(buf, sizeof(buf), L"�������� �����");
	drawload(120, buf, 55);
	LoadMap2("maps/Two_sidesREADY6.txt");
	if (current_mapmode==0)	{
		swprintf(buf, sizeof(buf), L"���������������");
		drawload(160, buf, 60);
		LoadMapObjects();
	}
	swprintf(buf, sizeof(buf), L"����������� �����");
	drawload(200, buf, 70);
	CreateGrid();
	if (current_mapmode==0)	{
		swprintf(buf, sizeof(buf), L"����������� �����");
		drawload(240, buf, 70);
		GenerateGrassArray();
	}
	if (current_mapmode==0)	{
		swprintf(buf, sizeof(buf), L"��������� �������");
		drawload(280, buf, 65);
		CreateMounts();
	}
	if (current_mapmode==0)	{
		swprintf(buf, sizeof(buf), L"�������� ������������");
		drawload(320, buf, 80);
		CreateTextures();
		FreeLandscapeMemory();
		swprintf(buf, sizeof(buf), L"������� ����");
		drawload(360, buf, 40);
		CreateForest();
	}
	
	swprintf(buf, sizeof(buf), L"���������� ������");
	drawload(365, buf, 60);

	//jazz.X=5;
	//jazz.Y=5;
	//jazz.rotdir=5;
	//jazz.type=1;
	//jazz.z_up=0.0f;
	//mainmass[5][5].unitptr=&jazz;

	prime.X=4;
	prime.Y=4;
	prime.rotdir=5;
	prime.type=2;
	//prime.z_up=0.0f;

	prime.widthBox.x=-7.5f;
	prime.widthBox.y=7.5f;

	prime.heightBox.x=11.5f;
	prime.heightBox.y=-16.5f;

	prime_trailer.widthBox.x=-7.5f;
	prime_trailer.widthBox.y=7.5f;

	prime_trailer.heightBox.x=26.0f;
	prime_trailer.heightBox.y=-25.0f;

	mainmass[4][4].unitptr=&prime;


	Vector2f p=MainToOxy(prime.X, prime.Y);
	prime.traildot.x=p.x-100;
	prime.traildot.y=p.y+200;
	prime.trailpos_x=3;
	prime.trailpos_y=3;
	
	mainmass[3][3].trailermark=1;
	trailer_fixings(&prime,p.x,p.y,prime.rotdir*60, 15.0f, 0.0f, 20.0f);

	prime_trailer.type=3;

	for (int i=0; i<10; i++)
		unitlist[i]=NULL;

	//unitlist[0]=&jazz;
	unitlist[0]=&prime;

	swprintf(buf, sizeof(buf), L"���������");
	drawload(400, buf, 35);

	CamPos.x=1010;
	CamPos.y=1830;
	CamPos.z=-940;
	CamM.x=0;
	CamM.y=-1;
	CamM.z=1;

	

	

	//m_startTime = GetCurrentTimeMillis();

	/*
	bezierPath.start(3,3,4);
	bezierPath.r1(4,3);
	bezierPath.r8(5,3);
	bezierPath.r10(6,3);
	bezierPath.r12(6,2);
	bezierPath.r2(5,1);
	bezierPath.r7(4,2);
	bezierPath.r2(3,1);
	bezierPath.r4(2,2);
	bezierPath.r6(2,3);
	bezierPath.end(3,3,5);
	bezierPath.GetDrawingPoints();
*/
	
	
	FaceFrameInitialization();
	gamestate=1;
	
}




void DrawMap()
{
	//glFrontFace(GL_CCW);
	//glCullFace(GL_BACK);
	//glEnable(GL_CULL_FACE);
	glLineWidth(0.1);

	if (current_mapmode==0)
	{

		glUseProgram(program_map_shadow);

		glUniform1i(glGetUniformLocation(program_map_shadow,"plainTexture"),0);
		glUniform1i(glGetUniformLocation(program_map_shadow,"rockTexture"),1);
		glUniform1i(glGetUniformLocation(program_map_shadow,"allBlend"),2);
		glUniform1i(glGetUniformLocation(program_map_shadow,"hexSample"),3);
		glUniform1i(glGetUniformLocation(program_map_shadow,"grassNormal"),4);
		glUniform1i(glGetUniformLocation(program_map_shadow,"grassTexture"),5);
		glUniform1i(glGetUniformLocation(program_map_shadow,"allNormal"),6);
		glUniform1i(glGetUniformLocation(program_map_shadow,"allBlend2"),7);
		glUniform1i(glGetUniformLocation(program_map_shadow,"rockNormal"),8);
		glUniform1i(glGetUniformLocation(program_map_shadow,"shadowMap"),9);

		glActiveTextureARB(GL_TEXTURE0_ARB);
		glBindTexture(GL_TEXTURE_2D, grain.texID);
		glActiveTextureARB(GL_TEXTURE1_ARB);
		glBindTexture(GL_TEXTURE_2D, rock.texID);
		glActiveTextureARB(GL_TEXTURE2_ARB);
		glBindTexture(GL_TEXTURE_2D, blend.texID);
		glActiveTextureARB(GL_TEXTURE3_ARB);
		glBindTexture(GL_TEXTURE_2D, hextex.texID);
		glActiveTextureARB(GL_TEXTURE4_ARB);
		glBindTexture(GL_TEXTURE_2D, grain_norm.texID);
		glActiveTextureARB(GL_TEXTURE5_ARB);
		glBindTexture(GL_TEXTURE_2D, plain.texID);
		glActiveTextureARB(GL_TEXTURE6_ARB);
		glBindTexture(GL_TEXTURE_2D, bignorm.texID);
		glActiveTextureARB(GL_TEXTURE7_ARB);
		glBindTexture(GL_TEXTURE_2D, blend2.texID);
		glActiveTextureARB(GL_TEXTURE8_ARB);
		glBindTexture(GL_TEXTURE_2D, rock_norm.texID);
		glActiveTextureARB(GL_TEXTURE9_ARB);
		glBindTexture(GL_TEXTURE_2D, texDepth.texID);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_R_TO_TEXTURE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_FUNC, GL_LEQUAL);

		glUniformMatrix4fv(glGetUniformLocation(program_map_shadow,"lightMatrix"), 1, false, lightMatrix);

		glEnableClientState(GL_VERTEX_ARRAY);
		glEnableClientState(GL_NORMAL_ARRAY );
		glNormalPointer(GL_FLOAT,  sizeof(vertex_type),&MapNormalArray[0]);
		glVertexPointer(3,GL_FLOAT,sizeof(vertex_type),&MapVertexArray[0]);
		glDrawElements(GL_TRIANGLES,MapFaceArray.size()*3,GL_UNSIGNED_INT,&MapFaceArray[0]);
		glDisableClientState(GL_VERTEX_ARRAY);
		glDisableClientState(GL_NORMAL_ARRAY);

		glActiveTextureARB(GL_TEXTURE9_ARB);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_NONE);
		glBindTexture(GL_TEXTURE_2D, 0);
		glActiveTextureARB(GL_TEXTURE0_ARB);
		glUseProgram(0);
	}
	else
	{

		glColor3f(0.0f, 0.0f, 0.0f);
		glEnableClientState(GL_NORMAL_ARRAY );
		glEnableClientState(GL_VERTEX_ARRAY);
		glNormalPointer(GL_FLOAT,  sizeof(vertex_type),&MapNormalArray[0]);
		glVertexPointer(3,GL_FLOAT,sizeof(vertex_type),&MapVertexArray[0]);

		glDrawElements(GL_LINES,MapFaceArray.size()*3,GL_UNSIGNED_INT,&MapFaceArray[0]);
		glDisableClientState(GL_VERTEX_ARRAY);
		glDisableClientState(GL_NORMAL_ARRAY);

	}
}



void draw_obj(obj_type *obj, float tX, float tY, float tZ, float dirrot, int iX, int iY, Texture *tex1, Texture *tex2, Texture *tex3, bool shadow_on, float angle) //����� ������� ��������� ��������
{

	glPushMatrix();
	glTranslatef(tX,tY,tZ);
	glRotatef(dirrot,0,0,1);
	glRotatef(angle,1,0,0);

	if ((int)cakepos.x==iX && (int)cakepos.y==iY && move_anim2.anim_move_flag==0) {
		if (shadow_on==1)	{
			glUseProgram(program_textured_normal_shadow);
			glUniform3f(glGetUniformLocation(program_textured_normal_shadow,"fvEyePosition"),CamPos.x, CamPos.y, CamPos.z);
			glUniform1f(glGetUniformLocation(program_textured_normal_shadow,"fSpecularPower"),specularPower);
			glUniform1i(glGetUniformLocation(program_textured_normal_shadow,"diffuseMap"),0);
			glUniform1i(glGetUniformLocation(program_textured_normal_shadow,"normalMap"),1);
			glUniform1i(glGetUniformLocation(program_textured_normal_shadow,"shadowMap"),2);
			glUniform1i(glGetUniformLocation(program_textured_normal_shadow,"emissionMap"),3);
			glActiveTextureARB(GL_TEXTURE0_ARB);
			glBindTexture(GL_TEXTURE_2D, tex1->texID);
			glActiveTextureARB(GL_TEXTURE1_ARB);
			glBindTexture(GL_TEXTURE_2D, tex2->texID);
			glActiveTextureARB(GL_TEXTURE2_ARB);
			glBindTexture(GL_TEXTURE_2D, texDepth.texID);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_R_TO_TEXTURE);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_FUNC, GL_LEQUAL);
			glActiveTextureARB(GL_TEXTURE3_ARB);
			glBindTexture(GL_TEXTURE_2D, tex3->texID);

			glUniformMatrix4fv(glGetUniformLocation(program_textured_normal_shadow,"lightMatrix"), 1, false, lightMatrix);
			glUniform3f(glGetUniformLocation(program_textured_normal_shadow,"lightPos"), mvLightPos[0], mvLightPos[1], mvLightPos[2]);
		}
	}
	else {
		if (shadow_on==1)	{
			glUseProgram(program_textured_normal_shadow);
			glUniform3f(glGetUniformLocation(program_textured_normal_shadow,"fvEyePosition"),CamPos.x, CamPos.y, CamPos.z);
			glUniform1f(glGetUniformLocation(program_textured_normal_shadow,"fSpecularPower"),specularPower);
			glUniform1i(glGetUniformLocation(program_textured_normal_shadow,"diffuseMap"),0);
			glUniform1i(glGetUniformLocation(program_textured_normal_shadow,"normalMap"),1);
			glUniform1i(glGetUniformLocation(program_textured_normal_shadow,"shadowMap"),2);
			glUniform1i(glGetUniformLocation(program_textured_normal_shadow,"emissionMap"),3);
			glActiveTextureARB(GL_TEXTURE0_ARB);
			glBindTexture(GL_TEXTURE_2D, tex1->texID);
			glActiveTextureARB(GL_TEXTURE1_ARB);
			glBindTexture(GL_TEXTURE_2D, tex2->texID);
			glActiveTextureARB(GL_TEXTURE2_ARB);
			glBindTexture(GL_TEXTURE_2D, texDepth.texID);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_R_TO_TEXTURE);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_FUNC, GL_LEQUAL);
			glActiveTextureARB(GL_TEXTURE3_ARB);
			glBindTexture(GL_TEXTURE_2D, tex3->texID);

			glUniformMatrix4fv(glGetUniformLocation(program_textured_normal_shadow,"lightMatrix"), 1, false, lightMatrix);
			glUniform3f(glGetUniformLocation(program_textured_normal_shadow,"lightPos"), mvLightPos[0], mvLightPos[1], mvLightPos[2]);

		}
	}

	glEnableClientState(GL_NORMAL_ARRAY ); 
	glEnableClientState(GL_VERTEX_ARRAY); 
	glEnableClientState(GL_TEXTURE_COORD_ARRAY);

	glTexCoordPointer(2, GL_FLOAT, sizeof(mapcoord_type),obj->mapcoord); 
	glNormalPointer(GL_FLOAT,  sizeof(vertex_type),obj->normals); 
	glVertexPointer(3,GL_FLOAT,sizeof(vertex_type),obj->vertex); 

	glDrawElements(GL_TRIANGLES,obj->polygons_qty*3,GL_UNSIGNED_INT,obj->polygon); 

	glDisableClientState(GL_TEXTURE_COORD_ARRAY);
	glDisableClientState(GL_VERTEX_ARRAY);
	glDisableClientState(GL_NORMAL_ARRAY);

	if (shadow_on==1)	{
		glActiveTextureARB(GL_TEXTURE2_ARB);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_NONE);
		glBindTexture(GL_TEXTURE_2D, 0);
		glActiveTextureARB(GL_TEXTURE0_ARB);
		glUseProgram(0);
	}

	glPopMatrix();
	
}



void setShaders()
{
	setShader1();
	setShader2();
	setShader3();
	setShader4();
	setShader5();
	setShader6();
	setShader7();
	setShader8();
	setShader9();
	setShader10();
	setShader11();
	setShader12();
}

void setShader1()
{
	char *vs,*fs;
	vertfile = glCreateShader(GL_VERTEX_SHADER);
	fragfile = glCreateShader(GL_FRAGMENT_SHADER);
	vs = textFileRead("shaders/forest.vert");
	fs = textFileRead("shaders/forest.frag");
	const char * vv = vs;
	const char * ff = fs;
	glShaderSource(vertfile, 1, &vv, NULL);
	glShaderSource(fragfile, 1, &ff, NULL);
	free(vs);
	free(fs);
	glCompileShader(vertfile);
	glCompileShader(fragfile);	
	//shaderLog(vertfile);
	//shaderLog(fragfile);
	program_forest = glCreateProgram();
	glAttachShader(program_forest,vertfile);
	glAttachShader(program_forest,fragfile);
	glLinkProgram(program_forest);
}


void setShader2()
{

	char *vs,*fs;
	vertfile = glCreateShader(GL_VERTEX_SHADER);
	fragfile = glCreateShader(GL_FRAGMENT_SHADER);
	vs = textFileRead("shaders/textured_normal_shadow.vert");
	fs = textFileRead("shaders/textured_normal_shadow.frag");
	const char * vv = vs;
	const char * ff = fs;
	glShaderSource(vertfile, 1, &vv, NULL);
	glShaderSource(fragfile, 1, &ff, NULL);
	free(vs);
	free(fs);
	glCompileShader(vertfile);
	glCompileShader(fragfile);
	//shaderLog(vertfile);
	//shaderLog(fragfile);
	program_textured_normal_shadow = glCreateProgram();
	glAttachShader(program_textured_normal_shadow,vertfile);
	glAttachShader(program_textured_normal_shadow,fragfile);
	glLinkProgram(program_textured_normal_shadow);
}


void setShader3()
{
	char *vs,*fs;
	vertfile = glCreateShader(GL_VERTEX_SHADER);
	fragfile = glCreateShader(GL_FRAGMENT_SHADER);
	vs = textFileRead("shaders/map_shadow.vert");
	fs = textFileRead("shaders/map_shadow.frag");
	const char * vv = vs;
	const char * ff = fs;
	glShaderSource(vertfile, 1, &vv, NULL);
	glShaderSource(fragfile, 1, &ff, NULL);
	free(vs);
	free(fs);
	glCompileShader(vertfile);
	glCompileShader(fragfile);
	//shaderLog(vertfile);
	//shaderLog(fragfile);
	program_map_shadow = glCreateProgram();
	glAttachShader(program_map_shadow,vertfile);
	glAttachShader(program_map_shadow,fragfile);
	glLinkProgram(program_map_shadow);
}

void setShader4()
{

	char *vs,*fs;
	vertfile = glCreateShader(GL_VERTEX_SHADER);
	fragfile = glCreateShader(GL_FRAGMENT_SHADER);
	vs = textFileRead("shaders/skeletal.vert");
	fs = textFileRead("shaders/skeletal.frag");
	const char * vv = vs;
	const char * ff = fs;
	glShaderSource(vertfile, 1, &vv, NULL);
	glShaderSource(fragfile, 1, &ff, NULL);
	free(vs);
	free(fs);
	glCompileShader(vertfile);
	glCompileShader(fragfile);
	//shaderLog(vertfile);
	//shaderLog(fragfile);
	skeletal = glCreateProgram();
	glAttachShader(skeletal,vertfile);
	glAttachShader(skeletal,fragfile);
	glLinkProgram(skeletal);

	
	/*
	for (unsigned int i = 0 ; i < ARRAY_SIZE_IN_ELEMENTS(m_boneLocation) ; i++) {
				std::string Name;
		Name = "gBones[" + IntToStr(i) + "]";
		m_boneLocation[i] = glGetUniformLocation(skeletal,Name.c_str());
	}
	*/
	
}

void setShader5()
{

	char *vs,*fs;
	vertfile = glCreateShader(GL_VERTEX_SHADER);
	fragfile = glCreateShader(GL_FRAGMENT_SHADER);
	vs = textFileRead("shaders/skeletal_shadow.vert");
	fs = textFileRead("shaders/skeletal_shadow.frag");
	const char * vv = vs;
	const char * ff = fs;
	glShaderSource(vertfile, 1, &vv, NULL);
	glShaderSource(fragfile, 1, &ff, NULL);
	free(vs);
	free(fs);
	glCompileShader(vertfile);
	glCompileShader(fragfile);
	shaderLog(vertfile);
	shaderLog(fragfile);
	skeletal_shadow = glCreateProgram();
	glAttachShader(skeletal_shadow,vertfile);
	glAttachShader(skeletal_shadow,fragfile);
	glLinkProgram(skeletal_shadow);

	

	for (unsigned int i = 0 ; i < ARRAY_SIZE_IN_ELEMENTS(m_boneLocation) ; i++) {
				std::string Name;
		Name = "gBones[" + IntToStr(i) + "]";
		m_boneLocation[i] = glGetUniformLocation(skeletal_shadow,Name.c_str());
	}

	
}

void setShader6()
{

	char *vs,*fs;
	vertfile = glCreateShader(GL_VERTEX_SHADER);
	fragfile = glCreateShader(GL_FRAGMENT_SHADER);
	vs = textFileRead("shaders/shadow_textured.vert");
	fs = textFileRead("shaders/shadow_textured.frag");
	const char * vv = vs;
	const char * ff = fs;
	glShaderSource(vertfile, 1, &vv, NULL);
	glShaderSource(fragfile, 1, &ff, NULL);
	free(vs);
	free(fs);
	glCompileShader(vertfile);
	glCompileShader(fragfile);
	shaderLog(vertfile);
	shaderLog(fragfile);
	shadow_textured= glCreateProgram();
	glAttachShader(shadow_textured,vertfile);
	glAttachShader(shadow_textured,fragfile);
	glLinkProgram(shadow_textured);

	
}

void setShader7()
{

	char *vs,*fs;
	vertfile = glCreateShader(GL_VERTEX_SHADER);
	fragfile = glCreateShader(GL_FRAGMENT_SHADER);
	vs = textFileRead("shaders/skeletal_shadow_part.vert");
	fs = textFileRead("shaders/skeletal_shadow_part.frag");
	const char * vv = vs;
	const char * ff = fs;
	glShaderSource(vertfile, 1, &vv, NULL);
	glShaderSource(fragfile, 1, &ff, NULL);
	free(vs);
	free(fs);
	glCompileShader(vertfile);
	glCompileShader(fragfile);
	shaderLog(vertfile);
	shaderLog(fragfile);
	skeletal_shadow_part = glCreateProgram();
	glAttachShader(skeletal_shadow_part,vertfile);
	glAttachShader(skeletal_shadow_part,fragfile);
	glLinkProgram(skeletal_shadow_part);
	
	

	
}

void setShader8()
{

	char *vs,*fs;
	vertfile = glCreateShader(GL_VERTEX_SHADER);
	fragfile = glCreateShader(GL_FRAGMENT_SHADER);
	vs = textFileRead("shaders/skeletal_shadow_glass.vert");
	fs = textFileRead("shaders/skeletal_shadow_glass.frag");
	const char * vv = vs;
	const char * ff = fs;
	glShaderSource(vertfile, 1, &vv, NULL);
	glShaderSource(fragfile, 1, &ff, NULL);
	free(vs);
	free(fs);
	glCompileShader(vertfile);
	glCompileShader(fragfile);
	shaderLog(vertfile);
	shaderLog(fragfile);
	skeletal_shadow_glass = glCreateProgram();
	glAttachShader(skeletal_shadow_glass,vertfile);
	glAttachShader(skeletal_shadow_glass,fragfile);
	glLinkProgram(skeletal_shadow_glass);
	
	

	
}

void setShader9()
{

	char *vs,*fs;
	vertfile = glCreateShader(GL_VERTEX_SHADER);
	fragfile = glCreateShader(GL_FRAGMENT_SHADER);
	vs = textFileRead("shaders/skeletal_shadow_morphing.vert");
	fs = textFileRead("shaders/skeletal_shadow_morphing.frag");
	const char * vv = vs;
	const char * ff = fs;
	glShaderSource(vertfile, 1, &vv, NULL);
	glShaderSource(fragfile, 1, &ff, NULL);
	free(vs);
	free(fs);
	glCompileShader(vertfile);
	glCompileShader(fragfile);
	shaderLog(vertfile);
	shaderLog(fragfile);
	skeletal_shadow_morphing = glCreateProgram();
	glAttachShader(skeletal_shadow_morphing,vertfile);
	glAttachShader(skeletal_shadow_morphing,fragfile);
	glLinkProgram(skeletal_shadow_morphing);

	

	for (unsigned int i = 0 ; i < ARRAY_SIZE_IN_ELEMENTS(m_boneLocation) ; i++) {
				std::string Name;
		Name = "gBones[" + IntToStr(i) + "]";
		m_boneLocation[i] = glGetUniformLocation(skeletal_shadow_morphing,Name.c_str());
	}

	
}

void setShader10()
{

	char *vs,*fs;
	vertfile = glCreateShader(GL_VERTEX_SHADER);
	fragfile = glCreateShader(GL_FRAGMENT_SHADER);
	vs = textFileRead("shaders/skeletal_shadow_morphing_glass.vert");
	fs = textFileRead("shaders/skeletal_shadow_morphing_glass.frag");
	const char * vv = vs;
	const char * ff = fs;
	glShaderSource(vertfile, 1, &vv, NULL);
	glShaderSource(fragfile, 1, &ff, NULL);
	free(vs);
	free(fs);
	glCompileShader(vertfile);
	glCompileShader(fragfile);
	shaderLog(vertfile);
	shaderLog(fragfile);
	skeletal_shadow_morphing_glass = glCreateProgram();
	glAttachShader(skeletal_shadow_morphing_glass,vertfile);
	glAttachShader(skeletal_shadow_morphing_glass,fragfile);
	glLinkProgram(skeletal_shadow_morphing_glass);

	
	
}

void setShader11()
{

	char *vs,*fs;
	vertfile = glCreateShader(GL_VERTEX_SHADER);
	fragfile = glCreateShader(GL_FRAGMENT_SHADER);
	vs = textFileRead("shaders/cube.vert");
	fs = textFileRead("shaders/cube.frag");
	const char * vv = vs;
	const char * ff = fs;
	glShaderSource(vertfile, 1, &vv, NULL);
	glShaderSource(fragfile, 1, &ff, NULL);
	free(vs);
	free(fs);
	glCompileShader(vertfile);
	glCompileShader(fragfile);
	shaderLog(vertfile);
	shaderLog(fragfile);
	cube = glCreateProgram();
	glAttachShader(cube,vertfile);
	glAttachShader(cube,fragfile);
	glLinkProgram(cube);

	
	
}

void setShader12()
{

	char *vs,*fs;
	vertfile = glCreateShader(GL_VERTEX_SHADER);
	fragfile = glCreateShader(GL_FRAGMENT_SHADER);
	vs = textFileRead("shaders/skeletal_shadow_part_turbo.vert");
	fs = textFileRead("shaders/skeletal_shadow_part_turbo.frag");
	const char * vv = vs;
	const char * ff = fs;
	glShaderSource(vertfile, 1, &vv, NULL);
	glShaderSource(fragfile, 1, &ff, NULL);
	free(vs);
	free(fs);
	glCompileShader(vertfile);
	glCompileShader(fragfile);
	shaderLog(vertfile);
	shaderLog(fragfile);

	skeletal_shadow_part_turbo = glCreateProgram();
	glAttachShader(skeletal_shadow_part_turbo,vertfile);
	glAttachShader(skeletal_shadow_part_turbo,fragfile);
	glLinkProgram(skeletal_shadow_part_turbo);
	
	

	
}



void LoadTextures(char *FileName, Texture *texture, int flag)
{
	if (LoadTGA(texture,FileName))
		glGenTextures(1, &texture->texID);
	glBindTexture(GL_TEXTURE_2D, texture->texID);
	if (flag==0 || flag==1)	{
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR_MIPMAP_LINEAR);
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_LINEAR);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
	}
	if (flag==2 || flag==3)	{
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
	}

	glTexImage2D(GL_TEXTURE_2D, 0, texture->type, texture->width,  texture->height, 0, texture->type, GL_UNSIGNED_BYTE,  texture->imageData);
	if (flag==0 || flag==1) 	{
		gluBuild2DMipmaps(GL_TEXTURE_2D, texture->type, texture->width, texture->height, texture->type, GL_UNSIGNED_BYTE, texture->imageData);
	}
	if (flag==1 || flag==3)
		free(texture->imageData);

}

void LoadTextures(char *FileName, int target, Texture *texture, int flag)
{
	if (LoadTGA(texture,FileName))
	glGenTextures(1, &texture->texID);
	glBindTexture(target, texture->texID);
	glTexImage2D(target, 0, texture->type, texture->width,  texture->height, 0, texture->type, GL_UNSIGNED_BYTE,  texture->imageData);
	if (flag==1) 
	gluBuild2DMipmaps(GL_TEXTURE_2D, texture->type, texture->width, texture->height, texture->type, GL_UNSIGNED_BYTE, texture->imageData);
	free(texture->imageData);

}


void shaderLog(unsigned int shader)
{

	int   infologLen   = 0;
	int   charsWritten = 0;
	char *infoLog;
	glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &infologLen);
	if(infologLen > 1)  {
		infoLog = new char[infologLen];
		if(infoLog == NULL)  {
			MessageBox(NULL, "��������", "INFO", MB_OK);
			exit(1);
		}
		glGetShaderInfoLog(shader, infologLen, &charsWritten, infoLog);
		MessageBox(NULL, infoLog, "INFO", MB_OK);
		delete[] infoLog;
	}

}

string IntToStr(int x)
{
	stringstream r;
	r << x;
	return r.str();
}

string FloatToStr(float x)
{
	stringstream r;
	r << x;
	return r.str();
}

void draw_forest() 
{

	glUseProgram(program_forest);
	glUniform1i(glGetUniformLocation(program_forest,"forestMap"),0);
	glActiveTextureARB(GL_TEXTURE0_ARB);
	glBindTexture(GL_TEXTURE_2D, forest.texID);

	glEnableClientState(GL_TEXTURE_COORD_ARRAY); 
	glEnableClientState(GL_VERTEX_ARRAY); 

	glTexCoordPointer(2, GL_FLOAT, sizeof(mapcoord_type),&ForestTexcoordArray[0]); 
	glVertexPointer(3,GL_FLOAT,sizeof(vertex_type),&ForestVertexArray[0]); 
	glDrawElements(GL_TRIANGLES,ForestFaceArray.size()*3,GL_UNSIGNED_INT,&ForestFaceArray[0]); 
	glDisableClientState(GL_VERTEX_ARRAY);
	glDisableClientState(GL_TEXTURE_COORD_ARRAY);


	glUseProgram(0);
	
}


Vector2f MainToOxy(int x, int y)
{
	Vector2f p;
	p.x=x*3*50/2+50;
	if (x%2==0)
		p.y=y*(50*1.732+1)+50*1.732/2;
	else
		p.y=(y+1)*(50*1.732+1);

	return p;
	
}

Vector2f MainToOxy(int x, int y, float x_1, float y_1)
{
	
	Vector2f p;
	p.x=x*3*50/2+50;
	if (x%2==0)
		p.y=y*(50*1.732+1)+50*1.732/2;
	else
		p.y=(y+1)*(50*1.732+1);

	p.x+=x_1;
	p.y+=y_1;
	return p;
	
}

void CursorPosition()
{
	Vector2f p;

	POINT mousePos;

	GetCursorPos(&mousePos);
	p.x=(float)mousePos.x;
	p.y=(float)mousePos.y;

	if (p.x>0 && p.y>0 && p.x<GetSystemMetrics(SM_CXSCREEN) && p.y<GetSystemMetrics(SM_CYSCREEN)) {
		MPos=p;
		ScrP = ScreenToOxy(int(MPos.x), int(MPos.y));
		MainP = OxyToMain(int(ScrP.x), int(ScrP.y));

		if (MainP.x < GridWei && MainP.x >= 0 && MainP.y < GridHei && MainP.y >= 0)
			cakepos=MainP;
		else {
			cakepos.x=-1;
			cakepos.y=-1;
		}
	}

}



Vector2f ScreenToOxy(int mouse_x, int mouse_y)
{

	Vector2f p1;
	GLint    viewport[4];
	GLdouble projection[16];
	GLdouble modelview[16];
	GLdouble wx,wy,wz,p2x,p2y,p2z;
	glGetIntegerv(GL_VIEWPORT,viewport);
	glGetDoublev(GL_PROJECTION_MATRIX,projection);
	glGetDoublev(GL_MODELVIEW_MATRIX,modelview);
	gluUnProject(mouse_x, viewport[3] - mouse_y - 1, 0.0, modelview, projection, viewport, &wx, &wy, &wz);
	gluUnProject(mouse_x, viewport[3] - mouse_y - 1, 1.0, modelview, projection, viewport, &p2x, &p2y, &p2z);
	p1.x=wx-p2x*wz/(p2z-wz)+wx*wz/(p2z-wz);
	p1.y=wy-p2y*wz/(p2z-wz)+wy*wz/(p2z-wz);
	return p1;


}

Vector2f OxyToMain(float x, float y)
{

	
	float r=50*sqrt(3.0)/2;
	float R=50;
	float d1=999,d2=999,d3=999;
	Vector2f p;
	Vector2f M[3][2];

	M[0][0].x=(x-R)/(R*1.5);
	M[0][0].y=(y-r)/(2*r);
	M[0][1]=MainToOxy(M[0][0].x, M[0][0].y);
	d1=sqrt(pow(M[0][1].x-x,2)+pow(M[0][1].y-y,2));
	if (M[0][0].x+1<GridWei) {
		M[1][0].x=M[0][0].x+1;
		M[1][0].y=M[0][0].y;
		M[1][1]=MainToOxy(M[1][0].x, M[1][0].y);
		d2=sqrt(pow(M[1][1].x-x,2)+pow(M[1][1].y-y,2));
	}
	if (M[0][0].y+1<GridHei) {
		if ((int)M[0][0].x%2==0) {
			M[2][0].x=M[0][0].x;
		}
		if ((int)M[0][0].x%2!=0&&M[0][0].x+1<GridWei){
			M[2][0].x=M[0][0].x+1;
		}
		M[2][0].y=M[0][0].y+1;
		M[2][1]=MainToOxy(M[2][0].x, M[2][0].y);
		d3=sqrt(pow(M[2][1].x-x,2)+pow(M[2][1].y-y,2));
	}

	if (d1<d2) {
		if (d1<d3) {
			p=M[0][0];
		}
		else
		{
			p=M[2][0];
		}
	}
	else
	{
		if (d2<d3) {
			p=M[1][0];
		}
		else{
			p=M[2][0];
		}
	}

	return p;

}



void draw_GUI()
{
	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	glOrtho( 0,GetSystemMetrics(SM_CXSCREEN),GetSystemMetrics(SM_CYSCREEN), 0, 0, 1 );
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glDisable(GL_DEPTH_TEST);
	glColor3f(0.0f,0.0f,0.0f); 

	wchar_t buf[256];
	
	//swprintf(buf, sizeof(buf), L"x %f", Catmullt);
	//Font->Print(5,15, buf);
	//if (bignorm.imageData!=NULL)
	aiQuaternion temp = QuaternionFromMatrix(prime_trailer.GlassRotation);
	/*
	swprintf(buf, sizeof(buf), L"mainSpeed %f", mainSpeed);
	Font->Print(5,15, buf);
	swprintf(buf, sizeof(buf), L"anim_move_walk %i", prime.Current->timer.anim_move_walk);
	Font->Print(5,30, buf);
	swprintf(buf, sizeof(buf), L"anim_move_rotate %i", move_anim2.anim_move_rotate);
	Font->Print(5,45, buf);
	swprintf(buf, sizeof(buf), L"crossTime %f", crossTime);
	Font->Print(5,60, buf);
	swprintf(buf, sizeof(buf), L"animation_index %i", prime.Current->timer.animation_index);
	Font->Print(5,75, buf);
	swprintf(buf, sizeof(buf), L"timer.crossTime %f", prime.Current->timer.crossTime);
	Font->Print(5,90, buf);
	swprintf(buf, sizeof(buf), L"timer.time %f", prime.Current->timer.One[prime.Current->timer.animation_index].time);
	Font->Print(5,105, buf);
	swprintf(buf, sizeof(buf), L"RunningTime %f", RunningTime);
	Font->Print(5,120, buf);
	swprintf(buf, sizeof(buf), L"timeStart %f", prime.Main->timer.crossdata[2].timeStart);
	Font->Print(5,135, buf);
	swprintf(buf, sizeof(buf), L"timeDuration %f", prime.Main->timer.crossdata[2].timeDuration);
	Font->Print(5,150, buf);
	swprintf(buf, sizeof(buf), L"drawingPoints.size %i", bezierPath.drawingPoints.size());
	Font->Print(5,165, buf);
	swprintf(buf, sizeof(buf), L"drawingPoints.size %i", bezierPath.ch);
	Font->Print(5,180, buf);
	swprintf(buf, sizeof(buf), L"t_step %f", bezierPath.t_step);
	Font->Print(5,195, buf);
	swprintf(buf, sizeof(buf), L"t_step2 %f", bezierPath.t_step2);
	Font->Print(5,210, buf);
	swprintf(buf, sizeof(buf), L"form %i", prime.form);
	Font->Print(5,225, buf);
	if (move_anim2.anim_move_state==true)
	swprintf(buf, sizeof(buf), L"anim_move_state true");
	else
	swprintf(buf, sizeof(buf), L"anim_move_state false");
	Font->Print(5,240, buf);
	if (move_anim2.rotate_car==true)
	swprintf(buf, sizeof(buf), L"rotate_car true");
	else
	swprintf(buf, sizeof(buf), L"rotate_car false");
	Font->Print(5,255, buf);
	swprintf(buf, sizeof(buf), L"Liner_x %i", move_mark.Liner_x[move_anim2.ALineCh]);
	Font->Print(5,270, buf);
	swprintf(buf, sizeof(buf), L"Liner_y %i", move_mark.Liner_y[move_anim2.ALineCh]);
	Font->Print(5,285, buf);
	swprintf(buf, sizeof(buf), L"ALineCh %i", move_anim2.ALineCh);
	Font->Print(5,300, buf);
	swprintf(buf, sizeof(buf), L"rotdir %i", prime.rotdir);
	Font->Print(5,315, buf);
	swprintf(buf, sizeof(buf), L"Side %i", move_anim2._side);
	Font->Print(5,330, buf);
	swprintf(buf, sizeof(buf), L"start x%i y%i", move_anim2.start.x, move_anim2.start.y);
	Font->Print(5,345, buf);
	
	swprintf(buf, sizeof(buf), L"move_x %f", move_x);
	Font->Print(5,15, buf);
	swprintf(buf, sizeof(buf), L"move_y %f", move_y);
	Font->Print(5,60, buf);
	swprintf(buf, sizeof(buf), L"move_z %f", move_z);
	Font->Print(5,105, buf);
	swprintf(buf, sizeof(buf), L"center_x %f", center_x);
	Font->Print(5,150, buf);
	swprintf(buf, sizeof(buf), L"center_y %f", center_y);
	Font->Print(5,195, buf);
	swprintf(buf, sizeof(buf), L"center_z %f", center_z);
	Font->Print(5,240, buf);
	swprintf(buf, sizeof(buf), L"prime.Face->timer.One->time %f", prime.Face->timer.One[0].time);
	Font->Print(5,285, buf);
	swprintf(buf, sizeof(buf), L"Time %f", RunningTime);
	Font->Print(5,330, buf);
*/	
	swprintf(buf, sizeof(buf), L"Orthobasic[0] %f", Orthobasic[0]);
	Font->Print(5,15, buf);
	swprintf(buf, sizeof(buf), L"Orthobasic[1] %i", Orthobasic[1]);
	Font->Print(5,30, buf);
	swprintf(buf, sizeof(buf), L"Orthobasic[2] %i", Orthobasic[2]);
	Font->Print(5,45, buf);
	swprintf(buf, sizeof(buf), L"Orthobasic[3] %f", Orthobasic[3]);
	Font->Print(5,60, buf);
	swprintf(buf, sizeof(buf), L"Orthonew[0] %i", Orthonew[0]);
	Font->Print(5,75, buf);
	swprintf(buf, sizeof(buf), L"Orthonew[1] %f", Orthonew[1]);
	Font->Print(5,90, buf);
	swprintf(buf, sizeof(buf), L"Orthonew[2] %f", Orthonew[2]);
	Font->Print(5,105, buf);
	swprintf(buf, sizeof(buf), L"Orthonew[3] %f", Orthonew[3]);
	Font->Print(5,120, buf);
	swprintf(buf, sizeof(buf), L"lightPos[0] %f", lightPos[0]);
	Font->Print(5,135, buf);
	swprintf(buf, sizeof(buf), L"lightPos[1] %f", lightPos[1]);
	Font->Print(5,150, buf);
	swprintf(buf, sizeof(buf), L"lightPos[2] %f", lightPos[2]);
	Font->Print(5,165, buf);
	swprintf(buf, sizeof(buf), L"lightEye[0] %f", lightEye[0]);
	Font->Print(5,180, buf);
	swprintf(buf, sizeof(buf), L"lightEye[1] %f", lightEye[1]);
	Font->Print(5,195, buf);
	swprintf(buf, sizeof(buf), L"lightEye[2] %f", lightEye[2]);
	Font->Print(5,210, buf);
	swprintf(buf, sizeof(buf), L"lightUp[0] %f", lightUp[0]);
	Font->Print(5,225, buf);
	swprintf(buf, sizeof(buf), L"lightUp[1] %f", lightUp[1]);
	Font->Print(5,240, buf);
	swprintf(buf, sizeof(buf), L"lightUp[2] %f", lightUp[2]);
	Font->Print(5,255, buf);
	swprintf(buf, sizeof(buf), L"offset %i", offset);
	Font->Print(5,270, buf);
	swprintf(buf, sizeof(buf), L"offset2 %i", offset2);
	Font->Print(5,285, buf);
	int x;
	int y;
	if (move_anim2.ALineCh>0)
	{
	x=move_mark.Liner_x[move_anim2.ALineCh];
	y=move_mark.Liner_y[move_anim2.ALineCh];
	}
	else
	{
	x=move_anim2.end.x;
	y=move_anim2.end.y;
	}
	swprintf(buf, sizeof(buf), L"def x%i y%i", x, y);
	Font->Print(5,360, buf);
	
	//swprintf(buf, sizeof(buf), L"0   %f %f %f %f", temp.x, temp.y, temp.z, temp.w);

	//if (prime.Current->timer.crossTime>0.2)
	//system("Pause");
	/*
	swprintf(buf, sizeof(buf), L"1    %f", wheelController.WheelSpeed);
	Font->Print(5,30, buf);
	swprintf(buf, sizeof(buf), L"0   %f %f %f", wheelController.WheelMove.x, wheelController.WheelMove.y, wheelController.WheelMove.z);
	Font->Print(5,45, buf);
	swprintf(buf, sizeof(buf), L"0   %f %f %f %f", wheelController.WheelEngine.w, wheelController.WheelEngine.x, wheelController.WheelEngine.y, wheelController.WheelEngine.z);
	Font->Print(5,60, buf);
	swprintf(buf, sizeof(buf), L"0   %f %f %f %f", prime.Wheels2.x, prime.Wheels2.y, prime.Wheels2.z, prime.Wheels2.w);
	Font->Print(5,75, buf);
	
	swprintf(buf, sizeof(buf), L"������������  ������   %f �������   %f ������������   %f ", prime.Alternative->timer[0].time, prime.Alternative->timer[0].diff, prime.Alternative->timer[0].Duration);
	Font->Print(5,45, buf);
	swprintf(buf, sizeof(buf), L"����          ������   %f �������   %f ������������   %f ", prime.Main->timer[0].time, prime.Main->timer[0].diff, prime.Main->timer[0].Duration);
	Font->Print(5,75, buf);
	swprintf(buf, sizeof(buf), L"���������    ������   %f �������   %f ������������   %f ", prime.Transform->timer[0].time, prime.Transform->timer[0].diff, prime.Transform->timer[0].Duration);
	Font->Print(5,105, buf);
	

	for (int i=0; i<palka.vertices_qty; i++)
	{
		swprintf(buf, sizeof(buf), L"%f, %f, %f", palka.normals[i].x, palka.normals[i].y, palka.normals[i].z);
		Font->Print(5,15+i*15, buf);
	}
	m_pScene->mAnimations[0]->mDuration
	for (int i=0; i<NormalsTemp.size(); i++)
	{
	swprintf(buf, sizeof(buf), L"%f, %f, %f", NormalsTemp[i].x, NormalsTemp[i].y, NormalsTemp[i].z);
	Font->Print(315,15+15*i, buf);
	}
*/

	drawAvatar();

		
	//Draw_Window(800,550,520,192);
	/*
	glColor3f(1.0f,0.0f,0.0f);
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, R_W.texID);
	Draw_Window_Part(false, 900,600);
	glDisable(GL_TEXTURE_2D);

	glBegin(GL_LINES);
	glColor3f(1.0f,0.0f,0.0f);
	glVertex3f(960, 0, 0);
	glVertex3f(960, 1200, 0);
	glVertex3f(0, 600, 0);
	glVertex3f(1920, 600, 0);
	glEnd();
	*/
	glEnable(GL_DEPTH_TEST);
	glMatrixMode( GL_PROJECTION );
	glPopMatrix();
	glMatrixMode( GL_MODELVIEW );
}

void draw_mark(obj_type *obj, int tX, int tY, int tZ, int iX, int iY) 
{

	glPushMatrix();
	glColor3f(0.0f,0.0f,0.0f);
	glEnable(GL_BLEND);
	glDepthMask(GL_FALSE);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_TEXTURE_2D);


	if (mainmass[(int)cakepos.x][(int)cakepos.y].marked==1&&((int)cakepos.x!=unitptr->X||(int)cakepos.y!=unitptr->Y)) {


		if (unitptr->X==iX && unitptr->Y==iY) {
			switch(move_mark.LineStr) {
			case 0: {
				glBindTexture(GL_TEXTURE_2D, gextexture[13].texID);
				break; }
			case 1: {
				glBindTexture(GL_TEXTURE_2D, gextexture[12].texID);
				break; }
			case 2: {
				glBindTexture(GL_TEXTURE_2D, gextexture[11].texID);
				break; }
			case 3: {
				glBindTexture(GL_TEXTURE_2D, gextexture[10].texID);
				break; }
			case 4: {
				glBindTexture(GL_TEXTURE_2D, gextexture[15].texID);
				break; }
			case 5: {
				glBindTexture(GL_TEXTURE_2D, gextexture[14].texID);
				break; }
			}
		}
		else
		{
			if ((int)cakepos.x==iX && (int)cakepos.y==iY) {
				switch(move_mark.LineEnd) {
				case 0: {
					glBindTexture(GL_TEXTURE_2D, gextexture[19].texID);
					break; }
				case 1: {
					glBindTexture(GL_TEXTURE_2D, gextexture[18].texID);
					break; }
				case 2: {
					glBindTexture(GL_TEXTURE_2D, gextexture[17].texID);
					break; }
				case 3: {
					glBindTexture(GL_TEXTURE_2D, gextexture[16].texID);
					break; }
				case 4: {
					glBindTexture(GL_TEXTURE_2D, gextexture[21].texID);
					break; }
				case 5: {
					glBindTexture(GL_TEXTURE_2D, gextexture[20].texID);
					break; }
				}
			}
			else
			{
				if (mainmass[iX][iY].move_marked==0)
					glBindTexture(GL_TEXTURE_2D, gextexture[0].texID);
				if (mainmass[iX][iY].move_marked==1) {
					switch(mainmass[iX][iY].dirrot) {
					case 0: {
						glBindTexture(GL_TEXTURE_2D, gextexture[1].texID);
						break; }
					case 1: {
						glBindTexture(GL_TEXTURE_2D, gextexture[3].texID);
						break; }
					case 2: {
						glBindTexture(GL_TEXTURE_2D, gextexture[2].texID);
						break; }
					case 3: {
						glBindTexture(GL_TEXTURE_2D, gextexture[5].texID);
						break; }
					case 4: {
						glBindTexture(GL_TEXTURE_2D, gextexture[4].texID);
						break; }
					case 5: {
						glBindTexture(GL_TEXTURE_2D, gextexture[9].texID);
						break; }
					case 6: {
						glBindTexture(GL_TEXTURE_2D, gextexture[8].texID);
						break; }
					case 7: {
						glBindTexture(GL_TEXTURE_2D, gextexture[7].texID);
						break; }
					case 8: {
						glBindTexture(GL_TEXTURE_2D, gextexture[6].texID);
						break; }
					}


				}

			}
		}
	}
	else
		glBindTexture(GL_TEXTURE_2D, gextexture[0].texID);


	glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);

	glTranslatef(tX,tY,tZ);

	glRotatef(0,0,0,1);


	glEnableClientState(GL_VERTEX_ARRAY); 
	glEnableClientState(GL_TEXTURE_COORD_ARRAY); 
	//glEnableClientState(GL_NORMAL_ARRAY ); 
	glVertexPointer(3,GL_FLOAT,sizeof(vertex_type),obj->vertex); 
	glTexCoordPointer(2, GL_FLOAT, sizeof(mapcoord_type),obj->mapcoord); 
	//glNormalPointer(GL_FLOAT,  sizeof(vertex_type),obj->normals); 
	glDrawElements(GL_TRIANGLES,obj->polygons_qty*3,GL_UNSIGNED_INT,obj->polygon);
	glDisableClientState(GL_TEXTURE_COORD_ARRAY);
	glDisableClientState(GL_VERTEX_ARRAY);
	//glDisableClientState(GL_NORMAL_ARRAY);


	glDisable(GL_BLEND);
	glDisable(GL_TEXTURE_2D);
	glDepthMask(GL_TRUE);
	glActiveTextureARB(GL_TEXTURE0_ARB);
	//glUseProgram(0);
	glPopMatrix();
}


void fmove(int x, int y)
{

	int j=0;
	int z;
	int f;
	f=40+1;

	for (int i=0;i<(1000);i++) {
		func(x ,y-1 ,f);
		func(x ,y+1 ,f);
		func(x+1 ,y ,f);
		func(x-1 ,y ,f);
		if (x%2==0) {
			func(x+1 ,y-1 ,f);
			func(x-1 ,y-1 ,f);
		} else {
			func(x+1 ,y+1 ,f);
			func(x-1 ,y+1 ,f);
		}
		z=maxA();
		close[0][j]=open[0][z];
		close[1][j]=open[1][z];
		close[2][j]=open[2][z];
		x=open[0][z];
		y=open[1][z];
		f=open[2][z];
		open[0][z]=-11;
		open[1][z]=-11;
		open[2][z]=-11;

		if (close[2][j]<0) {
			uphod(0);
			return;
		}
		j++;
	}

}

void func(int x1, int y1 ,int f)
{


	if (x1>=0 && x1<GridWei && y1>=0 && y1<GridHei)
	{
		f=f-10-mainmass[x1][y1].passab*10;

		//if (mainmass[x1][y1].passab<3 && mainmass[x1][y1].unitptr==NULL && mainmass[x1][y1].trailermark==0 && unitptr->type==1 ||
		//	mainmass[x1][y1].passab<3 && mainmass[x1][y1].hill==0 && mainmass[x1][y1].unitptr==NULL && mainmass[x1][y1].trailermark==0 && unitptr->type==2) {
		if (mainmass[x1][y1].passab<3 && mainmass[x1][y1].unitptr==NULL && mainmass[x1][y1].trailermark==0)//move_anim2.VehicleCells(x1,y1)==false
		{

				for (int i=0;i<1000;i++) {
					if (close[0][i]==x1 && close[1][i]==y1)
						return;
				}
				for (int i=0;i<1000;i++) {
					if (open[0][i]==x1 && open[1][i]==y1) {
						if (open[2][i]<f) {
							open[2][i]=f;
							return;
						}
					}
				}

				for (int i=0;i<1000;i++) {
					if (open[0][i]==-11 && open[1][i]==-11) {
						open[0][i]=x1;
						open[1][i]=y1;
						open[2][i]=f;
						return;
					}
				}

		}
	}
}

int maxA()
{
	int mi=0;
	for (int i=0; i<1000; i++) {
		if (open[2][i]>open[2][mi])
			mi=i;
	}
	return mi;
}

void uphod(bool res)  
{

	if (res==0) {

		for (int i=0; i<1000; i++) {
			if (close[2][i]>0)	{
				mainmass[close[0][i]][close[1][i]].marked=1;
			}
		}

		for (int i=0; i<1000; i++) {
			for (int j=0; j<3; j++) {
				open[j][i]=-11;
				close[j][i]=0;
			}
		}
	} else {

		for (int i=0; i<GridWei; i++){
			for (int j=0; j<GridHei; j++){
				mainmass[i][j].marked=0;
				mainmass[i][j].move_marked=0;
			}
		}

		for (int i=0; i<1000; i++) {
			for (int j=0; j<3; j++) {
				open[j][i]=-11;
				close[j][i]=-11;
			}
		}
	}

} 

void move_anim2::anim_func(bool vehicle)
{

bezierPath.ClearPath();
if (vehicle==false)
bezierPath.start(start.x,start.y,start.z);
else
{
if (end_animation==false)
bezierPath.AddPoint(9,b_start,b_exStart,b_exEnd,oldRot);
else
{
bezierPath.AddPoint(2,b_start,oldRot);
end_animation=false;
}
}


for (int i = ALineCh; i>=0; i--)
{
if (move_mark.Liner_z[i]==1) 
bezierPath.l1(move_mark.Liner_x[i],move_mark.Liner_y[i]);
if (move_mark.Liner_z[i]==2) 
bezierPath.l2(move_mark.Liner_x[i],move_mark.Liner_y[i]);
if (move_mark.Liner_z[i]==3) 
bezierPath.l3(move_mark.Liner_x[i],move_mark.Liner_y[i]);
if (move_mark.Liner_z[i]==4) 
bezierPath.l4(move_mark.Liner_x[i],move_mark.Liner_y[i]);
if (move_mark.Liner_z[i]==5) 
bezierPath.l5(move_mark.Liner_x[i],move_mark.Liner_y[i]);
if (move_mark.Liner_z[i]==6) 
bezierPath.l6(move_mark.Liner_x[i],move_mark.Liner_y[i]);
if (move_mark.Liner_z[i]==7) 
bezierPath.r1(move_mark.Liner_x[i],move_mark.Liner_y[i]);
if (move_mark.Liner_z[i]==8) 
bezierPath.r2(move_mark.Liner_x[i],move_mark.Liner_y[i]);
if (move_mark.Liner_z[i]==9) 
bezierPath.r7(move_mark.Liner_x[i],move_mark.Liner_y[i]);
if (move_mark.Liner_z[i]==10) 
bezierPath.r8(move_mark.Liner_x[i],move_mark.Liner_y[i]);
if (move_mark.Liner_z[i]==11) 
bezierPath.r3(move_mark.Liner_x[i],move_mark.Liner_y[i]);
if (move_mark.Liner_z[i]==12) 
bezierPath.r4(move_mark.Liner_x[i],move_mark.Liner_y[i]);	
if (move_mark.Liner_z[i]==13) 
bezierPath.r5(move_mark.Liner_x[i],move_mark.Liner_y[i]);
if (move_mark.Liner_z[i]==14) 
bezierPath.r6(move_mark.Liner_x[i],move_mark.Liner_y[i]);
if (move_mark.Liner_z[i]==15) 
bezierPath.r9(move_mark.Liner_x[i],move_mark.Liner_y[i]);
if (move_mark.Liner_z[i]==16) 
bezierPath.r10(move_mark.Liner_x[i],move_mark.Liner_y[i]);
if (move_mark.Liner_z[i]==17) 
bezierPath.r11(move_mark.Liner_x[i],move_mark.Liner_y[i]);
if (move_mark.Liner_z[i]==18) 
bezierPath.r12(move_mark.Liner_x[i],move_mark.Liner_y[i]);
}



bezierPath.end(end.x,end.y,end.z);
if (unitptr->form==0)
bezierPath.GetDrawingPoints(unitptr->form+1, true);
else
bezierPath.GetDrawingPoints(unitptr->form+1, false);


}


int aStar_min()

{

	int mi=0;
	for (int i=0;i<(GridWei*GridHei);i++)
	{
		if (openMa[0][mi]==-1)
			mi++;
		else
		{
			if ((openMa[2][i] + openMa[3][i])<=(openMa[2][mi] + openMa[3][mi]) && openMa[0][i]!=-1)
			{
				mi=i;
			}
		}
	}

	return mi;

}

void aStar_func(int x,int y,int par)
{

	//if (x>-1 && x<GridWei && y>-1 && y<GridHei && mainmass[x][y].passab!=3 && mainmass[x][y].unitptr==NULL && mainmass[x][y].trailermark==0 && unitptr->type==1 ||
	//	x>-1 && x<GridWei && y>-1 && y<GridHei && mainmass[x][y].passab!=3  && mainmass[x][y].hill==0 && mainmass[x][y].unitptr==NULL && mainmass[x][y].trailermark==0 && unitptr->type==2)
	if (x>-1 && x<GridWei && y>-1 && y<GridHei && mainmass[x][y].passab!=3 && mainmass[x][y].unitptr==NULL && mainmass[x][y].trailermark==0)//move_anim2.VehicleCells(x,y)==false
	{

		for (int i=0;i<(GridWei*GridHei);i++)
		{
			if (closeMa[0][i]==x && closeMa[1][i]==y)
			{

				return;
			}
		}
		for (int i=0;i<(GridWei*GridHei);i++)
		{
			if (openMa[0][i]==x && openMa[1][i]==y  )
			{

				if (openMa[2][i]>(mainmass[x][y].passab+1)*70+closeMa[2][par])
				{

					openMa[2][i]=(mainmass[x][y].passab+1)*70+closeMa[2][par];
					openMa[4][i]=par;
					return;
				}
				return;
			}
		}
		for (int i=0;i<(GridWei*GridHei);i++)
		{
			if ( openMa[0][i]==-1)
			{
				Vector2f temp=MainToOxy((int)cakepos.x, (int)cakepos.y);
				Vector2f temp2=MainToOxy(x, y);
				openMa[0][i]=x;
				openMa[1][i]=y;
				openMa[2][i]=(mainmass[x][y].passab+1)*70+closeMa[2][par];
				double temp3=(temp.x-temp2.x)*(temp.x-temp2.x) + (temp.y-temp2.y)*(temp.y-temp2.y);
				openMa[3][i]=sqrt(temp3);
				openMa[4][i]=par;

				return;

			}
		}
	}
	else
		return;

}

bool aStar(int x, int y)

{

	for (int i=0; i<GridWei; i++)
		for (int j=0; j<GridHei; j++)
		{{
			mainmass[i][j].move_marked=0;
		}}
		int z;

		mainmass[(int)cakepos.x][(int)cakepos.y].move_marked=1;
		mainmass[unitptr->X][unitptr->Y].move_marked=1;
		for (int i=0; i<(GridWei*GridHei); i++)
		{
			openMa[0][i]=-1;
			openMa[1][i]=-1;
			openMa[2][i]=-1;
			openMa[3][i]=-1;
			openMa[4][i]=-1;

			closeMa[0][i]=-1;
			closeMa[1][i]=-1;
			closeMa[2][i]=-1;
			closeMa[3][i]=-1;
			closeMa[4][i]=-1;
		}
		Vector2f temp=MainToOxy((int)cakepos.x, (int)cakepos.y);
		Vector2f temp2=MainToOxy(x, y);
		openMa[0][0]=x;
		openMa[1][0]=y;
		openMa[2][0]=0;
		double temp3=(temp.x-temp2.x)*(temp.x-temp2.x) + (temp.y-temp2.y)*(temp.y-temp2.y);
		openMa[3][0]=sqrt(temp3);
		openMa[4][0]=0;

		for (int u=0;u<(GridWei*GridHei);u++)
		{
			z=aStar_min();
			if (openMa[0][z]==-1 || openMa[1][z]==-1)
			{
				return 0;
			}
			for (int i=0;i<5;i++)
			{
				closeMa[i][u]=openMa[i][z];
				openMa[i][z]=-1;
			}

			x=closeMa[0][u];
			y=closeMa[1][u];

			aStar_func(x ,y-1,u );
			aStar_func(x ,y+1,u );
			aStar_func(x+1 ,y,u );
			aStar_func(x-1 ,y,u );
			if (x%2==0)
			{
				aStar_func(x+1 ,y-1,u );
				aStar_func(x-1 ,y-1,u );
			}
			else
			{
				aStar_func(x+1 ,y+1,u );
				aStar_func(x-1 ,y+1,u );
			}


			for (int i=0; i<(GridWei*GridHei); i++)
			{
				if (openMa[0][i]==(int)cakepos.x && openMa[1][i]==(int)cakepos.y) //������� � �������� ���� (���� ������)
				{
					//MessageBox(NULL, "���� ������", "INFO", MB_OK);
					aStar_func1(i);
					return 1;
					//return openMa[2][i]/70;
				}

			}
		}

		return 0;


}
void aStar_func1(int z)
{


	move_mark.LineCh=0;
	int par=openMa[4][z];
	while(par != 0)
	{
		mainmass[closeMa[0][par]][closeMa[1][par]].move_marked=1;

		move_mark.LineCh++;
		move_mark.Liner_x[move_mark.LineCh]=closeMa[0][par];
		move_mark.Liner_y[move_mark.LineCh]=closeMa[1][par];


		par=closeMa[4][par];
	}


	return;

}

void SetLineCh()
{
	if ((int)cakepos.x==unitptr->X && (int)cakepos.y==unitptr->Y) 
	{
		return;
	}
	for (int i = move_mark.LineCh; i > 0; i--) {
		if (i==move_mark.LineCh) {
			move_mark.Liner_ox[i]=unitptr->X;
			move_mark.Liner_oy[i]=unitptr->Y;
		}
		else
		{
			move_mark.Liner_ox[i]=move_mark.Liner_x[i+1];
			move_mark.Liner_oy[i]=move_mark.Liner_y[i+1];
		}
	}


	for (int i = move_mark.LineCh; i > 0; i--) {
		if (mainmass[move_mark.Liner_x[i]][move_mark.Liner_y[i]+1].move_marked==1
			&& mainmass[move_mark.Liner_x[i]][move_mark.Liner_y[i]-1].move_marked==1)
		{
			if (move_mark.Liner_oy[i]==move_mark.Liner_y[i]+1)
				move_mark.Liner_z[i]=1;
			else
				move_mark.Liner_z[i]=2;	
			mainmass[move_mark.Liner_x[i]][move_mark.Liner_y[i]].dirrot=0;
		}

		if (move_mark.Liner_x[i]%2==0) {
			if (mainmass[move_mark.Liner_x[i]+1][move_mark.Liner_y[i]-1].move_marked==1
				&& mainmass[move_mark.Liner_x[i]-1][move_mark.Liner_y[i]].move_marked==1)
			{
				if (move_mark.Liner_oy[i]==move_mark.Liner_y[i] && move_mark.Liner_ox[i]==move_mark.Liner_x[i]-1)
					move_mark.Liner_z[i]=3;
				else
					move_mark.Liner_z[i]=4;
				mainmass[move_mark.Liner_x[i]][move_mark.Liner_y[i]].dirrot=1;
			}

			if (mainmass[move_mark.Liner_x[i]+1][move_mark.Liner_y[i]].move_marked==1
				&& mainmass[move_mark.Liner_x[i]-1][move_mark.Liner_y[i]-1].move_marked==1)
			{
				if (move_mark.Liner_oy[i]==move_mark.Liner_y[i] && move_mark.Liner_ox[i]==move_mark.Liner_x[i]+1)
					move_mark.Liner_z[i]=5;
				else
					move_mark.Liner_z[i]=6;
				mainmass[move_mark.Liner_x[i]][move_mark.Liner_y[i]].dirrot=2;
			}

			if (mainmass[move_mark.Liner_x[i]+1][move_mark.Liner_y[i]].move_marked==1
				&& mainmass[move_mark.Liner_x[i]-1][move_mark.Liner_y[i]].move_marked==1)
			{
				if (move_mark.Liner_ox[i]==move_mark.Liner_x[i]+1)
					move_mark.Liner_z[i]=8;
				else
					move_mark.Liner_z[i]=7;
				mainmass[move_mark.Liner_x[i]][move_mark.Liner_y[i]].dirrot=3;
			}

			if (mainmass[move_mark.Liner_x[i]+1][move_mark.Liner_y[i]-1].move_marked==1
				&& mainmass[move_mark.Liner_x[i]-1][move_mark.Liner_y[i]-1].move_marked==1)
			{
				if (move_mark.Liner_ox[i]==move_mark.Liner_x[i]+1)
					move_mark.Liner_z[i]=9;
				else
					move_mark.Liner_z[i]=10;
				mainmass[move_mark.Liner_x[i]][move_mark.Liner_y[i]].dirrot=4;
			}

			if (mainmass[move_mark.Liner_x[i]][move_mark.Liner_y[i]-1].move_marked==1
				&& mainmass[move_mark.Liner_x[i]-1][move_mark.Liner_y[i]].move_marked==1)
			{
				if (move_mark.Liner_oy[i]==move_mark.Liner_y[i] && move_mark.Liner_ox[i]==move_mark.Liner_x[i]-1)
					move_mark.Liner_z[i]=16;
				else
					move_mark.Liner_z[i]=15;
				mainmass[move_mark.Liner_x[i]][move_mark.Liner_y[i]].dirrot=5;
			}

			if (mainmass[move_mark.Liner_x[i]][move_mark.Liner_y[i]+1].move_marked==1
				&& mainmass[move_mark.Liner_x[i]+1][move_mark.Liner_y[i]-1].move_marked==1)
			{
				if (move_mark.Liner_oy[i]==move_mark.Liner_y[i]+1)
					move_mark.Liner_z[i]=11;
				else
					move_mark.Liner_z[i]=12;
				mainmass[move_mark.Liner_x[i]][move_mark.Liner_y[i]].dirrot=6;
			}

			if (mainmass[move_mark.Liner_x[i]][move_mark.Liner_y[i]-1].move_marked==1
				&& mainmass[move_mark.Liner_x[i]+1][move_mark.Liner_y[i]].move_marked==1)
			{
				if (move_mark.Liner_oy[i]==move_mark.Liner_y[i]-1)
					move_mark.Liner_z[i]=14;
				else
					move_mark.Liner_z[i]=13;
				mainmass[move_mark.Liner_x[i]][move_mark.Liner_y[i]].dirrot=7;
			}

			if (mainmass[move_mark.Liner_x[i]][move_mark.Liner_y[i]+1].move_marked==1
				&& mainmass[move_mark.Liner_x[i]-1][move_mark.Liner_y[i]-1].move_marked==1)
			{
				if (move_mark.Liner_oy[i]==move_mark.Liner_y[i]+1)
					move_mark.Liner_z[i]=18;
				else
					move_mark.Liner_z[i]=17;
				mainmass[move_mark.Liner_x[i]][move_mark.Liner_y[i]].dirrot=8;
			}

		}
		else
		{
			if (mainmass[move_mark.Liner_x[i]+1][move_mark.Liner_y[i]].move_marked==1
				&& mainmass[move_mark.Liner_x[i]-1][move_mark.Liner_y[i]+1].move_marked==1)
			{
				if (move_mark.Liner_oy[i]==move_mark.Liner_y[i]+1 && move_mark.Liner_ox[i]==move_mark.Liner_x[i]-1)
					move_mark.Liner_z[i]=3;
				else
					move_mark.Liner_z[i]=4;
				mainmass[move_mark.Liner_x[i]][move_mark.Liner_y[i]].dirrot=1;
			}


			if (mainmass[move_mark.Liner_x[i]+1][move_mark.Liner_y[i]+1].move_marked==1
				&& mainmass[move_mark.Liner_x[i]-1][move_mark.Liner_y[i]].move_marked==1)
			{
				if (move_mark.Liner_oy[i]==move_mark.Liner_y[i]+1 && move_mark.Liner_ox[i]==move_mark.Liner_x[i]+1)
					move_mark.Liner_z[i]=5;
				else
					move_mark.Liner_z[i]=6;
				mainmass[move_mark.Liner_x[i]][move_mark.Liner_y[i]].dirrot=2;
			}

			if (mainmass[move_mark.Liner_x[i]+1][move_mark.Liner_y[i]+1].move_marked==1
				&& mainmass[move_mark.Liner_x[i]-1][move_mark.Liner_y[i]+1].move_marked==1)
			{
				if (move_mark.Liner_ox[i]==move_mark.Liner_x[i]+1)
					move_mark.Liner_z[i]=8;
				else
					move_mark.Liner_z[i]=7;
				mainmass[move_mark.Liner_x[i]][move_mark.Liner_y[i]].dirrot=3;
			}

			if (mainmass[move_mark.Liner_x[i]+1][move_mark.Liner_y[i]].move_marked==1
				&& mainmass[move_mark.Liner_x[i]-1][move_mark.Liner_y[i]].move_marked==1)
			{
				if (move_mark.Liner_ox[i]==move_mark.Liner_x[i]+1)
					move_mark.Liner_z[i]=9;
				else
					move_mark.Liner_z[i]=10;
				mainmass[move_mark.Liner_x[i]][move_mark.Liner_y[i]].dirrot=4;
			}

			if (mainmass[move_mark.Liner_x[i]][move_mark.Liner_y[i]-1].move_marked==1
				&& mainmass[move_mark.Liner_x[i]-1][move_mark.Liner_y[i]+1].move_marked==1)
			{
				if (move_mark.Liner_oy[i]==move_mark.Liner_y[i]+1 && move_mark.Liner_ox[i]==move_mark.Liner_x[i]-1)
					move_mark.Liner_z[i]=16;
				else
					move_mark.Liner_z[i]=15;
				mainmass[move_mark.Liner_x[i]][move_mark.Liner_y[i]].dirrot=5;
			}

			if (mainmass[move_mark.Liner_x[i]][move_mark.Liner_y[i]+1].move_marked==1
				&& mainmass[move_mark.Liner_x[i]+1][move_mark.Liner_y[i]].move_marked==1)
			{
				if (move_mark.Liner_oy[i]==move_mark.Liner_y[i]+1)
					move_mark.Liner_z[i]=11;
				else
					move_mark.Liner_z[i]=12;
				mainmass[move_mark.Liner_x[i]][move_mark.Liner_y[i]].dirrot=6;
			}

			if (mainmass[move_mark.Liner_x[i]][move_mark.Liner_y[i]-1].move_marked==1
				&& mainmass[move_mark.Liner_x[i]+1][move_mark.Liner_y[i]+1].move_marked==1)
			{
				if (move_mark.Liner_oy[i]==move_mark.Liner_y[i]-1)
					move_mark.Liner_z[i]=14;
				else
					move_mark.Liner_z[i]=13;
				mainmass[move_mark.Liner_x[i]][move_mark.Liner_y[i]].dirrot=7;
			}

			if (mainmass[move_mark.Liner_x[i]][move_mark.Liner_y[i]+1].move_marked==1
				&& mainmass[move_mark.Liner_x[i]-1][move_mark.Liner_y[i]].move_marked==1)
			{
				if (move_mark.Liner_oy[i]==move_mark.Liner_y[i]+1)
					move_mark.Liner_z[i]=18;
				else
					move_mark.Liner_z[i]=17;
				mainmass[move_mark.Liner_x[i]][move_mark.Liner_y[i]].dirrot=8;
			}


		}
	}
	move_mark.LineStr=SetLineSE(unitptr->X,unitptr->Y);
	move_mark.LineStr_E=SetLineSE_E(unitptr->X,unitptr->Y);
	move_mark.LineEnd=SetLineSE((int)cakepos.x,(int)cakepos.y);
}

int SetLineSE(int x,int y)
{
	int buf;
	buf=6;
	if (mainmass[x][y+1].move_marked==1)
		buf=3;
	if (mainmass[x][y-1].move_marked==1)
		buf=0;
	if (x%2==0) {
		if (mainmass[x+1][y-1].move_marked==1)
			buf=1;
		if (mainmass[x+1][y].move_marked==1)
			buf=2;
		if (mainmass[x-1][y-1].move_marked==1)
			buf=5;
		if (mainmass[x-1][y].move_marked==1)
			buf=4;
	}
	else
	{
		if (mainmass[x+1][y].move_marked==1)
			buf=1;
		if (mainmass[x+1][y+1].move_marked==1)
			buf=2;
		if (mainmass[x-1][y].move_marked==1)
			buf=5;
		if (mainmass[x-1][y+1].move_marked==1)
			buf=4;
	}
	return buf;
}

int SetLineSE_E(int x,int y)
{
	int buf;
	buf=6;
	if (mainmass[x][y+1].move_marked==1)
		buf=0;
	if (mainmass[x][y-1].move_marked==1)
		buf=3;
	if (x%2==0) {
		if (mainmass[x+1][y-1].move_marked==1)
			buf=4;
		if (mainmass[x+1][y].move_marked==1)
			buf=5;
		if (mainmass[x-1][y-1].move_marked==1)
			buf=2;
		if (mainmass[x-1][y].move_marked==1)
			buf=1;
	}
	else
	{
		if (mainmass[x+1][y].move_marked==1)
			buf=4;
		if (mainmass[x+1][y+1].move_marked==1)
			buf=5;
		if (mainmass[x-1][y].move_marked==1)
			buf=2;
		if (mainmass[x-1][y+1].move_marked==1)
			buf=1;
	}
	return buf;
}

void move_anim2::anim_move_main()
{
	t=0;
	anim_move_flag=true;
	start.x=unitptr->X;
	start.y=unitptr->Y;
	oldRot=unitptr->rotdir;

	start.z=move_mark.LineStr_E;
	end.x=(int)cakepos.x;
	end.y=(int)cakepos.y;
	end.z=move_mark.LineEnd;
	ALineCh=move_mark.LineCh;
	
	rotate=oldRot*60;

	if (unitptr->form==1)
	{
	anim_move_rotate=true;
	anim_func(false);
	unitptr->Current->timer.animation_index=7;
	unitptr->Current->timer.One[unitptr->Current->timer.animation_index].Start();
	rotate_around();
	}
	else
	if (Forward()==true)
	{
	anim_func(false);
	unitptr->Current->timer.One[unitptr->Current->timer.animation_index].on=false;
	unitptr->Current->timer.animation_index=1;
	unitptr->Current->timer.One[unitptr->Current->timer.animation_index].Start();
	wheelController.Start(1);
	}
	else
	{
	unitptr->CarMoveBack();
	}
	
	
	

	

	
}

bool move_anim2::Forward()
{
switch (unitptr->rotdir)
{
case 3:
	if (move_mark.Liner_y[ALineCh]==(start.y-1) && move_mark.Liner_x[ALineCh]==start.x && ALineCh>0 || ALineCh==0 && end.y==(start.y-1) && end.x==start.x)
return true;
break;
case 0:
	if (move_mark.Liner_y[ALineCh]==(start.y+1) && move_mark.Liner_x[ALineCh]==start.x && ALineCh>0 || ALineCh==0 && end.y==(start.y+1) && end.x==start.x)
return true;
break;
case 1:
	if (move_mark.Liner_y[ALineCh]==(start.y+1) && move_mark.Liner_x[ALineCh]==(start.x-1) && ALineCh>0 && start.x%2==1 ||
		move_mark.Liner_y[ALineCh]==(start.y) && move_mark.Liner_x[ALineCh]==(start.x-1) && ALineCh>0 && start.x%2==0 ||
		end.y==(start.y+1) && end.x==(start.x-1) && ALineCh==0 && start.x%2==1 ||
		end.y==(start.y) && end.x==(start.x-1) && ALineCh==0 && start.x%2==0)
		
return true;
break;
case 5:
	if (move_mark.Liner_y[ALineCh]==(start.y+1) && move_mark.Liner_x[ALineCh]==(start.x+1) && ALineCh>0 && start.x%2==1 ||
		move_mark.Liner_y[ALineCh]==(start.y) && move_mark.Liner_x[ALineCh]==(start.x+1) && ALineCh>0 && start.x%2==0 ||
		end.y==(start.y+1) && end.x==(start.x+1) && ALineCh==0 && start.x%2==1 ||
		end.y==(start.y) && end.x==(start.x+1) && ALineCh==0 && start.x%2==0)
		
return true;
break;
case 2:
if (move_mark.Liner_y[ALineCh]==(start.y) && move_mark.Liner_x[ALineCh]==(start.x-1) && ALineCh>0 && start.x%2==1 ||
		move_mark.Liner_y[ALineCh]==(start.y-1) && move_mark.Liner_x[ALineCh]==(start.x-1) && ALineCh>0 && start.x%2==0 ||
		end.y==(start.y) && end.x==(start.x-1) && ALineCh==0 && start.x%2==1 ||
		end.y==(start.y-1) && end.x==(start.x-1) && ALineCh==0 && start.x%2==0)
		
return true;
break;
case 4:
if (move_mark.Liner_y[ALineCh]==(start.y) && move_mark.Liner_x[ALineCh]==(start.x+1) && ALineCh>0 && start.x%2==1 ||
		move_mark.Liner_y[ALineCh]==(start.y-1) && move_mark.Liner_x[ALineCh]==(start.x+1) && ALineCh>0 && start.x%2==0 ||
		end.y==(start.y) && end.x==(start.x+1) && ALineCh==0 && start.x%2==1 ||
		end.y==(start.y-1) && end.x==(start.x+1) && ALineCh==0 && start.x%2==0)
		
return true;
break;
}
return false;
}


int move_anim2::Side(int rot)
{
	int result=15;
	int x;
	int y;
	if (ALineCh>0)
	{
	x=move_mark.Liner_x[ALineCh];
	y=move_mark.Liner_y[ALineCh];
	}
	else
	{
	x=end.x;
	y=end.y;
	}

	switch (rot)
	{
		
	case 3:
		

		if (x==start.x-1 && y==start.y-1 && start.x%2==0 || x==start.x-1 && y==start.y && start.x%2==1)
		result=1;
		else
		if (x==start.x-1 && y==start.y && start.x%2==0 || x==start.x-1 && y==start.y+1 && start.x%2==1)
		result=3;
		else
		if (x==start.x+1 && y==start.y-1 && start.x%2==0 || x==start.x+1 && y==start.y && start.x%2==1)
		result=2;
		else
		if (x==start.x+1 && y==start.y && start.x%2==0 || x==start.x+1 && y==start.y+1 && start.x%2==1)
		result=4;
		else
		if (x==start.x && y==start.y+1)
		result=5;
		break;
			
	case 0:
		if (x==start.x-1 && y==start.y-1 && start.x%2==0 || x==start.x-1 && y==start.y   && start.x%2==1)
		result=3;
		else
		if (x==start.x-1 && y==start.y   && start.x%2==0 || x==start.x-1 && y==start.y+1 && start.x%2==1)
		result=1;
		else
		if (x==start.x+1 && y==start.y-1 && start.x%2==0 || x==start.x+1 && y==start.y   && start.x%2==1)
		result=4;
		else
		if (x==start.x+1 && y==start.y   && start.x%2==0 || x==start.x+1 && y==start.y+1 && start.x%2==1)
		result=2;
		else
		if (x==start.x && y==start.y-1)
		result=5;
		
		break;
	case 2:
		if (x==start.x && y==start.y-1)
		result=1;
		if (x==start.x-1 && y==start.y && start.x%2==0 || x==start.x-1 && y==start.y+1 && start.x%2==1)
		result=2;
		if (x==start.x && y==start.y+1)
		result=3;
		if (x==start.x+1 && y==start.y-1 && start.x%2==0 || x==start.x+1 && y==start.y && start.x%2==1)
		result=4;
		if (x==start.x+1 && y==start.y && start.x%2==0 || x==start.x+1 && y==start.y+1 && start.x%2==1)
		result=5;
		break;
	
	case 1:
		if (x==start.x && y==start.y-1)
		result=3;
		if (x==start.x-1 && y==start.y-1 && start.x%2==0 || x==start.x-1 && y==start.y && start.x%2==1)
		result=2;
		if (x==start.x && y==start.y+1)
		result=1;
		if (x==start.x+1 && y==start.y && start.x%2==0 || x==start.x+1 && y==start.y+1 && start.x%2==1)
		result=4;
		if (x==start.x+1 && y==start.y-1 && start.x%2==0 || x==start.x+1 && y==start.y && start.x%2==1)
		result=5;
		break;

	case 4:
		if (x==start.x && y==start.y-1)
		result=1;
		if (x==start.x-1 && y==start.y-1 && start.x%2==0 || x==start.x-1 && y==start.y && start.x%2==1)
		result=4;
		if (x==start.x && y==start.y+1)
		result=3;
		if (x==start.x+1 && y==start.y && start.x%2==0 || x==start.x+1 && y==start.y+1 && start.x%2==1)
		result=2;
		if (x==start.x-1 && y==start.y && start.x%2==0 || x==start.x-1 && y==start.y+1 && start.x%2==1)
		result=5;
		break;
	
	case 5:
		if (x==start.x && y==start.y-1)
		result=3;
		if (x==start.x-1 && y==start.y && start.x%2==0 || x==start.x-1 && y==start.y+1 && start.x%2==1)
		result=4;
		if (x==start.x && y==start.y+1)
		result=1;
		if (x==start.x+1 && y==start.y-1 && start.x%2==0 || x==start.x+1 && y==start.y && start.x%2==1)
		result=2;
		if (x==start.x-1 && y==start.y-1 && start.x%2==0 || x==start.x-1 && y==start.y && start.x%2==1)
		result=5;
		break;
		
	default:
		result=10;
		break;
		
}

return result;

}




void DrawMove()
{
	int ch=0;
	for (int j = 0; j < GridHei; j++)
		for (int i = 0; i < GridWei; i++) {
			Vector2f p=MainToOxy(i, j);
			if (move_mark.on_mark==true)
				if (mainmass[i][j].marked==1 || i==unitptr->X && j==unitptr->Y)
				{
					if (i%2==0)
						draw_mark(&move_mark.marknum[ch],(int)p.x+1,(int)p.y-18.45,-2, i, j);
					else
						draw_mark(&move_mark.marknum[ch],(int)p.x+1,(int)p.y-19, -2, i, j);
					ch++;
				}
		}

}

void CalcMark(obj_type *obj, int wey, int hey)
{

	float jump;
	if (wey%2==0) jump=0; else jump=24;
	wey=wey*6;
	hey=hey*7;
	int polch=0;
	for (int j = 0; j < 8; j++) {
		for (int i = 0; i < 7*8; i++) {
			obj->vertex[GetTriangleDotO(obj,0,polch)].z=MapVertexArray[GetTriangleDot(0,i+j*56+hey*8+jump+GridSetHei*8*wey+(GridSetHei-7)*8*j)].z;
			obj->vertex[GetTriangleDotO(obj,1,polch)].z=MapVertexArray[GetTriangleDot(1,i+j*56+hey*8+jump+GridSetHei*8*wey+(GridSetHei-7)*8*j)].z;
			obj->vertex[GetTriangleDotO(obj,2,polch)].z=MapVertexArray[GetTriangleDot(2,i+j*56+hey*8+jump+GridSetHei*8*wey+(GridSetHei-7)*8*j)].z;
			polch++;
		}
	}
}



void CalcAllMarks()
{
	move_mark.markch=0;
	for (int j = 0; j < GridHei; j++)
		for (int i = 0; i < GridWei; i++) {
			if (mainmass[i][j].marked==1  || (i==unitptr->X && j==unitptr->Y)) {
				CalcMark(&move_mark.marknum[move_mark.markch], i, j);
				move_mark.markch++;
			}
		}
}


void drawrect(int width, wchar_t *buf, int size)
{
	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	glOrtho( 0,GetSystemMetrics(SM_CXSCREEN),GetSystemMetrics(SM_CYSCREEN), 0,0, 1 );
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glDisable(GL_DEPTH_TEST);

	glColor4f(0.0f,0.0f,0.3f,1.0f);   

	glBegin(GL_QUADS);  
	glVertex2d(759,999);
	glVertex2d(1161,999);
	glVertex2d(1161,1031);
	glVertex2d(759,1031);
	glEnd();

	glColor4f(0.2f,0.2f,1.0f,1.0f);

	glBegin(GL_QUADS); 
	glVertex2d(760,1000);
	glVertex2d(760+width,1000);
	glVertex2d(760+width,1030);
	glVertex2d(760,1030);
	glEnd();

	Font->Print(960-size,980, buf);

	glEnable(GL_DEPTH_TEST);
	glMatrixMode( GL_PROJECTION );
	glPopMatrix();
	glMatrixMode( GL_MODELVIEW );
}


void drawload(int width, wchar_t *buf, int size)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glDisable(GL_TEXTURE_2D);
	drawrect(width, buf, size);
	SwapBuffers(g_hDC);
	glLoadIdentity();
	gluLookAt(0,0,0,0,0,0,0,0,-1);
}

void Mouseclick()
{
	if (mainmass[(int)cakepos.x][(int)cakepos.y].unitptr!=NULL) {
		if (move_mark.on_mark==false) {
			move_mark.on_mark=true;
			unitptr = mainmass[(int)cakepos.x][(int)cakepos.y].unitptr;
			uphod(1);
			//move_anim2.VehicleCells(unitptr->rotdir, (int)cakepos.x, (int)cakepos.y);
			fmove(unitptr->X,unitptr->Y);
			CalcAllMarks();
			return;
		}
		else {
			if (mainmass[(int)cakepos.x][(int)cakepos.y].unitptr!=unitptr)
			{

				uphod(1);
				unitptr = mainmass[(int)cakepos.x][(int)cakepos.y].unitptr;
				fmove(unitptr->X,unitptr->Y);
				CalcAllMarks();
				return;
			}
			else
			{
				move_mark.on_mark=false;
				uphod(1);
				return;
			}
		}
	} else {
		if ((move_mark.on_mark==true) && (mainmass[(int)cakepos.x][(int)cakepos.y].marked==1) && (mainmass[(int)cakepos.x][(int)cakepos.y].unitptr==NULL))  {
			move_anim2.anim_move_main();
			
			if (unitptr->type==2)
			{
				mainmass[unitptr->trailpos_x][unitptr->trailpos_y].trailermark=0;
				if (move_mark.LineCh>0) {
					mainmass[move_mark.Liner_x[1]][move_mark.Liner_y[1]].trailermark=1;
					unitptr->trailpos_x=move_mark.Liner_x[1];
					unitptr->trailpos_y=move_mark.Liner_y[1];
				}
				else {
					mainmass[unitptr->X][unitptr->Y].trailermark=1;
					unitptr->trailpos_x=unitptr->X;
					unitptr->trailpos_y=unitptr->Y;
				}
			}
			mainmass[unitptr->X][unitptr->Y].unitptr=NULL;
			
			unitptr->X=(int)cakepos.x;
			unitptr->Y=(int)cakepos.y;
			unitptr->rotdir=move_anim2.end.z;
			mainmass[(int)cakepos.x][(int)cakepos.y].unitptr=unitptr;

			//move_anim.anim_z[0]=unitptr->z_up;
			//move_anim.z_count=0;
			/*
			if (unitptr->X%2==0)
				unitptr->z_up=MapVertexArray[GetTriangleDot(1,unitptr->X*6*GridSetHei*8+unitptr->Y*7*8+4*GridSetHei*8+8*3)].z;
			else
				unitptr->z_up=MapVertexArray[GetTriangleDot(0,unitptr->X*6*GridSetHei*8+unitptr->Y*7*8+4*GridSetHei*8+8*7)].z;
			*/


			move_mark.on_mark=false;
			uphod(1);
			return;
		}
		else
		{
			move_mark.on_mark=false;
			uphod(1);
			return;
		}
	}

}

void RenderSceneMenu()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glColor3f(1.0f,1.0f,1.0f);
	CreateSkyBox(CamPos.x, CamPos.y, CamPos.z, 800, 400, 800);


	DrawMenu();
	//draw_GUI();
	SwapBuffers(g_hDC);
	glLoadIdentity();
	MoveCamera();
	gluLookAt(CamPos.x,CamPos.y,CamPos.z,0,0,1.1,0,1,0);
	CamPos.x+=5;
}

void DrawMenu()
{
	wchar_t buf[256];
	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	glOrtho( 0,GetSystemMetrics(SM_CXSCREEN),GetSystemMetrics(SM_CYSCREEN), 0,0, 1 );
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glDisable(GL_DEPTH_TEST);


	//DrawScreen();
	//draw_GUI();

	DrawButton(50, 500, 0);
	if (buttoncheck[0]==0)
		glColor4f(0.0f,0.0f,0.0f,1.0f);
	else
		glColor4f(1.0f,1.0f,1.0f,1.0f);
	swprintf(buf, sizeof(buf), L"������ ����");
	Font->Print(130,525, buf);
	DrawButton(50, 570, 1);
	if (buttoncheck[1]==0)
		glColor4f(0.0f,0.0f,0.0f,1.0f);
	else
		glColor4f(1.0f,1.0f,1.0f,1.0f); 
	swprintf(buf, sizeof(buf), L"�����");
	Font->Print(145,595, buf);

	DrawButton(50, 640, 2);

	glEnable(GL_DEPTH_TEST);
	glMatrixMode( GL_PROJECTION );
	glPopMatrix();
	glMatrixMode( GL_MODELVIEW );
}

void DrawButton(int x, int y, int check)
{
	glEnable(GL_BLEND);

	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glColor4f(1.0f,1.0f,1.0f,1.0f);
	if (buttoncheck[check]==0)
		glBindTexture(GL_TEXTURE_2D, button.texID);
	if (buttoncheck[check]==1)
		glBindTexture(GL_TEXTURE_2D, button_on.texID);

	if (check==2)
	{
		if (buttoncheck[check]==0)
		{
			if (current_mapmode==0)
				glBindTexture(GL_TEXTURE_2D, TRI1.texID);
			if (current_mapmode==1)
				glBindTexture(GL_TEXTURE_2D, LIN1.texID);
		}
		if (buttoncheck[check]==1)
		{
			if (current_mapmode==0)
				glBindTexture(GL_TEXTURE_2D, TRI2.texID);
			if (current_mapmode==1)
				glBindTexture(GL_TEXTURE_2D, LIN1.texID);
		}
		if (buttoncheck[check]==2)
		{
			if (current_mapmode==0)
				glBindTexture(GL_TEXTURE_2D, TRI1.texID);
			if (current_mapmode==1)
				glBindTexture(GL_TEXTURE_2D, LIN2.texID);
		}

	}

	glBegin(GL_QUADS); 
	glTexCoord2f(0.0235f, 0.0f); glVertex2d(x,y);
	glTexCoord2f(1.0f, 0.0f); glVertex2d(x+250,y);
	glTexCoord2f(1.0f, 0.15f); glVertex2d(x+250,y+40);
	glTexCoord2f(0.0235f, 0.15f); glVertex2d(x,y+40);
	glEnd();

	//glUseProgram(0);
	glDisable(GL_BLEND);
}


void MenuMouseclick()
{
	POINT mousePos;
	GetCursorPos(&mousePos);
	if (mousePos.x>50 && mousePos.x<300 && mousePos.y>500 && mousePos.y<540) {
		glClearColor(0.0f, 0.0f, 0.0f, 1.5f);
		MainCreateMap();

		glClearColor(1.0f, 1.0f, 1.0f, 1.5f);
	}
	if (mousePos.x>50 && mousePos.x<300 && mousePos.y>570 && mousePos.y<610) {
		PostQuitMessage(0);
	}
	if (mousePos.x>50 && mousePos.x<175 && mousePos.y>640 && mousePos.y<680) {
		current_mapmode=1;
	}
	if (mousePos.x>174 && mousePos.x<300 && mousePos.y>640 && mousePos.y<680) {
		current_mapmode=0;
	}
}


void MainCreate()
{
	glActiveTextureARB = (PFNGLACTIVETEXTUREARBPROC) wglGetProcAddress("glActiveTextureARB");
	glMultiTexCoord2fARB = (PFNGLMULTITEXCOORD2FARBPROC) wglGetProcAddress("glMultiTexCoord2fARB");
	glClientActiveTextureARB = (PFNGLCLIENTACTIVETEXTUREARBPROC) wglGetProcAddress ( "glClientActiveTextureARB" );
	glCreateShader = (PFNGLCREATESHADERPROC)  wglGetProcAddress("glCreateShader");
	glShaderSource = (PFNGLSHADERSOURCEPROC)  wglGetProcAddress("glShaderSource");
	glCompileShader = (PFNGLCOMPILESHADERARBPROC)  wglGetProcAddress("glCompileShader");
	glCreateProgram = (PFNGLCREATEPROGRAMOBJECTARBPROC) wglGetProcAddress("glCreateProgram");
	glAttachShader = (PFNGLATTACHSHADERPROC) wglGetProcAddress("glAttachShader");
	glLinkProgram = (PFNGLLINKPROGRAMPROC) wglGetProcAddress("glLinkProgram");
	glUseProgram = (PFNGLUSEPROGRAMPROC) wglGetProcAddress("glUseProgram");
	glGetShaderInfoLog=(PFNGLGETSHADERINFOLOGPROC) wglGetProcAddress("glGetShaderInfoLog");
	glGetShaderiv=(PFNGLGETSHADERIVPROC) wglGetProcAddress("glGetShaderiv");
	glGetUniformLocation = (PFNGLGETUNIFORMLOCATIONPROC) wglGetProcAddress("glGetUniformLocation");
	glUniform4fv = (PFNGLUNIFORM4FVPROC) wglGetProcAddress("glUniform4fv");
	glUniform4f = (PFNGLUNIFORM4FPROC) wglGetProcAddress("glUniform4f");
	glUniform3f = (PFNGLUNIFORM3FPROC) wglGetProcAddress("glUniform3f");
	glUniform1i = (PFNGLUNIFORM1IPROC) wglGetProcAddress("glUniform1i");
	glGetAttribLocation = (PFNGLGETATTRIBLOCATIONPROC) wglGetProcAddress("glGetAttribLocation");
	glGenBuffers = (PFNGLGENBUFFERSPROC) wglGetProcAddress("glGenBuffers");  
	glBindBuffer = (PFNGLBINDBUFFERPROC) wglGetProcAddress("glBindBuffer");
	glBufferData = (PFNGLBUFFERDATAPROC) wglGetProcAddress("glBufferData");
	glEnableVertexAttribArray = (PFNGLENABLEVERTEXATTRIBARRAYPROC) wglGetProcAddress("glEnableVertexAttribArray");
	glVertexAttribPointer = (PFNGLVERTEXATTRIBPOINTERPROC) wglGetProcAddress("glVertexAttribPointer");
	glDeleteBuffers = (PFNGLDELETEBUFFERSPROC) wglGetProcAddress("glDeleteBuffers");
	glUniformMatrix4fv = (PFNGLUNIFORMMATRIX4FVPROC) wglGetProcAddress("glUniformMatrix4fv");
	glUniform1f = (PFNGLUNIFORM1FPROC) wglGetProcAddress("glUniform1f");
	glGenFramebuffers = (PFNGLGENFRAMEBUFFERSPROC) wglGetProcAddress("glGenFramebuffers");
	glBindFramebuffer = (PFNGLBINDFRAMEBUFFERPROC) wglGetProcAddress("glBindFramebuffer");
	glFramebufferTexture2D = (PFNGLFRAMEBUFFERTEXTURE2DPROC) wglGetProcAddress("glFramebufferTexture2D");
	glDrawElementsBaseVertex = (PFNGLDRAWELEMENTSBASEVERTEXPROC) wglGetProcAddress("glDrawElementsBaseVertex");
	glGenVertexArrays = (PFNGLGENVERTEXARRAYSPROC)	wglGetProcAddress("glGenVertexArrays");					
	glBindVertexArray = (PFNGLBINDVERTEXARRAYPROC)	wglGetProcAddress("glBindVertexArray");
	glVertexAttribIPointer = (PFNGLVERTEXATTRIBIPOINTERPROC) wglGetProcAddress("glVertexAttribIPointer");
	glDeleteVertexArrays = (PFNGLDELETEVERTEXARRAYSPROC) wglGetProcAddress("glDeleteVertexArrays");
	glActiveTexture = (PFNGLACTIVETEXTUREPROC) wglGetProcAddress("glActiveTexture");
	glBindRenderbuffer = (PFNGLBINDRENDERBUFFERPROC) wglGetProcAddress("glBindRenderbuffer");
	glFramebufferRenderbuffer = (PFNGLFRAMEBUFFERRENDERBUFFERPROC) wglGetProcAddress("glFramebufferRenderbuffer");
	glFramebufferTexture = (PFNGLFRAMEBUFFERTEXTUREPROC) wglGetProcAddress("glFramebufferTexture");
	glDrawBuffers = (PFNGLDRAWBUFFERSPROC) wglGetProcAddress("glDrawBuffers");
	glGenRenderbuffers = (PFNGLGENRENDERBUFFERSPROC) wglGetProcAddress("glGenRenderbuffers");
	glRenderbufferStorage = (PFNGLRENDERBUFFERSTORAGEPROC) wglGetProcAddress("glRenderbufferStorage");
	glRenderbufferStorageMultisample = (PFNGLRENDERBUFFERSTORAGEMULTISAMPLEPROC) wglGetProcAddress("glRenderbufferStorageMultisample");
		

	setShaders();
		
	wheelController.on=false;
	wheelController.WheelMove=Vector3f(0.0f,0.0f,0.0f);
	wheelController.WheelEngine=aiQuaternion(1.0,0.0,0.0,0.0);
	myfile = fopen ("hello.txt", "w");
	//myfile2 = fopen ("hello2.txt", "w");
	
	bignorm.imageData=NULL;
	blend.imageData=NULL;
	blend2.imageData=NULL;
	grain.imageData=NULL;
	plain.imageData=NULL;
	rock.imageData=NULL;
	rock_norm.imageData=NULL;
	grain_norm.imageData=NULL;
	hextex.imageData=NULL;
	clear_normal.imageData=NULL;
	wheels.imageData=NULL;
	button.imageData=NULL;
	button_on.imageData=NULL;
	TRI1.imageData=NULL;
	TRI2.imageData=NULL;
	LIN1.imageData=NULL;
	LIN2.imageData=NULL;
	texDepth.imageData=NULL;
	forest.imageData=NULL;


	wchar_t buf[256];

	swprintf(buf, sizeof(buf), L"�������� ������");
	drawload(10, buf, 60);
		
		prime.morphing=true;
		prime.glass=true;
		prime.transformer=true;
	
	std::vector<Vector3f> _Positions2;
	std::vector<Vector3f> _Normals2;
	std::vector<unsigned int> _Indices2;
	
	Temp = new Mesh;
	Temp->LoadMorphData("models/Prime/PrimeTransform_M.dae",  _Positions2, _Normals2, _Indices2, 1);
	Temp->Clear();
	delete Temp;
	prime.Transform->LoadMesh("models/Prime/PrimeTransform.dae", 1, 2, _Positions2, _Normals2, _Indices2, skeletal_shadow_morphing);

	_Positions2.clear();
	_Normals2.clear();
	_Indices2.clear();

	Temp = new Mesh;
	Temp->LoadMorphData("models/Prime/PrimeGlass_M.dae",  _Positions2, _Normals2, _Indices2, -1);
	Temp->Clear();
	delete Temp;
	prime.Glass->LoadMesh("models/Prime/PrimeGlass.dae", 0, 0, _Positions2, _Normals2, _Indices2, skeletal_shadow_morphing_glass);

	_Positions2.clear();
	_Normals2.clear();
	_Indices2.clear();
	
	prime.Alternative->LoadMesh("models/Prime/PrimeAlternative.dae", 1, 3, skeletal_shadow);
	prime.Main->LoadMesh("models/Prime/PrimeMain.dae", 1, 9, skeletal_shadow);
	prime.Gun->LoadMesh("models/Prime/PrimeGun.dae", 0, 0, skeletal_shadow_part);
	prime.Head->LoadMesh("models/Prime/PrimeHead.dae", 0, 0, skeletal_shadow_part);
	prime.Glass02->LoadMesh("models/Prime/PrimeGlass_M.dae", 0, 0, skeletal_shadow_glass);
	prime.Glass03->LoadMesh("models/Prime/PrimeGlass.dae", 0, 0, skeletal_shadow_glass);
	prime.Face->LoadMesh("models/Prime/FacePrime.dae",1 , 1, skeletal_shadow);
	
	prime.Transform->timer.One[1].AddAnimation("models/Prime/PrimeTransform02.dae");
	prime.Main->timer.One[1].AddAnimation("models/Prime/PrimeIdle.dae"); //PrimeRotateLeft
	prime.Main->timer.One[2].AddAnimation("models/Prime/PrimeMain02.dae");
	prime.Main->timer.One[3].AddAnimation("models/Prime/PrimeWalk.dae");
	prime.Main->timer.One[4].AddAnimation("models/Prime/PrimeWalkStart.dae");
	prime.Main->timer.One[5].AddAnimation("models/Prime/PrimeWalkEnd.dae");
	prime.Main->timer.One[6].AddAnimation("models/Prime/PrimeGunOn.dae");
	prime.Main->timer.One[7].AddAnimation("models/Prime/PrimeRotateLeft.dae");
	prime.Main->timer.One[8].AddAnimation("models/Prime/PrimeGunOff.dae");
	prime.Alternative->timer.One[1].AddAnimation("models/Prime/PrimeAlternative02.dae");
	prime.Alternative->timer.One[2].AddAnimation("models/Prime/PrimeAlternative03.dae");
	prime.Main->timer.crossdata = new an_timer::crossData[3];
	prime.Main->timer.crossdata[0].Add(0,0,3,1,1,0,0.7f);
	prime.Main->timer.crossdata[1].Add(0,0,1,4,4,1,0.3f);
	prime.Main->timer.crossdata[2].Add(0,0,7,1,1,0,0.3f);

	prime.Trailer=&prime_trailer;
	
	prime_trailer.morphing=false;
	prime_trailer.transformer=false;
	prime_trailer.glass=true;

	prime_trailer.Transform->LoadMesh("models/Prime/PrimeTrailerTransform.dae", 1, 2, skeletal_shadow);
	prime_trailer.Alternative->LoadMesh("models/Prime/PrimeTrailer.dae", 1, 1, skeletal_shadow);
	prime_trailer.Main->LoadMesh("models/Prime/PrimeTrailerMain.dae", 1, 3, skeletal_shadow);
	
	prime_trailer.Transform->timer.One[1].AddAnimation("models/Prime/PrimeTrailerTransform02.dae");
	prime_trailer.Main->timer.One[1].AddAnimation("models/Prime/PrimeTrailerMain.dae");
	prime_trailer.Main->timer.One[2].AddAnimation("models/Prime/PrimeTrailerMain.dae");
	prime_trailer.Glass02->LoadMesh("models/Prime/PrimeTrailerGlass.dae", 0, 0, skeletal_shadow_glass);
	

	RightFist = new Mesh;
	RightFist->LoadMesh("models/Prime/RightFist.dae", 0, 0, skeletal_shadow_part);

	LeftFist = new Mesh;
	LeftFist->LoadMesh("models/Prime/LeftFist.dae", 0, 0, skeletal_shadow_part);

	for (int i = 0; i < 77; i++)
	{
		Load3DS (&move_mark.marknum[i],"meshes/mark.3ds");
		PrepareObject(&move_mark.marknum[i],25,0,0);
		move_mark.marknum[i].normals=NULL;
	}

	swprintf(buf, sizeof(buf), L"�������� ������");
	drawload(150, buf, 60);
	//PrepareObject(&jazz.model,7.5,1,0);
	//PrepareObject(&jazz.wheels,7.5,1,0);
	//PrepareObject(&prime.model,7.5,1,0);
	//PrepareObject(&prime.wheels,7.5,1,0);
	PrepareObject(&trailer,7.5,1,0);
	PrepareObject(&trailer_wheels,7.5,1,0);
	//PrepareObject(&mark,25);
	/*
	for (int i = 0; i < 50; i++)
	{
	move_mark.marknum[i].vertex = new vertex_type[mark.vertices_qty];
	move_mark.marknum[i].mapcoord = new mapcoord_type[mark.vertices_qty];
	move_mark.marknum[i].polygon = new polygon_type[mark.polygons_qty];
	move_mark.marknum[i]=mark;
	}
	*/
	swprintf(buf, sizeof(buf), L"�������� ����������");
	drawload(300, buf, 70);

	LoadTextures("textures/button8.tga",&button,3);
	LoadTextures("textures/button7.tga",&button_on,3);
	LoadTextures("textures/button9_TRI1.tga",&TRI1,3);
	LoadTextures("textures/button9_TRI2.tga",&TRI2,3);
	LoadTextures("textures/button9_LIN1.tga",&LIN1,3);
	LoadTextures("textures/button9_LIN2.tga",&LIN2,3);

	LoadTextures("textures/clear_normal.tga",&clear_normal,3);
	//LoadTextures("textures/jazz_diffuse.tga",&jazz.diffuse_map,1);
	//LoadTextures("textures/jazz_emission.tga",&jazz.emission_map,3);

	LoadTextures("textures/prime_diffuse.tga",&prime.diffuse_map,1);
	LoadTextures("textures/prime_emission.tga",&prime.emission_map,3);
	LoadTextures("textures/Prime_n.tga",&prime.normal_map,1);

	LoadTextures("textures/prime_diffuse_trailer.tga",&prime_trailer.diffuse_map,1);
	LoadTextures("textures/prime_normal_trailer.tga",&prime_trailer.normal_map,3);
	LoadTextures("textures/prime_ao_trailer.tga",&prime_trailer.emission_map,3);
	

	LoadTextures("textures/prime_head_diffuse.tga",&prime.head_diffuse_map,1);
	LoadTextures("textures/prime_head_n.tga",&prime.head_normal_map,1);
	LoadTextures("textures/prime_head_ao.tga",&prime.head_emission_map,3);

	LoadTextures("textures/primeGun_diffuse.tga",&prime.gun_diffuse_map,1);
	LoadTextures("textures/primeGun_n.tga",&prime.gun_normal_map,1);
	LoadTextures("textures/PrimeGun_ao.tga",&prime.gun_emission_map,3);

	LoadTextures("textures/fist_diffuse.tga",&fist_diffuse_map,1);
	LoadTextures("textures/fist_n.tga",&fist_normal_map,1);
	LoadTextures("textures/fist_ao.tga",&fist_ao_map,3);

	LoadTextures("textures/glass_ao.tga",&glass_ao,3);

	
	//LoadTextures("textures/prime_head_diffuse.tga",&prime.diffuse_map,1);
	//LoadTextures("textures/prime_head_ao.tga",&prime.emission_map,3);
	//LoadTextures("textures/Prime_head_n.tga",&prime.normal_map,3);

	//LoadTextures("textures/trailer_diffuse.tga",&trailer.texture,1);

	
	

	LoadTextures("textures/Menu/nBack.tga",&skytextures[BACK_ID],0);
	LoadTextures("textures/Menu/nFront.tga",&skytextures[FRONT_ID],0);
	LoadTextures("textures/Menu/nBottom.tga",&skytextures[BOTTOM_ID],0);
	LoadTextures("textures/Menu/nTop.tga",&skytextures[TOP_ID],0);
	LoadTextures("textures/Menu/nLeft.tga",&skytextures[RIGHT_ID],0);
	LoadTextures("textures/Menu/nRight.tga",&skytextures[LEFT_ID],0);

	LoadTextures("textures/Window_GUI/Canvas_W.tga",&Canvas_W,3);
	
	LoadTextures("textures/Window_GUI/faceframe.tga",&faceframe,3);
	LoadTextures("textures/Window_GUI/prime2D.tga",&prime2D,3);

	LoadTextures("textures/Window_GUI/R_W.tga",&R_W,3);
	LoadTextures("textures/Window_GUI/D_W.tga",&D_W,3);
	LoadTextures("textures/Window_GUI/U_W.tga",&U_W,3);
	LoadTextures("textures/Window_GUI/L_W.tga",&L_W,3);

	LoadTextures("textures/Window_GUI/LD_W.tga",&LD_W,3);
	LoadTextures("textures/Window_GUI/LU_W2.tga",&LU_W,3);
	LoadTextures("textures/Window_GUI/RU_W.tga",&RU_W,3);
	LoadTextures("textures/Window_GUI/RD_W.tga",&RD_W,3);

	buttoncheck[0]=0;
	buttoncheck[1]=0;

	ShadowInitialization();

	swprintf(buf, sizeof(buf), L"���������");
	drawload(400, buf, 30);
	CamM.x=0;
	CamM.y=-1;
	CamM.z=1;
	CamPos.x=-3000;
	CamPos.y=-65;
	CamPos.z=-375;


}

void DrawUnits(bool shadow_on)
{
	for (int i = 0; i < 10; i++)
		if (unitlist[i]!=NULL)
			DrawUnit(i, shadow_on);

	//Vector2f p=MainToOxy(6, 6); 
	//draw_obj2(&jadr,p.x,p.y,0,dir,6,6);
	
}

void DrawUnit(int number, bool shadow_on)
{

	Vector2f p;
	float rotate; 

	if (move_anim2.anim_move_flag==0 || unitlist[number]!=unitptr )
	{
		p=MainToOxy(unitlist[number]->X, unitlist[number]->Y);
		rotate=unitlist[number]->rotdir*60;
		DrawComplexUnit(unitlist[number], p, rotate, shadow_on);
	}
	else
	{
		if (unitlist[number]==unitptr)
		{
			if (move_anim2.anim_move_trailer==false)
			{
				if (move_anim2.anim_move_rotate==true)
				{
					rotate=move_anim2.rotate;
					p=MainToOxy(move_anim2.start.x, move_anim2.start.y);
					if (shadow_on==true)
					move_anim2.rotate_around();
				}
				else
				{
					rotate=bezierPath.rotation[bezierPath.ch];
					p=bezierPath.drawingPoints[bezierPath.ch];
				}
				if (move_anim2.anim_move_state==true && move_anim2.rotate_car==true)
				{
					if (shadow_on==true)
					move_anim2.timer--;
					if (move_anim2.timer==0 && shadow_on==true)
					{
						if (move_anim2.double_rotate_car==true)
						{
						unitptr->CarMoveBackTwo();
						move_anim2.anim_move_state=false;
						}
						else
						{
						move_anim2.rotate_car=false;
						move_anim2.anim_move_state=false;
						move_anim2.anim_func(true);
						unitlist[number]->Current->timer.One[unitptr->Current->timer.animation_index].on=false;
						unitlist[number]->Current->timer.animation_index=1;
						unitlist[number]->Current->timer.One[unitptr->Current->timer.animation_index].Start();
						wheelController.Start(1);
						}

						
					}
				
				}
				
				DrawComplexUnit(unitptr, p, rotate, shadow_on);

				//if (move_anim2.anim_move_rotate==true)
					
				
			}
			else
			{
				rotate=bezierPath.rotation[bezierPath.ch];
				p=bezierPath.drawingPoints[bezierPath.ch];
				if (unitptr->type==2)
				DrawComplexUnit(unitptr->Trailer, p, rotate, shadow_on);
				else
				DrawComplexUnit(unitptr, p, rotate, shadow_on);
				if (move_anim2.anim_move_state==true && shadow_on==true)
				{
					move_anim2.timer--;
					if (move_anim2.timer==0)
					{
						unitptr->SeparateTrailer();
						unitptr->MoveFromSeparate();
						move_anim2.anim_move_state=false;

						
					}
				
				}
			}
		}

	}
}

void DrawComplexUnit(unit* unit, Vector2f p, float rotate, bool shadow_on)
{
	
	fprintf(myfile, "%i %f %f %f ", bezierPath.ch, p.x, p.x, rotate);
	if (shadow_on==true)
	fprintf(myfile, "true\n");
	else
	fprintf(myfile, "false\n");
		
	BoxGetZ(unit, p, rotate);
	glCullFace(GL_BACK);
	if (unit->Current==unit->Transform && unit->morphing==1)
	draw_skinning_obj_morphing(unit, p.x, p.y, unit->z_up, rotate, shadow_on, unit->forward_angles, unit->side_angles); 
	else
	draw_skinning_obj(unit, p.x, p.y, unit->z_up, rotate, shadow_on, unit->forward_angles, unit->side_angles); 
	
	//DrawBox(p.x, p.y, GetZ(p), rotate, unit->widthBox, unit->heightBox);
	glCullFace(GL_FRONT);
	if ((unit->Current==unit->Transform || unit->Current==unit->Main) && unit->transformer==true)
	{
	draw_state_obj(unit, p.x, p.y, unit->z_up, rotate, shadow_on, unit->forward_angles, unit->side_angles, 0);
	draw_state_obj(unit, p.x, p.y, unit->z_up, rotate, shadow_on, unit->forward_angles, unit->side_angles, 1);
	draw_state_obj(unit, p.x, p.y, unit->z_up, rotate, shadow_on, unit->forward_angles, unit->side_angles, 3);
	draw_state_obj(unit, p.x, p.y, unit->z_up, rotate, shadow_on, unit->forward_angles, unit->side_angles, 4);
	}
	
	if (unit->glass==true)
	{
	glCullFace(GL_BACK);
	draw_state_obj(unit, p.x, p.y, unit->z_up, rotate, shadow_on, unit->forward_angles, unit->side_angles, 2);
	}
	if (unit->type==2)
	{
		BoxGetZ(unit->Trailer, unit->traildot, unit->trailangle);
		glCullFace(GL_BACK);
		draw_skinning_obj(unit->Trailer, unit->traildot.x, unit->traildot.y, unit->Trailer->z_up, unit->trailangle, shadow_on, unit->Trailer->forward_angles, unit->Trailer->side_angles); 
		if (shadow_on==1)
		trailer_fixings(unit,p.x,p.y,rotate, 15.0f, 0.0f, 20.0f);
		//DrawBox(unit->traildot.x, unit->traildot.y, GetZ(unit->traildot), unit->trailangle, unit->Trailer->widthBox, unit->Trailer->heightBox);
		
	}
	if (move_anim2.anim_move_trailer==true)
	{
		if (unitptr->type==2)
		{
		BoxGetZ(unitptr, unit->traildot, unit->trailangle);
		glCullFace(GL_BACK);
		draw_skinning_obj(unitptr, unit->traildot.x, unit->traildot.y, unitptr->z_up, unit->trailangle, shadow_on, 0, 0);
		draw_state_obj(unitptr, unit->traildot.x, unit->traildot.y, unitptr->z_up, unit->trailangle, shadow_on, 0, 0, 2);
		if (shadow_on==1)
		trailer_fixings(unit,p.x,p.y,rotate, -20.0f, 180.0f, 15.0f);
		}	
	}
}

		





void ButtonMenuMouseCheck()
{
	POINT mousePos;
	GetCursorPos(&mousePos);
	if (mousePos.x>50 && mousePos.x<300 && mousePos.y>500 && mousePos.y<540) {
		buttoncheck[0]=1;
	}
	else
		buttoncheck[0]=0;
	if (mousePos.x>50 && mousePos.x<300 && mousePos.y>570 && mousePos.y<610) {
		buttoncheck[1]=1;
	}
	else
		buttoncheck[1]=0;
	if (mousePos.x>50 && mousePos.x<175 && mousePos.y>640 && mousePos.y<680) {
		buttoncheck[2]=1;
	}
	else
		if (mousePos.x>174 && mousePos.x<300 && mousePos.y>640 && mousePos.y<680) {
			buttoncheck[2]=2;
		}
		else
			buttoncheck[2]=0;


}

void trailer_fixings(unit *unit, float px, float py, float r, float OneObjDist, float back, float TwoObjDist)
{
	
	float ta, tb, tc;
	float c, xa, ya, lx, ly;
	px += 0 * cos(r*3.1415926/180.0)+ OneObjDist * sin(r*3.1415926/180.0); 
	py += 0 * sin(r*3.1415926/180.0)- OneObjDist * cos(r*3.1415926/180.0);
	
	lx = px-unit->traildot.x;
	ly = py-unit->traildot.y;
	


	c = sqrt(lx*lx+ly*ly);
	xa = (TwoObjDist*lx)/c;
	ya = (TwoObjDist*ly)/c;
	unit->traildot.x=px-xa;
	unit->traildot.y=py-ya;

	//DrawLine(px, py);
	//DrawLine(unit->traildot.x, unit->traildot.y);

	ta = TwoObjDist;
	tb = sqrt((px-unit->traildot.x)*(px-unit->traildot.x)+(py-unit->traildot.y)*(py-unit->traildot.y));
	tc = sqrt((px-unit->traildot.x)*(px-unit->traildot.x)+(py+TwoObjDist-unit->traildot.y)*(py+TwoObjDist-unit->traildot.y));
	unit->trailangle = (acos((tb*tb+tc*tc-ta*ta)/(2.0*tb*tc)))/(3.1415926/180.0)*2.0+back;
	if (px>unit->traildot.x)
		unit->trailangle*=-1.0;
	
	
}


void CreateSkyBox(float x, float y, float z, float width, float height, float length)
{
	
	x = x - width  / 2;
	y = y - height / 2;
	z = z - length / 2;

	
	glBindTexture(GL_TEXTURE_2D, skytextures[BACK_ID].texID);
	glBegin(GL_QUADS);

	
	glTexCoord2f(0.999f, 0.001f); glVertex3f(x + width, y,z);
	glTexCoord2f(0.999f, 0.999f); glVertex3f(x + width, y + height, z);
	glTexCoord2f(0.001f, 0.999f); glVertex3f(x, y + height, z);
	glTexCoord2f(0.001f, 0.001f); glVertex3f(x, y,z);

	glEnd();

	
	glBindTexture(GL_TEXTURE_2D, skytextures[FRONT_ID].texID);

	
	glBegin(GL_QUADS);

	
	glTexCoord2f(0.999f, 0.001f); glVertex3f(x, y, z + length);
	glTexCoord2f(0.999f, 0.999f); glVertex3f(x, y + height, z + length);
	glTexCoord2f(0.001f, 0.999f); glVertex3f(x + width, y + height, z + length);
	glTexCoord2f(0.001f, 0.001f); glVertex3f(x + width, y,z + length);
	glEnd();

	
	glBindTexture(GL_TEXTURE_2D, skytextures[BOTTOM_ID].texID);

	
	glBegin(GL_QUADS);

	
	glTexCoord2f(0.999f, 0.001f); glVertex3f(x, y,z);
	glTexCoord2f(0.999f, 0.999f); glVertex3f(x, y,z + length);
	glTexCoord2f(0.001f, 0.999f); glVertex3f(x + width, y,z + length);
	glTexCoord2f(0.001f, 0.001f); glVertex3f(x + width, y,z);
	glEnd();

	
	glBindTexture(GL_TEXTURE_2D, skytextures[TOP_ID].texID);

	
	glBegin(GL_QUADS);

	
	glTexCoord2f(0.001f, 0.999f); glVertex3f(x + width, y + height, z);
	glTexCoord2f(0.001f, 0.001f); glVertex3f(x + width, y + height, z + length);
	glTexCoord2f(0.999f, 0.001f); glVertex3f(x, y + height,z + length);
	glTexCoord2f(0.999f, 0.999f); glVertex3f(x, y + height,z);

	glEnd();

	
	glBindTexture(GL_TEXTURE_2D, skytextures[LEFT_ID].texID);

	
	glBegin(GL_QUADS);

	
	glTexCoord2f(0.999f, 0.999f); glVertex3f(x, y + height,z);
	glTexCoord2f(0.001f, 0.999f); glVertex3f(x, y + height,z + length);
	glTexCoord2f(0.001f, 0.001f); glVertex3f(x, y,z + length);
	glTexCoord2f(0.999f, 0.001f); glVertex3f(x, y,z);

	glEnd();

	
	glBindTexture(GL_TEXTURE_2D, skytextures[RIGHT_ID].texID);

	
	glBegin(GL_QUADS);

	
	glTexCoord2f(0.001f, 0.001f); glVertex3f(x + width, y,z);
	glTexCoord2f(0.999f, 0.001f); glVertex3f(x + width, y,z + length);
	glTexCoord2f(0.999f, 0.999f); glVertex3f(x + width, y + height,z + length);
	glTexCoord2f(0.001f, 0.999f); glVertex3f(x + width, y + height,z);
	glEnd();
}



/*
void draw_obj2(model_ptr model, float tX, float tY, float tZ, int dirrot, int iX, int iY) 
{

//glFrontFace(GL_CCW);
glPushMatrix();
glTranslatef(tX,tY,tZ);
glRotatef(dirrot,0,0,1);




glUseProgram(program_textured_normal);
glUniform1i(loc[6],0);
glUniform1i(loc[7],1);



glActiveTextureARB(GL_TEXTURE0_ARB);
glBindTexture(GL_TEXTURE_2D, jazz.diffuse_map.texID);
glActiveTextureARB(GL_TEXTURE1_ARB);
glBindTexture(GL_TEXTURE_2D, jazz.diffuse_map_norm.texID);


glEnableClientState(GL_VERTEX_ARRAY); 
glEnableClientState(GL_NORMAL_ARRAY ); 

glVertexPointer(3,GL_FLOAT,sizeof(vertex_types),jadr.vertex); 

glClientActiveTextureARB(GL_TEXTURE0_ARB);
glEnableClientState(GL_TEXTURE_COORD_ARRAY);
glTexCoordPointer(2, GL_FLOAT, sizeof(mapcoord_types),jadr.mapcoord); 

glClientActiveTextureARB(GL_TEXTURE1_ARB);
glEnableClientState(GL_TEXTURE_COORD_ARRAY);
glTexCoordPointer(3, GL_FLOAT, sizeof(vertex_types),jadr.binormals); 

glClientActiveTextureARB(GL_TEXTURE2_ARB);
glEnableClientState(GL_TEXTURE_COORD_ARRAY);
glTexCoordPointer(3, GL_FLOAT, sizeof(vertex_types),jadr.tangents); 


glNormalPointer(GL_FLOAT,  sizeof(vertex_types),jadr.normals); 



glDrawElements(GL_TRIANGLES,jadr.polygons_qty*3,GL_UNSIGNED_INT,jadr.polygon); 

glClientActiveTextureARB(GL_TEXTURE2_ARB);
glDisableClientState(GL_TEXTURE_COORD_ARRAY);

glClientActiveTextureARB(GL_TEXTURE1_ARB);
glDisableClientState(GL_TEXTURE_COORD_ARRAY);

glClientActiveTextureARB(GL_TEXTURE0_ARB);
glDisableClientState(GL_TEXTURE_COORD_ARRAY);

glDisableClientState(GL_VERTEX_ARRAY);
glDisableClientState(GL_NORMAL_ARRAY);

glUseProgram(0);
glActiveTextureARB(GL_TEXTURE0_ARB);

glPopMatrix();

}
*/

void ShadowInitialization()
{

	glGenTextures(1, &texDepth.texID);
	glBindTexture(GL_TEXTURE_2D, texDepth.texID);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT24, texDepthSizeX, texDepthSizeY,0, GL_DEPTH_COMPONENT, GL_UNSIGNED_BYTE, 0);
	glBindTexture(GL_TEXTURE_2D, 0);

	glGenFramebuffers(1, &fbDepth);
	glBindFramebuffer(GL_FRAMEBUFFER, fbDepth);
	glDrawBuffer(GL_NONE);
	glReadBuffer(GL_NONE);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, texDepth.texID, 0);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

}

void RenderToShadowMap()
{

	Orthonew[0]=(Orthobasic[0]-1830+CamPos.y+940+CamPos.z)+(940+CamPos.z)*0.915;
	Orthonew[1]=(Orthobasic[1]-1830+CamPos.y+940+CamPos.z)-(CamPos.z+940)*0.502;
	Orthonew[2]=(Orthobasic[2]-1100*0.624+CamPos.x*0.624)+(940+CamPos.z)*0.7;
	Orthonew[3]=(Orthobasic[3]-1100*0.624+CamPos.x*0.624)-(CamPos.z+940)*0.7;
	
	//Orthonew[0]=Orthobasic[0];
	//Orthonew[1]=Orthobasic[1];
	//Orthonew[2]=Orthobasic[2];
	//Orthonew[3]=Orthobasic[3];
	
	glViewport(0, 0, texDepthSizeX, texDepthSizeY);
	glBindFramebuffer(GL_FRAMEBUFFER, fbDepth);
	glClear(GL_DEPTH_BUFFER_BIT);
	
	glEnable(GL_POLYGON_OFFSET_FILL);
	glPolygonOffset (offset, offset2);


	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(Orthonew[0],Orthonew[1],Orthonew[2],Orthonew[3],-2000.0f, 2000.0f);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	gluLookAt(lightPos[0], lightPos[1], lightPos[2], lightEye[0], lightEye[1], lightEye[2], lightUp[0], lightUp[1], lightUp[2]);
	glGetFloatv(GL_PROJECTION_MATRIX, lightProjectionMatrix);
	glGetFloatv(GL_MODELVIEW_MATRIX, lightModelViewMatrix);

	//glFrontFace(GL_CCW);
	//glEnable(GL_CULL_FACE);
	//glCullFace(GL_BACK);
	//DrawMap();
	DrawUnits(0);
	draw_forest();
	//glDisable(GL_CULL_FACE);

	//SwapBuffers(g_hDC);
	//glLoadIdentity();
	

	glDisable(GL_POLYGON_OFFSET_FILL);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

}

void RenderFrameFace()
{
glBindFramebuffer(GL_FRAMEBUFFER, FaceFrameBuffer);

	
	glViewport(0, 0, 512, 512);
	
	glClearColor(0.4f, 0.4f, 0.4f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	glMatrixMode(GL_PROJECTION);                        
    glLoadIdentity();                           
    gluPerspective(8.0f,1,0.1f,1000.0f);
    glMatrixMode(GL_MODELVIEW);                     
    glLoadIdentity();
	gluLookAt(9.0f, -34.0f, -191.0f, 9.0f, -9.0f, 32.0f, 0, -1, 0);
	

	
	glLightModelfv(GL_LIGHT_MODEL_AMBIENT, LightAmbient);

	glLightfv(GL_LIGHT0, GL_AMBIENT, LightAmbient);
	glLightfv(GL_LIGHT1, GL_SPECULAR, LightSpecular);
	glLightfv(GL_LIGHT1, GL_AMBIENT, LightAmbient2);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, LightDiffuse);
	glLightfv(GL_LIGHT1, GL_DIFFUSE, LightDiffuse2);
	glLightfv(GL_LIGHT0, GL_POSITION, LightPosition3);
	glLightfv(GL_LIGHT1, GL_POSITION, LightPosition3);


	//glEnable(GL_CULL_FACE);
	glCullFace(GL_FRONT);
	
	Vector3f eye = Vector3f(15,9,0);
	
	glCullFace(GL_BACK);
	draw_frame_obj(&prime,0.0f,0.0f,0.0f,0.0f,1.0f,eye);
	glCullFace(GL_FRONT);
	draw_frame_part(&prime,0.0f,0.0f,0.0f,0.0f,1.0f,0,eye);
	glDisable(GL_CULL_FACE);
	draw_frame_part(&prime,0.0f,0.0f,0.0f,0.0f,1.0f,1,eye);
	
	

	//MoveCamera();
	//CamPos.x+=CamXC;
	//CamPos.y+=CamYC;
	//CamPos.z+=CamZC;
	//CamTgt.x=CamPos.x+CamM.x+CamRotate.x;
	//CamTgt.y=CamPos.y+CamM.y+CamRotate.y;
	//CamTgt.z=CamPos.z+CamM.z+CamRotate.z;
	//gluLookAt(0, 0, 1, 0, 0, 0, 0, 1, 0);
	

	//draw_GUI();
	//CamZC=0;
	//CamYC=0;
	//CamXC=0;
	//SwapBuffers(g_hDC);
	//glLoadIdentity();
	
	

glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void RenderShadowedScene()
{
	//glViewport(0, 0, 512, 512);
	//glBindTexture(GL_TEXTURE_2D,renderTexture.texID);
	//glCopyTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, 0, 0, 512, 512, 0);
	
	
	
	
	
	glViewport(0, 0, GetSystemMetrics(SM_CXSCREEN), GetSystemMetrics(SM_CYSCREEN));

	glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(perspective, double(GetSystemMetrics(SM_CXSCREEN)) / double(GetSystemMetrics(SM_CYSCREEN)), 1.0f, 8000.0f);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	gluLookAt(CamPos.x, CamPos.y+cam_watch, CamPos.z-cam_watch, CamTgt.x+CamRotate.x, CamTgt.y+CamRotate.y, CamTgt.z+CamRotate.z,0, 0, -1);

	glGetFloatv(GL_PROJECTION_MATRIX, cameraProjectionMatrix);
	glGetFloatv(GL_MODELVIEW_MATRIX, cameraModelViewMatrix);
	InverseMatrix(cameraInverseModelViewMatrix, cameraModelViewMatrix);

	glPushMatrix();
	glLoadIdentity();
	glTranslatef(0.5f, 0.5f, 0.5f); // + 0.5
	glScalef(0.5f, 0.5f, 0.5f); // * 0.5
	glMultMatrixf(lightProjectionMatrix);
	glMultMatrixf(lightModelViewMatrix);
	glMultMatrixf(cameraInverseModelViewMatrix);
	glGetFloatv(GL_MODELVIEW_MATRIX, lightMatrix);
	glPopMatrix();

	
	mvLightPos[0] = cameraModelViewMatrix[0] * lightPos[0] + cameraModelViewMatrix[4] * lightPos[1] + cameraModelViewMatrix[8] * lightPos[2] + cameraModelViewMatrix[12];
	mvLightPos[1] = cameraModelViewMatrix[1] * lightPos[0] + cameraModelViewMatrix[5] * lightPos[1] + cameraModelViewMatrix[9] * lightPos[2] + cameraModelViewMatrix[13];
	mvLightPos[2] = cameraModelViewMatrix[2] * lightPos[0] + cameraModelViewMatrix[6] * lightPos[1] + cameraModelViewMatrix[10] * lightPos[2] + cameraModelViewMatrix[14];
	glUniform3f(glGetUniformLocation(skeletal_shadow_morphing,"lightPos"), mvLightPos[0], mvLightPos[1], mvLightPos[2]);

	glColor3f(0.0f,0.0f,0.0f);
	
	glLightModelfv(GL_LIGHT_MODEL_AMBIENT, LightAmbient);
	LightPosition[0] = move_x;
	LightPosition[1] = move_y;
	LightPosition[2] = move_z;
	LightPosition[3] = center_x;
	LightPosition2[0] = move_x;
	LightPosition2[1] = move_y;
	LightPosition2[2] = move_z;
	LightPosition2[3] = center_x;


	glLightfv(GL_LIGHT0, GL_AMBIENT, LightAmbient);
	glLightfv(GL_LIGHT1, GL_SPECULAR, LightSpecular);
	glLightfv(GL_LIGHT1, GL_AMBIENT, LightAmbient2);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, LightDiffuse);
	glLightfv(GL_LIGHT1, GL_DIFFUSE, LightDiffuse2);
	glLightfv(GL_LIGHT0, GL_POSITION, LightPosition);
	glLightfv(GL_LIGHT1, GL_POSITION, LightPosition2);
	//glDisable(GL_CULL_FACE);
	
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D,renderTexture.texID);
	glCopyTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, 0, 0, 100, 100, 0);	
	glDisable(GL_TEXTURE_2D);

	//glFrontFace(GL_CCW);
	
	//glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);
	glColor3f(0.0f,0.0f,0.0f);
	DrawMap();
	
	if (current_mapmode==0)
		draw_forest();
	if (current_mapmode==1)
	{
		DrawHexGrid();
		if (move_anim2.anim_move_flag==1)
		DrawSpline();
	}
	glColor3f(0.0f,0.0f,0.0f);
	glCullFace(GL_FRONT);
	DrawMove();
	glCullFace(GL_BACK);
	DrawUnits(1);
	glCullFace(GL_FRONT);
	/*
	glUseProgram(cube);
	glUniform1i(glGetUniformLocation(cube,"cubeMap"),0);
	glActiveTextureARB(GL_TEXTURE0_ARB);
	glEnable  ( GL_TEXTURE_CUBE_MAP_ARB );
	glBindTexture ( GL_TEXTURE_CUBE_MAP_ARB, texCubeMap );
	//glTexParameteri ( GL_TEXTURE_CUBE_MAP_ARB, GL_TEXTURE_WRAP_S, GL_CLAMP );
	//glTexParameteri ( GL_TEXTURE_CUBE_MAP_ARB, GL_TEXTURE_WRAP_T, GL_CLAMP );
	//glEnable  ( GL_TEXTURE_GEN_S );
	//glEnable  ( GL_TEXTURE_GEN_T );
	//glEnable  ( GL_TEXTURE_GEN_R );
	glPushMatrix();
	glRotatef(rotcub,0.0f,0.0f,1.0f);
	glutSolidTeapot(20);
	CreatePlane();
	glPopMatrix();
	glUseProgram(0);
	glDisable( GL_TEXTURE_CUBE_MAP_ARB );
	*/
	glPushMatrix();
	glTranslatef(-100.0f,0.0f,0.0f);
	glRotatef(0,0,0,1);
	glScalef(30.0f,30.0f,30.0f);
	


	glUseProgram(skeletal_shadow_part_turbo);
	glUniform3f(glGetUniformLocation(skeletal_shadow_part_turbo,"fvEyePosition"),CamPos.x, CamPos.y, CamPos.z);
	glUniform1f(glGetUniformLocation(skeletal_shadow_part_turbo,"fSpecularPower"),specularPower);
	glUniform1i(glGetUniformLocation(skeletal_shadow_part_turbo,"diffuseMap"),0);
	glUniform1i(glGetUniformLocation(skeletal_shadow_part_turbo,"normalMap"),1);
	glUniform1i(glGetUniformLocation(skeletal_shadow_part_turbo,"shadowMap"),2);
	glUniform1i(glGetUniformLocation(skeletal_shadow_part_turbo,"emissionMap"),3);
	glActiveTextureARB(GL_TEXTURE0_ARB);
	glBindTexture(GL_TEXTURE_2D, prime.gun_diffuse_map.texID);
	glActiveTextureARB(GL_TEXTURE1_ARB);
	glBindTexture(GL_TEXTURE_2D, prime.gun_normal_map.texID);
	glActiveTextureARB(GL_TEXTURE2_ARB);
	glBindTexture(GL_TEXTURE_2D, texDepth.texID);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_R_TO_TEXTURE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_FUNC, GL_LEQUAL);
	glActiveTextureARB(GL_TEXTURE3_ARB);
	glBindTexture(GL_TEXTURE_2D, prime.gun_emission_map.texID);

	glUniformMatrix4fv(glGetUniformLocation(skeletal_shadow_part_turbo,"lightMatrix"), 1, false, lightMatrix);
	glUniform3f(glGetUniformLocation(skeletal_shadow_part_turbo,"lightPos"), mvLightPos[0], mvLightPos[1], mvLightPos[2]);
	
	
	//prime.Gun->BoneTransform(prime.Current->timer.One[0].time, Transforms);
	
	//for (unsigned int i = 0 ; i < Transforms.size() ; i++)
	//{
	//SetBoneTransform(i, Transforms[i]);
	//}
	

	prime.Gun->RenderSkinning();


	
	glActiveTextureARB(GL_TEXTURE2_ARB);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_NONE);
	glBindTexture(GL_TEXTURE_2D, 0);
	glActiveTextureARB(GL_TEXTURE0_ARB);
	glUseProgram(0);

	glPopMatrix();




	glDisable(GL_CULL_FACE);

	draw_GUI();

	
	
	 
	
	SwapBuffers(g_hDC);
	glLoadIdentity();

	MoveCamera();
	CamPos.x+=CamXC;
	CamPos.y+=CamYC;
	CamPos.z+=CamZC;
	CamTgt.x=CamPos.x+CamM.x+CamRotate.x;
	CamTgt.y=CamPos.y+CamM.y+CamRotate.y;
	CamTgt.z=CamPos.z+CamM.z+CamRotate.z;
	gluLookAt(CamPos.x, CamPos.y+cam_watch, CamPos.z-cam_watch, CamTgt.x, CamTgt.y, CamTgt.z,0, 0, -1);
	

	
	CamZC=0;
	CamYC=0;
	CamXC=0;


}

void InverseMatrix(float dst[16], float src[16])
{
	dst[0] = src[0];
	dst[1] = src[4];
	dst[2] = src[8];
	dst[3] = 0.0;
	dst[4] = src[1];
	dst[5] = src[5];
	dst[6]  = src[9];
	dst[7] = 0.0;
	dst[8] = src[2];
	dst[9] = src[6];
	dst[10] = src[10];
	dst[11] = 0.0;
	dst[12] = -(src[12] * src[0]) - (src[13] * src[1]) - (src[14] * src[2]);
	dst[13] = -(src[12] * src[4]) - (src[13] * src[5]) - (src[14] * src[6]);
	dst[14] = -(src[12] * src[8]) - (src[13] * src[9]) - (src[14] * src[10]);
	dst[15] = 1.0;
}

void DrawPlane()
{
	
	glColor3f(0.4f, 0.5f, 0.6f);
	glBegin(GL_QUADS);
	glNormal3f(0.0, 0.0, -1.0);
	glVertex3f(-1000.0, -1000.0, 0.0);
	glVertex3f(-1000.0, 1000.0, 0.0);
	glVertex3f(1000.0, 1000.0, 0.0);
	glVertex3f(1000.0, -1000.0, 0.0);
	glEnd();
}

void DrawPlaneOld()
{
	
	glColor3f(0.4f, 0.5f, 0.6f);
	glBegin(GL_QUADS);
	glNormal3f(0.0, 1.0, 0.0);
	glVertex3f(-1000.0, 0.0, -1000.0);
	glVertex3f(-1000.0, 0.0, 1000.0);
	glVertex3f(1000.0, 0.0, 1000.0);
	glVertex3f(1000.0, 0.0, -1000.0);
	glEnd();
}

void DrawModel(float z)
{

	glColor3f(0.9f, 0.9f, 0.9f);

	glPushMatrix();
	glTranslatef(0, 0, z);
	glRotatef(-90, 1, 0, 0);
	glScalef(30.000f, 30.000f, 30.000f);

	glutSolidTeapot(1);
	glPopMatrix();

}

void CleanAll()
{
	delete [10] unitlist;
}


void DeleteMenu()
{

	glDeleteTextures(1, &skytextures[0].texID);
	glDeleteTextures(1, &skytextures[1].texID);
	glDeleteTextures(1, &skytextures[2].texID);
	glDeleteTextures(1, &skytextures[3].texID);
	glDeleteTextures(1, &skytextures[4].texID);
	glDeleteTextures(1, &skytextures[5].texID);
	glDeleteTextures(1, &button.texID);
	glDeleteTextures(1, &button_on.texID);
	glDeleteTextures(1, &TRI1.texID);
	glDeleteTextures(1, &TRI2.texID);
	glDeleteTextures(1, &LIN1.texID);
	glDeleteTextures(1, &LIN2.texID);
}

void DrawHex()
{
	glBegin(GL_LINE_STRIP);
	glColor3d(0.95,0.95,0);     
	glVertex3d(25,0,-0.5); 
	glVertex3d(75,0,-0.5);
	glColor3d(0.95,0.95,0);     
	glVertex3d(75,0,-0.5); 
	glVertex3d(100,43.75,-0.5);
	glColor3d(0.95,0.95,0);     
	glVertex3d(100,43.75,-0.5); 
	glVertex3d(75,87.5,-0.5);
	glColor3d(0.95,0.95,0);    
	glVertex3d(25,87.5,-0.5); 
	glVertex3d(75,87.5,-0.5);
	glColor3d(0.95,0.95,0);    
	glVertex3d(25,87.5,-0.5); 
	glVertex3d(0,43.75,-0.5);
	glColor3d(0.95,0.95,0);     
	glVertex3d(0,43.75,-0.5); 
	glVertex3d(25,0,-0.5);
	glEnd();
}

void DrawHexGrid()
{
	glEnable(GL_LINE_SMOOTH);
	glEnable(GL_BLEND);
	glBlendFunc(GL_DST_COLOR,GL_ONE_MINUS_DST_COLOR);
	glHint(GL_LINE_SMOOTH_HINT, GL_NICEST);
	glLineWidth(3);

	for (int i = 0; i < GridWei/2; i++) {
		glPushMatrix();
		glTranslatef(150.0*i,0,0.0f);
		for (int i = 0; i < GridHei; i++) {
			DrawHex();
			if (i<GridHei)
				glTranslatef(0.0,87.5,0.0f);
		}
		glPopMatrix();
		glPushMatrix();
		glTranslatef(75.0+150.0*i,43.75,0.0f);
		for (int i = 0; i < GridHei; i++) {
			DrawHex();
			if (i<GridHei-1)
				glTranslatef(0.0,87.5,0.0f);
		}
		glPopMatrix();
	}
	if (GridWei%2!=0) {
		glPushMatrix();
		glTranslatef(150.0*(GridWei/2),0,0.0f);
		for (int i = 0; i < GridHei; i++) {
			DrawHex();
			if (i<GridHei)
				glTranslatef(0.0,87.5,0.0f);
		}
		glPopMatrix();

	}
	glLineWidth(0.1);
	glDisable(GL_LINE_SMOOTH);
	glDisable(GL_BLEND);
}









void Draw_Window_Part(bool size, int x, int y)
{
	glBegin(GL_QUADS);
	int x_step;
	if (size==false)
		x_step=size_win;
	else
		x_step=size_win*4;
	glTexCoord2f(0.0f, 1.0f); 
	glVertex3f(x, y,  0); 

	glTexCoord2f(1.0f, 1.0f); 
	glVertex3f(x, y+size_win,  0); 

	glTexCoord2f(1.0f, 0.0f); 
	glVertex3f(x+x_step, y+size_win, 0); 

	glTexCoord2f(0.0f, 0.0f); 
	glVertex3f(x+x_step, y,  0); 

	glEnd();
}


void Draw_Window(int x, int y, int x_size, int y_size)
{
	glEnable(GL_TEXTURE_2D);

	glBindTexture(GL_TEXTURE_2D, Canvas_W.texID);
	Draw_Window_Canvas(x, y, x_size, y_size);


	glDepthMask(GL_FALSE);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glBindTexture(GL_TEXTURE_2D, LU_W.texID);
	glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
	Draw_Window_Part(false, x, y);
	glDisable(GL_BLEND);
	glDepthMask(GL_TRUE);




	glBindTexture(GL_TEXTURE_2D, RU_W.texID);
	Draw_Window_Part(true, x+x_size-size_win*4, y);

	glBindTexture(GL_TEXTURE_2D, RD_W.texID);
	Draw_Window_Part(false, x+x_size-size_win, y+y_size-size_win);

	glBindTexture(GL_TEXTURE_2D, LD_W.texID);
	Draw_Window_Part(true, x, y+y_size-size_win);

	glDisable(GL_TEXTURE_2D); 

}

void Draw_Window_Canvas(int x, int y, int x_size, int y_size)
{
	glColor3f(1.0f,0.0f,1.0f);
	glColor3f(1.0f,0.0f,1.0f);

	glBegin(GL_QUADS);

	glTexCoord2f(0.0f, y_size/size_win);  
	glVertex3f(x+size_win/9.14, y+6,  0); 

	glTexCoord2f(x_size/size_win, y_size/size_win);  
	glVertex3f(x+size_win/9.14, y+y_size-8,  0); 

	glTexCoord2f(x_size/size_win, 0.0f);
	glVertex3f(x+x_size-size_win/9.14, y+y_size-8, 0); 

	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(x+x_size-size_win/9.14, y+6,  0); 

	glEnd();



}

float StringToFloat(LPCTSTR lpcstr)
{
	string str = lpcstr;
	//MessageBox(NULL, str.c_str(), "INFO", MB_OK);
	if (str.find(" ")>-1)
	{
		str.erase(str.find(" "),str.find(" ")+1);
	}
	int decimate=1;

	if (str.find("-")>-1)
	{
		str.erase(str.find("-"),str.find("-")+1);
		decimate=-1;
	}
	if (str.find("e")!=-1)
		str=("0");
	float fin=atof(str.c_str());
	return fin*decimate;
}

int StringToInt(LPCTSTR lpcstr)
{
	string str = lpcstr;
	//MessageBox(NULL, str.c_str(), "INFO", MB_OK);
	if (str.find(" ")>-1)
	{
		str.erase(str.find(" "),str.find(" ")+1);
	}

	int fin=atoi(str.c_str());
	return fin;
}



void ColladaLoader(const char *path, obj_type_ptr p_object)
{



	using namespace rapidxml;
	xml_document<> doc;
	ifstream file(path);
	stringstream buffer;
	buffer << file.rdbuf();
	string content(buffer.str());
	doc.parse<0>(&content[0]);
	//int decimate;
	xml_node<> *collada = doc.first_node("COLLADA");
	xml_node<> *library_geometries = collada->first_node("library_geometries");
	for (xml_node<> *geometry = library_geometries->first_node("geometry"); geometry; geometry = geometry->next_sibling())
	{
		xml_node<> *sourcevert = geometry->first_node("mesh")->first_node("source")->first_node("float_array");
		string allvert = sourcevert->value();
		string sinvert;
		string count = sourcevert->first_attribute("count")->value();
		p_object->vertices_qty=atoi(count.c_str())/3;
		p_object->vertex = new vertex_type[p_object->vertices_qty];
		p_object->normals = new vertex_type[p_object->vertices_qty];
		p_object->mapcoord = new mapcoord_type[p_object->vertices_qty];



		//MessageBox(NULL, IntToStr(a).c_str(), "INFO", MB_OK);



		for (unsigned int i=0; i<p_object->vertices_qty; i++)
		{
			sinvert=allvert.substr(0,allvert.find(" "));
			allvert=allvert.substr(allvert.find(" ")+1,allvert.length());
			p_object->vertex[i].x=StringToFloat(sinvert.c_str());

			sinvert=allvert.substr(0,allvert.find(" "));
			allvert=allvert.substr(allvert.find(" ")+1,allvert.length());
			p_object->vertex[i].y=StringToFloat(sinvert.c_str());

			sinvert=allvert.substr(0,allvert.find(" "));
			allvert=allvert.substr(allvert.find(" ")+1,allvert.length());
			p_object->vertex[i].z=StringToFloat(sinvert.c_str());

		}

		sourcevert = geometry->first_node("mesh")->last_node("source")->first_node("float_array");
		allvert = sourcevert->value();
		count = sourcevert->first_attribute("count")->value();
		//p_object->mapcoords_qty=atoi(count.c_str())/2;
		//p_object->mapcoord = new mapcoord_type[p_object->mapcoords_qty];

		int fullmapcoord_qty=atoi(count.c_str())/2;
		mapcoord_type *tempmapcoords = new mapcoord_type [fullmapcoord_qty];

		for (int i=0; i<fullmapcoord_qty; i++)
		{
			sinvert=allvert.substr(0,allvert.find(" "));
			allvert=allvert.substr(allvert.find(" ")+1,allvert.length());
			tempmapcoords[i].u=StringToFloat(sinvert.c_str());

			sinvert=allvert.substr(0,allvert.find(" "));
			allvert=allvert.substr(allvert.find(" ")+1,allvert.length());
			tempmapcoords[i].v=StringToFloat(sinvert.c_str());

		}

		sourcevert = geometry->first_node("mesh")->first_node("polylist");
		count = sourcevert->first_attribute("count")->value();
		p_object->polygons_qty=atoi(count.c_str());
		p_object->polygon = new polygon_type[p_object->polygons_qty];
		sourcevert = geometry->first_node("mesh")->first_node("polylist")->first_node("p");
		allvert = sourcevert->value();
		int *tempindvertex = new int [p_object->polygons_qty*3];
		int *tempmappolygons = new int [p_object->polygons_qty*3]; 
		//int tempmappolygons2 = new int [p_object->polygons_qty*3]; 
		int polch=0;
		for (unsigned int i=0; i<p_object->polygons_qty; i++)
		{
			sinvert=allvert.substr(0,allvert.find(" "));
			allvert=allvert.substr(allvert.find(" ")+1,allvert.length());
			p_object->polygon[i].a=StringToInt(sinvert.c_str());
			tempindvertex[polch]=p_object->polygon[i].a;

			sinvert=allvert.substr(0,allvert.find(" "));
			allvert=allvert.substr(allvert.find(" ")+1,allvert.length());

			sinvert=allvert.substr(0,allvert.find(" "));
			allvert=allvert.substr(allvert.find(" ")+1,allvert.length());
			tempmappolygons[polch]=StringToInt(sinvert.c_str());

			sinvert=allvert.substr(0,allvert.find(" "));
			allvert=allvert.substr(allvert.find(" ")+1,allvert.length());
			p_object->polygon[i].b=StringToInt(sinvert.c_str());
			tempindvertex[polch+1]=p_object->polygon[i].b;

			sinvert=allvert.substr(0,allvert.find(" "));
			allvert=allvert.substr(allvert.find(" ")+1,allvert.length());

			sinvert=allvert.substr(0,allvert.find(" "));
			allvert=allvert.substr(allvert.find(" ")+1,allvert.length());
			tempmappolygons[polch+1]=StringToInt(sinvert.c_str());

			sinvert=allvert.substr(0,allvert.find(" "));
			allvert=allvert.substr(allvert.find(" ")+1,allvert.length());
			p_object->polygon[i].c=StringToInt(sinvert.c_str());
			tempindvertex[polch+2]=p_object->polygon[i].c;

			sinvert=allvert.substr(0,allvert.find(" "));
			allvert=allvert.substr(allvert.find(" ")+1,allvert.length());

			sinvert=allvert.substr(0,allvert.find(" "));
			allvert=allvert.substr(allvert.find(" ")+1,allvert.length());
			tempmappolygons[polch+2]=StringToInt(sinvert.c_str());
			polch+=3;
		}

		for (unsigned int i=0; i<p_object->polygons_qty; i++)
		{
			p_object->mapcoord[tempmappolygons[i]]=tempmapcoords[tempindvertex[i]];
		}


	}
}

void Mesh::VertexBoneData::AddBoneData(unsigned int BoneID, float Weight)
{

	for (unsigned int i = 0 ; i < ARRAY_SIZE_IN_ELEMENTS(IDs) ; i++) {
		if (Weights[i] == 0.0f) {
			IDs[i]     = BoneID;
			Weights[i] = Weight;
			return;
		}        
	}

	// should never get here - more bones than we have space for
	assert(0);

}



Mesh::Mesh()
{
	m_VAO = 0;
	ZERO_MEM(m_Buffers);
	ZERO_MEM(m_BuffersM);
	m_NumBones = 0;
	m_pScene = NULL;
	
}

Mesh::~Mesh()
{
	Clear();
}

void Mesh::Clear()
{
	if (m_Buffers[0] != 0) {
		glDeleteBuffers(ARRAY_SIZE_IN_ELEMENTS(m_Buffers), m_Buffers);
	}

	if (m_BuffersM[0] != 0) {
		glDeleteBuffers(ARRAY_SIZE_IN_ELEMENTS(m_BuffersM), m_BuffersM);
	}

	if (m_VAO != 0) {
		glDeleteVertexArrays(1, &m_VAO);
		m_VAO = 0;
	}

	
	
}

bool Mesh::LoadMesh(const std::string& Filename, int skinning_on, unsigned int animationCount, unsigned int _program)
{
	
	Clear();

	program=_program;

	glGenVertexArrays(1, &m_VAO);   
	glBindVertexArray(m_VAO);

	
	glGenBuffers(ARRAY_SIZE_IN_ELEMENTS(m_Buffers), m_Buffers);

	bool Ret = false;

	m_pScene = m_Importer.ReadFile(Filename.c_str(),
		aiProcess_Triangulate | aiProcess_JoinIdenticalVertices | aiProcess_GenSmoothNormals | aiProcess_CalcTangentSpace );


	if (m_pScene){
		m_GlobalInverseTransform = m_pScene->mRootNode->mTransformation;
		m_GlobalInverseTransform.Inverse();
		if (skinning_on==0)
		Ret = InitFromScene(m_pScene, Filename);
		else
		Ret = InitFromSceneSkinning(m_pScene, Filename);
	}
	else {
		string error="Dae Load Error"+Filename;
		MessageBox(NULL, error.c_str(), "INFO", MB_OK);
	}
	
	if (animationCount>0)
	timer.Create(animationCount, m_pScene);
	

	//delete m_pScene;

	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	return Ret;
}

bool Mesh::LoadMorphData(const std::string& Filename, vector<Vector3f>& Positions2, vector<Vector3f>& Normals2, vector<unsigned int>& Indices2, int side)
{

	Clear();

	bool Ret = false;
	
	m_pScene = m_Importer.ReadFile(Filename.c_str(),
		aiProcess_GenSmoothNormals | aiProcess_CalcTangentSpace );
		

	if (m_pScene)
	Ret = InitFromSceneMorphing(m_pScene,Filename,Positions2,Normals2,Indices2,side);
	else 
	{
	string error="Dae Load Error"+Filename;
	MessageBox(NULL, error.c_str(), "INFO", MB_OK);
	}


	//delete m_pScene;
	return Ret;
}

bool Mesh::LoadMesh(const std::string& Filename, int skinning_on, unsigned int animationCount, vector<Vector3f>& _Positions2, vector<Vector3f>& _Normals2, vector<unsigned int>& _Indices2, unsigned int _program)
{
	
	Clear();

	program=_program;

	glGenVertexArrays(1, &m_VAO);   
	glBindVertexArray(m_VAO);

	
	
	glGenBuffers(ARRAY_SIZE_IN_ELEMENTS(m_BuffersM), m_BuffersM);

	bool Ret = false;

	m_pScene = m_Importer.ReadFile(Filename.c_str(),
		 aiProcess_GenSmoothNormals | aiProcess_CalcTangentSpace );
	
	std::vector<Vector3f> Positions;
	std::vector<Vector3f> Positions2;
	std::vector<Vector3f> Normals;
	std::vector<Vector3f> Normals2;
	std::vector<Vector2f> TexCoords;
	std::vector<VertexBoneData> Bones;
	std::vector<unsigned int> Indices;
	std::vector<unsigned int> Indices2;

	//MessageBox(NULL, IntToStr(_Indices2.size()).c_str(), "INFO", MB_OK);

	Positions2.reserve(_Positions2.size());
	Normals2.reserve(_Positions2.size());
	Indices2.reserve(_Indices2.size());

	if (m_pScene){
		m_GlobalInverseTransform = m_pScene->mRootNode->mTransformation;
		m_GlobalInverseTransform.Inverse();
		if (skinning_on==0)
		Ret = InitFromSceneMorphing(m_pScene,Filename,Positions,Positions2,Normals,Normals2,TexCoords,Indices,Indices2);
		else
		{
		Ret = InitFromSceneSkinningMorphing(m_pScene,Filename,Positions,Positions2,Normals,Normals2,TexCoords,Bones,Indices,Indices2);
		
		}

	}
	else {
		
		string error="Dae Load Error"+Filename;
		MessageBox(NULL, error.c_str(), "INFO", MB_OK);;
	}

	Positions2=_Positions2;
	Normals2=_Normals2;
	Indices2=_Indices2;
	
	if (animationCount>0)
	timer.Create(animationCount, m_pScene);
	
//	delete m_pScene;
	
	if (skinning_on==0)
	MergeMorphing(Positions,Positions2,Normals,Normals2,TexCoords,Indices);
	else
	MergeMorphing(Positions,Positions2,Normals,Normals2,TexCoords,Bones,Indices);
	
	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	
	

	return Ret;
}

bool Mesh::InitFromScene(const aiScene* pScene, const std::string& Filename)
{
	m_Entries.resize(pScene->mNumMeshes);
	//m_Textures.resize(pScene->mNumMaterials);

	std::vector<Vector3f> Positions;
	std::vector<Vector3f> Normals;
	std::vector<Vector2f> TexCoords;
	std::vector<unsigned int> Indices;

	unsigned int NumVertices = 0;
	unsigned int NumIndices = 0;

	for (unsigned int i = 0 ; i < m_Entries.size() ; i++) {
		m_Entries[i].MaterialIndex = pScene->mMeshes[i]->mMaterialIndex; 
		m_Entries[i].NumIndices   = pScene->mMeshes[i]->mNumFaces * 3;
		m_Entries[i].BaseVertex    = NumVertices;
		m_Entries[i].BaseIndex     = NumIndices;

		NumVertices += pScene->mMeshes[i]->mNumVertices;
		NumIndices  += m_Entries[i].NumIndices;
	}

	Positions.reserve(NumVertices);
	Normals.reserve(NumVertices);
	TexCoords.reserve(NumVertices);
	Indices.reserve(NumIndices);
	NormalsTemp.reserve(NumIndices);

	for (unsigned int i = 0; i < m_Entries.size(); i++){
		const aiMesh* paiMesh = pScene->mMeshes[i];
		InitMesh(i, paiMesh, Positions, Normals, TexCoords, Indices);
	
	}

	//if (!InitMaterials(pScene, Filename)) {
	//	return false;
	//}

	//GLuint POSITION_LOCATION = glGetAttribLocation(shadow_textured,"Position"); 
    //GLuint TEX_COORD_LOCATION = glGetAttribLocation(shadow_textured,"TexCoord"); 
	//GLuint NORMAL_LOCATION = glGetAttribLocation(shadow_textured,"Normal");

	GLuint POSITION_LOCATION = glGetAttribLocation(program,"Position"); 
    GLuint TEX_COORD_LOCATION = glGetAttribLocation(program,"TexCoord"); 
	GLuint NORMAL_LOCATION = glGetAttribLocation(program,"Normal");
	
	glBindBuffer(GL_ARRAY_BUFFER, m_Buffers[POS_VB]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(Positions[0]) * Positions.size(), &Positions[0], GL_STATIC_DRAW);
	glEnableVertexAttribArray(POSITION_LOCATION);
	glVertexAttribPointer(POSITION_LOCATION, 3, GL_FLOAT, GL_FALSE, 0, 0);    

	glBindBuffer(GL_ARRAY_BUFFER, m_Buffers[TEXCOORD_VB]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(TexCoords[0]) * TexCoords.size(), &TexCoords[0], GL_STATIC_DRAW);
	glEnableVertexAttribArray(TEX_COORD_LOCATION);
	glVertexAttribPointer(TEX_COORD_LOCATION, 2, GL_FLOAT, GL_FALSE, 0, 0);

	glBindBuffer(GL_ARRAY_BUFFER, m_Buffers[NORMAL_VB]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(Normals[0]) * Normals.size(), &Normals[0], GL_STATIC_DRAW);
	glEnableVertexAttribArray(NORMAL_LOCATION);
	glVertexAttribPointer(NORMAL_LOCATION, 3, GL_FLOAT, GL_FALSE, 0, 0);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_Buffers[INDEX_BUFFER]);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(Indices[0]) * Indices.size(), &Indices[0], GL_STATIC_DRAW);

	

	return true;

}

bool Mesh::InitFromSceneSkinning(const aiScene* pScene, const std::string& Filename)
{
	m_Entries.resize(pScene->mNumMeshes);
	

	std::vector<Vector3f> Positions;
	std::vector<Vector3f> Normals;
	std::vector<Vector2f> TexCoords;
	std::vector<VertexBoneData> Bones;
	std::vector<unsigned int> Indices;

	unsigned int NumVertices = 0;
	unsigned int NumIndices = 0;

	for (unsigned int i = 0 ; i < m_Entries.size() ; i++) {
		m_Entries[i].MaterialIndex = pScene->mMeshes[i]->mMaterialIndex; 
		m_Entries[i].NumIndices   = pScene->mMeshes[i]->mNumFaces * 3;
		m_Entries[i].BaseVertex    = NumVertices;
		m_Entries[i].BaseIndex     = NumIndices;

		NumVertices += pScene->mMeshes[i]->mNumVertices;
		NumIndices  += m_Entries[i].NumIndices;
	}

	Positions.reserve(NumVertices);
	Normals.reserve(NumVertices);
	TexCoords.reserve(NumVertices);
	Bones.resize(NumVertices);
	Indices.reserve(NumIndices);

	for (unsigned int i = 0; i < m_Entries.size(); i++){
		const aiMesh* paiMesh = pScene->mMeshes[i];
		InitMesh(i, paiMesh, Positions, Normals, TexCoords, Bones, Indices);
		
	}

	GLuint POSITION_LOCATION = glGetAttribLocation(program,"Position"); 
	GLuint TEX_COORD_LOCATION = glGetAttribLocation(program,"TexCoord"); 
	GLuint NORMAL_LOCATION = glGetAttribLocation(program,"Normal");
	GLuint BONE_ID_LOCATION	= glGetAttribLocation(program,"BoneIDs"); 
	GLuint BONE_WEIGHT_LOCATION = glGetAttribLocation(program,"Weights");
	
	glBindBuffer(GL_ARRAY_BUFFER, m_Buffers[POS_VB]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(Positions[0]) * Positions.size(), &Positions[0], GL_STATIC_DRAW);
	glEnableVertexAttribArray(POSITION_LOCATION);
	glVertexAttribPointer(POSITION_LOCATION, 3, GL_FLOAT, GL_FALSE, 0, 0);    

	glBindBuffer(GL_ARRAY_BUFFER, m_Buffers[TEXCOORD_VB]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(TexCoords[0]) * TexCoords.size(), &TexCoords[0], GL_STATIC_DRAW);
	glEnableVertexAttribArray(TEX_COORD_LOCATION);
	glVertexAttribPointer(TEX_COORD_LOCATION, 2, GL_FLOAT, GL_FALSE, 0, 0);

	glBindBuffer(GL_ARRAY_BUFFER, m_Buffers[NORMAL_VB]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(Normals[0]) * Normals.size(), &Normals[0], GL_STATIC_DRAW);
	glEnableVertexAttribArray(NORMAL_LOCATION);
	glVertexAttribPointer(NORMAL_LOCATION, 3, GL_FLOAT, GL_FALSE, 0, 0);

	glBindBuffer(GL_ARRAY_BUFFER, m_Buffers[BONE_VB]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(Bones[0]) * Bones.size(), &Bones[0], GL_STATIC_DRAW);
	glEnableVertexAttribArray(BONE_ID_LOCATION);
	glVertexAttribIPointer(BONE_ID_LOCATION, 4, GL_INT, sizeof(VertexBoneData), (const GLvoid*)0);
	glEnableVertexAttribArray(BONE_WEIGHT_LOCATION);    
	glVertexAttribPointer(BONE_WEIGHT_LOCATION, 4, GL_FLOAT, GL_FALSE, sizeof(VertexBoneData), (const GLvoid*)16);
	
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_Buffers[INDEX_BUFFER]);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(Indices[0]) * Indices.size(), &Indices[0], GL_STATIC_DRAW);

	

	return true;

}

bool Mesh::InitFromSceneSkinningMorphing(const aiScene* pScene,
										const std::string& Filename,
										vector<Vector3f>& Positions,
										vector<Vector3f>& Positions2,
										vector<Vector3f>& Normals,
										vector<Vector3f>& Normals2,
										vector<Vector2f>& TexCoords,
										vector<VertexBoneData>& Bones,
										vector<unsigned int>& Indices,
										vector<unsigned int>& Indices2)
{
	m_Entries.resize(pScene->mNumMeshes);
	
	

	unsigned int NumVertices = 0;
	unsigned int NumIndices = 0;

	for (unsigned int i = 0 ; i < m_Entries.size() ; i++) {
		m_Entries[i].NumIndices   = pScene->mMeshes[i]->mNumFaces * 3;
		m_Entries[i].BaseVertex    = NumVertices;
		m_Entries[i].BaseIndex     = NumIndices;

		NumVertices += pScene->mMeshes[i]->mNumVertices;
		NumIndices  += m_Entries[i].NumIndices;
	}

	Positions.reserve(NumVertices);
	Positions2.reserve(NumVertices);
	Normals.reserve(NumVertices);
	Normals2.reserve(NumVertices);
	TexCoords.reserve(NumVertices);
	Bones.resize(NumVertices);
	Indices.reserve(NumIndices);
	Indices2.reserve(NumIndices);

	for (unsigned int i = 0; i < m_Entries.size(); i++){
		const aiMesh* paiMesh = pScene->mMeshes[i];
		InitMesh(i, paiMesh, Positions, Normals, TexCoords, Bones, Indices);
		
	}

	//for (unsigned int i = 0; i < m_Entries.size(); i++){
	//	const aiMesh* paiMesh = pScene2->mMeshes[i];
	//	InitMesh(i, paiMesh, Positions2, Normals2);
	//	
	//}
	
	
	return true;
}

bool Mesh::InitFromSceneMorphing(const aiScene* pScene,
										const std::string& Filename,
										vector<Vector3f>& Positions,
										vector<Vector3f>& Positions2,
										vector<Vector3f>& Normals,
										vector<Vector3f>& Normals2,
										vector<Vector2f>& TexCoords,
										vector<unsigned int>& Indices,
										vector<unsigned int>& Indices2)
{
	m_Entries.resize(pScene->mNumMeshes);
	
	

	unsigned int NumVertices = 0;
	unsigned int NumIndices = 0;

	for (unsigned int i = 0 ; i < m_Entries.size() ; i++) {
		m_Entries[i].NumIndices   = pScene->mMeshes[i]->mNumFaces * 3;
		m_Entries[i].BaseVertex    = NumVertices;
		m_Entries[i].BaseIndex     = NumIndices;

		NumVertices += pScene->mMeshes[i]->mNumVertices;
		NumIndices  += m_Entries[i].NumIndices;
	}

	Positions.reserve(NumVertices);
	Positions2.reserve(NumVertices);
	Normals.reserve(NumVertices);
	Normals2.reserve(NumVertices);
	TexCoords.reserve(NumVertices);
	Indices.reserve(NumIndices);
	Indices2.reserve(NumIndices);

	for (unsigned int i = 0; i < m_Entries.size(); i++){
		const aiMesh* paiMesh = pScene->mMeshes[i];
		InitMesh(i, paiMesh, Positions, Normals, TexCoords, Indices);
		
	}

	//for (unsigned int i = 0; i < m_Entries.size(); i++){
	//	const aiMesh* paiMesh = pScene2->mMeshes[i];
	//	InitMesh(i, paiMesh, Positions2, Normals2);
	//	
	//}

	
	return true;
}



bool Mesh::InitFromSceneMorphing(const aiScene* pScene,
										const std::string& Filename,
										vector<Vector3f>& Positions2,
										vector<Vector3f>& Normals2,
										vector<unsigned int>& Indices2,
										int side)
{
	m_Entries.resize(pScene->mNumMeshes);
	
	unsigned int NumVertices = 0;
	unsigned int NumIndices = 0;

	for (unsigned int i = 0 ; i < m_Entries.size() ; i++) {
		m_Entries[i].NumIndices   = pScene->mMeshes[i]->mNumFaces * 3;
		m_Entries[i].BaseVertex    = NumVertices;
		m_Entries[i].BaseIndex     = NumIndices;

		NumVertices += pScene->mMeshes[i]->mNumVertices;
		NumIndices  += m_Entries[i].NumIndices;
		
	}

	Positions2.reserve(NumVertices);
	Normals2.reserve(NumVertices);
	Indices2.reserve(NumIndices);
	

	for (unsigned int i = 0; i < m_Entries.size(); i++){
		const aiMesh* paiMesh = pScene->mMeshes[i];
		InitMesh(i, paiMesh, Positions2, Normals2, Indices2, side);
		//MessageBox(NULL, IntToStr(i).c_str(), "INFO", MB_OK);
		
	}
	//MessageBox(NULL, IntToStr(m_Entries.size()).c_str(), "INFO", MB_OK);
	//MessageBox(NULL, IntToStr(NumIndices).c_str(), "INFO", MB_OK);
	//MessageBox(NULL, IntToStr(Indices2.size()).c_str(), "INFO", MB_OK);
	return true;
}

bool Mesh::MergeMorphing(vector<Vector3f>& Positions,
						 vector<Vector3f>& Positions2,
						vector<Vector3f>& Normals,
						vector<Vector3f>& Normals2,
						vector<Vector2f>& TexCoords,
						vector<VertexBoneData>& Bones,
						vector<unsigned int>& Indices)
{
	GLuint POSITION_LOCATION = glGetAttribLocation(program,"Position");
	GLuint POSITION_LOCATION_2 = glGetAttribLocation(program,"Position2");
	GLuint TEX_COORD_LOCATION = glGetAttribLocation(program,"TexCoord"); 
	GLuint NORMAL_LOCATION = glGetAttribLocation(program,"Normal");
	GLuint NORMAL_LOCATION_2 = glGetAttribLocation(program,"Normal2");
	GLuint BONE_ID_LOCATION	= glGetAttribLocation(program,"BoneIDs"); 
	GLuint BONE_WEIGHT_LOCATION = glGetAttribLocation(program,"Weights");

	//MessageBox(NULL, IntToStr(Positions.size()).c_str(), "INFO", MB_OK);

	glBindBuffer(GL_ARRAY_BUFFER, m_BuffersM[mPOS_VB]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(Positions[0]) * Positions.size(), &Positions[0], GL_STATIC_DRAW);
	glEnableVertexAttribArray(POSITION_LOCATION);
	glVertexAttribPointer(POSITION_LOCATION, 3, GL_FLOAT, GL_FALSE, 0, 0);    

	glBindBuffer(GL_ARRAY_BUFFER, m_BuffersM[mPOS_VB2]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(Positions2[0]) * Positions2.size(), &Positions2[0], GL_STATIC_DRAW);
	glEnableVertexAttribArray(POSITION_LOCATION_2);
	glVertexAttribPointer(POSITION_LOCATION_2, 3, GL_FLOAT, GL_FALSE, 0, 0); 

	glBindBuffer(GL_ARRAY_BUFFER, m_BuffersM[mTEXCOORD_VB]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(TexCoords[0]) * TexCoords.size(), &TexCoords[0], GL_STATIC_DRAW);
	glEnableVertexAttribArray(TEX_COORD_LOCATION);
	glVertexAttribPointer(TEX_COORD_LOCATION, 2, GL_FLOAT, GL_FALSE, 0, 0);

	glBindBuffer(GL_ARRAY_BUFFER, m_BuffersM[mNORMAL_VB]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(Normals[0]) * Normals.size(), &Normals[0], GL_STATIC_DRAW);
	glEnableVertexAttribArray(NORMAL_LOCATION);
	glVertexAttribPointer(NORMAL_LOCATION, 3, GL_FLOAT, GL_FALSE, 0, 0);

	glBindBuffer(GL_ARRAY_BUFFER, m_BuffersM[mNORMAL_VB2]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(Normals2[0]) * Normals2.size(), &Normals2[0], GL_STATIC_DRAW);
	glEnableVertexAttribArray(NORMAL_LOCATION_2);
	glVertexAttribPointer(NORMAL_LOCATION_2, 3, GL_FLOAT, GL_FALSE, 0, 0);

	glBindBuffer(GL_ARRAY_BUFFER, m_BuffersM[mBONE_VB]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(Bones[0]) * Bones.size(), &Bones[0], GL_STATIC_DRAW);
	glEnableVertexAttribArray(BONE_ID_LOCATION);
	glVertexAttribIPointer(BONE_ID_LOCATION, 4, GL_INT, sizeof(VertexBoneData), (const GLvoid*)0);
	glEnableVertexAttribArray(BONE_WEIGHT_LOCATION);    
	glVertexAttribPointer(BONE_WEIGHT_LOCATION, 4, GL_FLOAT, GL_FALSE, sizeof(VertexBoneData), (const GLvoid*)16);
	
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_BuffersM[mINDEX_BUFFER]);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(Indices[0]) * Indices.size(), &Indices[0], GL_STATIC_DRAW);



	

	return true;
}

bool Mesh::MergeMorphing(vector<Vector3f>& Positions,
						 vector<Vector3f>& Positions2,
						vector<Vector3f>& Normals,
						vector<Vector3f>& Normals2,
						vector<Vector2f>& TexCoords,
						vector<unsigned int>& Indices)
{
	
	GLuint POSITION_LOCATION = glGetAttribLocation(program,"Position");
	GLuint POSITION_LOCATION_2 = glGetAttribLocation(program,"Position2");
	GLuint TEX_COORD_LOCATION = glGetAttribLocation(program,"TexCoord"); 
	GLuint NORMAL_LOCATION = glGetAttribLocation(program,"Normal");
	GLuint NORMAL_LOCATION_2 = glGetAttribLocation(program,"Normal2");
	
	glBindBuffer(GL_ARRAY_BUFFER, m_BuffersM[mPOS_VB]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(Positions[0]) * Positions.size(), &Positions[0], GL_STATIC_DRAW);
	glEnableVertexAttribArray(POSITION_LOCATION);
	glVertexAttribPointer(POSITION_LOCATION, 3, GL_FLOAT, GL_FALSE, 0, 0);    

	glBindBuffer(GL_ARRAY_BUFFER, m_BuffersM[mPOS_VB2]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(Positions2[0]) * Positions2.size(), &Positions2[0], GL_STATIC_DRAW);
	glEnableVertexAttribArray(POSITION_LOCATION_2);
	glVertexAttribPointer(POSITION_LOCATION_2, 3, GL_FLOAT, GL_FALSE, 0, 0); 

	glBindBuffer(GL_ARRAY_BUFFER, m_BuffersM[mTEXCOORD_VB]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(TexCoords[0]) * TexCoords.size(), &TexCoords[0], GL_STATIC_DRAW);
	glEnableVertexAttribArray(TEX_COORD_LOCATION);
	glVertexAttribPointer(TEX_COORD_LOCATION, 2, GL_FLOAT, GL_FALSE, 0, 0);

	glBindBuffer(GL_ARRAY_BUFFER, m_BuffersM[mNORMAL_VB]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(Normals[0]) * Normals.size(), &Normals[0], GL_STATIC_DRAW);
	glEnableVertexAttribArray(NORMAL_LOCATION);
	glVertexAttribPointer(NORMAL_LOCATION, 3, GL_FLOAT, GL_FALSE, 0, 0);

	glBindBuffer(GL_ARRAY_BUFFER, m_BuffersM[mNORMAL_VB2]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(Normals2[0]) * Normals2.size(), &Normals2[0], GL_STATIC_DRAW);
	glEnableVertexAttribArray(NORMAL_LOCATION_2);
	glVertexAttribPointer(NORMAL_LOCATION_2, 3, GL_FLOAT, GL_FALSE, 0, 0);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_BuffersM[mINDEX_BUFFER]);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(Indices[0]) * Indices.size(), &Indices[0], GL_STATIC_DRAW);


	
	

	return true;
}

void Mesh::InitMesh(unsigned int MeshIndex,
					const aiMesh* paiMesh,
					vector<Vector3f>& Positions,
					vector<Vector3f>& Normals,
					vector<Vector2f>& TexCoords,
					vector<VertexBoneData>& Bones,
					vector<unsigned int>& Indices)
{
	const aiVector3D Zero3D(0.0f, 0.0f, 0.0f);

	for (unsigned int i = 0 ; i < paiMesh->mNumVertices ; i++)
	{
		const aiVector3D* pPos      = &(paiMesh->mVertices[i]);
		const aiVector3D* pNormal   = &(paiMesh->mNormals[i]);
		const aiVector3D* pTexCoord = paiMesh->HasTextureCoords(0) ? &(paiMesh->mTextureCoords[0][i]) : &Zero3D;

		Positions.push_back(Vector3f(pPos->x, pPos->y, pPos->z));
		Normals.push_back(Vector3f(pNormal->x, pNormal->y, pNormal->z));
		TexCoords.push_back(Vector2f(pTexCoord->x, pTexCoord->y));        
	}

	LoadBones(MeshIndex, paiMesh, Bones);
	

	for (unsigned int i = 0; i < paiMesh->mNumFaces; i++)
	{
		const aiFace& Face = paiMesh->mFaces[i];
		assert(Face.mNumIndices == 3);
		Indices.push_back(Face.mIndices[0]);
		Indices.push_back(Face.mIndices[1]);
		Indices.push_back(Face.mIndices[2]);
	}





}

void Mesh::InitMesh(unsigned int MeshIndex,
					const aiMesh* paiMesh,
					vector<Vector3f>& Positions,
					vector<Vector3f>& Normals,
					vector<Vector2f>& TexCoords,
					vector<unsigned int>& Indices)
{
	const aiVector3D Zero3D(0.0f, 0.0f, 0.0f);

	for (unsigned int i = 0 ; i < paiMesh->mNumVertices ; i++) {
		const aiVector3D* pPos      = &(paiMesh->mVertices[i]);
		const aiVector3D* pNormal   = &(paiMesh->mNormals[i]);
		const aiVector3D* pTexCoord = paiMesh->HasTextureCoords(0) ? &(paiMesh->mTextureCoords[0][i]) : &Zero3D;

		Positions.push_back(Vector3f(pPos->x, pPos->y, pPos->z*-1));
		Normals.push_back(Vector3f(pNormal->x, pNormal->y, pNormal->z*-1));
		TexCoords.push_back(Vector2f(pTexCoord->x, pTexCoord->y)); 

		
		
	}

	for (unsigned int i = 0; i < paiMesh->mNumFaces; i++){
		const aiFace& Face = paiMesh->mFaces[i];
		assert(Face.mNumIndices == 3);
		Indices.push_back(Face.mIndices[0]);
		Indices.push_back(Face.mIndices[1]);
		Indices.push_back(Face.mIndices[2]);
	}





}

void Mesh::InitMesh(unsigned int MeshIndex,
					const aiMesh* paiMesh,
					vector<Vector3f>& Positions2,
					vector<Vector3f>& Normals2,
					vector<unsigned int>& Indices2,
					int side
					)
{
	const aiVector3D Zero3D(0.0f, 0.0f, 0.0f);

	for (unsigned int i = 0 ; i < paiMesh->mNumVertices ; i++)
	{
		const aiVector3D* pPos      = &(paiMesh->mVertices[i]);
		const aiVector3D* pNormal   = &(paiMesh->mNormals[i]);

		Positions2.push_back(Vector3f(pPos->x, pPos->y, pPos->z*side));
		Normals2.push_back(Vector3f(pNormal->x, pNormal->y, pNormal->z*side));

				
	}
	for (unsigned int i = 0; i < paiMesh->mNumFaces; i++)
	{
			const aiFace& Face = paiMesh->mFaces[i];
			assert(Face.mNumIndices == 3);
			Indices2.push_back(Face.mIndices[0]);
			Indices2.push_back(Face.mIndices[1]);
			Indices2.push_back(Face.mIndices[2]);
	}
	
}

void Mesh::Render()
{
	glBindVertexArray(m_VAO);

	for (unsigned int i = 0 ; i < m_Entries.size() ; i++) {
		//const unsigned int MaterialIndex = m_Entries[i].MaterialIndex;

		//assert(MaterialIndex < m_Textures.size());

		//if (m_Textures[MaterialIndex]) {
		//    m_Textures[MaterialIndex]->Bind(GL_TEXTURE0);
		// }
		glDrawElementsBaseVertex(GL_TRIANGLES,m_Entries[i].NumIndices,GL_UNSIGNED_INT,(void*)(sizeof(unsigned int) * m_Entries[i].BaseIndex),m_Entries[i].BaseVertex);
		
	}

	
	glBindVertexArray(0);
}

void Mesh::RenderSkinning()
{
	glBindVertexArray(m_VAO);

	for (unsigned int i = 0 ; i < m_Entries.size() ; i++)
	glDrawElementsBaseVertex(GL_TRIANGLES,m_Entries[i].NumIndices,GL_UNSIGNED_INT,(void*)(sizeof(unsigned int) * m_Entries[i].BaseIndex),m_Entries[i].BaseVertex);
	

	   
	glBindVertexArray(0);
}


void Mesh::LoadBones(unsigned int Index, const aiMesh* pMesh, std::vector<VertexBoneData>& Bones)
{

	for (unsigned int i = 0 ; i < pMesh->mNumBones ; i++) {                
		unsigned int BoneIndex = 0;        
		std::string BoneName(pMesh->mBones[i]->mName.data);
		//MessageBox(NULL, pMesh->mBones[i]->mName.data, "INFO", MB_OK);


		if (m_BoneMapping.find(BoneName) == m_BoneMapping.end()) {
			
			
			BoneIndex = m_NumBones;
			m_NumBones++;            
			BoneInfo bi;			
			m_BoneInfo.push_back(bi);
			//m_BoneInfo[BoneIndex].BoneOffset = pMesh->mBones[i]->mOffsetMatrix;            
			//m_BoneMapping[BoneName] = BoneIndex;
		}
		else {
			BoneIndex = m_BoneMapping[BoneName];
		}   

		m_BoneMapping[BoneName] = BoneIndex;
        m_BoneInfo[BoneIndex].BoneOffset = pMesh->mBones[i]->mOffsetMatrix;

		for (unsigned int j = 0 ; j < pMesh->mBones[i]->mNumWeights ; j++) {
			unsigned int VertexID = m_Entries[Index].BaseVertex + pMesh->mBones[i]->mWeights[j].mVertexId;
			float Weight  = pMesh->mBones[i]->mWeights[j].mWeight;                   
			Bones[VertexID].AddBoneData(BoneIndex, Weight);
			
			//MessageBox(NULL, pMesh->mBones[i]->mName.data, "INFO", MB_OK);

		}

	} 

}



void Mesh::ReadNodeHeirarchy(float AnimationTime, const aiNode* pNode, const Matrix4f& ParentTransform)
{    
	std::string NodeName(pNode->mName.data);
	Matrix4f NodeTransformation(pNode->mTransformation);
	
	const aiNodeAnim* pNodeAnim = FindNodeAnim(timer.One[timer.animation_index].Animation, NodeName);
	
	if (pNodeAnim) {
		
		aiVector3D Scaling;
		CalcInterpolatedScaling(Scaling, AnimationTime, pNodeAnim);
		Matrix4f ScalingM;
		ScalingM.InitScaleTransform(Scaling.x, Scaling.y, Scaling.z);

		
		
		aiQuaternion RotationQ;
		CalcInterpolatedRotation(RotationQ, AnimationTime, pNodeAnim); 
		//
		if (NodeName.substr(0,5)=="Wheel")
		{
		RotationQ=ManualTransform(RotationQ);
		QNormalize(&RotationQ);
		}
				
		
	
		Matrix4f RotationM = Matrix4f(RotationQ.GetMatrix());
		

		
		aiVector3D Translation;
		CalcInterpolatedPosition(Translation, AnimationTime, pNodeAnim);
		Matrix4f TranslationM;
		TranslationM.InitTranslationTransform(Translation.x, Translation.y, Translation.z);
		
	
		
		NodeTransformation = TranslationM * RotationM * ScalingM;
		
		
	}
	Matrix4f GlobalTransformation = ParentTransform * NodeTransformation;

	if (NodeName=="GUN")
	current->GunRotation=m_GlobalInverseTransform * GlobalTransformation;
	
	if (NodeName=="Head")
	current->HeadRotation=m_GlobalInverseTransform * GlobalTransformation;
	
	if (NodeName=="Glass")
	current->GlassRotation=m_GlobalInverseTransform * GlobalTransformation;

	if (NodeName=="BrushLeft03")
	current->LeftFistRotation=m_GlobalInverseTransform * GlobalTransformation;

	if (NodeName=="BrushRight03")
	current->RightFistRotation=m_GlobalInverseTransform * GlobalTransformation;

	if (NodeName=="HEADFRAME")
	prime.HeadFrameRotation=m_GlobalInverseTransform * GlobalTransformation;

	if (NodeName=="FRAMEGLASS")
	prime.GlassFrameRotation=m_GlobalInverseTransform * GlobalTransformation;
		
	if (m_BoneMapping.find(NodeName) != m_BoneMapping.end()) {
		unsigned int BoneIndex = m_BoneMapping[NodeName];
		m_BoneInfo[BoneIndex].FinalTransformation = m_GlobalInverseTransform * GlobalTransformation * m_BoneInfo[BoneIndex].BoneOffset;
		
	}

	for (unsigned int i = 0 ; i < pNode->mNumChildren ; i++) {
		ReadNodeHeirarchy(AnimationTime, pNode->mChildren[i], GlobalTransformation);
	}
}

void Mesh::ReadNodeHeirarchyCross(float AnimationTime, const aiNode* pNode, const Matrix4f& ParentTransform, float time)
{    
	
	
	std::string NodeName(pNode->mName.data);
	Matrix4f NodeTransformation(pNode->mTransformation);
	const aiNodeAnim* pNodeAnim = FindNodeAnim(timer.One[timer.crossdata[timer.crossIndex].animStart].Animation, NodeName); //
	const aiNodeAnim* pNodeAnim2 = FindNodeAnim(timer.One[timer.crossdata[timer.crossIndex].animEnd].Animation, NodeName); //
	
	if (pNodeAnim && pNodeAnim2) {
		
		aiVector3D Scaling;
		aiVector3D Scaling2;
		CalcInterpolatedScaling(Scaling, timer.crossdata[timer.crossIndex].timeStart , pNodeAnim); //
		CalcInterpolatedScaling(Scaling2, AnimationTime , pNodeAnim2); //
		Matrix4f ScalingM;
		ScalingM.InitScaleTransform(Scaling.x*time+Scaling2.x*(1-time), Scaling.y*time+Scaling2.y*(1-time), Scaling.z*time+Scaling2.z*(1-time));

		
		
		aiQuaternion RotationQ;
		aiQuaternion RotationQ2;
		CalcInterpolatedRotation(RotationQ, timer.crossdata[timer.crossIndex].timeStart, pNodeAnim); 
		CalcInterpolatedRotation(RotationQ2, AnimationTime, pNodeAnim2); 
		if (NodeName.substr(0,5)=="Wheel")
		{
		RotationQ=ManualTransform(RotationQ);
		QNormalize(&RotationQ);
		}
				
		aiQuaternion RotationQ3;
		aiQuaternion::Interpolate(RotationQ3, RotationQ, RotationQ2, time);
		RotationQ3 = RotationQ3.Normalize();
		
		Matrix4f RotationM = Matrix4f(RotationQ3.GetMatrix());
		

		
		aiVector3D Translation;
		aiVector3D Translation2;
		CalcInterpolatedPosition(Translation, timer.crossdata[timer.crossIndex].timeStart, pNodeAnim);
		CalcInterpolatedPosition(Translation2, AnimationTime , pNodeAnim2); //timer.crossdata.timeEnd
		Matrix4f TranslationM;
		TranslationM.InitTranslationTransform(Translation.x*(1-time)+Translation2.x*time, Translation.y*(1-time)+Translation2.y*time, Translation.z*(1-time)+Translation2.z*time);
		//TranslationM.InitTranslationTransform(Translation2.x, Translation2.y, Translation2.z);
	
		
		NodeTransformation = TranslationM * RotationM * ScalingM;
		
		
	}
	
	Matrix4f GlobalTransformation = ParentTransform * NodeTransformation;

	if (NodeName=="GUN")
	current->GunRotation=m_GlobalInverseTransform * GlobalTransformation;
	
	if (NodeName=="Head")
	current->HeadRotation=m_GlobalInverseTransform * GlobalTransformation;
	
	if (NodeName=="Glass")
	current->GlassRotation=m_GlobalInverseTransform * GlobalTransformation;

	if (NodeName=="BrushLeft03")
	current->LeftFistRotation=m_GlobalInverseTransform * GlobalTransformation;

	if (NodeName=="BrushRight03")
	current->RightFistRotation=m_GlobalInverseTransform * GlobalTransformation;
		
	if (m_BoneMapping.find(NodeName) != m_BoneMapping.end()) {
		unsigned int BoneIndex = m_BoneMapping[NodeName];
		m_BoneInfo[BoneIndex].FinalTransformation = m_GlobalInverseTransform * GlobalTransformation * m_BoneInfo[BoneIndex].BoneOffset;
		
	}

	for (unsigned int i = 0 ; i < pNode->mNumChildren ; i++) {
		ReadNodeHeirarchyCross(AnimationTime, pNode->mChildren[i], GlobalTransformation, time);
	}
}

unsigned int Mesh::FindPosition(float AnimationTime, const aiNodeAnim* pNodeAnim)
{    
	for (unsigned int i = 0 ; i < pNodeAnim->mNumPositionKeys - 1 ; i++) {
		if (AnimationTime < (float)pNodeAnim->mPositionKeys[i + 1].mTime) {
			return i;
		}
	}

	assert(0);

	return 0;
}


unsigned int Mesh::FindRotation(float AnimationTime, const aiNodeAnim* pNodeAnim)
{
	assert(pNodeAnim->mNumRotationKeys > 0);

	for (unsigned int i = 0 ; i < pNodeAnim->mNumRotationKeys - 1 ; i++) {
		if (AnimationTime < (float)pNodeAnim->mRotationKeys[i + 1].mTime) {
			return i;
		}
	}

	assert(0);

	return 0;
}


unsigned int Mesh::FindScaling(float AnimationTime, const aiNodeAnim* pNodeAnim)
{
	assert(pNodeAnim->mNumScalingKeys > 0);

	for (unsigned int i = 0 ; i < pNodeAnim->mNumScalingKeys - 1 ; i++) {
		if (AnimationTime < (float)pNodeAnim->mScalingKeys[i + 1].mTime) {
			return i;
		}
	}

	assert(0);

	return 0;
}


void Mesh::CalcInterpolatedPosition(aiVector3D& Out, float AnimationTime, const aiNodeAnim* pNodeAnim)
{
	if (pNodeAnim->mNumPositionKeys == 1) {
		Out = pNodeAnim->mPositionKeys[0].mValue;
		return;
	}

	unsigned int PositionIndex = FindPosition(AnimationTime, pNodeAnim);
	unsigned int NextPositionIndex = (PositionIndex + 1);
	assert(NextPositionIndex < pNodeAnim->mNumPositionKeys);
	float DeltaTime = (float)(pNodeAnim->mPositionKeys[NextPositionIndex].mTime - pNodeAnim->mPositionKeys[PositionIndex].mTime);
	float Factor = (AnimationTime - (float)pNodeAnim->mPositionKeys[PositionIndex].mTime) / DeltaTime;
	assert(Factor >= 0.0f && Factor <= 1.0f);
	const aiVector3D& Start = pNodeAnim->mPositionKeys[PositionIndex].mValue;
	const aiVector3D& End = pNodeAnim->mPositionKeys[NextPositionIndex].mValue;
	aiVector3D Delta = End - Start;
	Out = Start + Factor * Delta;
	
}


void Mesh::CalcInterpolatedRotation(aiQuaternion& Out, float AnimationTime, const aiNodeAnim* pNodeAnim)
{

	if (pNodeAnim->mNumRotationKeys == 1) {
		Out = pNodeAnim->mRotationKeys[0].mValue;
		return;
	}

	unsigned int RotationIndex = FindRotation(AnimationTime, pNodeAnim);
	unsigned int NextRotationIndex = (RotationIndex + 1);
	assert(NextRotationIndex < pNodeAnim->mNumRotationKeys);
	float DeltaTime = (float)(pNodeAnim->mRotationKeys[NextRotationIndex].mTime - pNodeAnim->mRotationKeys[RotationIndex].mTime);
	float Factor = (AnimationTime - (float)pNodeAnim->mRotationKeys[RotationIndex].mTime) / DeltaTime;
	assert(Factor >= 0.0f && Factor <= 1.0f);

	const aiQuaternion& StartRotationQ = pNodeAnim->mRotationKeys[RotationIndex].mValue;
	const aiQuaternion& EndRotationQ   = pNodeAnim->mRotationKeys[NextRotationIndex].mValue;    
	aiQuaternion::Interpolate(Out, StartRotationQ, EndRotationQ, Factor);
	Out = Out.Normalize();
}


void Mesh::CalcInterpolatedScaling(aiVector3D& Out, float AnimationTime, const aiNodeAnim* pNodeAnim)
{
	if (pNodeAnim->mNumScalingKeys == 1) {
		Out = pNodeAnim->mScalingKeys[0].mValue;
		return;
	}

	unsigned int ScalingIndex = FindScaling(AnimationTime, pNodeAnim);
	unsigned int NextScalingIndex = (ScalingIndex + 1);
	assert(NextScalingIndex < pNodeAnim->mNumScalingKeys);
	float DeltaTime = (float)(pNodeAnim->mScalingKeys[NextScalingIndex].mTime - pNodeAnim->mScalingKeys[ScalingIndex].mTime);
	float Factor = (AnimationTime - (float)pNodeAnim->mScalingKeys[ScalingIndex].mTime) / DeltaTime;
	assert(Factor >= 0.0f && Factor <= 1.0f);
	const aiVector3D& Start = pNodeAnim->mScalingKeys[ScalingIndex].mValue;
	const aiVector3D& End   = pNodeAnim->mScalingKeys[NextScalingIndex].mValue;
	aiVector3D Delta = End - Start;
	Out = Start + Factor * Delta;

}

const aiNodeAnim* Mesh::FindNodeAnim(const aiAnimation* pAnimation, const std::string NodeName)
{
  	for (unsigned int i = 0 ; i < pAnimation->mNumChannels ; i++) {
		const aiNodeAnim* pNodeAnim = pAnimation->mChannels[i];

		if (std::string(pNodeAnim->mNodeName.data) == NodeName) {
			return pNodeAnim;
		}
	}

	return NULL;
}



void draw_skinning_obj(unit *unit, float tX, float tY, float tZ, float dirrot, float shadow_on, float forward_angle, float side_angle)
{
	glPushMatrix();
	glTranslatef(tX,tY,tZ);
	glColor3f(0.0f,0.0f,0.0f);

	glRotatef(dirrot,0,0,1);
	glRotatef(forward_angle,1,0,0);
	glRotatef(side_angle,0,1,0);
	

	glRotatef(180,0,1,0);
	glScalef(7,7,7);
	
	glUseProgram(skeletal_shadow);

	glUniform1f(glGetUniformLocation(skeletal_shadow,"shadow_on"),shadow_on);
	glUniform3f(glGetUniformLocation(skeletal_shadow,"fvEyePosition"),CamPos.x, CamPos.y, CamPos.z);
	glUniform1f(glGetUniformLocation(skeletal_shadow,"fSpecularPower"),specularPower);
	glUniform1i(glGetUniformLocation(skeletal_shadow,"diffuseMap"),0);
	glUniform1i(glGetUniformLocation(skeletal_shadow,"normalMap"),1);
	glUniform1i(glGetUniformLocation(skeletal_shadow,"shadowMap"),2);
	glUniform1i(glGetUniformLocation(skeletal_shadow,"emissionMap"),3);

			
	glActiveTextureARB(GL_TEXTURE0_ARB);
	glBindTexture(GL_TEXTURE_2D, unit->diffuse_map.texID);
	glActiveTextureARB(GL_TEXTURE1_ARB);
	glBindTexture(GL_TEXTURE_2D, unit->normal_map.texID);
	glActiveTextureARB(GL_TEXTURE2_ARB);
	glBindTexture(GL_TEXTURE_2D, texDepth.texID);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_R_TO_TEXTURE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_FUNC, GL_LEQUAL);
	glActiveTextureARB(GL_TEXTURE3_ARB);
	glBindTexture(GL_TEXTURE_2D, unit->emission_map.texID);

	glUniformMatrix4fv(glGetUniformLocation(skeletal_shadow,"lightMatrix"), 1, false, lightMatrix);
	glUniform3f(glGetUniformLocation(skeletal_shadow,"lightPos"), mvLightPos[0], mvLightPos[1], mvLightPos[2]);
	
	current=unit;
	unit->Current->BoneTransform(unit->Current->timer.One[unit->Current->timer.animation_index].time, Transforms);
	
	for (unsigned int i = 0 ; i < Transforms.size() ; i++)
	{
	SetBoneTransform(i, Transforms[i]);
	}

	unit->Current->RenderSkinning();
	
	glActiveTextureARB(GL_TEXTURE2_ARB);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_NONE);
	glBindTexture(GL_TEXTURE_2D, 0);
	glActiveTextureARB(GL_TEXTURE0_ARB);
	
	glUseProgram(0);
	glPopMatrix();

}

void draw_frame_obj(unit *unit, float tX, float tY, float tZ, float dirrot, float shadow_on, Vector3f eyepos)
{
	glPushMatrix();
	glTranslatef(tX,tY,tZ);
	glColor3f(0.0f,0.0f,0.0f);

	glRotatef(dirrot,0,0,1);
	

	glRotatef(180,0,1,0);
	glScalef(7,7,7);
	
	glUseProgram(skeletal_shadow);

	glUniform1f(glGetUniformLocation(skeletal_shadow,"shadow_on"),shadow_on);
	glUniform3f(glGetUniformLocation(skeletal_shadow,"fvEyePosition"),eyepos.x, eyepos.y, eyepos.z);
	glUniform1f(glGetUniformLocation(skeletal_shadow,"fSpecularPower"),specularPower);
	glUniform1i(glGetUniformLocation(skeletal_shadow,"diffuseMap"),0);
	glUniform1i(glGetUniformLocation(skeletal_shadow,"normalMap"),1);
	glUniform1i(glGetUniformLocation(skeletal_shadow,"shadowMap"),2);
	glUniform1i(glGetUniformLocation(skeletal_shadow,"emissionMap"),3);

			
	glActiveTextureARB(GL_TEXTURE0_ARB);
	glBindTexture(GL_TEXTURE_2D, unit->diffuse_map.texID);
	glActiveTextureARB(GL_TEXTURE1_ARB);
	glBindTexture(GL_TEXTURE_2D, unit->normal_map.texID);
	glActiveTextureARB(GL_TEXTURE2_ARB);
	glBindTexture(GL_TEXTURE_2D, texDepth.texID);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_R_TO_TEXTURE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_FUNC, GL_LEQUAL);
	glActiveTextureARB(GL_TEXTURE3_ARB);
	glBindTexture(GL_TEXTURE_2D, unit->emission_map.texID);

	glUniformMatrix4fv(glGetUniformLocation(skeletal_shadow,"lightMatrix"), 1, false, lightMatrix);
	glUniform3f(glGetUniformLocation(skeletal_shadow,"lightPos"), mvLightPos[0], mvLightPos[1], mvLightPos[2]);
	current=unit;
	unit->Face->BoneTransform(unit->Face->timer.One[unit->Face->timer.animation_index].time, Transforms);
	
	for (unsigned int i = 0 ; i < Transforms.size() ; i++)
	{
	SetBoneTransform(i, Transforms[i]);
	}

	unit->Face->RenderSkinning();
	
	glActiveTextureARB(GL_TEXTURE2_ARB);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_NONE);
	glBindTexture(GL_TEXTURE_2D, 0);
	glActiveTextureARB(GL_TEXTURE0_ARB);
	
	glUseProgram(0);
	glPopMatrix();

}

void draw_skinning_obj_morphing(unit *unit, float tX, float tY, float tZ, float dirrot, float shadow_on, float forward_angle, float side_angle)
{
	glPushMatrix();
	glTranslatef(tX,tY,tZ);
	glColor3f(0.0f,0.0f,0.0f);

	glRotatef(dirrot,0,0,1);
	glRotatef(forward_angle,1,0,0);
	glRotatef(side_angle,0,1,0);
	

	glRotatef(180,0,1,0);
	glScalef(7,7,7);
	
	glUseProgram(skeletal_shadow_morphing);

	float mtime;
	if (unit->Current->timer.animation_index==0)
	mtime=unit->Current->timer.One[unit->Current->timer.animation_index].time/2.0;
	else
	mtime=1-(unit->Current->timer.One[unit->Current->timer.animation_index].time/2.0);

	glUniform1f(glGetUniformLocation(skeletal_shadow_morphing,"shadow_on"),shadow_on);
	glUniform3f(glGetUniformLocation(skeletal_shadow_morphing,"fvEyePosition"),CamPos.x, CamPos.y, CamPos.z);
	glUniform1f(glGetUniformLocation(skeletal_shadow_morphing,"fSpecularPower"),specularPower);
	glUniform1f(glGetUniformLocation(skeletal_shadow_morphing,"time"),mtime);
	glUniform1i(glGetUniformLocation(skeletal_shadow_morphing,"diffuseMap"),0);
	glUniform1i(glGetUniformLocation(skeletal_shadow_morphing,"normalMap"),1);
	glUniform1i(glGetUniformLocation(skeletal_shadow_morphing,"shadowMap"),2);
	glUniform1i(glGetUniformLocation(skeletal_shadow_morphing,"emissionMap"),3);

			
	glActiveTextureARB(GL_TEXTURE0_ARB);
	glBindTexture(GL_TEXTURE_2D, unit->diffuse_map.texID);
	glActiveTextureARB(GL_TEXTURE1_ARB);
	glBindTexture(GL_TEXTURE_2D, unit->normal_map.texID);
	glActiveTextureARB(GL_TEXTURE2_ARB);
	glBindTexture(GL_TEXTURE_2D, texDepth.texID);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_R_TO_TEXTURE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_FUNC, GL_LEQUAL);
	glActiveTextureARB(GL_TEXTURE3_ARB);
	glBindTexture(GL_TEXTURE_2D, unit->emission_map.texID);

	glUniformMatrix4fv(glGetUniformLocation(skeletal_shadow_morphing,"lightMatrix"), 1, false, lightMatrix);
	glUniform3f(glGetUniformLocation(skeletal_shadow_morphing,"lightPos"), mvLightPos[0], mvLightPos[1], mvLightPos[2]);

	current=unit;
	unit->Current->BoneTransform(unit->Current->timer.One[unit->Current->timer.animation_index].time, Transforms);
	
	for (unsigned int i = 0 ; i < Transforms.size() ; i++)
	{
	SetBoneTransform(i, Transforms[i]);
	}

	unit->Current->RenderSkinning();
	
	glActiveTextureARB(GL_TEXTURE2_ARB);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_NONE);
	glBindTexture(GL_TEXTURE_2D, 0);
	glActiveTextureARB(GL_TEXTURE0_ARB);
	
	glUseProgram(0);
	glPopMatrix();

}

void draw_state_obj(unit *unit, float tX, float tY, float tZ, float dirrot, float shadow_on, float forward_angle, float side_angle, int part)
{
	glPushMatrix();
	glTranslatef(tX,tY,tZ);
	glColor3f(0.0f,0.0f,0.0f);

	glRotatef(dirrot,0,0,1);
	glRotatef(forward_angle,1,0,0);
	glRotatef(side_angle,0,1,0);


	//
	glRotatef(180,0,1,0);
	//glRotatef(180,0,0,1);
	glScalef(7,7,7);

	
	if (part==0)
	{
		glUseProgram(skeletal_shadow_part);
		glUniform1f(glGetUniformLocation(skeletal_shadow_part,"shadow_on"),shadow_on);
		glUniform3f(glGetUniformLocation(skeletal_shadow_part,"fvEyePosition"),CamPos.x, CamPos.y, CamPos.z);
		glUniform1f(glGetUniformLocation(skeletal_shadow_part,"fSpecularPower"),specularPower);
		glUniform1i(glGetUniformLocation(skeletal_shadow_part,"diffuseMap"),0);
		glUniform1i(glGetUniformLocation(skeletal_shadow_part,"normalMap"),1);
		glUniform1i(glGetUniformLocation(skeletal_shadow_part,"shadowMap"),2);
		glUniform1i(glGetUniformLocation(skeletal_shadow_part,"emissionMap"),3);


		glActiveTextureARB(GL_TEXTURE0_ARB);
		glBindTexture(GL_TEXTURE_2D, prime.gun_diffuse_map.texID);
		glActiveTextureARB(GL_TEXTURE1_ARB);
		glBindTexture(GL_TEXTURE_2D, prime.gun_normal_map.texID);
		glActiveTextureARB(GL_TEXTURE2_ARB);
		glBindTexture(GL_TEXTURE_2D, texDepth.texID);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_R_TO_TEXTURE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_FUNC, GL_LEQUAL);
		glActiveTextureARB(GL_TEXTURE3_ARB);
		glBindTexture(GL_TEXTURE_2D, prime.gun_emission_map.texID);

		glUniformMatrix4fv(glGetUniformLocation(skeletal_shadow_part,"lightMatrix"), 1, false, lightMatrix);
		glUniform3f(glGetUniformLocation(skeletal_shadow_part,"lightPos"), mvLightPos[0], mvLightPos[1], mvLightPos[2]);
	}
	if (part==1)
	{
		glUseProgram(skeletal_shadow_part);
		glUniform1f(glGetUniformLocation(skeletal_shadow_part,"shadow_on"),shadow_on);
		glUniform3f(glGetUniformLocation(skeletal_shadow_part,"fvEyePosition"),CamPos.x, CamPos.y, CamPos.z);
		glUniform1f(glGetUniformLocation(skeletal_shadow_part,"fSpecularPower"),specularPower);
		glUniform1i(glGetUniformLocation(skeletal_shadow_part,"diffuseMap"),0);
		glUniform1i(glGetUniformLocation(skeletal_shadow_part,"normalMap"),1);
		glUniform1i(glGetUniformLocation(skeletal_shadow_part,"shadowMap"),2);
		glUniform1i(glGetUniformLocation(skeletal_shadow_part,"emissionMap"),3);


		glActiveTextureARB(GL_TEXTURE0_ARB);
		glBindTexture(GL_TEXTURE_2D, prime.head_diffuse_map.texID);
		glActiveTextureARB(GL_TEXTURE1_ARB);
		glBindTexture(GL_TEXTURE_2D, prime.head_normal_map.texID);
		glActiveTextureARB(GL_TEXTURE2_ARB);
		glBindTexture(GL_TEXTURE_2D, texDepth.texID);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_R_TO_TEXTURE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_FUNC, GL_LEQUAL);
		glActiveTextureARB(GL_TEXTURE3_ARB);
		glBindTexture(GL_TEXTURE_2D, prime.head_emission_map.texID);

		glUniformMatrix4fv(glGetUniformLocation(skeletal_shadow_part,"lightMatrix"), 1, false, lightMatrix);
		glUniform3f(glGetUniformLocation(skeletal_shadow_part,"lightPos"), mvLightPos[0], mvLightPos[1], mvLightPos[2]);
	}
	if (part==2)
	{
		if (unit->Current==unit->Transform && unit->morphing==true)
		{
			float mtime;
			if (unit->Current->timer.animation_index==0)
			mtime=unit->Current->timer.One[unit->Current->timer.animation_index].time/2.0;
			else
			mtime=1-(unit->Current->timer.One[unit->Current->timer.animation_index].time/2.0);

			glUseProgram(skeletal_shadow_morphing_glass);
			glUniform1f(glGetUniformLocation(skeletal_shadow_morphing_glass,"shadow_on"),shadow_on);
			glUniform3f(glGetUniformLocation(skeletal_shadow_morphing_glass,"fvEyePosition"),CamPos.x, CamPos.y, CamPos.z);
			glUniform1f(glGetUniformLocation(skeletal_shadow_morphing_glass,"time"),mtime);
			glUniform1i(glGetUniformLocation(skeletal_shadow_morphing_glass,"cubeMap"),0);
			glUniform1i(glGetUniformLocation(skeletal_shadow_morphing_glass,"shadowMap"),2);
			glUniform1i(glGetUniformLocation(skeletal_shadow_morphing_glass,"glass_ao"),3);
			glUniform1f(glGetUniformLocation(skeletal_shadow_morphing_glass,"fSpecularPower"),specularPower);
			glUniformMatrix4fv(glGetUniformLocation(skeletal_shadow_morphing_glass,"lightMatrix"), 1, false, lightMatrix);
			glUniform3f(glGetUniformLocation(skeletal_shadow_morphing_glass,"lightPos"), mvLightPos[0], mvLightPos[1], mvLightPos[2]);
		}
		else
		{
			glUseProgram(skeletal_shadow_glass);
			glUniform1f(glGetUniformLocation(skeletal_shadow_glass,"shadow_on"),shadow_on);
			glUniform3f(glGetUniformLocation(skeletal_shadow_glass,"fvEyePosition"),CamPos.x, CamPos.y, CamPos.z);
			glUniform1i(glGetUniformLocation(skeletal_shadow_glass,"cubeMap"),0);
			glUniform1i(glGetUniformLocation(skeletal_shadow_glass,"shadowMap"),2);
			glUniform1i(glGetUniformLocation(skeletal_shadow_glass,"glass_ao"),3);
			glUniform1f(glGetUniformLocation(skeletal_shadow_glass,"fSpecularPower"),specularPower);
			glUniformMatrix4fv(glGetUniformLocation(skeletal_shadow_glass,"lightMatrix"), 1, false, lightMatrix);
			glUniform3f(glGetUniformLocation(skeletal_shadow_glass,"lightPos"), mvLightPos[0], mvLightPos[1], mvLightPos[2]);
		}
		glActiveTextureARB(GL_TEXTURE0_ARB);
		glEnable  ( GL_TEXTURE_CUBE_MAP_ARB );
		glBindTexture ( GL_TEXTURE_CUBE_MAP_ARB, texCubeMap );
		glTexParameteri ( GL_TEXTURE_CUBE_MAP_ARB, GL_TEXTURE_WRAP_S, GL_CLAMP );
		glTexParameteri ( GL_TEXTURE_CUBE_MAP_ARB, GL_TEXTURE_WRAP_T, GL_CLAMP );
		glEnable  ( GL_TEXTURE_GEN_S );
		glEnable  ( GL_TEXTURE_GEN_T );
		glEnable  ( GL_TEXTURE_GEN_R );
		glActiveTextureARB(GL_TEXTURE2_ARB);
		glBindTexture(GL_TEXTURE_2D, texDepth.texID);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_R_TO_TEXTURE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_FUNC, GL_LEQUAL);
		glActiveTextureARB(GL_TEXTURE3_ARB);
		glBindTexture(GL_TEXTURE_2D, glass_ao.texID);
		
	}
	if (part==3 || part==4)
	{
		
		glUseProgram(skeletal_shadow_part);
		glUniform1f(glGetUniformLocation(skeletal_shadow_part,"shadow_on"),shadow_on);
		glUniform3f(glGetUniformLocation(skeletal_shadow_part,"fvEyePosition"),CamPos.x, CamPos.y, CamPos.z);
		glUniform1f(glGetUniformLocation(skeletal_shadow_part,"fSpecularPower"),specularPower);
		glUniform1i(glGetUniformLocation(skeletal_shadow_part,"diffuseMap"),0);
		glUniform1i(glGetUniformLocation(skeletal_shadow_part,"normalMap"),1);
		glUniform1i(glGetUniformLocation(skeletal_shadow_part,"shadowMap"),2);
		glUniform1i(glGetUniformLocation(skeletal_shadow_part,"emissionMap"),3);


		glActiveTextureARB(GL_TEXTURE0_ARB);
		glBindTexture(GL_TEXTURE_2D, fist_diffuse_map.texID);
		glActiveTextureARB(GL_TEXTURE1_ARB);
		glBindTexture(GL_TEXTURE_2D, fist_normal_map.texID);
		glActiveTextureARB(GL_TEXTURE2_ARB);
		glBindTexture(GL_TEXTURE_2D, texDepth.texID);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_R_TO_TEXTURE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_FUNC, GL_LEQUAL);
		glActiveTextureARB(GL_TEXTURE3_ARB);
		glBindTexture(GL_TEXTURE_2D, fist_ao_map.texID);


		glUniformMatrix4fv(glGetUniformLocation(skeletal_shadow_part,"lightMatrix"), 1, false, lightMatrix);
		glUniform3f(glGetUniformLocation(skeletal_shadow_part,"lightPos"), mvLightPos[0], mvLightPos[1], mvLightPos[2]);
		
		
		
	}
	if (part==5)
	{
		glUseProgram(skeletal_shadow_glass);
		glUniform1f(glGetUniformLocation(skeletal_shadow_glass,"shadow_on"),shadow_on);
		glUniform3f(glGetUniformLocation(skeletal_shadow_glass,"fvEyePosition"),CamPos.x, CamPos.y, CamPos.z);
		glUniform1i(glGetUniformLocation(skeletal_shadow_glass,"cubeMap"),0);
		glUniform1i(glGetUniformLocation(skeletal_shadow_glass,"shadowMap"),2);
		glUniform1i(glGetUniformLocation(skeletal_shadow_glass,"glass_ao"),3);
		glUniform1f(glGetUniformLocation(skeletal_shadow_glass,"fSpecularPower"),specularPower);
		glUniformMatrix4fv(glGetUniformLocation(skeletal_shadow_glass,"lightMatrix"), 1, false, lightMatrix);
		glUniform3f(glGetUniformLocation(skeletal_shadow_glass,"lightPos"), mvLightPos[0], mvLightPos[1], mvLightPos[2]);
		glActiveTextureARB(GL_TEXTURE0_ARB);
		glEnable  ( GL_TEXTURE_CUBE_MAP_ARB );
		glBindTexture ( GL_TEXTURE_CUBE_MAP_ARB, texCubeMap );
		glTexParameteri ( GL_TEXTURE_CUBE_MAP_ARB, GL_TEXTURE_WRAP_S, GL_CLAMP );
		glTexParameteri ( GL_TEXTURE_CUBE_MAP_ARB, GL_TEXTURE_WRAP_T, GL_CLAMP );
		glEnable  ( GL_TEXTURE_GEN_S );
		glEnable  ( GL_TEXTURE_GEN_T );
		glEnable  ( GL_TEXTURE_GEN_R );
		glActiveTextureARB(GL_TEXTURE2_ARB);
		glBindTexture(GL_TEXTURE_2D, texDepth.texID);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_R_TO_TEXTURE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_FUNC, GL_LEQUAL);
		glActiveTextureARB(GL_TEXTURE3_ARB);
		glBindTexture(GL_TEXTURE_2D, glass_ao.texID);
	}

		//SetBoneTransform(0, prime.GunRotation);
		if (part==0)
		{
			glUniformMatrix4fv(glGetUniformLocation(skeletal_shadow_part,"gBones"), 1, GL_TRUE, (const GLfloat*)unit->GunRotation.m);
			unit->Gun->RenderSkinning();
		}

		if (part==1)
		{
			glUniformMatrix4fv(glGetUniformLocation(skeletal_shadow_part,"gBones"), 1, GL_TRUE, (const GLfloat*)unit->HeadRotation.m);
			unit->Head->RenderSkinning();
		}
	
		if (part==2)
		{
			if ((unit->Current==unit->Transform || unit->Current==unit->Alternative) && unit->morphing==true)
			{
				if (unit->Current==unit->Transform)
				{
					glUniformMatrix4fv(glGetUniformLocation(skeletal_shadow_morphing_glass,"gBones"), 1, GL_TRUE, (const GLfloat*)unit->GlassRotation.m);
					unit->Glass->RenderSkinning();
				}
				if (unit->Current==unit->Alternative)
				{
					glUniformMatrix4fv(glGetUniformLocation(skeletal_shadow_glass,"gBones"), 1, GL_TRUE, (const GLfloat*)unit->GlassRotation.m);
					unit->Glass03->RenderSkinning();
				}
				
			}
			else
			{
				glUniformMatrix4fv(glGetUniformLocation(skeletal_shadow_glass,"gBones"), 1, GL_TRUE, (const GLfloat*)unit->GlassRotation.m);
				unit->Glass02->RenderSkinning();
			}
		}

		if (part==3)
		{
			glUniformMatrix4fv(glGetUniformLocation(skeletal_shadow_part,"gBones"), 1, GL_TRUE, (const GLfloat*)unit->RightFistRotation.m);
			RightFist->RenderSkinning();
		}
		if (part==4)
		{
			glUniformMatrix4fv(glGetUniformLocation(skeletal_shadow_part,"gBones"), 1, GL_TRUE, (const GLfloat*)unit->LeftFistRotation.m);
			LeftFist->RenderSkinning();
		}
		if (part==5)
		{
			glUniformMatrix4fv(glGetUniformLocation(skeletal_shadow_glass,"gBones"), 1, GL_TRUE, (const GLfloat*)unit->GlassRotation.m);
			unit->Glass->RenderSkinning();
		}

		glActiveTextureARB(GL_TEXTURE2_ARB);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_NONE);
		glBindTexture(GL_TEXTURE_2D, 0);
		glActiveTextureARB(GL_TEXTURE0_ARB);
		if (part==2 || part==5)
		{
		glActiveTextureARB(GL_TEXTURE0_ARB);
		glDisable( GL_TEXTURE_CUBE_MAP_ARB );
		glDisable  ( GL_TEXTURE_GEN_S );
		glDisable  ( GL_TEXTURE_GEN_T );
		glDisable  ( GL_TEXTURE_GEN_R );
		glEnable(GL_TEXTURE_2D);
		}

		glUseProgram(0);
		glPopMatrix();

	
}

void draw_frame_part(unit *unit, float tX, float tY, float tZ, float dirrot, float shadow_on, int part, Vector3f eyepos)
{
	glPushMatrix();
	glTranslatef(tX,tY,tZ);
	glColor3f(0.0f,0.0f,0.0f);
	glRotatef(dirrot,0,0,1);
	glRotatef(180,0,1,0);
	glScalef(7,7,7);

	if (part==0)
	{
		glUseProgram(skeletal_shadow_part);
		glUniform1f(glGetUniformLocation(skeletal_shadow_part,"shadow_on"),shadow_on);
		glUniform3f(glGetUniformLocation(skeletal_shadow_part,"fvEyePosition"),eyepos.x, eyepos.y, eyepos.z);
		glUniform1f(glGetUniformLocation(skeletal_shadow_part,"fSpecularPower"),specularPower);
		glUniform1i(glGetUniformLocation(skeletal_shadow_part,"diffuseMap"),0);
		glUniform1i(glGetUniformLocation(skeletal_shadow_part,"normalMap"),1);
		glUniform1i(glGetUniformLocation(skeletal_shadow_part,"shadowMap"),2);
		glUniform1i(glGetUniformLocation(skeletal_shadow_part,"emissionMap"),3);


		glActiveTextureARB(GL_TEXTURE0_ARB);
		glBindTexture(GL_TEXTURE_2D, prime.head_diffuse_map.texID);
		glActiveTextureARB(GL_TEXTURE1_ARB);
		glBindTexture(GL_TEXTURE_2D, prime.head_normal_map.texID);
		glActiveTextureARB(GL_TEXTURE2_ARB);
		glBindTexture(GL_TEXTURE_2D, texDepth.texID);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_R_TO_TEXTURE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_FUNC, GL_LEQUAL);
		glActiveTextureARB(GL_TEXTURE3_ARB);
		glBindTexture(GL_TEXTURE_2D, prime.head_emission_map.texID);

		glUniformMatrix4fv(glGetUniformLocation(skeletal_shadow_part,"lightMatrix"), 1, false, lightMatrix);
		glUniform3f(glGetUniformLocation(skeletal_shadow_part,"lightPos"), mvLightPos[0], mvLightPos[1], mvLightPos[2]);
	
		glUniformMatrix4fv(glGetUniformLocation(skeletal_shadow_part,"gBones"), 1, GL_TRUE, (const GLfloat*)unit->HeadFrameRotation.m);
		unit->Head->RenderSkinning();
		
		glActiveTextureARB(GL_TEXTURE2_ARB);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_NONE);
		glBindTexture(GL_TEXTURE_2D, 0);
		glActiveTextureARB(GL_TEXTURE0_ARB);
	}
	else
	{
		
			glUseProgram(skeletal_shadow_glass);
			glUniform1f(glGetUniformLocation(skeletal_shadow_glass,"shadow_on"),shadow_on);
			glUniform3f(glGetUniformLocation(skeletal_shadow_glass,"fvEyePosition"),eyepos.x, eyepos.y, eyepos.z);
			glUniform1i(glGetUniformLocation(skeletal_shadow_glass,"cubeMap"),0);
			glUniform1i(glGetUniformLocation(skeletal_shadow_glass,"shadowMap"),2);
			glUniform1i(glGetUniformLocation(skeletal_shadow_glass,"glass_ao"),3);
			glUniform1f(glGetUniformLocation(skeletal_shadow_glass,"fSpecularPower"),specularPower);
			
		glActiveTextureARB(GL_TEXTURE0_ARB);
		glEnable  ( GL_TEXTURE_CUBE_MAP_ARB );
		glBindTexture ( GL_TEXTURE_CUBE_MAP_ARB, texCubeMap );
		glTexParameteri ( GL_TEXTURE_CUBE_MAP_ARB, GL_TEXTURE_WRAP_S, GL_CLAMP );
		glTexParameteri ( GL_TEXTURE_CUBE_MAP_ARB, GL_TEXTURE_WRAP_T, GL_CLAMP );
		glEnable  ( GL_TEXTURE_GEN_S );
		glEnable  ( GL_TEXTURE_GEN_T );
		glEnable  ( GL_TEXTURE_GEN_R );
		glActiveTextureARB(GL_TEXTURE2_ARB);
		glBindTexture(GL_TEXTURE_2D, texDepth.texID);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_R_TO_TEXTURE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_FUNC, GL_LEQUAL);
		glActiveTextureARB(GL_TEXTURE3_ARB);
		glBindTexture(GL_TEXTURE_2D, glass_ao.texID);

		glUniformMatrix4fv(glGetUniformLocation(skeletal_shadow_glass,"lightMatrix"), 1, false, lightMatrix);
		glUniform3f(glGetUniformLocation(skeletal_shadow_glass,"lightPos"), mvLightPos[0], mvLightPos[1], mvLightPos[2]);
	
		glUniformMatrix4fv(glGetUniformLocation(skeletal_shadow_glass,"gBones"), 1, GL_TRUE, (const GLfloat*)unit->GlassFrameRotation.m);
		unit->Glass02->RenderSkinning();
		
		glActiveTextureARB(GL_TEXTURE2_ARB);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_NONE);
		glBindTexture(GL_TEXTURE_2D, 0);
		glActiveTextureARB(GL_TEXTURE0_ARB);
		glActiveTextureARB(GL_TEXTURE0_ARB);
		glDisable( GL_TEXTURE_CUBE_MAP_ARB );
		glDisable  ( GL_TEXTURE_GEN_S );
		glDisable  ( GL_TEXTURE_GEN_T );
		glDisable  ( GL_TEXTURE_GEN_R );
		glEnable(GL_TEXTURE_2D);
	}

		glUseProgram(0);
		glPopMatrix();

	
}

void SetBoneTransform(unsigned int Index, const Matrix4f& Transform)
{
	assert(Index < MAX_BONES);
	glUniformMatrix4fv(m_boneLocation[Index], 1, GL_TRUE, (const GLfloat*)Transform.m); 
	
	
}


void an_timer::Create(unsigned int animation_counts, const aiScene* pScene)
{
animation_count=animation_counts;
animation_index=0;
part=0;
One = new one[animation_count];
One[0].Add(pScene);
crossFlag=false;
crossTime=0.0f;
anim_move_walk=false;
freeze=false;

	
}



void an_timer::one::Add(const aiScene* pScene)
{
Animation=LoadAnimation(pScene);
Data=LoadNodes(pScene);
time = 0.0f;
diff = 0.0f;
on=false;
	
}

void an_timer::crossData::Add(float _timeStart, float _timeEnd, int _animStart, int _animEnd, int _animExit, bool _add, float _timeDuration)
{
timeStart=_timeStart;
timeEnd=_timeEnd;
animStart=_animStart;
animEnd=_animEnd;
animExit=_animExit;
add=_add;	
timeDuration=_timeDuration;
}


unit::unit()
{
	Transform = new Mesh;
	Main = new Mesh;
	Alternative = new Mesh;
	Gun = new Mesh;
	Head = new Mesh;
	Glass = new Mesh;
	Glass02 = new Mesh;
	Glass03 = new Mesh;
	Current = Alternative;
	Face = new Mesh;
	Main->timer.current=this;
	Alternative->timer.current=this;
	Transform->timer.current=this;
	GlassRotation.InitIdentity();
	HeadRotation.InitIdentity();
	
	
	
}

void an_timer::one::Start()
{
	time = 0.0f;
	diff = RunningTime;
	on=true;
}

void an_timer::Look(int type)
{
	
	One[animation_index].time=RunningTime-One[animation_index].diff;
	if (One[animation_index].time>=One[animation_index].Animation->mDuration) 
	{
		One[animation_index].time = 0.0f;
		One[animation_index].diff = RunningTime;
		
		if (type==0)
		{
	
		if (static_cast<unit*>(current)->Current==static_cast<unit*>(current)->Transform)
		{
			One[animation_index].on=false;
			static_cast<unit*>(current)->Transformation();
		}
		else
		if (static_cast<unit*>(current)->Current==static_cast<unit*>(current)->Main)
		{
			if (static_cast<unit*>(current)->morphing==true)
			{
			switch (animation_index)
			{
			case 0:
				One[animation_index].on=false;
				animation_index=6;
				static_cast<unit*>(current)->Current->timer.One[animation_index].Start();
			break;
			case 2:
				One[animation_index].on=false;
				static_cast<unit*>(current)->Transformation();
			break;
			case 3:
			if (anim_move_walk==true)
				{
				//One[animation_index].on=false;
				//crossTime=1.0f;
				//animation_index=5;
				//One[animation_index].Start();
				//move_anim2.anim_move_walk=false;
				}
			break;
			case 4:
				One[animation_index].on=false;
				animation_index=3;
				One[animation_index].Start();
			break;
			case 5:
				One[animation_index].on=false;
				animation_index=1;
				One[animation_index].Start();
			break;
			case 6:
				One[animation_index].on=false;
				animation_index=1;
				One[animation_index].Start();
			break;
			case 8:
				One[animation_index].on=false;
				animation_index=2;
				One[animation_index].Start();
			break;
			}
			}
			
		}
		if (static_cast<unit*>(current)->Current==static_cast<unit*>(current)->Alternative)
		{
			switch (animation_index)
			{
			case 1:
				One[animation_index].on=false;
				animation_index=0;
				static_cast<unit*>(current)->Current->timer.One[animation_index].Start();
			break;
			case 2:
				One[animation_index].on=false;
				animation_index=0;
				static_cast<unit*>(current)->Current->timer.One[animation_index].Start();
			break;
			}
		}
		}
		else
		{
		prime.Face->timer.One[animation_index].Start();
		}
		
		
		

	}
}

void Mesh::BoneTransform(float TimeInSeconds, std::vector<Matrix4f>& Transforms)
{
	Matrix4f Identity;
	Identity.InitIdentity();
	if (timer.crossFlag==true)
	{
		timer.crossTime=TimeInSeconds;
		timer.crossFlag=false;
		timer.One[timer.animation_index].on=false;
		timer.animation_index=4;
		timer.One[timer.animation_index].Start();
		timer.crossdata[timer.crossIndex].timeCheck=RunningTime;
		timer.crossdata[timer.crossIndex].timeStart=timer.crossTime;
		timer.anim_move_walk=true;
		crossTime=0.0f;
	}
	if (timer.anim_move_walk==true)
	{
		float Atime;
		if (timer.crossdata[timer.crossIndex].add==false)
			Atime=timer.crossdata[timer.crossIndex].timeEnd;
		else
			Atime=timer.One[timer.animation_index].time;

		//
		ReadNodeHeirarchyCross(Atime, timer.One->Data, Identity, crossTime);
		
		/*
		if (crossTime<=0.985) crossTime+=0.015f; else 
		{
			crossTime=1.0f;
			timer.anim_move_walk=false;
			unitptr->Current->timer.animation_index=timer.crossdata[timer.crossIndex].animExit;
			if (timer.crossdata[timer.crossIndex].add==0)
				unitptr->Current->timer.One[current->Current->timer.animation_index].Start();
		}
		*/
		if (crossTime<=0.985f)
			crossTime=(RunningTime-timer.crossdata[timer.crossIndex].timeCheck)/timer.crossdata[timer.crossIndex].timeDuration;
		else
		{
			crossTime=1.0f;
			timer.anim_move_walk=false;
			unitptr->Current->timer.animation_index=timer.crossdata[timer.crossIndex].animExit;
			if (timer.crossdata[timer.crossIndex].add==0)
				unitptr->Current->timer.One[current->Current->timer.animation_index].Start();
		}
		
	}
	else
		ReadNodeHeirarchy(timer.One[timer.animation_index].time, timer.One->Data, Identity);
	
	
	
	Transforms.resize(m_NumBones);
	
	for (unsigned int i = 0 ; i < m_NumBones ; i++) {
		Transforms[i] = m_BoneInfo[i].FinalTransformation;
	}

	
}

aiQuaternion Mesh::ManualTransform(aiQuaternion quat)
{
	quat=Multiply(quat,wheelController.WheelEngine);
	
	return quat;
}

aiQuaternion Mesh::CopyTransform(aiQuaternion *quat)
{
	aiQuaternion Temp(1.0,0.0,0.0,0.0);
	Temp.w=quat->w;
	Temp.x=quat->x;
	Temp.y=quat->y;
	Temp.z=quat->z;
	return Temp;
}

void unit::Transformation()
{
	if (Current==Alternative)
	{
		form=1;
		Current=Transform;
		Transform->timer.animation_index=0;
		Transform->timer.One[Transform->timer.animation_index].Start();
		
	}
	else
		if (Current==Main)
		{
			form=0;
			if (morphing==true)
			{
				if (Main->timer.animation_index!=2)
				{
					Main->timer.animation_index=8;
					Main->timer.One[Main->timer.animation_index].Start();
				}
				else
					if (Main->timer.animation_index==2)
					{
						
						Current=Transform;
						Main->timer.animation_index=0;
						Transform->timer.animation_index=1;
						Transform->timer.One[Transform->timer.animation_index].Start();
					}
			}
			else
			{

				Current=Transform;
				Transform->timer.animation_index=1;
				Transform->timer.One[Transform->timer.animation_index].Start();
			}

		
							
	}
	else
	if (Current==Transform)
	{
			
		if (form==0)
		{
			Current=Alternative;
			Alternative->timer.animation_index=0;
			Alternative->timer.One[Alternative->timer.animation_index].Start();
					
		}
		else
		{
			
			Current=Main;
			Main->timer.animation_index=0;
			Main->timer.One[Main->timer.animation_index].Start();
		}
				
		}


}

void unit::SeparateTrailer()
{
type=1;
unitlist[1]=Trailer;
unitlist[1]->X=trailpos_x;
unitlist[1]->Y=trailpos_y;
unitlist[1]->rotdir=rotdir;
mainmass[unitlist[1]->X][unitlist[1]->Y].unitptr=unitlist[1];

}

void unit::MoveToSeparate()
{
wheelController.Start(-1);
move_anim2.t=0;
move_anim2.anim_move_flag=true;
move_anim2.anim_move_trailer=true;



bezierPath.ClearPath();

bezierPath.AddPoint(5,traildot,unitptr->trailangle);
bezierPath.endAlt(unitptr->trailpos_x,unitptr->trailpos_y,traildot,rotdir);

bezierPath.GetDrawingPoints(1, true);

Trailer->traildot=MainToOxy(X, Y);
Trailer->trailangle=(float)rotdir*60;

}

void unit::MoveFromSeparate()
{
wheelController.Start(1);
move_anim2.t=0;
move_anim2.anim_move_flag=true;
move_anim2.anim_move_trailer=true;

bezierPath.ClearPath();

bezierPath.AddPoint(5,Trailer->traildot,unitptr->Trailer->trailangle);
bezierPath.endAlt(unitptr->X,unitptr->Y,Trailer->traildot,rotdir);

bezierPath.GetDrawingPoints(1, true);
}

aiAnimation* an_timer::one::LoadAnimation(const aiScene* m_pScene)
{
	
	aiAnimation* Animation = new aiAnimation;
	Animation->mName = m_pScene->mAnimations[0]->mName;
	Animation->mNumChannels = m_pScene->mAnimations[0]->mNumChannels;
	Animation->mDuration = m_pScene->mAnimations[0]->mDuration;
	Animation->mTicksPerSecond = m_pScene->mAnimations[0]->mTicksPerSecond;
	Animation->mChannels = new aiNodeAnim *[m_pScene->mAnimations[0]->mNumChannels];
	
	for (unsigned int i = 0; i < m_pScene->mAnimations[0]->mNumChannels; i++)
	{
		Animation->mChannels[i] = new aiNodeAnim;
		Animation->mChannels[i]->mNodeName = m_pScene->mAnimations[0]->mChannels[i]->mNodeName;
		Animation->mChannels[i]->mNumPositionKeys = m_pScene->mAnimations[0]->mChannels[i]->mNumPositionKeys;
		Animation->mChannels[i]->mNumRotationKeys = m_pScene->mAnimations[0]->mChannels[i]->mNumRotationKeys;
		Animation->mChannels[i]->mNumScalingKeys = m_pScene->mAnimations[0]->mChannels[i]->mNumScalingKeys;

		Animation->mChannels[i]->mPositionKeys = new aiVectorKey[Animation->mChannels[i]->mNumPositionKeys];
		Animation->mChannels[i]->mRotationKeys = new aiQuatKey[Animation->mChannels[i]->mNumRotationKeys];
		Animation->mChannels[i]->mScalingKeys = new aiVectorKey[Animation->mChannels[i]->mNumScalingKeys];

		for (unsigned int a = 0; a < Animation->mChannels[i]->mNumPositionKeys; a++)	{
			Animation->mChannels[i]->mPositionKeys[a].mTime = m_pScene->mAnimations[0]->mChannels[i]->mPositionKeys[a].mTime;
			Animation->mChannels[i]->mPositionKeys[a].mValue = m_pScene->mAnimations[0]->mChannels[i]->mPositionKeys[a].mValue;
		}

		for (unsigned int a = 0; a < Animation->mChannels[i]->mNumRotationKeys; a++)	{
			Animation->mChannels[i]->mRotationKeys[a].mTime = m_pScene->mAnimations[0]->mChannels[i]->mRotationKeys[a].mTime;
			Animation->mChannels[i]->mRotationKeys[a].mValue = m_pScene->mAnimations[0]->mChannels[i]->mRotationKeys[a].mValue;
		}

		for (unsigned int a = 0; a < Animation->mChannels[i]->mNumScalingKeys; a++)	{
			Animation->mChannels[i]->mScalingKeys[a].mTime = m_pScene->mAnimations[0]->mChannels[i]->mScalingKeys[a].mTime;
			Animation->mChannels[i]->mScalingKeys[a].mValue = m_pScene->mAnimations[0]->mChannels[i]->mScalingKeys[a].mValue;
		}
	}

	return Animation;
}

aiNode* an_timer::one::LoadNodes(const aiScene* m_pScene)
{
	aiNode* vNode = new aiNode;
	vNode->mName = m_pScene->mRootNode->mName;
	vNode->mParent = NULL;
	vNode->mTransformation = m_pScene->mRootNode->mTransformation; 
	vNode->mNumChildren = m_pScene->mRootNode->mNumChildren;
	vNode->mChildren = new aiNode *[vNode->mNumChildren];
	for (unsigned int i = 0; i<vNode->mNumChildren; i++)
	vNode->mChildren[i]=LoadNode(m_pScene->mRootNode->mChildren[i], vNode);
	return vNode;
}

aiNode* an_timer::one::LoadNode(aiNode* pNode, aiNode* aNode)
{
	aiNode* vNode = new aiNode;
	vNode->mName = pNode->mName; 
	vNode->mTransformation = pNode->mTransformation; 
	vNode->mParent = aNode;
	vNode->mNumChildren = pNode->mNumChildren;
	vNode->mChildren = new aiNode *[vNode->mNumChildren];
	for (unsigned int i = 0; i < vNode->mNumChildren; i++)
	vNode->mChildren[i]=LoadNode(pNode->mChildren[i], vNode);
	return vNode;
}

void an_timer::one::AddAnimation(const std::string& Filename)
{
	
	Assimp::Importer m_Importer;
	const aiScene* pScene;
	pScene = m_Importer.ReadFile(Filename.c_str(),
		aiProcess_Triangulate | aiProcess_JoinIdenticalVertices);
	

	Add(pScene);

	//delete pScene;
	//delete m_Importer;

	

}


Vector2f CursorPositionEx()
{
	POINT mousePos;
	Vector2f p;
	GetCursorPos(&mousePos);
	p.x=(float)mousePos.x;
	p.y=(float)mousePos.y;
	return ScreenToOxy((int)p.x, (int)p.y);
	

}



void DrawSpline()
{

glEnable(GL_LINE_SMOOTH);
glEnable(GL_BLEND);
glBlendFunc(GL_DST_COLOR,GL_ONE_MINUS_DST_COLOR);
glHint(GL_LINE_SMOOTH_HINT, GL_NICEST);
glLineWidth(3);


Vector2f q0,q1;

glBegin(GL_LINES);



q0 = bezierPath.drawingPoints[0];
 
for (int i = 1; i <= bezierPath.drawingPoints.size()-1; i++)
{
	Vector2f Dist= q1-q0;
	q1 = bezierPath.drawingPoints[i];
    glVertex3d(q0.x, q0.y, -Dist.length()*100);
	glVertex3d(q1.x, q1.y, -Dist.length()*100);
    q0 = q1;
}

glEnd();

glLineWidth(0.1);
glDisable(GL_LINE_SMOOTH);
glDisable(GL_BLEND);
}

Vector2f BezierPath::CalculateBezierPoint(float t, Vector2f p0, Vector2f p1, Vector2f p2, Vector2f p3)
{
    float u = 1 - t;
    float tt = t * t;
    float uu = u * u;
    float uuu = uu * u;
    float ttt = tt * t;
 
    Vector2f p = p0 * uuu;    //first term
    p += (p1 * (3 * uu * t));    //second term
    p += (p2 * (3 * u * tt));    //third term
    p += (p3 * ttt);           //fourth term
 
    return p;
}

Vector2f BezierPath::CalculateBezierPoint(float t, Vector2f p0, Vector2f p1)
{
    
	Vector2f p = p1*t;
	p += p0*(1-t);

    return p;
}

float BezierPath::CalculateRotation(float t, float p0, float p1)
{
    
	float p = p1*t;
	p += p0*(1-t);
	//p=fmod(abs(p),360);
	

    return p;
}

void BezierPath::GetDrawingPoints(int setSpeed, bool amplitudeOn)
{
	
	drawingPoints.clear();
	Vector2f end;
	float startRot;
	float endRot;
	float t;
	float f;
	float a;
	int timeCh;


	for(int i = 0; i < splinePoint.size(); i++) 
	{
		if (i<splinePoint.size()-1)
		{
			end = splinePoint[i+1].startPoint;  
			endRot = splinePoint[i+1].rotation;
		}
		else
		{
			end = endPoint;
			endRot = endRotation;
		}
		startRot = splinePoint[i].rotation;

		if (abs(startRot-endRot)>180)
		{
			if (startRot>endRot)
				endRot+=360;
			else
				startRot+=360;
		}

		splinePoint[i].length=sqrt((splinePoint[i].startPoint.x-end.x)*(splinePoint[i].startPoint.x-end.x)+(splinePoint[i].startPoint.y-end.y)*(splinePoint[i].startPoint.y-end.y));
		t=splinePoint[i].length*0.01;
		t_step=((1/t)/(100*setSpeed+50*(setSpeed-1)))*mainSpeed;
		t_step2=((1/t)/(140*setSpeed+100*(setSpeed-1)))*mainSpeed;
		if (amplitudeOn==false && splinePoint[i].type!=0)
		if (splinePoint[i].type==2)
		splinePoint[i].type=6;
		else
		splinePoint[i].type=1;
		
		switch (splinePoint[i].type)
		{
		case 7:
			for (f = 0; f < 1; f=f+t_step)
			{
				drawingPoints.push_back(CalculateBezierPoint(f, splinePoint[i].startPoint, splinePoint[i].startEx, splinePoint[i].endEx, end));
				rotation.push_back(CalculateRotation(f,startRot,endRot));
			}
			break;
		case 1:
			for (f = 0; f < 1; f=f+t_step)
			{
				drawingPoints.push_back(CalculateBezierPoint(f, splinePoint[i].startPoint, end));
				rotation.push_back(CalculateRotation(f,startRot,endRot));
			}
			break;
		case 2:
			a=t_step*t_step/2.0f;
			f=0;
			timeCh=0;
			while (f<1)
			{
				drawingPoints.push_back(CalculateBezierPoint(f, splinePoint[i].startPoint, end));
				rotation.push_back(CalculateRotation(f,startRot,endRot));
				f+=a*timeCh;
				timeCh++;
			}
			break;
		case 3:
			f=0;
			a=t_step*t_step/2.0f;
			timeCh=0;
			while (f<1)
			{
				drawingPoints.push_back(CalculateBezierPoint(f, splinePoint[i].startPoint, end));
				rotation.push_back(CalculateRotation(f, startRot, endRot));
				f+=t_step-a*timeCh;			
				timeCh++;
			}
			break;
		case 4:
			f=0;
			a=t_step2*t_step2/2.0f;
			timeCh=0;
			while (f<1)
			{
				drawingPoints.push_back(CalculateBezierPoint(f, splinePoint[i].startPoint, end));
				rotation.push_back(CalculateRotation(f, startRot, endRot));
				f+=t_step2-a*timeCh;
				timeCh++;
			}
			break;
		case 5:
			a=t_step2*t_step2/2.0f;
			f=0;
			timeCh=0;
			while (f<1)
			{
				drawingPoints.push_back(CalculateBezierPoint(f, splinePoint[i].startPoint, end));
				rotation.push_back(CalculateRotation(f,startRot,endRot));
				f+=a*timeCh;
				timeCh++;
			}
			break;
		case 6:
			a=0;
			f=0;
			
			while (f<1)
			{
				drawingPoints.push_back(CalculateBezierPoint(f, splinePoint[i].startPoint, end));
				rotation.push_back(CalculateRotation(f,startRot,endRot));
				if (a<t_step)
				a+=0.0015;
				else
				a=t_step;
				f+=a;
				
				
				
			}
			break;
			case 0:
			a=(t_step*0.2*t_step*0.2-t_step*t_step)/2.0f;
			f=0;
			timeCh=0;
			while (f<=1)
			{
				drawingPoints.push_back(CalculateBezierPoint(f, splinePoint[i].startPoint, splinePoint[i].startEx, splinePoint[i].endEx, end));
				rotation.push_back(CalculateRotation(f,startRot,endRot));
				f+=t_step+a*timeCh;
				if (f>=0.5 && timeCh!=0)
				timeCh--;
				else
				timeCh++;
				//draw_GUI();
			}
			break;
			case 8:
			a=(t_step*t_step)/2.0f;
			f=0;
			timeCh=0;
			while (f<=1)
			{
				drawingPoints.push_back(CalculateBezierPoint(f, splinePoint[i].startPoint, splinePoint[i].startEx, splinePoint[i].endEx, end));
				rotation.push_back(CalculateRotation(f,startRot,endRot));
				f+=a*timeCh;
				if (f>=0.5 && timeCh!=0)
				timeCh--;
				else
				timeCh++;
			}
			break;
			/*
			case 8:
			a=(t_step*t_step)/2.0f;
			f=0;
			timeCh=0;
			while (f<=1)
			{
				drawingPoints.push_back(CalculateBezierPoint(f, splinePoint[i].startPoint, splinePoint[i].startEx, splinePoint[i].endEx, end));
				rotation.push_back(CalculateRotation(f,startRot,endRot));
				f+=a*timeCh;
				if (f>=0.5 && timeCh!=0)
				timeCh--;
				else
				timeCh++;
			}
			break;
			*/
			case 9:
			a=(t_step*t_step)/2.0f;
			f=0;
			timeCh=0;
			while (f<1)
			{
				drawingPoints.push_back(CalculateBezierPoint(f, splinePoint[i].startPoint, splinePoint[i].startEx, splinePoint[i].endEx, end));
				rotation.push_back(CalculateRotation(f,startRot,endRot));
				if (a*timeCh<t_step)
				{
				f+=a*timeCh;
				timeCh++;
				}
				else
				f+=t_step;
			}
			break;
			case 10:
			a=(t_step*t_step)/2.0f;
			f=0;
			timeCh=0;
			while (f<=1)
			{
				drawingPoints.push_back(CalculateBezierPoint(f, splinePoint[i].startPoint, end));
				rotation.push_back(CalculateRotation(f,startRot,endRot));
				f+=a*timeCh;
				if (f>=0.5 && timeCh!=0)
				timeCh--;
				else
				timeCh++;
			}
			break;
			/*
		default:
			for (float f = 0; f < 1; f=f+t_step)
			{
				drawingPoints.push_back(CalculateBezierPoint(f, splinePoint[i].startPoint, end));
				rotation.push_back(CalculateRotation(f,startRot,endRot));
			}
			break;
			*/
		}
	}

}
	
	



void BezierPath::AddPoint(int _type, Vector2f _startPoint, Vector2f _startEx, Vector2f _endEx, float _rotation)
{
	splinePoint.push_back(_splinePoint(_type, _startPoint, _startEx, _endEx, _rotation));
}

void BezierPath::AddPoint(int _type, Vector2f _startPoint, float _rotation)
{
	splinePoint.push_back(_splinePoint(_type, _startPoint, _rotation));
}

void BezierPath::start(int x, int y, int rot)
{
AddPoint(2, 
			MainToOxy(x, y),
			(float)rot*60);
}

void BezierPath::r1(int x, int y)
{
AddPoint(0, 
			MainToOxy(x, y, -37.5f, 21.875f),
			MainToOxy(x, y, -9.526f, 5.5f),
			MainToOxy(x, y, 9.526f, 5.5f),
			240.0f);
}

void BezierPath::r2(int x, int y)
{
AddPoint(0, 
			MainToOxy(x, y, 37.5f, 21.875f),
			MainToOxy(x, y, 9.526f, 5.5f),
			MainToOxy(x, y, -9.526f, 5.5f),
			120.0f);
}

void BezierPath::r3(int x, int y)
{
AddPoint(0, 
			MainToOxy(x, y, 0, +43.75f),
			MainToOxy(x, y, 0, +11.0f),
			MainToOxy(x, y, 9.526f, -5.5f),
			180.0f);
}

void BezierPath::r4(int x, int y)
{
AddPoint(0, 
			MainToOxy(x, y, 37.5f, -21.875f),
			MainToOxy(x, y, 9.526f, -5.5f),
			MainToOxy(x, y, 0, +11.0f),
			60.0f);
}


void BezierPath::r5(int x, int y)
{
AddPoint(0, 
			MainToOxy(x, y, 37.5f, 21.875f),
			MainToOxy(x, y, 9.526f, 5.5f),
			MainToOxy(x, y, 0, -11.0f),
			120.0f);	

}

void BezierPath::r6(int x, int y)
{
AddPoint(0, 
			MainToOxy(x, y, 0, -43.75f),
			MainToOxy(x, y, 0, -11.0f),
			MainToOxy(x, y, 9.526f, 5.5f),
			0.0f);	

}

void BezierPath::r7(int x, int y)
{
AddPoint(0, 
			MainToOxy(x, y, 37.5f, -21.875f),
			MainToOxy(x, y, 9.526f, -5.5f),
			MainToOxy(x, y, -9.526f, -5.5f),
			60.0f);
}

void BezierPath::r8(int x, int y)
{
AddPoint(0, 
			MainToOxy(x, y, -37.5f, -21.875f),
			MainToOxy(x, y, -9.526f, -5.5f),
			MainToOxy(x, y, 9.526f, -5.5f),
			300.0f);
}

void BezierPath::r9(int x, int y)
{
AddPoint(0, 
			MainToOxy(x, y, 0, -43.75f),
			MainToOxy(x, y, 0, -11.0f),
			MainToOxy(x, y, -9.526f, 5.5f),
			0.0f);	

}

void BezierPath::r10(int x, int y)
{
AddPoint(0, 
			MainToOxy(x, y, -37.5f, 21.875f),
			MainToOxy(x, y, -9.526f, 5.5f),
			MainToOxy(x, y, 0, -11.0f),
			240.0f);	

}

void BezierPath::r11(int x, int y)
{
AddPoint(0, 
			MainToOxy(x, y, -37.5f, -21.875f),
			MainToOxy(x, y, -9.526f, -5.5f),
			MainToOxy(x, y, 0, +11.0f),
			
			300.0f);
}

void BezierPath::r12(int x, int y)
{
AddPoint(0, 
			MainToOxy(x, y, 0, +43.75f),
			MainToOxy(x, y, 0, +11.0f),
			MainToOxy(x, y, -9.526f, -5.5f),
			180.0f);
}

void BezierPath::end(int x, int y, int rot)
{
float _rot=(float)rot*60;
if (rot==0)
AddPoint(3,	MainToOxy(x, y, 0, -43.75f),_rot);
if (rot==1)
AddPoint(3,	MainToOxy(x, y, 37.5f, -21.875f),_rot);
if (rot==2)
AddPoint(3, MainToOxy(x, y, 37.5f, 21.875f),_rot);
if (rot==3)
AddPoint(3, MainToOxy(x, y, 0, 43.75f),	_rot);
if (rot==4)
AddPoint(3,	MainToOxy(x, y, -37.5f, 21.875f),_rot);
if (rot==5)
AddPoint(3,	MainToOxy(x, y, -37.5f, -21.875f),_rot);


endPoint=Vector2f(MainToOxy(x, y));
bezierPath.endRotation=(float)rot*60;
}

void BezierPath::endAlt(int x, int y, Vector2f startPos, int rot)
{
Vector2f p = MainToOxy(x, y);
Vector2f fin;
fin.x=startPos.x-(startPos.x-p.x)/2;
fin.y=startPos.y-(startPos.y-p.y)/2;
float _rot=(float)rot*60;

AddPoint(4,	fin,_rot);


endPoint=Vector2f(MainToOxy(x, y));
bezierPath.endRotation=(float)rot*60;
}

void BezierPath::Loop()
{
	if (unitptr->Current->timer.animation_index==3)
	{
		if (ch==drawingPoints.size()-(int)(10.0f*mainSpeed))
		{
			if (unitptr->form==1) 
			{
				unitptr->Current->timer.crossFlag=true;
				unitptr->Current->timer.crossIndex=0;
			}
		}
	}
	if (ch>=drawingPoints.size()-2)
	{
			if (wheelController.on==true)
				wheelController.Stop();
			
			if (move_anim2.anim_move_trailer==true)
			{
				if (unitptr->type==2)
				{
					move_anim2.anim_move_state=true;
					move_anim2.timer=(int)(70.0f/mainSpeed);
					unitptr->Trailer->Transformation();
				}
				else
				{
					move_anim2.timer=0;
					move_anim2.anim_move_flag=false;
					move_anim2.anim_move_trailer=false;
					unitptr->Transformation();
				}
			}
			
			if (move_anim2.rotate_car==true)
			{
					move_anim2.anim_move_state=true;
					move_anim2.timer=(int)(5.0f/mainSpeed);
			}
			if (move_anim2.anim_move_trailer==false && move_anim2.rotate_car==false)
				move_anim2.anim_move_flag=false;
		
	}
	else
	{
	if(unitptr->form==0)
	ch+=2;
	else
	ch++;
	}
}

void BezierPath::l1(int x, int y)
{
	AddPoint(1, MainToOxy(x, y, 0, 43.75f),180.0f);
}

void BezierPath::l2(int x, int y)
{
	AddPoint(1, MainToOxy(x, y, 0, -43.75f),0.0f);
}

void BezierPath::l3(int x, int y)
{
	AddPoint(1, MainToOxy(x, y, -37.5f, 21.875f),240.0f);
}

void BezierPath::l4(int x, int y)
{
	AddPoint(1, MainToOxy(x, y, 37.5f, -21.875f),60.0f);
}

void BezierPath::l5(int x, int y)
{
	AddPoint(1, MainToOxy(x, y, 37.5f, 21.875f),120.0f);
}

void BezierPath::l6(int x, int y)
{
	AddPoint(1, MainToOxy(x, y, -37.5f, -21.875f),300.0f);
}

void BezierPath::ClearPath()
{
	ch=0;
	splinePoint.clear();
	drawingPoints.clear();
	rotation.clear();
}



void move_anim2::rotate_around()
{
float slow=(0.011115/3)*mainSpeed;
float middle=(0.01667/3)*mainSpeed;
float fast=(0.033335/3)*mainSpeed;
float rot=(2.0f/3)*mainSpeed;

if (unitptr->form==1 && t>0.7 && rotate_cross==0)
{
rotate_cross=1;
unitptr->Current->timer.crossFlag=true;
unitptr->Current->timer.crossIndex=2;
}


	
	switch(move_mark.LineStr) {
case 0:
	{
		switch (oldRot) {
case 0: {
	t+=slow;
	rotate+=rot;
	break;
		}
case 1: {
	t+=middle;
	rotate+=rot;
	break;
		}
case 2: {
	t+=fast;
	rotate+=rot;
	break;
		}
case 3: {
	t=1;
	break;
		}
case 4: {
	t+=fast;
	rotate-=rot;
	break;
		}
case 5: {
	t+=middle;
	rotate-=rot;
	break;
		}
		}
		break;
	}
case 1: {
	switch (oldRot) {
case 1: {
	t+=slow;
	rotate+=rot;
	break;
		}
case 2: {
	t+=middle;
	rotate+=rot;
	break;
		}
case 3: {
	t+=fast;
	rotate+=rot;
	break;
		}
case 4: {
	t=1;
	break;
		}
case 5: {
	t+=fast;
	rotate-=rot;
	break;
		}
case 0: {
	t+=middle;
	rotate-=rot;
	break;
		}
	}
	break;
		}
case 2: {
	switch (oldRot) {
case 2: {
	t+=slow;
	rotate+=rot;
	break;
		}
case 3: {
	t+=middle;
	rotate+=rot;
	break;
		}
case 4: {
	t+=fast;
	rotate+=rot;
	break;
		}
case 5: {
	t=1;
	break;
		}
case 0: {
	t+=fast;
	rotate-=rot;
	break;
		}
case 1: {
	t+=middle;
	rotate-=rot;
	break;
		}
	}
	break;
		}
case 3: {
	switch (oldRot) {
case 3: {
	t+=slow;
	rotate+=rot;
	break;
		}
case 4: {
	t+=middle;
	rotate+=rot;
	break;
		}
case 5: {
	t+=fast;
	rotate+=rot;
	break;
		}
case 0: {
	t=1;
	break;
		}
case 1: {
	t+=fast;
	rotate-=rot;
	break;
		}
case 2: {
	t+=middle;
	rotate-=rot;
	break;
		}
	}
	break;
		}
case 4: {
	switch (oldRot) {
case 4: {
	t+=slow;
	rotate+=rot;
	break;
		}
case 5: {
	t+=middle;
	rotate+=rot;
	break;
		}
case 0: {
	t+=fast;
	rotate+=rot;
	break;
		}
case 1: {
	t=1;
	break;
		}
case 2: {
	t+=fast;
	rotate-=rot;
	break;
		}
case 3: {
	t+=middle;
	rotate-=rot;
	break;
		}
	}
	break;
		}
case 5: {
	switch (oldRot) {
case 5: {
	t+=slow;
	rotate+=rot;
	break;
		}
case 0: {
	t+=middle;
	rotate+=rot;
	break;
		}
case 1: {
	t+=fast;
	rotate+=rot;
	break;
		}
case 2: {
	t=1;
	break;
		}
case 3: {
	t+=fast;
	rotate-=rot;
	break;
		}
case 4: {
	t+=middle;
	rotate-=rot;
	break;
		}
	}
	break;
		}
	}
	if (t>=1) {
		rotate_cross=0;
		t=0;
		anim_move_rotate=0;
		if (unitptr->Current==unitptr->Alternative)
		{
		unitptr->Current->timer.One[unitptr->Current->timer.animation_index].on=false;
		unitptr->Current->timer.animation_index=1;
		unitptr->Current->timer.One[unitptr->Current->timer.animation_index].Start();
		wheelController.Start(1);
		}
		if (unitptr->Current==unitptr->Main)
		{
		unitptr->Current->timer.crossFlag=true;
		unitptr->Current->timer.crossIndex=1;
		}
		
	}

}


move_anim2::move_anim2()
{
t=0;
}


void GetFrameTime()
{
RunningTime = (float)((double)GetCurrentTimeMillis() - (double)m_startTime) / (1000.0f/mainSpeed);
g_FrameInterval = RunningTime - LastTime;
LastTime = RunningTime;
++framesPerSecond;

	if (RunningTime-FPSTime > 1.0f)
	{     
		FPSTime=RunningTime;				
		FPS=framesPerSecond;
		framesPerSecond = 0;
	}

	
}

void DrawBox(float tX, float tY, float tZ, float dirrot, Vector2f Width, Vector2f Height)
{
	glPushMatrix();
	glTranslatef(tX,tY,tZ);
	glRotatef(dirrot,0,0,1);
	Vector2f p=MainToOxy(prime.X, prime.Y);
	glColor3f(0.4f, 0.5f, 0.6f);
	glBegin(GL_LINES);
	
	
	glVertex3f(Width.x, Height.x, 0.0);
	glVertex3f(Width.x, Height.y, 0.0);
	glVertex3f(Width.y, Height.y, 0.0);
	glVertex3f(Width.y, Height.x, 0.0);

	glVertex3f(Width.y, Height.x, 0.0);
	glVertex3f(Width.y, Height.x, -40.0);
	glVertex3f(Width.y, Height.y, -40.0);
	glVertex3f(Width.y, Height.y, 0.0);

	glVertex3f(Width.y, Height.y, 0.0);
	glVertex3f(Width.y, Height.y, -40.0);
	glVertex3f(Width.x, Height.y, -40.0);
	glVertex3f(Width.x, Height.y, 0.0);

	glVertex3f(Width.x, Height.y, 0.0);
	glVertex3f(Width.x, Height.y, -40.0);
	glVertex3f(Width.x, Height.x, -40.0);
	glVertex3f(Width.x, Height.x, 0.0);
	
	glVertex3f(Width.x, Height.x, 0.0);
	glVertex3f(Width.y, Height.x, 0.0);
	glVertex3f(Width.y, Height.x, -40.0);
	glVertex3f(Width.x, Height.x, -40.0);

	glVertex3f(Width.x, Height.x, -40.0);
	glVertex3f(Width.x, Height.y, -40.0);
	glVertex3f(Width.y, Height.y, -40.0);
	glVertex3f(Width.y, Height.x, -40.0);

	
	glEnd();
	glPopMatrix();
}
//X = x0 + (x - x0) * cos(a) - (y - y0) * sin(a); Y = y0 + (y - y0) * cos(a) + (x - x0) * sin(a);
void BoxGetZ(unit* unit, Vector2f pos, float rotate)
{

Vector2f left_top=GetLineZ(unit->widthBox.x,unit->heightBox.x,pos,rotate);
Vector2f right_top=GetLineZ(unit->widthBox.y,unit->heightBox.x,pos,rotate);
Vector2f right_bottom=GetLineZ(unit->widthBox.y,unit->heightBox.y,pos,rotate);
Vector2f left_bottom=GetLineZ(unit->widthBox.x,unit->heightBox.y,pos,rotate);

float _left_top=GetZ(left_top);
float _right_top=GetZ(right_top);
float _right_bottom=GetZ(right_bottom);
float _left_bottom=GetZ(left_bottom);

float left_suspension;
float right_suspension;
float top_suspension;
float bottom_suspension;

float left_angle = GetAngle (_left_top, _left_bottom, unit->heightBox.x, unit->heightBox.y, left_suspension, 90);
float right_angle = GetAngle (_right_top, _right_bottom, unit->heightBox.x, unit->heightBox.y, right_suspension, 90);
float top_angle = GetAngle (_left_top, _right_top, unit->widthBox.y, unit->widthBox.x, top_suspension, 90);
float bottom_angle = GetAngle (_left_bottom, _right_bottom, unit->widthBox.y, unit->widthBox.x, bottom_suspension, 90);


unit->forward_angles = (right_angle+left_angle)/2;
unit->side_angles = (top_angle+bottom_angle)/2;
float suspension=(abs(left_suspension)+abs(right_suspension)+abs(top_suspension)+abs(bottom_suspension))/4;
float z=(_left_top+_right_top+_right_bottom+_left_bottom)/4;
unit->z_up=z-suspension;

}

void DrawLine(float tX, float tY)
{
	glPushMatrix();
	glTranslatef(tX,tY,0);
	glColor3f(0.4f, 0.5f, 0.6f);
	glBegin(GL_LINES);		
	glVertex3f(0, 0, -40.0);
	glVertex3f(0, 0, 40.0);	
	glEnd();
	glPopMatrix();
}

Vector2f GetLineZ(float x, float y, Vector2f pos, float rotate)
{
Vector2f Temp;
Temp.x = x * cos(rotate*3.1415926/180) - y * sin(rotate*3.1415926/180) + pos.x;
Temp.y = y * cos(rotate*3.1415926/180) + x * sin(rotate*3.1415926/180) + pos.y;
//DrawLine(Temp.x,Temp.y);
return Temp;
}

float GetAngle (float x, float y, float x2, float y2, float &b, int must)
{
float decimator=1;
b = x-y;
float a = x2-y2;
float c = sqrt(b*b+a*a);

b=b/2;

if (y<x)
decimator=-1;
else
decimator=1;

float final=asin(a/c)*180/3.1415926-must;
return final*decimator;
}



void WheelController::Start(int _forward)
{
on = true;
forward=_forward;
half = true;
WheelEngine=CreateFromYawPitchRoll(WheelMove);
}

void WheelController::Loop()
{
if (bezierPath.ch>=bezierPath.drawingPoints.size()-(int)(61.0f*mainSpeed))
half = false;
if (bezierPath.ch>=bezierPath.drawingPoints.size()-(int)(61.0f*mainSpeed))
WheelSpeed-=(0.012)*forward;
else
{
if (WheelSpeed<0.4)
WheelSpeed+=(0.008)*forward;
}

if (half == false)
{
half = true;
//unitptr->Current->timer.One[unitptr->Current->timer.animation_index].on=false;
unitptr->Current->timer.animation_index=2;
unitptr->Current->timer.One[unitptr->Current->timer.animation_index].Start();
}
WheelMove.y-=WheelSpeed;
//aiVector3t<float> Vector2(0.0f,0.0f,4.0f);

WheelEngine=CreateFromYawPitchRoll(WheelMove);
//WheelEngine=WheelEngine.Rotate( Vector2);
}

void WheelController::Stop()
{
on = false;
WheelSpeed=0;
WheelEngine=CreateFromYawPitchRoll(WheelMove);
}


void setupCubeMap(GLuint &texture) {
    glActiveTextureARB(GL_TEXTURE0);
    glEnable(GL_TEXTURE_CUBE_MAP);
    glGenTextures(1, &texture);
    glBindTexture(GL_TEXTURE_CUBE_MAP, texture);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR); 
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
}
 
void setupCubeMap(GLuint &texture, Texture *xpos, Texture *xneg, Texture *ypos, Texture *yneg, Texture *zpos, Texture *zneg) {
    setupCubeMap(texture);
    glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X, 0, GL_RGBA, xpos->width, xpos->height, 0, xpos->bpp == 4 ? GL_RGBA : GL_RGB, GL_UNSIGNED_BYTE, xpos->imageData);
    glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_X, 0, GL_RGBA, xneg->width, xneg->height, 0, xneg->bpp == 4 ? GL_RGBA : GL_RGB, GL_UNSIGNED_BYTE, xneg->imageData);
    glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Y, 0, GL_RGBA, ypos->width, ypos->height, 0, ypos->bpp == 4 ? GL_RGBA : GL_RGB, GL_UNSIGNED_BYTE, ypos->imageData);
    glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Y, 0, GL_RGBA, yneg->width, yneg->height, 0, yneg->bpp == 4 ? GL_RGBA : GL_RGB, GL_UNSIGNED_BYTE, yneg->imageData);
    glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Z, 0, GL_RGBA, zpos->width, zpos->height, 0, zpos->bpp == 4 ? GL_RGBA : GL_RGB, GL_UNSIGNED_BYTE, zpos->imageData);
    glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Z, 0, GL_RGBA, zneg->width, zneg->height, 0, zneg->bpp == 4 ? GL_RGBA : GL_RGB, GL_UNSIGNED_BYTE, zneg->imageData);
	//glTexParameteri ( GL_TEXTURE_CUBE_MAP_ARB, GL_TEXTURE_WRAP_S, GL_CLAMP );
	//glTexParameteri ( GL_TEXTURE_CUBE_MAP_ARB, GL_TEXTURE_WRAP_T, GL_CLAMP );
	//glEnable  ( GL_TEXTURE_GEN_S );
	//glEnable  ( GL_TEXTURE_GEN_T );
	//glEnable  ( GL_TEXTURE_GEN_R );
	glDisable(GL_TEXTURE_CUBE_MAP);
	glEnable(GL_TEXTURE_2D);
}
 
void deleteCubeMap(GLuint &texture) {
    glDeleteTextures(1, &texture);
}

void CreatePyramid()
{

glPushMatrix();
glTranslatef(1000.0f,0.0f,0.0f);
glRotatef(rotcub,0.0f,0.0f,1.0f);

//glScalef(20.0f,20.0f,20.0f);


	glBegin(GL_TRIANGLES);


  glNormal3f( 0.0f, 0.0f, -1.0f);     

  glTexCoord2f(0.0f, 0.0f); glVertex3f(-100.0f, -100.0f,  100.0f); 

  glTexCoord2f(100.0f, 0.0f); glVertex3f( 100.0f, -100.0f,  100.0f); 

  glTexCoord2f(100.0f, 100.0f); glVertex3f( 100.0f,  100.0f,  100.0f); 

  //glNormal3f( 0.0f, 0.0f, 1.0f);  
  

  glTexCoord2f(100.0f, 100.0f); glVertex3f( 100.0f,  100.0f,  100.0f); 

  glTexCoord2f(0.0f, 100.0f); glVertex3f(-100.0f,  100.0f,  100.0f);

  glTexCoord2f(0.0f, 0.0f); glVertex3f(-100.0f, -100.0f,  100.0f); 

  glEnd();

  

  glBegin(GL_QUADS); 

  glNormal3f( 0.0f, 0.0f,1.0f);     

  glTexCoord2f(1.0f, 0.0f); glVertex3f(-100.0f, -100.0f, -100.0f); 

  glTexCoord2f(1.0f, 1.0f); glVertex3f(-100.0f,  100.0f, -100.0f); 

  glTexCoord2f(0.0f, 1.0f); glVertex3f( 100.0f,  100.0f, -100.0f); 

  glTexCoord2f(0.0f, 0.0f); glVertex3f( 100.0f, -100.0f, -100.0f); 

  

  glNormal3f( 0.0f, -1.0f, 000.0f);     

  glTexCoord2f(0.0f, 1.0f); glVertex3f(-100.0f,  100.0f, -100.0f); 

  glTexCoord2f(0.0f, 0.0f); glVertex3f(-100.0f,  100.0f,  100.0f); 

  glTexCoord2f(1.0f, 0.0f); glVertex3f( 100.0f,  100.0f,  100.0f); 

  glTexCoord2f(1.0f, 1.0f); glVertex3f( 100.0f,  100.0f, -100.0f); 


  glNormal3f( 0.0f,1.0f, 0.0f);     

  glTexCoord2f(1.0f, 1.0f); glVertex3f(-100.0f, -100.0f, -100.0f); // ����� 1 (���)

  glTexCoord2f(0.0f, 1.0f); glVertex3f( 100.0f, -100.0f, -100.0f); // ����� 2 (���)

  glTexCoord2f(0.0f, 0.0f); glVertex3f( 100.0f, -100.0f,  100.0f); // ����� 3 (���)

  glTexCoord2f(1.0f, 0.0f); glVertex3f(-100.0f, -100.0f,  100.0f); // ����� 4 (���)

  // ������ �����

  glNormal3f( -1.0f, 0.0f, 0.0f);   

  glTexCoord2f(1.0f, 0.0f); glVertex3f( 100.0f, -100.0f, -100.0f); // ����� 1 (�����)

  glTexCoord2f(1.0f, 1.0f); glVertex3f( 100.0f,  100.0f, -100.0f); // ����� 2 (�����)

  glTexCoord2f(0.0f, 1.0f); glVertex3f( 100.0f,  100.0f,  100.0f); // ����� 3 (�����)

  glTexCoord2f(0.0f, 0.0f); glVertex3f( 100.0f, -100.0f,  100.0f); // ����� 4 (�����)

  // ����� �����

  glNormal3f(1.0f, 0.0f, 0.0f);    

  glTexCoord2f(0.0f, 0.0f); glVertex3f(-100.0f, -100.0f, -100.0f); 

  glTexCoord2f(1.0f, 0.0f); glVertex3f(-100.0f, -100.0f,  100.0f); 

  glTexCoord2f(1.0f, 1.0f); glVertex3f(-100.0f,  100.0f,  100.0f); 

  glTexCoord2f(0.0f, 1.0f); glVertex3f(-100.0f,  100.0f, -100.0f);

 glEnd();

glPopMatrix();

   
}


void CreatePlane()
{

glPushMatrix();
glTranslatef(1000.0f,0.0f,0.0f);
glRotatef(rotcub,0.0f,0.0f,1.0f);

//glScalef(20.0f,20.0f,20.0f);


	glBegin(GL_QUADS);
   

  glNormal3f( 0.0f, 0.0f, 1.0f);     

  glTexCoord2f(0.0f, 0.0f); glVertex3f(-100.0f, -100.0f,  10.0f); 

  glTexCoord2f(100.0f, 0.0f); glVertex3f( 100.0f, -100.0f,  10.0f); 

  glTexCoord2f(100.0f, 100.0f); glVertex3f( 100.0f,  100.0f,  10.0f); 

  glTexCoord2f(0.0f, 100.0f); glVertex3f( -100.0f,  100.0f,  10.0f); 

  glNormal3f( 0.0f, 0.0f, 1.0f);     

  glTexCoord2f(0.0f, 0.0f); glVertex3f(000.0f, 000.0f,  10.0f);

  glTexCoord2f(100.0f, 0.0f); glVertex3f( 100.0f, 000.0f,  10.0f);

  glTexCoord2f(100.0f, 100.0f); glVertex3f( 100.0f,  200.0f,  10.0f); 

  glTexCoord2f(0.0f, 100.0f); glVertex3f( 000.0f,  200.0f,  10.0f); 

  

 glEnd();

glPopMatrix();

   
}


void unit::CarMoveBack()
{
	move_anim2._side=move_anim2.Side(move_anim2.oldRot);
	if (move_anim2._side<3)
	wheelController.Start(-1);
	else
	wheelController.Start(1);
	move_anim2.t=0;
	move_anim2.anim_move_flag=true;
	bezierPath.ClearPath();
	Vector2f StartOne;
	Vector2f ExOne;
	Vector2f ExTwo;
	Vector2f EndOne;
	Vector2f ExOne2;
	Vector2f ExTwo2;
	float DirectOne;
	float DirectTwo;
	switch (rotdir)
	{
	case 2:
		if (move_anim2._side<3)
		{
		StartOne=MainToOxy(move_anim2.start.x, move_anim2.start.y, 21.875f, 12.5f);
		if (move_anim2._side==1)	{
			ExOne=MainToOxy(move_anim2.start.x, move_anim2.start.y, 6.75f, +6.75f);
			ExTwo=MainToOxy(move_anim2.start.x, move_anim2.start.y, 0, -21.875f);
			} else {
			ExOne=MainToOxy(move_anim2.start.x, move_anim2.start.y, 0.0f, 0.0f);
			ExTwo=MainToOxy(move_anim2.start.x, move_anim2.start.y, -19.0f, 11.0f);
			}
		}
		else
		{
		if (move_anim2._side==4)	{
			ExOne=MainToOxy(move_anim2.start.x, move_anim2.start.y, -11.0f, -6.0f);
			ExTwo=MainToOxy(move_anim2.start.x, move_anim2.start.y, -18.0f, -19.0f);
			EndOne=MainToOxy(move_anim2.start.x, move_anim2.start.y, -18.0f, -31.0f);
			ExOne2=MainToOxy(move_anim2.start.x, move_anim2.start.y, -18.0f, -19.0f);
			ExTwo2=MainToOxy(move_anim2.start.x, move_anim2.start.y, -12.0f, 7.0f);
			StartOne=MainToOxy(move_anim2.start.x, move_anim2.start.y, -29.0f, 17.0f);
			DirectOne=rotdir*60+50;
			DirectTwo=240;
			}
		else
		if (move_anim2._side==3)
		{
			ExOne=MainToOxy(move_anim2.start.x, move_anim2.start.y, -9.0f, -6.0f);
			ExTwo=MainToOxy(move_anim2.start.x, move_anim2.start.y, -23.0f, -7.4f);
			EndOne=MainToOxy(move_anim2.start.x, move_anim2.start.y, -28.0f, -3.0f);
			ExOne2=MainToOxy(move_anim2.start.x, move_anim2.start.y, -23.0f, -7.4f);
			ExTwo2=MainToOxy(move_anim2.start.x, move_anim2.start.y, 0.0f, -10.0f);
			StartOne=MainToOxy(move_anim2.start.x, move_anim2.start.y, 0.0f, -36.0f);
			DirectOne=rotdir*60-50;
			DirectTwo=0;
		}
		else
		{
			StartOne=MainToOxy(move_anim2.start.x, move_anim2.start.y, -28.0f, -17.0f);
			ExOne=MainToOxy(move_anim2.start.x, move_anim2.start.y, -19.0f, -9.0f);
			ExTwo=MainToOxy(move_anim2.start.x, move_anim2.start.y, 0.0f, 0.0f);
			EndOne=MainToOxy(move_anim2.start.x, move_anim2.start.y, -19.0f, 33.0f);
			ExOne2=MainToOxy(move_anim2.start.x, move_anim2.start.y, 0.0f, 0.0f);
			ExTwo2=MainToOxy(move_anim2.start.x, move_anim2.start.y, 28.0f, 17.0f);
			DirectOne=rotdir*60;
			DirectTwo=210;
		}
		}
		break;
	case 1:
		if (move_anim2._side<3)
		{
		StartOne=MainToOxy(move_anim2.start.x, move_anim2.start.y, 21.875f, -12.5f);
		if (move_anim2._side==1)	{
			ExOne=MainToOxy(move_anim2.start.x, move_anim2.start.y, 6.75f, -6.75f);
			ExTwo=MainToOxy(move_anim2.start.x, move_anim2.start.y, 0.0f, +21.875f);
			} else {
			ExOne=MainToOxy(move_anim2.start.x, move_anim2.start.y, 0.0f, 0.0f);
			ExTwo=MainToOxy(move_anim2.start.x, move_anim2.start.y, -19, -11);
			}
		}
		else
		{
		if (move_anim2._side==4)	{
			ExOne=MainToOxy(move_anim2.start.x, move_anim2.start.y, -11.0f, 6.0f);
			ExTwo=MainToOxy(move_anim2.start.x, move_anim2.start.y, -18.0f, 19.0f);
			EndOne=MainToOxy(move_anim2.start.x, move_anim2.start.y, -18.0f, 31.0f);
			ExOne2=MainToOxy(move_anim2.start.x, move_anim2.start.y, -18.0f, 19.0f);
			ExTwo2=MainToOxy(move_anim2.start.x, move_anim2.start.y, -12.0f, -7.0f);
			StartOne=MainToOxy(move_anim2.start.x, move_anim2.start.y, -29.0f, -17.0f);
			DirectOne=rotdir*60-50;
			DirectTwo=300;
			}
		else
		if (move_anim2._side==3)	
		{
			ExOne=MainToOxy(move_anim2.start.x, move_anim2.start.y, -9.0f, 6.0f);
			ExTwo=MainToOxy(move_anim2.start.x, move_anim2.start.y, -23.0f, 7.4f);
			EndOne=MainToOxy(move_anim2.start.x, move_anim2.start.y, -28.0f, 3.0f);
			ExOne2=MainToOxy(move_anim2.start.x, move_anim2.start.y, -23.0f, 7.4f);
			ExTwo2=MainToOxy(move_anim2.start.x, move_anim2.start.y, 0.0f, 10.0f);
			StartOne=MainToOxy(move_anim2.start.x, move_anim2.start.y, 0.0f, 36.0f);
			DirectOne=rotdir*60+50;
			DirectTwo=180;
		}
		else
		{
			StartOne=MainToOxy(move_anim2.start.x, move_anim2.start.y, -28.0f, 17.0f);
			ExOne=MainToOxy(move_anim2.start.x, move_anim2.start.y, -19.0f, 9.0f);
			ExTwo=MainToOxy(move_anim2.start.x, move_anim2.start.y, 0.0f, 0.0f);
			EndOne=MainToOxy(move_anim2.start.x, move_anim2.start.y, -19.0f, -33.0f);
			ExOne2=MainToOxy(move_anim2.start.x, move_anim2.start.y, 0.0f, 0.0f);
			ExTwo2=MainToOxy(move_anim2.start.x, move_anim2.start.y, 28.0f, -17.0f);
			DirectOne=rotdir*60;
			DirectTwo=330;
		}
		}
		break;
	case 4:
		if (move_anim2._side<3)
		{
		StartOne=MainToOxy(move_anim2.start.x, move_anim2.start.y, -21.875f, 12.5f);
		if (move_anim2._side==1)	{
			ExOne=MainToOxy(move_anim2.start.x, move_anim2.start.y, -6.75f, +6.75f);
			ExTwo=MainToOxy(move_anim2.start.x, move_anim2.start.y, 0, -21.875f);
			} else {
			ExOne=MainToOxy(move_anim2.start.x, move_anim2.start.y, 0, 0);
			ExTwo=MainToOxy(move_anim2.start.x, move_anim2.start.y, 19, 11);
			}
		}
		else
		{
		if (move_anim2._side==4)	{
			ExOne=MainToOxy(move_anim2.start.x, move_anim2.start.y, 11.0f, -6.0f);
			ExTwo=MainToOxy(move_anim2.start.x, move_anim2.start.y, 18.0f, -19.0f);
			EndOne=MainToOxy(move_anim2.start.x, move_anim2.start.y, 18.0f, -31.0f);
			ExOne2=MainToOxy(move_anim2.start.x, move_anim2.start.y, 18.0f, -19.0f);
			ExTwo2=MainToOxy(move_anim2.start.x, move_anim2.start.y, 12.0f, 7.0f);
			StartOne=MainToOxy(move_anim2.start.x, move_anim2.start.y, 29.0f, 17.0f);
			DirectOne=rotdir*60-50;
			DirectTwo=120;
		} 
		else
		if (move_anim2._side==3)	
		{
			ExOne=MainToOxy(move_anim2.start.x, move_anim2.start.y, 9.0f, -6.0f);
			ExTwo=MainToOxy(move_anim2.start.x, move_anim2.start.y, 23.0f, -7.4f);
			EndOne=MainToOxy(move_anim2.start.x, move_anim2.start.y, 28.0f, -3.0f);
			ExOne2=MainToOxy(move_anim2.start.x, move_anim2.start.y, 23.0f, -7.4f);
			ExTwo2=MainToOxy(move_anim2.start.x, move_anim2.start.y, 0.0f, -10.0f);
			StartOne=MainToOxy(move_anim2.start.x, move_anim2.start.y, 0.0f, -36.0f);
			DirectOne=rotdir*60+50;
			DirectTwo=0;
		}
		else
		{
			StartOne=MainToOxy(move_anim2.start.x, move_anim2.start.y, 28.0f, -17.0f);
			ExOne=MainToOxy(move_anim2.start.x, move_anim2.start.y, 19.0f, -9.0f);
			ExTwo=MainToOxy(move_anim2.start.x, move_anim2.start.y, 0.0f, 0.0f);
			EndOne=MainToOxy(move_anim2.start.x, move_anim2.start.y, 19.0f, 33.0f);
			ExOne2=MainToOxy(move_anim2.start.x, move_anim2.start.y, 0.0f, 0.0f);
			ExTwo2=MainToOxy(move_anim2.start.x, move_anim2.start.y, -28.0f, 17.0f);
			DirectOne=rotdir*60;
			DirectTwo=150;
		}
		}
		break;
	case 5:
		if (move_anim2._side<3)
		{
		StartOne=MainToOxy(move_anim2.start.x, move_anim2.start.y, -21.875f, -12.5f);
		if (move_anim2._side==1)	{
			ExOne=MainToOxy(move_anim2.start.x, move_anim2.start.y, -6.75f, -6.75f);
			ExTwo=MainToOxy(move_anim2.start.x, move_anim2.start.y, 0, +21.875f);
			} else {
			ExOne=MainToOxy(move_anim2.start.x, move_anim2.start.y, 0, 0);
			ExTwo=MainToOxy(move_anim2.start.x, move_anim2.start.y, 19, -11);
			}
		}
		else
		{
		if (move_anim2._side==4)	{
			ExOne=MainToOxy(move_anim2.start.x, move_anim2.start.y, 11.0f, 6.0f);
			ExTwo=MainToOxy(move_anim2.start.x, move_anim2.start.y, 18.0f, 19.0f);
			EndOne=MainToOxy(move_anim2.start.x, move_anim2.start.y, 18.0f, 31.0f);
			ExOne2=MainToOxy(move_anim2.start.x, move_anim2.start.y, 18.0f, 19.0f);
			ExTwo2=MainToOxy(move_anim2.start.x, move_anim2.start.y, 12.0f, -7.0f);
			StartOne=MainToOxy(move_anim2.start.x, move_anim2.start.y, 29.0f, -17.0f);
			DirectOne=rotdir*60+50;
			DirectTwo=60;
			}
		else
		if (move_anim2._side==3)
		{
			ExOne=MainToOxy(move_anim2.start.x, move_anim2.start.y, 9.0f, 6.0f);
			ExTwo=MainToOxy(move_anim2.start.x, move_anim2.start.y, 23.0f, 7.4f);
			EndOne=MainToOxy(move_anim2.start.x, move_anim2.start.y, 28.0f, 3.0f);
			ExOne2=MainToOxy(move_anim2.start.x, move_anim2.start.y, 23.0f, 7.4f);
			ExTwo2=MainToOxy(move_anim2.start.x, move_anim2.start.y, 0.0f, 10.0f);
			StartOne=MainToOxy(move_anim2.start.x, move_anim2.start.y, 0.0f, 36.0f);
			DirectOne=rotdir*60-50;
			DirectTwo=180;
		}
		else
		{
			StartOne=MainToOxy(move_anim2.start.x, move_anim2.start.y, 28.0f, 17.0f);
			ExOne=MainToOxy(move_anim2.start.x, move_anim2.start.y, 19.0f, 9.0f);
			ExTwo=MainToOxy(move_anim2.start.x, move_anim2.start.y, 0.0f, 0.0f);
			EndOne=MainToOxy(move_anim2.start.x, move_anim2.start.y, 19.0f, -33.0f);
			ExOne2=MainToOxy(move_anim2.start.x, move_anim2.start.y, 0.0f, 0.0f);
			ExTwo2=MainToOxy(move_anim2.start.x, move_anim2.start.y, -28.0f, -17.0f);
			DirectOne=rotdir*60;
			DirectTwo=30;
		}
		}
		break;
	
	case 0:
		if (move_anim2._side<3)
		{
		StartOne=MainToOxy(move_anim2.start.x, move_anim2.start.y, 0, -32.8125f);
		if (move_anim2._side==1)	{
			ExOne=MainToOxy(move_anim2.start.x, move_anim2.start.y, 0, 0);
			ExTwo=MainToOxy(move_anim2.start.x, move_anim2.start.y, -19.0f, 11.0f);
			} else {
			ExOne=MainToOxy(move_anim2.start.x, move_anim2.start.y, 0, 0);
			ExTwo=MainToOxy(move_anim2.start.x, move_anim2.start.y, 19.0f, 11.0f);
			}
		}
		else
		{
		if (move_anim2._side==4)	{
			ExOne=MainToOxy(move_anim2.start.x, move_anim2.start.y, 0, 10.9375f);
			ExTwo=MainToOxy(move_anim2.start.x, move_anim2.start.y, 7.0f, 23.0f);
			EndOne=MainToOxy(move_anim2.start.x, move_anim2.start.y, 18.0f, 35.0f);
			ExOne2=MainToOxy(move_anim2.start.x, move_anim2.start.y, 9.0f, 24.0f);
			ExTwo2=MainToOxy(move_anim2.start.x, move_anim2.start.y, 0.0f, 0.0f);
			StartOne=MainToOxy(move_anim2.start.x, move_anim2.start.y, -29.0f, 17.0f);
			DirectOne=rotdir*60-40;
			DirectTwo=240;
		}
		else 
		if (move_anim2._side==3)
		{
			ExOne=MainToOxy(move_anim2.start.x, move_anim2.start.y, 0, 10.9375f);
			ExTwo=MainToOxy(move_anim2.start.x, move_anim2.start.y, -7.0f, 23.0f);
			EndOne=MainToOxy(move_anim2.start.x, move_anim2.start.y, -18.0f, 35.0f);
			ExOne2=MainToOxy(move_anim2.start.x, move_anim2.start.y, -9.0f, 24.0f);
			ExTwo2=MainToOxy(move_anim2.start.x, move_anim2.start.y, 0.0f, 0.0f);
			StartOne=MainToOxy(move_anim2.start.x, move_anim2.start.y, 29.0f, 17.0f);
			DirectOne=rotdir*60+40;
			DirectTwo=120;
		}
		else 
		{
			StartOne=MainToOxy(move_anim2.start.x, move_anim2.start.y, 0.0f, 32.8125f);
			ExOne=MainToOxy(move_anim2.start.x, move_anim2.start.y, 0.0f, 21.875f);
			ExTwo=MainToOxy(move_anim2.start.x, move_anim2.start.y, 0.0f, 0.0f);
			EndOne=MainToOxy(move_anim2.start.x, move_anim2.start.y, -37.5f, 0.0f);
			ExOne2=MainToOxy(move_anim2.start.x, move_anim2.start.y, 0.0f, 0.0f);
			ExTwo2=MainToOxy(move_anim2.start.x, move_anim2.start.y, 0.0f, -32.8125f);
			DirectOne=rotdir*60;
			DirectTwo=270;
		}
		}
		break;
		case 3:
		if (move_anim2._side<3)
		{
		StartOne=MainToOxy(move_anim2.start.x, move_anim2.start.y, 0, 32.8125f);
		if (move_anim2._side==2)	{
			ExOne=MainToOxy(move_anim2.start.x, move_anim2.start.y, 0, 0);
			ExTwo=MainToOxy(move_anim2.start.x, move_anim2.start.y, 19.0f, -11.0f);
			} else {
			ExOne=MainToOxy(move_anim2.start.x, move_anim2.start.y, 0, 0);
			ExTwo=MainToOxy(move_anim2.start.x, move_anim2.start.y, -19.0f, -11.0f);
			}
		}
		else
		{
		if (move_anim2._side==4)	{
			ExOne=MainToOxy(move_anim2.start.x, move_anim2.start.y, 0.0f, -10.9375f);
			ExTwo=MainToOxy(move_anim2.start.x, move_anim2.start.y, 7.0f, -23.0f);
			EndOne=MainToOxy(move_anim2.start.x, move_anim2.start.y, 18.0f, -35.0f);
			ExOne2=MainToOxy(move_anim2.start.x, move_anim2.start.y, 9.0f, -24.0f);
			ExTwo2=MainToOxy(move_anim2.start.x, move_anim2.start.y, 0.0f, 0.0f);
			StartOne=MainToOxy(move_anim2.start.x, move_anim2.start.y, -29.0f, -17.0f);
			DirectOne=rotdir*60+40;
			DirectTwo=300;
			}
		else
		if (move_anim2._side==3)
		{
			ExOne=MainToOxy(move_anim2.start.x, move_anim2.start.y, 0.0f, -10.9375f);
			ExTwo=MainToOxy(move_anim2.start.x, move_anim2.start.y, -7.0f, -23.0f);
			EndOne=MainToOxy(move_anim2.start.x, move_anim2.start.y, -18.0f, -35.0f);
			ExOne2=MainToOxy(move_anim2.start.x, move_anim2.start.y, -9.0f, -24.0f);
			ExTwo2=MainToOxy(move_anim2.start.x, move_anim2.start.y, 0.0f, 0.0f);
			StartOne=MainToOxy(move_anim2.start.x, move_anim2.start.y, 29.0f, -17.0f);
			DirectOne=rotdir*60-40;
			DirectTwo=60;
		}
		else
		{
			StartOne=MainToOxy(move_anim2.start.x, move_anim2.start.y, 0.0f, -32.8125f);
			ExOne=MainToOxy(move_anim2.start.x, move_anim2.start.y, 0.0f, -21.875f);
			ExTwo=MainToOxy(move_anim2.start.x, move_anim2.start.y, 0.0f, 0.0f);
			EndOne=MainToOxy(move_anim2.start.x, move_anim2.start.y, 37.5f, 0.0f);
			ExOne2=MainToOxy(move_anim2.start.x, move_anim2.start.y, 0.0f, 0.0f);
			ExTwo2=MainToOxy(move_anim2.start.x, move_anim2.start.y, 0.0f, 32.8125f);
			DirectOne=rotdir*60;
			DirectTwo=90;
		}
		}
		break;
		}

		if (move_anim2._side<3)
		{
			bezierPath.ClearPath();
			move_anim2.double_rotate_car=false;
			bezierPath.AddPoint(10, MainToOxy(move_anim2.start.x, move_anim2.start.y), rotdir*60);
			bezierPath.endPoint=StartOne;
			bezierPath.endRotation=rotdir*60;
			bezierPath.GetDrawingPoints(1, true);
			move_anim2.rotate_car=true;
			move_anim2.oldRot=rotdir*60;
			move_anim2.b_start=StartOne;
			move_anim2.b_exStart=ExOne;
			move_anim2.b_exEnd=ExTwo;
		}
		else
		if (move_anim2._side<5)
		{
			bezierPath.ClearPath();
			move_anim2.double_rotate_car=true;
			bezierPath.AddPoint(8, MainToOxy(move_anim2.start.x, move_anim2.start.y),ExOne,ExTwo,rotdir*60);
			bezierPath.endPoint=EndOne;
			bezierPath.endRotation=DirectOne;
			bezierPath.GetDrawingPoints(1, true);
			move_anim2.rotate_car=true;
			move_anim2.oldRot=DirectOne;
			move_anim2.oldRotTwo=DirectTwo;
			move_anim2.b_start=EndOne;
			move_anim2.b_exStart=ExOne2;
			move_anim2.b_exEnd=ExTwo2;
			move_anim2.b_end=StartOne;

		}
		else
		{
			bezierPath.ClearPath();
			move_anim2.double_rotate_car=true;
			bezierPath.AddPoint(10, MainToOxy(move_anim2.start.x, move_anim2.start.y), rotdir*60);
			bezierPath.endPoint=StartOne;
			bezierPath.endRotation=DirectOne;
			bezierPath.GetDrawingPoints(1, true);
			move_anim2.rotate_car=true;
			move_anim2.oldRot=DirectOne;
			move_anim2.oldRotTwo=DirectTwo;
			move_anim2.b_start=StartOne;
			move_anim2.b_exStart=ExOne;
			move_anim2.b_exEnd=ExTwo;
			move_anim2.b_end=EndOne;
			move_anim2.end_rotate=true;
			move_anim2.b_exStart2=ExOne2;
			move_anim2.b_exEnd2=ExTwo2;

		}
} 


void unit::CarMoveBackTwo()
{
wheelController.Start(-1);
move_anim2.t=0;
move_anim2.anim_move_flag=true;

bezierPath.ClearPath();
bezierPath.AddPoint(8, move_anim2.b_start,	move_anim2.b_exStart, move_anim2.b_exEnd, move_anim2.oldRot);
bezierPath.endPoint=move_anim2.b_end;
bezierPath.endRotation=move_anim2.oldRotTwo;
bezierPath.GetDrawingPoints(1, true);

move_anim2.oldRot=move_anim2.oldRotTwo;
move_anim2.b_start=move_anim2.b_end;
if (move_anim2.end_rotate==true)
{
move_anim2.end_rotate=false;
move_anim2.b_exStart=move_anim2.b_exStart2;
move_anim2.b_exEnd=move_anim2.b_exEnd2;
move_anim2.end_animation=false;
}
else
move_anim2.end_animation=true;
move_anim2.double_rotate_car=false;




}


void drawAvatar()
{

	//if (unitptr!=NULL)
	//{

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glColor4f(1.0f,1.0f,1.0f,1.0f);
	glBindTexture(GL_TEXTURE_2D, faceframe.texID);
	glBegin(GL_QUADS);
	 glTexCoord2f(0.0f, 1.0f);
     glVertex2f(0.0f, 885.0f);
	 glTexCoord2f(1.0f, 1.0f);
     glVertex2f(210.0f, 885.0f);
	 glTexCoord2f(1.0f, 0.0f);
     glVertex2f(210.0f, 1200.0f);
	 glTexCoord2f(0.0f, 0.0f);
     glVertex2f(0.0f, 1200.0f);
	glEnd();
	glDisable(GL_BLEND);

	glColor4f(1.0f,1.0f,1.0f,1.0f);
	glBindTexture(GL_TEXTURE_2D, renderedTexture);
	glBegin(GL_QUADS);
	 glTexCoord2f(0.0f, 0.5f);
     glVertex2f(23.0f, 910.0f);
	 glTexCoord2f(0.322265625f, 0.5f);
     glVertex2f(184.0f, 910.0f);
	 glTexCoord2f(0.322265625f, 0.0f);
     glVertex2f(184.0f, 1177.0f);
	 glTexCoord2f(0.0f, 0.0f);
     glVertex2f(23.0f, 1177.0f);
	
	glEnd();


	//}


}




void FaceFrameInitialization()
{
	glGenFramebuffers(1, &FaceFrameBuffer);
	glBindFramebuffer(GL_FRAMEBUFFER, FaceFrameBuffer);

	glGenRenderbuffers(1, &FaceColorBuffer);
	glBindRenderbuffer(GL_RENDERBUFFER, FaceColorBuffer);
	glRenderbufferStorageMultisample(GL_RENDERBUFFER, 16, GL_RGB, 512, 512);
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, FaceColorBuffer);
		
	glGenRenderbuffers(1, &depthrenderbuffer);
	glBindRenderbuffer(GL_RENDERBUFFER, depthrenderbuffer);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, 512, 512);
	//glRenderbufferStorageMultisample(GL_RENDERBUFFER, 8, GL_RGB, 512, 512);
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthrenderbuffer);

	glGenTextures(1, &renderedTexture);
	glBindTexture(GL_TEXTURE_2D, renderedTexture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, 512, 512, 0, GL_RGB, GL_UNSIGNED_BYTE, 0);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

	glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, renderedTexture, 0);
 
	GLenum DrawBuffers[1] = {GL_COLOR_ATTACHMENT0};
	glDrawBuffers(1, DrawBuffers);
	
	glBindRenderbuffer(GL_RENDERBUFFER, 0);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	prime.Face->timer.animation_index=0;
	prime.Face->timer.One[0].Start();

/*

	glGenFramebuffers(1, &rendertarget->frameBuffer);
	glBindFramebuffer(GL_FRAMEBUFFER, rendertarget->frameBuffer);

	glGenRenderbuffers(1, &rendertarget->colorBuffer);
	glBindRenderbuffer(GL_RENDERBUFFER, rendertarget->colorBuffer);
	glRenderbufferStorageMultisample(GL_RENDERBUFFER, 4, GL_RGBA8, WindowRect.right - WindowRect.left, WindowRect.bottom - WindowRect.top);
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, rendertarget->colorBuffer);


	glGenRenderbuffers(1, &rendertarget->depthBuffer);
	glBindRenderbuffer(GL_RENDERBUFFER_EXT, rendertarget->depthBuffer);
	glRenderbufferStorageMultisample(GL_RENDERBUFFER, 4, GL_DEPTH_COMPONENT, WindowRect.right - WindowRect.left, WindowRect.bottom - WindowRect.top);
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, rendertarget->depthBuffer);


	glGenTextures(1, &rendertarget->BaseTexture);
	glBindTexture(GL_TEXTURE_2D, rendertarget->BaseTexture);
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST );
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST );
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, WindowRect.right - WindowRect.left, WindowRect.bottom - WindowRect.top, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);

	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, rendertarget->BaseTexture, 0);
	*/
}
























