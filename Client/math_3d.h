#ifndef MATH_3D_H
#define	MATH_3D_H

#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#include <stdio.h>
#include <math.h>
#ifndef WIN32
#include <unistd.h>
#endif
#include <stdlib.h>
#include <string.h>

#define ZERO_MEM(a) memset(a, 0, sizeof(a))
#define ToRadian(x) ((x) * 3.1415926f / 180.0f)
#define ToDegree(x) ((x) * 180.0f / 3.1415926f)

float SIGN(float x);
float MAX(float x, float y);



struct Vector2i
{
    int x;
    int y;
};

struct Vector3i
{
    int x;
    int y;
	int z;
};

struct Vector2f
{
    float x;
    float y;
	float length();

    Vector2f()
    {
    }

    Vector2f(float _x, float _y)
    {
        x = _x;
        y = _y;
    }

	Vector2f& operator-=(const Vector2f& r)
    {
        x -= r.x;
        y -= r.y;
        
        return *this;
    }

	Vector2f& operator+=(const Vector2f& r)
    {
        x += r.x;
        y += r.y;
        
        return *this;
    }

	Vector2f& operator*=(float f)
    {
        x *= f;
        y *= f;
        
        return *this;
    }

	



};

struct PersProjInfo
{
    float FOV;
    float Width; 
    float Height;
    float zNear;
    float zFar;
};

struct Vector3f
{
    float x;
    float y;
    float z;

    Vector3f()
    {
    }

    Vector3f(float _x, float _y, float _z)
    {
        x = _x;
        y = _y;
        z = _z;
    }

    Vector3f& operator+=(const Vector3f& r)
    {
        x += r.x;
        y += r.y;
        z += r.z;

        return *this;
    }

	Vector3f& operator=(const Vector3f& r)
    {
        x = r.x;
        y = r.y;
        z = r.z;

        return *this;
    }

	
    Vector3f& operator-=(const Vector3f& r)
    {
        x -= r.x;
        y -= r.y;
        z -= r.z;

        return *this;
    }

    Vector3f& operator*=(float f)
    {
        x *= f;
        y *= f;
        z *= f;

        return *this;
    }

	

    Vector3f Cross(const Vector3f& v) const;

    Vector3f& Normalize();

    void Rotate(float Angle, const Vector3f& Axis);

    void Print() const
    {
        printf("(%.02f, %.02f, %.02f", x, y, z);
    }
};

inline Vector2f operator+(const Vector2f& l, const Vector2f& r)
{
    Vector2f Ret(l.x + r.x,
                 l.y + r.y);

    return Ret;
}

inline Vector2f operator-(const Vector2f& l, const Vector2f& r)
{
    Vector2f Ret(l.x - r.x,
                 l.y - r.y);

    return Ret;
}

inline Vector2f operator*(const Vector2f& l, float f)
{
    Vector2f Ret(l.x * f,
                 l.y * f);

    return Ret;
}

inline Vector2f operator * (const Vector2f& l, const Vector2f& r) 
{
	Vector2f Ret(l.x * r.x,
				l.y * r.y);
	 return Ret;
}


inline Vector3f operator+(const Vector3f& l, const Vector3f& r)
{
    Vector3f Ret(l.x + r.x,
                 l.y + r.y,
                 l.z + r.z);

    return Ret;
}


inline Vector3f operator-(const Vector3f& l, const Vector3f& r)
{
    Vector3f Ret(l.x - r.x,
                 l.y - r.y,
                 l.z - r.z);

    return Ret;
}

inline Vector3f operator*(const Vector3f& l, float f)
{
    Vector3f Ret(l.x * f,
                 l.y * f,
                 l.z * f);

    return Ret;
}



class Matrix4f
{
public:
    float m[4][4];

    Matrix4f()
    {        
    }
	
	Matrix4f(const aiMatrix4x4& AssimpMatrix)
    {
        m[0][0] = AssimpMatrix.a1; m[0][1] = AssimpMatrix.a2; m[0][2] = AssimpMatrix.a3; m[0][3] = AssimpMatrix.a4;
        m[1][0] = AssimpMatrix.b1; m[1][1] = AssimpMatrix.b2; m[1][2] = AssimpMatrix.b3; m[1][3] = AssimpMatrix.b4;
        m[2][0] = AssimpMatrix.c1; m[2][1] = AssimpMatrix.c2; m[2][2] = AssimpMatrix.c3; m[2][3] = AssimpMatrix.c4;
        m[3][0] = AssimpMatrix.d1; m[3][1] = AssimpMatrix.d2; m[3][2] = AssimpMatrix.d3; m[3][3] = AssimpMatrix.d4;
    }

	Matrix4f(const aiMatrix3x3& AssimpMatrix)
    {
        m[0][0] = AssimpMatrix.a1; m[0][1] = AssimpMatrix.a2; m[0][2] = AssimpMatrix.a3; m[0][3] = 0.0f;
        m[1][0] = AssimpMatrix.b1; m[1][1] = AssimpMatrix.b2; m[1][2] = AssimpMatrix.b3; m[1][3] = 0.0f;
        m[2][0] = AssimpMatrix.c1; m[2][1] = AssimpMatrix.c2; m[2][2] = AssimpMatrix.c3; m[2][3] = 0.0f;
        m[3][0] = 0.0f           ; m[3][1] = 0.0f           ; m[3][2] = 0.0f           ; m[3][3] = 1.0f;
    } 

    inline void InitIdentity()
    {
        m[0][0] = 1.0f; m[0][1] = 0.0f; m[0][2] = 0.0f; m[0][3] = 0.0f;
        m[1][0] = 0.0f; m[1][1] = 1.0f; m[1][2] = 0.0f; m[1][3] = 0.0f;
        m[2][0] = 0.0f; m[2][1] = 0.0f; m[2][2] = 1.0f; m[2][3] = 0.0f;
        m[3][0] = 0.0f; m[3][1] = 0.0f; m[3][2] = 0.0f; m[3][3] = 1.0f;
    }

    inline Matrix4f operator*(const Matrix4f& Right) const
    {
        Matrix4f Ret;

        for (unsigned int i = 0 ; i < 4 ; i++) {
            for (unsigned int j = 0 ; j < 4 ; j++) {
                Ret.m[i][j] = m[i][0] * Right.m[0][j] +
                              m[i][1] * Right.m[1][j] +
                              m[i][2] * Right.m[2][j] +
                              m[i][3] * Right.m[3][j];
            }
        }

        return Ret;
    }

	void SetZero()
    {
        ZERO_MEM(m);
    }
	float Determinant() const;
	Matrix4f& Inverse();
    void InitScaleTransform(float ScaleX, float ScaleY, float ScaleZ);
    void InitRotateTransform(float RotateX, float RotateY, float RotateZ);
    void InitTranslationTransform(float x, float y, float z);
    void InitCameraTransform(const Vector3f& Target, const Vector3f& Up);
    void InitPersProjTransform(float FOV, float Width, float Height, float zNear, float zFar);
	void InitPersProjTransform(const PersProjInfo& p);
};


struct Quaternion
{
    float x, y, z, w;

    Quaternion(float _x, float _y, float _z, float _w);

    //void Normalize();

    Quaternion Conjugate();  
 };

void QNormalize(aiQuaternion *quat);

Quaternion operator*(const Quaternion& l, const Quaternion& r);

Quaternion operator*(const Quaternion& q, const Vector3f& v);

aiQuaternion Multiply (aiQuaternion l, aiQuaternion r);

aiQuaternion QuaternionFromMatrix(Matrix4f res);

Matrix4f QuaternionToMatrix(const Quaternion * quat);

Vector3f QuaternionToEuler(aiQuaternion quaternion);

aiQuaternion QuaternionFromEuler(Vector3f axis, float angle);

aiQuaternion CreateFromYawPitchRoll(Vector3f euler);

Vector3f ToEulerAngles(aiQuaternion q);

#endif	/* MATH_3D_H */

